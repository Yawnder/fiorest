﻿#if WITH_JUMPCALCULATOR
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Dijkstra.NET.Graph;
using Dijkstra.NET.ShortestPath;

using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest
{
    public static class JumpCalculator
    {
        private static object JumpDataLock = new object();
        private static List<Database.Models.System> Systems;
        private static Dictionary<string, uint> SystemIdToGraphId;
        private static Graph<string, int> graph;

        private static object PendingModelsToCacheLockObject = new object();
        private static List<JumpCache> PendingModelsToCache = new List<JumpCache>();

        private static Dictionary<string, int> MemoryJumpCountCache = new Dictionary<string, int>();

        public static bool IsInitialized()
        {
            return graph != null;
        }

        public static string GetSystemId( string value, PRUNDataContext context = null )
        {
            PRUNDataContext DB = context ?? PRUNDataContext.GetNewContext();

            value = value.ToUpper();

            string SystemId = null;

            SystemId = DB.PlanetDataModels
                .AsNoTracking()
                .Where(pdm => pdm.PlanetName.ToUpper() == value || pdm.PlanetId.ToUpper() == value || pdm.PlanetNaturalId.ToUpper() == value)
                .Select(pdm => pdm.SystemId)
                .FirstOrDefault();

            if (SystemId == null)
            {
                SystemId = SystemId = DB.Systems
                    .AsNoTracking()
                    .Where(ssm => ssm.SystemId.ToUpper() == value || ssm.Name.ToUpper() == value || ssm.NaturalId.ToUpper() == value)
                    .Select(ssm => ssm.SystemId)
                    .FirstOrDefault();

                if (SystemId == null)
                {
                    SystemId = DB.Stations
                        .AsNoTracking()
                        .Where(s => s.Name.ToUpper() == value || s.NaturalId.ToUpper() == value)
                        .Select(s => s.SystemId)
                        .FirstOrDefault();
                }
            }

            if (context == null)
            {
                DB.Dispose();
            }

            return SystemId;
        }

        public static List<string> GetSystemIds(List<string> values, PRUNDataContext context = null)
        {
            PRUNDataContext DB = context ?? PRUNDataContext.GetNewContext();

            var queryNames = values.Select(name => name.ToUpper());
            var systemIds1 = DB.PlanetDataModels
                .AsNoTracking()
                .Where(pl =>
                    queryNames.Contains(pl.PlanetName.ToUpper())
                    || queryNames.Contains(pl.PlanetNaturalId.ToUpper())
                    || queryNames.Contains(pl.PlanetId.ToUpper()))
                .Select(pl => pl.SystemId);
            var systemIds2 = DB.Systems
                .AsNoTracking()
                .Where(ssm =>
                    queryNames.Contains(ssm.SystemId.ToUpper())
                    || queryNames.Contains(ssm.Name.ToUpper())
                    || queryNames.Contains(ssm.NaturalId.ToUpper()))
                .Select(ssm => ssm.SystemId);
            var systemIds3 = DB.Stations
                .AsNoTracking()
                .Where(st =>
                    queryNames.Contains(st.Name.ToUpper())
                    || queryNames.Contains(st.NaturalId.ToUpper()))
                .Select(st => st.SystemId);

            if (context == null)
            {
                DB.Dispose();
            }

            return systemIds1.Concat(systemIds2).Concat(systemIds3).ToList();
        }

        public static void Initialize(List<Database.Models.System> systems)
        {
            Systems = systems;
            if (Systems.Count == 0)
                return;

            lock (JumpDataLock)
            {
                SystemIdToGraphId = new Dictionary<string, uint>();
                graph = new Graph<string, int>();

                foreach (var system in Systems)
                {
                    uint nodeId = 0;
                    if (!SystemIdToGraphId.ContainsKey(system.SystemId))
                    {
                        nodeId = graph.AddNode(system.SystemId);
                        SystemIdToGraphId.Add(system.SystemId, nodeId);
                    }
                    else
                    {
                        nodeId = SystemIdToGraphId[system.SystemId];
                    }

                    foreach (var connection in system.Connections)
                    {
                        if (connection.ConnectingId == null)
                        {
                            continue;
                        }

                        uint connectionNodeId = 0;
                        if (!SystemIdToGraphId.ContainsKey(connection.ConnectingId))
                        {
                            connectionNodeId = graph.AddNode(connection.ConnectingId);
                            SystemIdToGraphId.Add(connection.ConnectingId, connectionNodeId);
                        }
                        else
                        {
                            connectionNodeId = SystemIdToGraphId[connection.ConnectingId];
                        }

                        var connectionStar = Systems.Where(ssm => ssm.SystemId == connection.ConnectingId).FirstOrDefault();
                        if (connectionStar != null)
                        {
                            double distance = Math.Sqrt(Math.Pow((system.PositionX - connectionStar.PositionX), 2) + Math.Pow((system.PositionY - connectionStar.PositionY), 2) + Math.Pow((system.PositionZ - connectionStar.PositionZ), 2));

                            // Since Dijkstra.Graph only takes ints, multiply by 1000 and truncate
                            int dijCost = (int)(distance * 1000.0);

                            graph.Connect(nodeId, connectionNodeId, dijCost, dijCost);
                        }
                    }
                }
            }
        }

        public static void InitExchangeMemoryCache(List<Database.Models.System> systemStars, PRUNDataContext context)
        {
            var systemIds = context.Stations
                .AsNoTracking()
                .Select(s => s.SystemId)
                .ToList();
            foreach (var systemId in systemIds)
            {
                string SourceSystemId = systemId;
                foreach (var systemStar in systemStars)
                {
                    string DestinationSystemId = systemStar.SystemId;
                    int JumpCount = GetJumpCount(SourceSystemId, DestinationSystemId, context);

                    string SourceDestKey = $"{SourceSystemId}-{DestinationSystemId}";
                    MemoryJumpCountCache.Add(SourceDestKey, JumpCount);
                }
            }
        }

        private static int? GetJumpCountFromCache(string SourceSystemId, string DestinationSystemId)
        {
            int JumpCount;
            if (MemoryJumpCountCache.TryGetValue($"{SourceSystemId}-{DestinationSystemId}", out JumpCount))
            {
                return JumpCount;
            }

            return null;
        }

        public static int GetJumpCount(string SourceSystemId, string DestinationSystemId, PRUNDataContext context = null)
        {
            PRUNDataContext DB = context ?? PRUNDataContext.GetNewContext();

            if (SourceSystemId == DestinationSystemId)
            {
                return 0;
            }

            int? CachedJumpCount = GetJumpCountFromCache(SourceSystemId, DestinationSystemId);
            if ( CachedJumpCount != null )
            {
                return (int)CachedJumpCount;
            }

            JumpCache model = null;
            model = DB.JumpCache
                .AsNoTracking()
                .Include(jc => jc.Jumps)
                .Where(jcm => jcm.SourceSystemId == SourceSystemId && jcm.DestinationSystemId == DestinationSystemId)
                .FirstOrDefault();
            if (model != null)
            {
                return model.JumpCount;
            }

            // Fallback to retrieving the full route (which will end up caching the result)
            var route = GetRoute(SourceSystemId, DestinationSystemId);

            if ( context == null )
            {
                DB.Dispose();
            }

            return route.Count;
        }

        public static List<JumpCacheRouteJump> GetRoute(string SourceSystemId, string DestinationSystemId, PRUNDataContext context = null)
        {
            PRUNDataContext DB = context ?? PRUNDataContext.GetNewContext();

            var route = new List<JumpCacheRouteJump>();
            if ( SourceSystemId == DestinationSystemId )
            {
                return route;
            }

            JumpCache model = null;
            model = DB.JumpCache
                .AsNoTracking()
                .Include(jc => jc.Jumps)
                .Where(jc => jc.SourceSystemId == SourceSystemId && jc.DestinationSystemId == DestinationSystemId)
                .FirstOrDefault();
            if ( model != null )
            {
                return model.Jumps;
            }

            if (context == null)
            {
                DB.Dispose();
            }

            lock (JumpDataLock)
            {
                Debug.Assert(IsInitialized());
                uint SourceNodeId = SystemIdToGraphId[SourceSystemId];
                uint DestinationNodeId = SystemIdToGraphId[DestinationSystemId];

                List<uint> routeNodeIds = graph.Dijkstra(SourceNodeId, DestinationNodeId).GetPath().ToList();
                for (int routeNodeIter = 0; routeNodeIter + 1 < routeNodeIds.Count; ++routeNodeIter)
                {
                    var sourceNodeId = routeNodeIds[routeNodeIter];
                    var sourceSysId = SystemIdToGraphId.FirstOrDefault(x => x.Value == sourceNodeId).Key;
                    var sourceSystem = Systems.Where(ssm => ssm.SystemId == sourceSysId).First();

                    var destNodeId = routeNodeIds[routeNodeIter + 1];
                    var destSysId = SystemIdToGraphId.FirstOrDefault(x => x.Value == destNodeId).Key;
                    var destSystem = Systems.Where(ssm => ssm.SystemId == destSysId).First();

                    JumpCacheRouteJump desc = new JumpCacheRouteJump();
                    desc.SourceSystemId = sourceSysId;
                    desc.SourceSystemName = sourceSystem.Name;
                    desc.SourceSystemNaturalId = sourceSystem.NaturalId;
                    desc.DestinationSystemId = destSysId;
                    desc.DestinationSystemName = destSystem.Name;
                    desc.DestinationNaturalId = destSystem.NaturalId;
                    desc.Distance = Math.Sqrt(Math.Pow((sourceSystem.PositionX - destSystem.PositionX), 2) + Math.Pow((sourceSystem.PositionY - destSystem.PositionY), 2) + Math.Pow((sourceSystem.PositionZ - destSystem.PositionZ), 2));

                    route.Add(desc);
                }
            }
  
            if (route.Count == 0)
            {
                return new List<JumpCacheRouteJump>();
            }

            {
                // Cache the route
                var start = route[0];
                var destination = route[route.Count - 1];

                if (model == null)
                {
                    model = new JumpCache();
                    model.JumpCacheId = $"{start.SourceSystemId}-{destination.DestinationSystemId}";
                    model.SourceSystemId = start.SourceSystemId;
                    model.SourceSystemName = start.SourceSystemName;
                    model.SourceSystemNaturalId = start.SourceSystemNaturalId;
                    model.DestinationSystemId = destination.DestinationSystemId;
                    model.DestinationSystemName = destination.DestinationSystemName;
                    model.DestinationNaturalId = destination.DestinationNaturalId;

                    model.OverallDistance = 0.0;
                    model.JumpCount = route.Count;

                    foreach (var routeEntry in route)
                    {
                        var jump = new JumpCacheRouteJump();
                        jump.JumpCacheRouteJumpId = $"{model.JumpCacheId}-{routeEntry.SourceSystemId}-{routeEntry.DestinationSystemId}";
                        jump.SourceSystemId = routeEntry.SourceSystemId;
                        jump.SourceSystemName = routeEntry.SourceSystemName;
                        jump.SourceSystemNaturalId = routeEntry.SourceSystemNaturalId;
                        jump.DestinationSystemId = routeEntry.DestinationSystemId;
                        jump.DestinationSystemName = routeEntry.DestinationSystemName;
                        jump.DestinationNaturalId = routeEntry.DestinationNaturalId;
                        jump.Distance = routeEntry.Distance;
                        jump.JumpCacheId = model.JumpCacheId;

                        model.OverallDistance += jump.Distance;

                        model.Jumps.Add(jump);
                    }

                    model.Validate();

                    lock (PendingModelsToCacheLockObject)
                    {
                        PendingModelsToCache.Add(model);
                    }
                }
            }

            return route;
        }

        public static void CachePendingRoutes()
        {
            List<JumpCache> PendingToCache = null;
            lock(PendingModelsToCacheLockObject)
            {
                if ( PendingModelsToCache.Count > 0)
                {
                    // Distinct by JumpCacheId
                    PendingToCache = PendingModelsToCache
                        .GroupBy(mtc => mtc.JumpCacheId)
                        .Select(g => g.First())
                        .ToList();

                    PendingModelsToCache = new List<JumpCache>();
                }
            }

            if (PendingToCache != null)
            {
                using (var DB = PRUNDataContext.GetNewContext())
                using (var transaction = DB.Database.BeginTransaction())
                {
                    DB.JumpCache.UpsertRange(PendingToCache)
                        .On(jc => new { jc.JumpCacheId })
                        .Run();

                    var jumpCacheRoutes = PendingToCache.SelectMany(jc => jc.Jumps);
                    DB.JumpCacheRoutes.UpsertRange(jumpCacheRoutes)
                        .On(jcr => new { jcr.JumpCacheRouteJumpId })
                        .Run();

                    DB.SaveChanges();
                    transaction.Commit();
                }
            }
        }
    }
}
#endif // WITH_JUMPCALCULATOR