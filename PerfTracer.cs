﻿//#define PERF_TRACING_ENABLED

using System;
using System.Diagnostics;

namespace FIORest
{
	public class PerfTracer : IDisposable
	{
#if PERF_TRACING_ENABLED
		private Stopwatch sw = new Stopwatch();
		private string traceMessage = null;
#endif // PERF_TRACING_ENABLED

		public PerfTracer(bool bAutoStart = true)
		{
#if PERF_TRACING_ENABLED
			if (bAutoStart)
			{
				sw.Start();
			}

			var sf = new System.Diagnostics.StackFrame(1);
			var method = sf.GetMethod();

			string className = method.ReflectedType.Name;
			string functionName = method.Name;

			traceMessage = $"[{className}::{functionName}]";
#endif // PERF_TRACING_ENABLED
		}

		public PerfTracer(string message, bool bAutoStart = true)
		{
#if PERF_TRACING_ENABLED
			traceMessage = message;
			if (bAutoStart)
			{
				sw.Start();
			}
#endif // PERF_TRACING_ENABLED
		}

		public void Start()
		{
#if PERF_TRACING_ENABLED
			sw.Start();
#endif // PERF_TRACING_ENABLED
		}

		public void Stop()
		{
#if PERF_TRACING_ENABLED
			sw.Stop();
#endif // PERF_TRACING_ENABLED

		}

		public void Dispose()
		{
#if PERF_TRACING_ENABLED
			sw.Stop();
			Trace.WriteLine(String.Format("[{0}]: {1} ", traceMessage, sw.Elapsed));
#endif // PERF_TRACING_ENABLED
		}
	}
}
