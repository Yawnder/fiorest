﻿using System;
using System.Linq;

using Nancy;
using Nancy.Bootstrapper;
using Nancy.LeakyBucket;
using Nancy.LeakyBucket.Identifiers;
using Nancy.Gzip;
using Nancy.TinyIoc;

namespace FIORest
{
    public class XForwardedForClientIdentifier : IClientIdentifier
    {
        private const string XForwardedForHeaderKey = "X-Forwarded-For";
        public string Identifier { get; }

        public XForwardedForClientIdentifier(Request req)
        {
            if ( req.Headers.Keys.Contains(XForwardedForHeaderKey))
            {
                Identifier = String.Join(";", req.Headers[XForwardedForHeaderKey]);
            }
            else
            {
                Identifier = req.UserHostAddress;
            }

            RequestTracker.Store(req);
        }

        public override bool Equals(object obj)
        {
            return obj is XForwardedForClientIdentifier &&
                ((XForwardedForClientIdentifier)obj).Identifier == Identifier;
        }

        public override int GetHashCode()
        {
            return Identifier?.GetHashCode() ?? 0;
        }
    }

    public class FIORestBootstrapper : DefaultNancyBootstrapper
    {
        public FIORestBootstrapper()
        {

        }

        protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
        {
            base.RequestStartup(container, pipelines, context);

            pipelines.AfterRequest.AddItemToEndOfPipeline((ctx) =>
            {
                ctx.Response
                    .WithHeader("Access-Control-Allow-Origin", "*")
                    .WithHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
                    .WithHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Age")
                    .WithHeader("Access-Control-Expose-Headers", "Age");

                const string defaultContentType = "text/plain; charset=utf-8";
                if (ctx.Response.ContentType == defaultContentType)
                {
                    ctx.Response.ContentType = "application/json";
                }
            });
        }

        private static Func<NancyContext, IClientIdentifier> XForwardedForIdentifierFunc()
        {
            return ctx => new XForwardedForClientIdentifier(ctx.Request);
        }

        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            LeakyBucketRateLimiter.Enable(pipelines, new LeakyBucketRateLimiterConfiguration
            {
                MaxNumberOfRequests = 20,
                RefreshRate = TimeSpan.FromSeconds(1),
                ClientIdentifierFunc = XForwardedForIdentifierFunc()
            });
            LeakyBucketRateLimiter.RateLimitedEvent += OnRateLimitHit;

            var gzipSettings = new GzipCompressionSettings();
            gzipSettings.MinimumBytes = 2048;
            pipelines.EnableGzipCompression(gzipSettings);

            base.ApplicationStartup(container, pipelines);

            pipelines.OnError.AddItemToEndOfPipeline((ctx, ex) =>
            {
                if (ctx.Request != null)
                {
                    return NancyExtensionMethods.ReturnBadResponse(ctx.Request, ex);
                }

                return null;
            });

            pipelines.AfterRequest += (ctx) =>
            {
                ctx.DefaultUncached();
            };

            if (UserDataTracking.IsEnabled)
            {
                pipelines.BeforeRequest.AddItemToStartOfPipeline(UserDataTracking.BeforeRequest());
                pipelines.AfterRequest.AddItemToEndOfPipeline(UserDataTracking.AfterRequest());
            }

            if (TimingTracker.IsEnabled)
            {
                pipelines.BeforeRequest.AddItemToStartOfPipeline(TimingTracker.BeforeRequest());
                pipelines.AfterRequest.AddItemToEndOfPipeline(TimingTracker.AfterRequest());
                pipelines.OnError.AddItemToEndOfPipeline(TimingTracker.OnError());
            }
        }

        private static void OnRateLimitHit(IClientIdentifier identifier)
        {
            RequestTracker.StoreRateLimitHit(identifier);
        }
    }
}
