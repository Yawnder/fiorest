﻿# JsonRepresentations
- Create a new file
- In Visual Studio (while *not* debugging):
   - Edit
   - Paste Special
   - Paste JSON as Classes

# Setup information
- In the GitLab Project configuration, create a sensitive variable `$NUGETORG_APIKEY` containing the Nuget.org ApiKey.
- That key will be used by the CI/CD and should never be commited to the repository.

# Release Information
- Whenver there is a new Model, or a model changes, increment the version number in FIORest.JSONRepresentations.csproj.
- Commit and the `deploy-nuget` CI/CD step will push to nuget.org.