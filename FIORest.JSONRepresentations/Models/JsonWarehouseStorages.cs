﻿namespace FIORest.JSONRepresentations.WarehouseStorages
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Storage[] storages { get; set; }
    }

    public class Storage
    {
        public string warehouseId { get; set; }
        public string storeId { get; set; }
        public int units { get; set; }
        public double weightCapacity { get; set; }
        public double volumeCapacity { get; set; }
        public Nextpayment nextPayment { get; set; }
        public Fee fee { get; set; }
        public Feecollector feeCollector { get; set; }
        public string status { get; set; }
        public Address address { get; set; }
    }

    public class Nextpayment
    {
        public long timestamp { get; set; }
    }

    public class Fee
    {
        public double amount { get; set; }
        public string currency { get; set; }
    }

    public class Feecollector
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Address
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }
}