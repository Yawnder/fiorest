﻿namespace FIORest.JSONRepresentations.ShipFlightFlights
{

    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Flight[] flights { get; set; }
    }

    public class Flight
    {
        public string id { get; set; }
        public string shipId { get; set; }
        public Origin origin { get; set; }
        public Destination destination { get; set; }
        public Departure departure { get; set; }
        public Arrival arrival { get; set; }
        public Segment[] segments { get; set; }
        public int currentSegmentIndex { get; set; }
        public float stlDistance { get; set; }
        public float ftlDistance { get; set; }
        public bool aborted { get; set; }
    }

    public class Origin
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Destination
    {
        public Line1[] lines { get; set; }
    }

    public class Line1
    {
        public Entity1 entity { get; set; }
        public string type { get; set; }
    }

    public class Entity1
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Departure
    {
        public long timestamp { get; set; }
    }

    public class Arrival
    {
        public long timestamp { get; set; }
    }

    public class Segment
    {
        public string type { get; set; }
        public Origin1 origin { get; set; }
        public Departure1 departure { get; set; }
        public Destination1 destination { get; set; }
        public Arrival1 arrival { get; set; }
        public float? stlDistance { get; set; }
        public float? stlFuelConsumption { get; set; }
        public Transferellipse transferEllipse { get; set; }
        public float? ftlDistance { get; set; }
        public float? ftlFuelConsumption { get; set; }
    }

    public class Origin1
    {
        public Line2[] lines { get; set; }
    }

    public class Line2
    {
        public Entity2 entity { get; set; }
        public string type { get; set; }
        public Orbit orbit { get; set; }
    }

    public class Entity2
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Orbit
    {
        public float semiMajorAxis { get; set; }
        public float eccentricity { get; set; }
        public float inclination { get; set; }
        public float rightAscension { get; set; }
        public float periapsis { get; set; }
    }

    public class Departure1
    {
        public long timestamp { get; set; }
    }

    public class Destination1
    {
        public Line3[] lines { get; set; }
    }

    public class Line3
    {
        public Entity3 entity { get; set; }
        public string type { get; set; }
        public Orbit1 orbit { get; set; }
    }

    public class Entity3
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Orbit1
    {
        public float semiMajorAxis { get; set; }
        public float eccentricity { get; set; }
        public float inclination { get; set; }
        public float rightAscension { get; set; }
        public float periapsis { get; set; }
    }

    public class Arrival1
    {
        public long timestamp { get; set; }
    }

    public class Transferellipse
    {
        public Startposition startPosition { get; set; }
        public Targetposition targetPosition { get; set; }
        public Center center { get; set; }
        public float alpha { get; set; }
        public float semiMajorAxis { get; set; }
        public float semiMinorAxis { get; set; }
    }

    public class Startposition
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
    }

    public class Targetposition
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
    }

    public class Center
    {
        public float x { get; set; }
        public float y { get; set; }
        public int z { get; set; }
    }

}
