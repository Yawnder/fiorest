﻿namespace FIORest.JSONRepresentations.SitePlatformBuilt
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string siteId { get; set; }
        public string id { get; set; }
        public Module module { get; set; }
        public int area { get; set; }
        public Creationtime creationTime { get; set; }
        public Reclaimablematerial[] reclaimableMaterials { get; set; }
        public Repairmaterial[] repairMaterials { get; set; }
        public Bookvalue bookValue { get; set; }
        public float condition { get; set; }
        public Lastrepair lastRepair { get; set; }
    }

    public class Module
    {
        public string id { get; set; }
        public string platformId { get; set; }
        public string reactorId { get; set; }
        public string reactorName { get; set; }
        public string reactorTicker { get; set; }
        public string type { get; set; }
    }

    public class Creationtime
    {
        public long timestamp { get; set; }
    }

    public class Lastrepair
	{
        public long timestamp { get; set; }
	}

    public class Bookvalue
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Reclaimablematerial
    {
        public Material1 material { get; set; }
        public int amount { get; set; }
    }

    public class Material1
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Repairmaterial
    {
        public Material2 material { get; set; }
        public int amount { get; set; }
    }

    public class Material2
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }
}
