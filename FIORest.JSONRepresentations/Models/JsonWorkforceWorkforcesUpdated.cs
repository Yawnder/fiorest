﻿namespace FIORest.JSONRepresentations.WorkforceWorkforcesUpdated
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public Address address { get; set; }
        public string siteId { get; set; }
        public Workforce[] workforces { get; set; }
    }

    public class Address
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Workforce
    {
        public string level { get; set; }
        public int population { get; set; }
        public int reserve { get; set; }
        public int capacity { get; set; }
        public int required { get; set; }
        public float satisfaction { get; set; }
        public Need[] needs { get; set; }
    }

    public class Need
    {
        public string category { get; set; }
        public bool essential { get; set; }
        public Material material { get; set; }
        public float satisfaction { get; set; }
        public float unitsPerInterval { get; set; }
        public float unitsPer100 { get; set; }
    }

    public class Material
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }
}
