﻿namespace FIORest.JSONRepresentations.UserDataOffices
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body[] body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public string type { get; set; }
        public Start start { get; set; }
        public End end { get; set; }
        public Address address { get; set; }
        public string id { get; set; }
    }

    public class Start
    {
        public long timestamp { get; set; }
    }

    public class End
    {
        public long timestamp { get; set; }
    }

    public class Address
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

}
