﻿namespace FIORest.JSONRepresentations.Channel.MessageAddedSelf
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public string messageId { get; set; }
        public string type { get; set; }
        public Sender sender { get; set; }
        public string message { get; set; }
        public Time time { get; set; }
        public string channelId { get; set; }
        public object deletingUser { get; set; }
    }

    public class Sender
    {
        public string id { get; set; }
        public string username { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Time
    {
        public long timestamp { get; set; }
    }
}
