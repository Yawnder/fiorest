﻿namespace FIORest.JSONRepresentations.ComexBrokerData
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public string id { get; set; }
        public string ticker { get; set; }
        public Exchange exchange { get; set; }
        public Address address { get; set; }
        public Currency currency { get; set; }
        public Material material { get; set; }
        public Previous previous { get; set; }
        public Price price { get; set; }
        public Pricetime priceTime { get; set; }
        public High high { get; set; }
        public Alltimehigh allTimeHigh { get; set; }
        public Low low { get; set; }
        public Alltimelow allTimeLow { get; set; }
        public Ask ask { get; set; }
        public Bid bid { get; set; }
        public int supply { get; set; }
        public int demand { get; set; }
        public int traded { get; set; }
        public Volume volume { get; set; }
        public Priceaverage priceAverage { get; set; }
        public Narrowpriceband narrowPriceBand { get; set; }
        public Widepriceband widePriceBand { get; set; }
        public Sellingorder[] sellingOrders { get; set; }
        public Buyingorder[] buyingOrders { get; set; }
    }

    public class Exchange
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Address
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Currency
    {
        public int numericCode { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public int decimals { get; set; }
    }

    public class Material
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Previous
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Price
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Pricetime
    {
        public long timestamp { get; set; }
    }

    public class High
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Alltimehigh
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Low
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Alltimelow
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Ask
    {
        public Price1 price { get; set; }
        public int amount { get; set; }
    }

    public class Price1
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Bid
    {
        public Price2 price { get; set; }
        public int amount { get; set; }
    }

    public class Price2
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Volume
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Priceaverage
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Narrowpriceband
    {
        public float low { get; set; }
        public float high { get; set; }
    }

    public class Widepriceband
    {
        public float low { get; set; }
        public float high { get; set; }
    }

    public class Sellingorder
    {
        public string id { get; set; }
        public Trader trader { get; set; }
        public int? amount { get; set; }
        public Limit limit { get; set; }
    }

    public class Trader
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Limit
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Buyingorder
    {
        public string id { get; set; }
        public Trader1 trader { get; set; }
        public int? amount { get; set; }
        public Limit1 limit { get; set; }
    }

    public class Trader1
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Limit1
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }
}
