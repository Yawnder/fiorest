﻿namespace FIORest.JSONRepresentations.SiteSites
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Site[] sites { get; set; }
    }

    public class Site
    {
        public string siteId { get; set; }
        public Address address { get; set; }
        public Founded founded { get; set; }
        public int investedPermits { get; set; }
        public int maximumPermits { get; set; }
        public Platform[] platforms { get; set; }
        public Buildoptions buildOptions { get; set; }
        public int area { get; set; }
    }

    public class Address
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Founded
    {
        public long timestamp { get; set; }
    }

    public class Buildoptions
    {
        public Option[] options { get; set; }
    }

    public class Option
    {
        public string id { get; set; }
        public string name { get; set; }
        public int area { get; set; }
        public string ticker { get; set; }
        public string expertiseCategory { get; set; }
        public bool needsFertileSoil { get; set; }
        public string type { get; set; }
        public Workforcecapacity[] workforceCapacities { get; set; }
        public Materials materials { get; set; }
    }

    public class Materials
    {
        public Quantity[] quantities { get; set; }
    }

    public class Quantity
    {
        public Material material { get; set; }
        public int amount { get; set; }
    }

    public class Material
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Workforcecapacity
    {
        public string level { get; set; }
        public int capacity { get; set; }
    }

    public class Platform
    {
        public string siteId { get; set; }
        public string id { get; set; }
        public Module module { get; set; }
        public int area { get; set; }
        public Creationtime creationTime { get; set; }
        public Reclaimablematerial[] reclaimableMaterials { get; set; }
        public Repairmaterial[] repairMaterials { get; set; }
        public Bookvalue bookValue { get; set; }
        public float condition { get; set; }
        public Lastrepair lastRepair { get; set; }
    }

    public class Module
    {
        public string id { get; set; }
        public string platformId { get; set; }
        public string reactorId { get; set; }
        public string reactorName { get; set; }
        public string reactorTicker { get; set; }
        public string type { get; set; }
    }

    public class Creationtime
    {
        public long timestamp { get; set; }
    }

    public class Lastrepair
    {
        public long timestamp { get; set; }
    }

    public class Bookvalue
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Reclaimablematerial
    {
        public Material1 material { get; set; }
        public int amount { get; set; }
    }

    public class Material1
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Repairmaterial
    {
        public Material2 material { get; set; }
        public int amount { get; set; }
    }

    public class Material2
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

}
