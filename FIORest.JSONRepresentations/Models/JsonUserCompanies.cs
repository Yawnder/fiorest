﻿namespace FIORest.JSONRepresentations.UserCompanies
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public string name { get; set; }
        public string code { get; set; }
        public Founded founded { get; set; }
        public User user { get; set; }
        public Country country { get; set; }
        public Corporation corporation { get; set; }
        public Ratingreport ratingReport { get; set; }
        public Siteaddress[] siteAddresses { get; set; }
        public string id { get; set; }
    }

    public class Founded
    {
        public long timestamp { get; set; }
    }

    public class User
    {
        public string id { get; set; }
        public string username { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Country
    {
        public string id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Corporation
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Ratingreport
    {
        public string overallRating { get; set; }
        public Subrating[] subRatings { get; set; }
    }

    public class Subrating
    {
        public string score { get; set; }
        public string rating { get; set; }
    }

    public class Siteaddress
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

}
