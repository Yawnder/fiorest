﻿namespace FIORest.JSONRepresentations.JsonPlanetSites
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public string planetId { get; set; }
        public Site[] sites { get; set; }
    }

    public class Site
    {
        public string planetId { get; set; }
        public string ownerId { get; set; }
        public Entity entity { get; set; }
        public string type { get; set; }
        public int plot { get; set; }
        public string id { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
        public string naturalId { get; set; }
    }

}
