﻿namespace FIORest.JSONRepresentations.SystemStarsdata
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Star[] stars { get; set; }
    }

    public class Star
    {
        public string systemId { get; set; }
        public Address address { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public Position position { get; set; }
        public string sectorId { get; set; }
        public string subSectorId { get; set; }
        public string[] connections { get; set; }
    }

    public class Address
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Position
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
    }

}
