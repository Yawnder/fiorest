﻿namespace FIORest.JSONRepresentations.JsonContractData
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Contract[] contracts { get; set; }
    }

    public class Contract
    {
        public string id { get; set; }
        public string localId { get; set; } // `CONT {localId}` buffer
        public Date date { get; set; }
        public string party { get; set; } // `PROVIDER` or `CUSTOMER`
        public Partner partner { get; set; }
        public string status { get; set; }
        public Condition[] conditions { get; set; }
        public Extensiondeadline extensionDeadline { get; set; }
        public bool canExtend { get; set; }
        public bool canRequestTermination { get; set; }
        public bool terminationSent { get; set; }
        public bool terminationReceived { get; set; }
        public Duedate dueDate { get; set; }
        public string name { get; set; }
        public string preamble { get; set; }
    }

    public class Date
    {
        public long timestamp { get; set; }
    }

    public class Partner
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Extensiondeadline
    {
        public long timestamp { get; set; }
    }

    public class Duedate
    {
        public long timestamp { get; set; }
    }

    public class Condition
    {
        public Address address { get; set; }
        public Quantity quantity { get; set; } // null if shipment
        public double weight { get; set; }
        public double volume { get; set; }
        public string blockId { get; set; }
        public string type { get; set; }
        public string id { get; set; }
        public string party { get; set; }
        public int index { get; set; }
        public string status { get; set; }
        public string[] dependencies { get; set; }
        public Deadline deadline { get; set; }
        public Amount amount { get; set; }
        public Destination destination { get; set; }
        public string shipmentItemId { get; set; }
        public Pickedup pickedUp { get; set; }
    }

    public class Address
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Quantity
    {
        public Material material { get; set; }
        public int amount { get; set; }
    }

    public class Material
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public double weight { get; set; }
        public double volume { get; set; }
    }

    public class Deadline
    {
        public long timestamp { get; set; }
    }

    public class Amount
    {
        public double amount { get; set; }
        public string currency { get; set; }
    }

    public class Destination
    {
        public Line1[] lines { get; set; }
    }

    public class Line1
    {
        public Entity1 entity { get; set; }
        public string type { get; set; }
    }

    public class Entity1
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Pickedup
    {
        public Material1 material { get; set; }
        public int amount { get; set; }
    }

    public class Material1
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public double weight { get; set; }
        public double volume { get; set; }
    }
}
