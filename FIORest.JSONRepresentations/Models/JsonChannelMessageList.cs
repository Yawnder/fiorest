﻿namespace FIORest.JSONRepresentations.Channel.MessageList
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public string channelId { get; set; }
        public Message1[] messages { get; set; }
        public bool hasMore { get; set; }
    }

    public class Message1
    {
        public string messageId { get; set; }
        public string type { get; set; }
        public Sender sender { get; set; }
        public string message { get; set; }
        public Time time { get; set; }
        public string channelId { get; set; }
        public Deletinguser deletingUser { get; set; }
    }

    public class Sender
    {
        public string id { get; set; }
        public string username { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Time
    {
        public long timestamp { get; set; }
    }

    public class Deletinguser
    {
        public string id { get; set; }
        public string username { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }
}
