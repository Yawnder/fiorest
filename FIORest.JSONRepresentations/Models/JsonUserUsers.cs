﻿namespace FIORest.JSONRepresentations.UserUsers
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public string username { get; set; }
        public string subscriptionLevel { get; set; }
        public string highestTier { get; set; }
        public bool pioneer { get; set; }
        public bool moderator { get; set; }
        public bool team { get; set; }
        public Created created { get; set; }
        public Company company { get; set; }
        public string id { get; set; }
    }

    public class Created
    {
        public long timestamp { get; set; }
    }

    public class Company
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }
}
