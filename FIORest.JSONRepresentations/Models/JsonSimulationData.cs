﻿namespace FIORest.JSONRepresentations.SimulationData
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public int simulationInterval { get; set; }
        public int flightSTLFactor { get; set; }
        public int flightFTLFactor { get; set; }
        public int planetaryMotionFactor { get; set; }
        public int parsecLength { get; set; }
    }

}
