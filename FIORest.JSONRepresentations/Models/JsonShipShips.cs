﻿namespace FIORest.JSONRepresentations.ShipShips
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Ship[] ships { get; set; }
    }

    public class Ship
    {
        public string id { get; set; }
        public string idShipStore { get; set; }
        public string idStlFuelStore { get; set; }
        public string idFtlFuelStore { get; set; }
        public string registration { get; set; }
        public string name { get; set; }
        public Commissioningtime commissioningTime { get; set; }
        public string blueprintNaturalId { get; set; }
        public Address address { get; set; }
        public string flightId { get; set; }
        public float acceleration { get; set; }
        public float thrust { get; set; }
        public float mass { get; set; }
        public float operatingEmptyMass { get; set; }
        public float volume { get; set; }
        public float reactorPower { get; set; }
        public float emitterPower { get; set; }
        public string stlFuelStoreId { get; set; }
        public float stlFuelFlowRate { get; set; }
        public string ftlFuelStoreId { get; set; }
        public Operatingtimestl operatingTimeStl { get; set; }
        public Operatingtimeftl operatingTimeFtl { get; set; }
        public float condition { get; set; }
        public LastRepair lastRepair { get; set; }
        public Repairmaterial[] repairMaterials { get; set; }
    }

    public class Commissioningtime
    {
        public long timestamp { get; set; }
    }

    public class Address
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Operatingtimestl
    {
        public long millis { get; set; }
    }

    public class Operatingtimeftl
    {
        public long millis { get; set; }
    }

    public class LastRepair
    {
        public long millis { get; set; }
    }

    public class Repairmaterial
    {
        public Material material { get; set; }
        public int amount { get; set; }
    }

    public class Material
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

}
