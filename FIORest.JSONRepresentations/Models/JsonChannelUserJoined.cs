﻿namespace FIORest.JSONRepresentations.Channel.UserJoined
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string messageId { get; set; }
        public User1 user { get; set; }
        public Time time { get; set; }
        public string channelId { get; set; }
    }

    public class User1
    {
        public User user { get; set; }
    }

    public class User
    {
        public string id { get; set; }
        public string username { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Time
    {
        public long timestamp { get; set; }
    }
}
