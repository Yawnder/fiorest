﻿namespace FIORest.JSONRepresentations.JsonComexTraderOrders
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Order[] orders { get; set; }
    }

    public class Order
    {
        public string id { get; set; }
        public Exchange exchange { get; set; }
        public string brokerId { get; set; }
        public string type { get; set; }
        public Material material { get; set; }
        public int amount { get; set; }
        public int initialAmount { get; set; }
        public Limit limit { get; set; }
        public string status { get; set; }
        public Created created { get; set; }
        public Trade[] trades { get; set; }
    }

    public class Exchange
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Material
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Limit
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Created
    {
        public long timestamp { get; set; }
    }

    public class Trade
    {
        public string id { get; set; }
        public int amount { get; set; }
        public Price price { get; set; }
        public Time time { get; set; }
        public Partner partner { get; set; }
    }

    public class Price
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Time
    {
        public long timestamp { get; set; }
    }

    public class Partner
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }
}
