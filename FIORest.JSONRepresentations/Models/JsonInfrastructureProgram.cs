﻿namespace FIORest.JSONRepresentations.InfrastructureProgram
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public Address address { get; set; }
        public Operator _operator { get; set; }
        public Completiondate completionDate { get; set; }
        public string currentTerm { get; set; }
        public string upcomingTerm { get; set; }
        public string[] previousTerms { get; set; }
        public string populationId { get; set; }
        public Programs programs { get; set; }
        public string id { get; set; }
    }

    public class Address
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Operator
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Completiondate
    {
        public long timestamp { get; set; }
    }

    public class Programs
    {
        public Currentprogram currentProgram { get; set; }
        public Nextprogram nextProgram { get; set; }
        public Pastprogram[] pastPrograms { get; set; }
    }

    public class Currentprogram
    {
        public int number { get; set; }
        public Start start { get; set; }
        public End end { get; set; }
        public string category { get; set; }
        public string program { get; set; }
    }

    public class Start
    {
        public long timestamp { get; set; }
    }

    public class End
    {
        public long timestamp { get; set; }
    }

    public class Nextprogram
    {
        public int number { get; set; }
        public Start1 start { get; set; }
        public End1 end { get; set; }
        public string category { get; set; }
        public string program { get; set; }
    }

    public class Start1
    {
        public long timestamp { get; set; }
    }

    public class End1
    {
        public long timestamp { get; set; }
    }

    public class Pastprogram
    {
        public int number { get; set; }
        public Start2 start { get; set; }
        public End2 end { get; set; }
        public string category { get; set; }
        public string program { get; set; }
    }

    public class Start2
    {
        public long timestamp { get; set; }
    }

    public class End2
    {
        public long timestamp { get; set; }
    }
}
