﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIORest.JSONRepresentations.Station
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public string naturalId { get; set; }
        public string name { get; set; }
        public Address address { get; set; }
        public Commissioningtime commissioningTime { get; set; }
        public Comex comex { get; set; }
        public string warehouseId { get; set; }
        public Country country { get; set; }
        public Currency currency { get; set; }
        public Governor governor { get; set; }
        public Governingentity governingEntity { get; set; }
        public string id { get; set; }
    }

    public class Address
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Commissioningtime
    {
        public long timestamp { get; set; }
    }

    public class Comex
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Country
    {
        public string id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Currency
    {
        public int numericCode { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public int decimals { get; set; }
    }

    public class Governor
    {
        public string id { get; set; }
        public string username { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Governingentity
    {
        public string id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }
}
