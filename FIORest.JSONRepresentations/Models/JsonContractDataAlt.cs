﻿namespace FIORest.JSONRepresentations.JsonContractData
{
    public class AltRootObject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class AltPayload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class AltMessage
    {
        public string messageType { get; set; }
        public Contract[] payload { get; set; }
    }
}
