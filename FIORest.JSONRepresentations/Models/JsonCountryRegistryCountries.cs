﻿namespace FIORest.JSONRepresentations.CountryRegistryCountries
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public object actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Country[] countries { get; set; }
    }

    public class Country
    {
        public string id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public Currency currency { get; set; }
        public int color { get; set; }
    }

    public class Currency
    {
        public int numericCode { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public int decimals { get; set; }
    }

}
