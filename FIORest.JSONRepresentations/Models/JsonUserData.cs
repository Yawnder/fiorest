﻿namespace FIORest.JSONRepresentations.UserData
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public string id { get; set; }
        public string username { get; set; }
        public string highestTier { get; set; }
        public bool team { get; set; }
        public bool pioneer { get; set; }
        public string[] perks { get; set; }
        public Created created { get; set; }
        public string companyId { get; set; }
        public int systemNamingRights { get; set; }
        public int planetNamingRights { get; set; }
        public bool isPayingUser { get; set; }
        public bool isModeratorChat { get; set; }
        public object[] mutedUsers { get; set; }
        public bool isMuted { get; set; }
    }

    public class Created
    {
        public long timestamp { get; set; }
    }
}
