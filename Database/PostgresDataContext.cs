﻿using Microsoft.EntityFrameworkCore;

namespace FIORest.Database
{
    public class PostgresDataContext : PRUNDataContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // Uncomment (todo: implement debug logging) to log SQL queries.
            PRUNDataContext.LogDebug(options);
            options.UseNpgsql(Globals.ConnectionString).UseLazyLoadingProxies();
        }
    }
}
