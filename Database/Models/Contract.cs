﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    [Microsoft.EntityFrameworkCore.Index(nameof(ContractLocalId))]
    [Microsoft.EntityFrameworkCore.Index(nameof(Party))]
    [Microsoft.EntityFrameworkCore.Index(nameof(PartnerName))]
    [Microsoft.EntityFrameworkCore.Index(nameof(PartnerCompanyCode))]
    public class Contract
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ContractId { get; set; }

        [StringLength(32)]
        public string ContractLocalId { get; set; }

        public long? DateEpochMs { get; set; }
        public long? ExtensionDeadlineEpochMs { get; set; }
        public long? DueDateEpochMs { get; set; }

        public bool CanExtend { get; set; }
        public bool CanRequestTermination { get; set; }
        public bool TerminationSent { get; set; }
        public bool TerminationReceived { get; set; }

        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(256)]
        public string Preamble { get; set; }

        [StringLength(32)]
        public string Party { get; set; }

        [StringLength(64)]
        public string Status { get; set; }

        [StringLength(32)]
        public string PartnerId { get; set; }
        [StringLength(64)]
        public string PartnerName { get; set; }
        [StringLength(8)]
        public string PartnerCompanyCode { get; set; }

        public virtual List<ContractCondition> Conditions { get; set; } = new List<ContractCondition>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }

        public bool CouldHaveChanges()
        {
            List<string> EndedStatuses = new List<string> { "BREACHED", "FULFILLED", "CANCELLED", "CLOSED" };
            if (EndedStatuses.Contains(Status))
            {
                return false;
            }

            if (Status == "DEADLINE_EXCEEDED" && CanExtend == false)
            {
                return false;
            }

            return true;
        }


        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(ContractId) && ContractId.Length == 32 &&
                !String.IsNullOrEmpty(ContractLocalId) && ContractLocalId.Length <= 32 &&
                (DateEpochMs == null || DateEpochMs > 0) &&
                (ExtensionDeadlineEpochMs == null || ExtensionDeadlineEpochMs > 0) &&
                (DueDateEpochMs == null || DueDateEpochMs > 0) &&
                !String.IsNullOrEmpty(Party) && Party.Length <= 32 &&
                !String.IsNullOrEmpty(Status) && Status.Length <= 64 &&
                !String.IsNullOrEmpty(PartnerId) && PartnerId.Length == 32 &&
                !String.IsNullOrEmpty(PartnerName) && PartnerName.Length <= 64 &&
                (PartnerCompanyCode == null || PartnerCompanyCode.Length <= 8) &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default);

            Conditions.ForEach(c => c.Validate());
        }
    }

    public class ContractCondition
    {
        [Key]
        [JsonIgnore]
        [StringLength(65)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ContractConditionId { get; set; }

        [StringLength(32)]
        public string Address { get; set; }
        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        public int? MaterialAmount { get; set; }

        public double? Weight { get; set; }
        public double? Volume { get; set; }

        [StringLength(32)]
        public string BlockId { get; set; }
        [StringLength(32)]
        public string Type { get; set; }
        [StringLength(32)]
        public string ConditionId { get; set; }
        [StringLength(32)]
        public string Party { get; set; }
        public int ConditionIndex { get; set; }
        [StringLength(64)]
        public string Status { get; set; }

        public virtual List<ContractDependency> Dependencies { get; set; } = new List<ContractDependency>();

        public long? DeadlineEpochMs { get; set; }

        public double? Amount { get; set; }
        [StringLength(8)]
        public string Currency { get; set; }

        [StringLength(32)]
        public string Destination { get; set; }

        [StringLength(32)]
        public string ShipmentItemId { get; set; }

        [StringLength(32)]
        public string PickedUpMaterialId { get; set; }
        [StringLength(8)]
        public string PickedUpMaterialTicker { get; set; }
        public int? PickedUpAmount { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string ContractId { get; set; }
        [JsonIgnore]
        public virtual Contract Contract { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(ContractConditionId) && ContractConditionId.Length == 65 &&
                (Address == null || Address.Length <= 32) &&
                (MaterialId == null || MaterialId.Length == 32) &&
                (MaterialTicker == null || MaterialTicker.Length <= 8) &&
                (MaterialAmount == null || MaterialAmount >= 1) &&
                (Weight == null || Weight >= 0.0) &&
                (Volume == null || Volume >= 0.0) &&
                (BlockId == null || BlockId.Length == 32) &&
                (Type == null || Type.Length <= 32) &&
                (ConditionId == null || ConditionId.Length == 32) &&
                !String.IsNullOrEmpty(Party) && Party.Length <= 32 &&
                ConditionIndex >= 0 &&
                (Status == null || Status.Length <= 64) &&
                (DeadlineEpochMs == null || DeadlineEpochMs > 0) &&
                (Amount == null || Amount > 0.0) &&
                (Currency == null || Currency.Length <= 8) &&
                (Destination == null || Destination.Length <= 32) &&
                (ShipmentItemId == null || ShipmentItemId.Length == 32) &&
                (PickedUpMaterialId == null || PickedUpMaterialId.Length == 32) &&
                (PickedUpMaterialTicker == null || PickedUpMaterialTicker.Length <= 8) &&
                (PickedUpAmount == null || PickedUpAmount >= 0) &&
                !String.IsNullOrEmpty(ContractId) && ContractId.Length == 32);

            Dependencies.ForEach(d => d.Validate());
        }
    }

    public class ContractDependency
    {
        [Key]
        [JsonIgnore]
        [StringLength(98)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ContractDependencyId { get; set; }

        [StringLength(32)]
        public string Dependency { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(65)]
        public string ContractConditionId { get; set; }
        [JsonIgnore]
        public virtual ContractCondition ContractCondition { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(ContractDependencyId) && ContractDependencyId.Length == 98 &&
                !String.IsNullOrEmpty(Dependency) && Dependency.Length == 32 &&
                !String.IsNullOrEmpty(ContractConditionId) && ContractConditionId.Length == 65
                );
        }
    }
}
