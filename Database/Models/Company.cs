﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [Index(nameof(UserName))]
    [Index(nameof(CompanyName))]
    [Index(nameof(CompanyCode))]
    public class Company
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CompanyId { get; set; }

        [StringLength(64)]
        public string CompanyName { get; set; }
        [StringLength(8)]
        public string CompanyCode { get; set; }

        [StringLength(32)]
        public string UserName { get; set; }
        [StringLength(32)]
        public string HighestTier { get; set; }

        public bool Pioneer { get; set; }
        public bool Team { get; set; }

        public long CreatedEpochMs { get; set; }

        [StringLength(32)]
        public string CountryId { get; set; }

        [StringLength(4)]
        public string CurrencyCode { get; set; }

        [StringLength(32)]
        public string StartingProfile { get; set; }

        [StringLength(64)]
        public string StartingLocation { get; set; }

        public virtual List<CompanyCurrencyBalance> Balances { get; set; } = new List<CompanyCurrencyBalance>();

        [StringLength(8)]
        public string OverallRating { get; set; }
        [StringLength(8)]
        public string ActivityRating { get; set; }
        [StringLength(8)]
        public string ReliabilityRating { get; set; }
        [StringLength(8)]
        public string StabilityRating { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(CompanyId) && CompanyId.Length == 32 &&
                !String.IsNullOrEmpty(CompanyName) && CompanyName.Length <= 64 &&
                (CompanyCode == null || CompanyCode.Length <= 8) &&
                (UserName == null || UserName.Length <= 32) &&
                (HighestTier == null || HighestTier.Length <= 32) &&
                //!String.IsNullOrEmpty(CountryId) && CountryId.Length <= 32 &&
                //!String.IsNullOrEmpty(CurrencyCode) && CurrencyCode.Length <= 4 &&
                //!String.IsNullOrEmpty(StartingProfile) && StartingProfile.Length <= 32 &&
                //!String.IsNullOrEmpty(StartingLocation) && StartingLocation.Length <= 64 &&
                //!String.IsNullOrEmpty(OverallRating) && OverallRating.Length <= 8 &&
                //!String.IsNullOrEmpty(ActivityRating) && ActivityRating.Length <= 8 &&
                //!String.IsNullOrEmpty(ReliabilityRating) && ReliabilityRating.Length <= 8 &&
                //!String.IsNullOrEmpty(StabilityRating) && StabilityRating.Length <= 8 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default);

            Balances.ForEach(b => b.Validate());
        }
    }

    public class CompanyCurrencyBalance
    {
        [Key]
        [StringLength(40)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CompanyCurrencyBalanceId { get; set; } // CompanyId-Currency

        [StringLength(8)]
        public string Currency { get; set; }

        public double Balance { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string CompanyId { get; set; }

        [JsonIgnore]
        public virtual Company Company { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(CompanyCurrencyBalanceId) && CompanyCurrencyBalanceId.Length <= 40 &&
                !String.IsNullOrEmpty(Currency) && Currency.Length <= 8 &&
                Balance >= 0.0 &&
                !String.IsNullOrEmpty(CompanyId) && CompanyId.Length == 32);
        }
    }
}
