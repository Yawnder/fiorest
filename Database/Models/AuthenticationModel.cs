﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class AuthenticationModel
    {
        public int AuthenticationModelId { get; set; }

        public bool AccountEnabled { get; set; }
        [StringLength(32)]
        public string DisabledReason { get; set; }

        [StringLength(32)]
        public string UserName { get; set; }
        [StringLength(128)]
        public string PasswordHash { get; set; }

        public bool IsAdministrator { get; set; }

        [StringLength(40)]
        public string DiscordId { get; set; }

        public virtual List<PermissionAllowance> Allowances { get; set; } = new List<PermissionAllowance>();
        public virtual List<FailedLoginAttempt> FailedAttempts { get; set; } = new List<FailedLoginAttempt>();

        public virtual List<APIKey> APIKeys { get; set; } = new List<APIKey>();

        public Guid AuthorizationKey { get; set; }
        public DateTime AuthorizationExpiry { get; set; }
    }

    public class PermissionAllowance
    {
        [JsonIgnore]
        public int PermissionAllowanceId { get; set; }

        [StringLength(32)]
        public string UserName { get; set; }

        [StringLength(42)]
        public string GroupNameAndId { get; set; }

        public bool FlightData { get; set; }
        public bool BuildingData { get; set; }
        public bool StorageData { get; set; }
        public bool ProductionData { get; set; }
        public bool WorkforceData { get; set; }
        public bool ExpertsData { get; set; }
        public bool ContractData { get; set; }
        public bool ShipmentTrackingData { get; set; }

        [JsonIgnore]
        public int AuthenticationModelId { get; set; }

        [JsonIgnore]
        public virtual AuthenticationModel AuthenticationModel { get; set; }

        public override bool Equals(object obj)
        {
            var PermAllowance = obj as PermissionAllowance;
            if (PermAllowance == null)
            {
                return false;
            }

            return
                UserName == PermAllowance.UserName &&
                GroupNameAndId == PermAllowance.GroupNameAndId &&
                FlightData == PermAllowance.FlightData &&
                BuildingData == PermAllowance.BuildingData &&
                StorageData == PermAllowance.StorageData &&
                ProductionData == PermAllowance.ProductionData &&
                WorkforceData == PermAllowance.WorkforceData &&
                ExpertsData == PermAllowance.ExpertsData &&
                ContractData == PermAllowance.ContractData &&
                ShipmentTrackingData == PermAllowance.ShipmentTrackingData;
        }

        public override int GetHashCode()
        {
            var hash = new HashCode();
            hash.Add(UserName);
            hash.Add(GroupNameAndId);
            hash.Add(FlightData);
            hash.Add(BuildingData);
            hash.Add(StorageData);
            hash.Add(ProductionData);
            hash.Add(WorkforceData);
            hash.Add(ExpertsData);
            hash.Add(ContractData);
            hash.Add(ShipmentTrackingData);
            return hash.ToHashCode();
        }

        public PermissionAllowance Merge(PermissionAllowance other)
        {
            return new PermissionAllowance
            {
                FlightData = FlightData || other.FlightData,
                BuildingData = BuildingData || other.BuildingData,
                StorageData = StorageData || other.StorageData,
                ProductionData = ProductionData || other.ProductionData,
                WorkforceData = WorkforceData || other.WorkforceData,
                ExpertsData = ExpertsData || other.ExpertsData,
                ContractData = ContractData || other.ContractData,
                ShipmentTrackingData = ShipmentTrackingData || other.ShipmentTrackingData
            };
        }
    }

    public class FailedLoginAttempt
    {
        [JsonIgnore]
        public int FailedLoginAttemptId { get; set; }

        [JsonIgnore]
        public int AuthenticationModelId { get; set; }

        [StringLength(32)]
        public string Address { get; set; }

        public DateTime FailedAttemptDateTime { get; set; }

        [JsonIgnore]
        public virtual AuthenticationModel AuthenticationModel { get; set; }
    }

    public class APIKey
    {
        [JsonIgnore]
        public int APIKeyId { get; set; }

        public Guid AuthAPIKey { get; set; }

        [StringLength(255)]
        public string Application { get; set; }

        public bool AllowWrites { get; set; }

        public DateTime LastAccessTime { get; set; }

        [JsonIgnore]
        public int AuthenticationModelId { get; set; }

        [JsonIgnore]
        public virtual AuthenticationModel AuthenticationModel { get; set; }
    }

    public class Registration
    {
        [JsonIgnore]
        public int RegistrationId { get; set; }

        [StringLength(32)]
        public string UserName { get; set; }
        [StringLength(64)]
        public string RegistrationGuid { get; set; }

        public DateTime RegistrationTime { get; set; }
    }
}
