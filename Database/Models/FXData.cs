using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace FIORest.Database.Models
{
    public class FXDataPair
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string FXPairId { get; set; }

        [StringLength(8)]
        public string BaseCurrencyCode { get; set; }
        [StringLength(32)]
        public string BaseCurrencyName { get; set; }
        public int BaseCurrencyNumericCode { get; set; }

        [StringLength(8)]
        public string QuoteCurrencyCode { get; set; }
        [StringLength(32)]
        public string QuoteCurrencyName { get; set; }
        public int QuoteCurrencyNumericCode { get; set; }

        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Open { get; set; }
        public decimal Previous { get; set; }
        public decimal Traded { get; set; }
        public decimal Volume { get; set; }
        public long PriceUpdateEpochMs { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(FXPairId) && FXPairId.Length == 32 &&
                !String.IsNullOrEmpty(BaseCurrencyCode) && BaseCurrencyCode.Length <= 8 &&
                !String.IsNullOrEmpty(BaseCurrencyName) && BaseCurrencyName.Length <= 32 &&
                !String.IsNullOrEmpty(QuoteCurrencyCode) && QuoteCurrencyCode.Length <= 8 &&
                !String.IsNullOrEmpty(QuoteCurrencyName) && QuoteCurrencyName.Length <= 32 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );
        }
    }
}

