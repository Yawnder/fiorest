﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [Index(nameof(SourceSystemId))]
    [Index(nameof(SourceSystemName))]
    [Index(nameof(SourceSystemNaturalId))]
    [Index(nameof(DestinationSystemId))]
    [Index(nameof(DestinationSystemName))]
    [Index(nameof(DestinationNaturalId))]
    public class JumpCache
    {
        [Key]
        [JsonIgnore]
        [StringLength(65)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string JumpCacheId { get; set; } // SourceSystemId-DestinationSystemId

        [StringLength(32)]
        public string SourceSystemId { get; set; }
        [StringLength(32)]
        public string SourceSystemName { get; set; }
        [StringLength(8)]
        public string SourceSystemNaturalId { get; set; }

        [StringLength(32)]
        public string DestinationSystemId { get; set; }
        [StringLength(32)]
        public string DestinationSystemName { get; set; }
        [StringLength(8)]
        public string DestinationNaturalId { get; set; }

        public double OverallDistance { get; set; }
        public int JumpCount { get; set; }

        public virtual List<JumpCacheRouteJump> Jumps { get; set; } = new List<JumpCacheRouteJump>();

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(JumpCacheId) && JumpCacheId.Length == 65 &&
                !String.IsNullOrEmpty(SourceSystemId) && SourceSystemId.Length == 32 &&
                !String.IsNullOrEmpty(SourceSystemName) && SourceSystemName.Length <= 32 &&
                !String.IsNullOrEmpty(SourceSystemNaturalId) && SourceSystemNaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(DestinationSystemId) && DestinationSystemId.Length == 32 &&
                !String.IsNullOrEmpty(DestinationSystemName) && DestinationSystemName.Length <= 32 &&
                !String.IsNullOrEmpty(DestinationNaturalId) && DestinationNaturalId.Length <= 8 &&
                OverallDistance >= 0.0 &&
                JumpCount >= 0
                );

            Jumps.ForEach(j => j.Validate());
        }
    }

    [Index(nameof(SourceSystemId))]
    [Index(nameof(DestinationSystemId))]
    public class JumpCacheRouteJump
    {
        [Key]
        [JsonIgnore]
        [StringLength(131)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string JumpCacheRouteJumpId { get; set; } // JumpCacheId-SourceSystemId-DestinationSystemId

        [StringLength(32)]
        public string SourceSystemId { get; set; }
        [StringLength(32)]
        public string SourceSystemName { get; set; }
        [StringLength(8)]
        public string SourceSystemNaturalId { get; set; }

        [StringLength(32)]
        public string DestinationSystemId { get; set; }
        [StringLength(32)]
        public string DestinationSystemName { get; set; }
        [StringLength(8)]
        public string DestinationNaturalId { get; set; }

        public double Distance { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(65)]
        public string JumpCacheId { get; set; }

        [JsonIgnore]
        public virtual JumpCache JumpCache { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(JumpCacheRouteJumpId) && JumpCacheRouteJumpId.Length == 131 &&
                !String.IsNullOrEmpty(SourceSystemId) && SourceSystemId.Length == 32 &&
                !String.IsNullOrEmpty(SourceSystemName) && SourceSystemName.Length <= 32 &&
                !String.IsNullOrEmpty(SourceSystemNaturalId) && SourceSystemNaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(DestinationSystemId) && DestinationSystemId.Length == 32 &&
                !String.IsNullOrEmpty(DestinationSystemName) && DestinationSystemName.Length <= 32 &&
                !String.IsNullOrEmpty(DestinationNaturalId) && DestinationNaturalId.Length <= 38 &&
                Distance >= 0.0 &&
                !String.IsNullOrEmpty(JumpCacheId) && JumpCacheId.Length == 65
                );
        }
    }
}
