﻿using System;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class PriceIndexModel
    {
        [JsonIgnore]
        public int PriceIndexModelId { get; set; }

        public DateTime TimeStamp { get; set; }

        [StringLength(32)]
        public string PriceIndexLabel { get; set; }
        public double PriceIndexValue { get; set; }
    }
}
