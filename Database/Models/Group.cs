﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
	public class GroupModel
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int GroupModelId { get; set; }

		[StringLength(32)]
		public string GroupOwner { get; set; }

		public virtual List<GroupModelAdmin> GroupAdmins { get; set; } = new List<GroupModelAdmin>();

		[StringLength(32)]
		public string GroupName { get; set; }

		public virtual List<GroupUserEntryModel> GroupUsers { get; set; } = new List<GroupUserEntryModel>();

		public void Validate()
		{
			Debug.Assert(
				!String.IsNullOrEmpty(GroupOwner) && GroupOwner.Length <= 32 &&
				(GroupName == null || GroupName.Length <= 32)
				);

			GroupAdmins.ForEach(a => a.Validate());
			GroupUsers.ForEach(gu => gu.Validate());
		}
	}

	public class GroupModelAdmin
    {
        [JsonIgnore]
		public int GroupModelAdminId { get; set; }

        [StringLength(32)]
		public string GroupAdminUserName { get; set; }

		[Required]
		[JsonIgnore]
		public int GroupModelId { get; set; }

        [JsonIgnore]
		public virtual GroupModel GroupModel { get; set; }

		public void Validate()
		{
			Debug.Assert(!String.IsNullOrEmpty(GroupAdminUserName) && GroupAdminUserName.Length <= 32);
		}
	}

	public class GroupUserEntryModel
	{
		[JsonIgnore]
		public int GroupUserEntryModelId { get; set; }

		[StringLength(32)]
		public string GroupUserName { get; set; }

		[Required]
		[JsonIgnore]
		public int GroupModelId { get; set; }

		[JsonIgnore]
		public virtual GroupModel GroupModel { get; set; }

		public void Validate()
		{
			Debug.Assert(!String.IsNullOrEmpty(GroupUserName) && GroupUserName.Length <= 32);
		}
	}
}
