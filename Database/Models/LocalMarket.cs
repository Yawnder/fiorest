﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    public class LocalMarket
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string LocalMarketId { get; set; }

        public virtual List<ShippingAd> ShippingAds { get; set; } = new List<ShippingAd>();
        public virtual List<BuyingAd> BuyingAds { get; set; } = new List<BuyingAd>();
        public virtual List<SellingAd> SellingAds { get; set; } = new List<SellingAd>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(LocalMarketId) && LocalMarketId.Length == 32 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );

            ShippingAds.ForEach(sa => sa.Validate());
            BuyingAds.ForEach(ba => ba.Validate());
            SellingAds.ForEach(sa => sa.Validate());
        }
    }

    [Index(nameof(PlanetId))]
    [Index(nameof(PlanetNaturalId))]
    [Index(nameof(PlanetName))]
    public class ShippingAd
    {
        [Key]
        [JsonIgnore]
        [StringLength(64)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ShippingAdId { get; set; } // LocalMarketId-ContractNaturalId

        public int ContractNaturalId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }
        [StringLength(8)]
        public string PlanetNaturalId { get; set; }
        [StringLength(32)]
        public string PlanetName { get; set; }

        [StringLength(32)]
        public string OriginPlanetId { get; set; }
        [StringLength(8)]
        public string OriginPlanetNaturalId { get; set; }
        [StringLength(32)]
        public string OriginPlanetName { get; set; }

        [StringLength(32)]
        public string DestinationPlanetId { get; set; }
        [StringLength(8)]
        public string DestinationPlanetNaturalId { get; set; }
        [StringLength(32)]
        public string DestinationPlanetName { get; set; }

        public double CargoWeight { get; set; }
        public double CargoVolume { get; set; }

        [StringLength(32)]
        public string CreatorCompanyId { get; set; }
        [StringLength(64)]
        public string CreatorCompanyName { get; set; }
        [StringLength(8)]
        public string CreatorCompanyCode { get; set; }

        public double PayoutPrice { get; set; }
        [StringLength(8)]
        public string PayoutCurrency { get; set; }

        public int DeliveryTime { get; set; }

        public long CreationTimeEpochMs { get; set; }
        public long ExpiryTimeEpochMs { get; set; }

        [StringLength(8)]
        public string MinimumRating { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string LocalMarketId { get; set; }

        [JsonIgnore]
        public virtual LocalMarket LocalMarket { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(ShippingAdId) && ShippingAdId.Length <= 64 &&
                ContractNaturalId >= 0 &&
                !String.IsNullOrEmpty(PlanetId) && PlanetId.Length == 32 &&
                !String.IsNullOrEmpty(PlanetNaturalId) && PlanetNaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(PlanetName) && PlanetName.Length <= 32 &&
                !String.IsNullOrEmpty(OriginPlanetId) && OriginPlanetId.Length == 32 &&
                !String.IsNullOrEmpty(OriginPlanetNaturalId) && OriginPlanetNaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(OriginPlanetName) && OriginPlanetName.Length <= 32 &&
                !String.IsNullOrEmpty(DestinationPlanetId) && DestinationPlanetId.Length == 32 &&
                !String.IsNullOrEmpty(DestinationPlanetNaturalId) && DestinationPlanetNaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(DestinationPlanetName) && DestinationPlanetName.Length <= 32 &&
                CargoWeight > 0.0 && CargoVolume > 0.0 &&
                !String.IsNullOrEmpty(CreatorCompanyId) && CreatorCompanyId.Length == 32 &&
                !String.IsNullOrEmpty(CreatorCompanyName) && CreatorCompanyName.Length <= 64 &&
                !String.IsNullOrEmpty(CreatorCompanyCode) && CreatorCompanyCode.Length <= 8 &&
                PayoutPrice > 0.0 &&
                !String.IsNullOrEmpty(PayoutCurrency) && PayoutCurrency.Length <= 8 &&
                DeliveryTime > 0 &&
                CreationTimeEpochMs > 0 &&
                ExpiryTimeEpochMs > 0 &&
                !String.IsNullOrEmpty(MinimumRating) && MinimumRating.Length <= 8 &&
                !String.IsNullOrEmpty(LocalMarketId) && LocalMarketId.Length == 32
                );
        }
    }

    [Index(nameof(PlanetId))]
    [Index(nameof(PlanetNaturalId))]
    [Index(nameof(PlanetName))]
    public class BuyingAd
    {
        [Key]
        [JsonIgnore]
        [StringLength(64)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string BuyingAdId { get; set; } // LocalMarketId-ContractNaturalId

        public int ContractNaturalId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }
        [StringLength(8)]
        public string PlanetNaturalId { get; set; }
        [StringLength(32)]
        public string PlanetName { get; set; }

        [StringLength(32)]
        public string CreatorCompanyId { get; set; }
        [StringLength(64)]
        public string CreatorCompanyName { get; set; }
        [StringLength(8)]
        public string CreatorCompanyCode { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(32)]
        public string MaterialCategory { get; set; }
        public double MaterialWeight { get; set; }
        public double MaterialVolume { get; set; }

        public int MaterialAmount { get; set; }

        public double Price { get; set; }

        [StringLength(8)]
        public string PriceCurrency { get; set; }

        public int DeliveryTime { get; set; }

        public long CreationTimeEpochMs { get; set; }
        public long ExpiryTimeEpochMs { get; set; }

        [StringLength(8)]
        public string MinimumRating { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string LocalMarketId { get; set; }

        [JsonIgnore]
        public virtual LocalMarket LocalMarket { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(BuyingAdId) && BuyingAdId.Length <= 64 &&
                ContractNaturalId >= 0 &&
                !String.IsNullOrEmpty(PlanetId) && PlanetId.Length == 32 &&
                !String.IsNullOrEmpty(PlanetNaturalId) && PlanetNaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(PlanetName) && PlanetName.Length <= 32 &&
                !String.IsNullOrEmpty(MaterialId) && MaterialId.Length == 32 &&
                !String.IsNullOrEmpty(MaterialName) && MaterialName.Length <= 64 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                !String.IsNullOrEmpty(MaterialCategory) && MaterialCategory.Length == 32 &&
                MaterialWeight > 0.0 && MaterialVolume > 0.0 && MaterialAmount > 0 &&
                Price > 0.0 &&
                !String.IsNullOrEmpty(PriceCurrency) && PriceCurrency.Length <= 8 &&
                DeliveryTime > 0 &&
                CreationTimeEpochMs > 0 &&
                ExpiryTimeEpochMs > 0 &&
                !String.IsNullOrEmpty(MinimumRating) && MinimumRating.Length <= 8 &&
                !String.IsNullOrEmpty(LocalMarketId) && LocalMarketId.Length == 32
                );
        }
    }

    [Index(nameof(PlanetId))]
    [Index(nameof(PlanetNaturalId))]
    [Index(nameof(PlanetName))]
    public class SellingAd
    {
        [Key]
        [JsonIgnore]
        [StringLength(64)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SellingAdId { get; set; } // LocalMarketId-ContractNaturalId

        public int ContractNaturalId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }
        [StringLength(8)]
        public string PlanetNaturalId { get; set; }
        [StringLength(32)]
        public string PlanetName { get; set; }

        [StringLength(32)]
        public string CreatorCompanyId { get; set; }
        [StringLength(64)]
        public string CreatorCompanyName { get; set; }
        [StringLength(8)]
        public string CreatorCompanyCode { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(32)]
        public string MaterialCategory { get; set; }
        public double MaterialWeight { get; set; }
        public double MaterialVolume { get; set; }

        public int MaterialAmount { get; set; }

        public double Price { get; set; }

        [StringLength(8)]
        public string PriceCurrency { get; set; }

        public int DeliveryTime { get; set; }

        public long CreationTimeEpochMs { get; set; }
        public long ExpiryTimeEpochMs { get; set; }

        [StringLength(8)]
        public string MinimumRating { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string LocalMarketId { get; set; }

        [JsonIgnore]
        public virtual LocalMarket LocalMarket { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(SellingAdId) && SellingAdId.Length <= 64 &&
                ContractNaturalId >= 0 &&
                !String.IsNullOrEmpty(PlanetId) && PlanetId.Length == 32 &&
                !String.IsNullOrEmpty(PlanetNaturalId) && PlanetNaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(PlanetName) && PlanetName.Length <= 32 &&
                !String.IsNullOrEmpty(MaterialId) && MaterialId.Length == 32 &&
                !String.IsNullOrEmpty(MaterialName) && MaterialName.Length <= 64 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                !String.IsNullOrEmpty(MaterialCategory) && MaterialCategory.Length == 32 &&
                MaterialWeight > 0.0 && MaterialVolume > 0.0 && MaterialAmount > 0 &&
                Price > 0.0 &&
                !String.IsNullOrEmpty(PriceCurrency) && PriceCurrency.Length <= 8 &&
                DeliveryTime > 0 &&
                CreationTimeEpochMs > 0 &&
                ExpiryTimeEpochMs > 0 &&
                !String.IsNullOrEmpty(MinimumRating) && MinimumRating.Length <= 8 &&
                !String.IsNullOrEmpty(LocalMarketId) && LocalMarketId.Length == 32
                );
        }
    }
}
