﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [Index(nameof(Name))]
    [Index(nameof(NaturalId))]
    public class System
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SystemId { get; set; }

        [StringLength(32)]
        public string Name { get; set; }

        [StringLength(8)]
        public string NaturalId { get; set; }

        [StringLength(16)]
        public string Type { get; set; }

        public double PositionX { get; set; }
        public double PositionY { get; set; }
        public double PositionZ { get; set; }

        [StringLength(32)]
        public string SectorId { get; set; }

        [StringLength(32)]
        public string SubSectorId { get; set; }

        public virtual List<SystemConnection> Connections { get; set; } = new List<SystemConnection>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(SystemId) && SystemId.Length == 32 &&
                !String.IsNullOrEmpty(Name) && Name.Length <= 32 &&
                !String.IsNullOrEmpty(NaturalId) && NaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(Type) && Type.Length <= 16 &&
                !String.IsNullOrEmpty(SectorId) && SectorId.Length <= 32 &&
                !String.IsNullOrEmpty(SubSectorId) && SubSectorId.Length <= 32 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );

            Connections.ForEach(c => c.Validate());
        }
    }

    [Index(nameof(SystemId))]
    public class SystemConnection
    {
        [Key]
        [StringLength(65)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SystemConnectionId { get; set; }

        [StringLength(32)]
        public string ConnectingId { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string SystemId { get; set; }

        [JsonIgnore]
        public virtual System System { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(SystemConnectionId) && SystemConnectionId.Length == 65 &&
                !String.IsNullOrEmpty(ConnectingId) && ConnectingId.Length == 32 &&
                !String.IsNullOrEmpty(SystemId) && SystemId.Length == 32
                );
        }
    }

    [Index(nameof(SystemId))]
    [Index(nameof(SystemNaturalId))]
    [Index(nameof(SystemName))]
    public class SystemStar
    {
        [Key]
        [JsonIgnore]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SystemStarId { get; set; } // SystemId

        [StringLength(32)]
        public string SystemId { get; set; }

        [StringLength(8)]
        public string SystemNaturalId { get; set; }

        [StringLength(32)]
        public string SystemName { get; set; }

        [StringLength(16)]
        public string Type { get; set; }

        public double Luminosity { get; set; }
        public double PositionX { get; set; }
        public double PositionY { get; set; }
        public double PositionZ { get; set; }

        [StringLength(32)]
        public string SectorId { get; set; }

        [StringLength(32)]
        public string SubSectorId { get; set; }

        public double Mass { get; set; }
        public double MassSol { get; set; }
        public double MeteoroidDensity { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(SystemStarId) && SystemStarId.Length == 32 &&
                !String.IsNullOrEmpty(SystemId) && SystemId.Length == 32 &&
                !String.IsNullOrEmpty(SystemNaturalId) && SystemNaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(SystemName) && SystemName.Length <= 32 &&
                !String.IsNullOrEmpty(Type) && Type.Length <= 16 &&
                !String.IsNullOrEmpty(SectorId) && SectorId.Length <= 32 &&
                !String.IsNullOrEmpty(SubSectorId) && SubSectorId.Length <= 32 &&
                MeteoroidDensity >= 0.0
                );
        }
    }
}
