﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [Index(nameof(PlanetId))]
    [Index(nameof(PlanetIdentifier))]
    [Index(nameof(PlanetName))]
    public class Site
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SiteId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }

        [StringLength(8)]
        public string PlanetIdentifier { get; set; }

        [StringLength(32)]
        public string PlanetName { get; set; }

        public long PlanetFoundedEpochMs { get; set; }

        public int InvestedPermits { get; set; }

        public int MaximumPermits { get; set; }

        public virtual List<SiteBuilding> Buildings { get; set; } = new List<SiteBuilding>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(SiteId) && SiteId.Length == 32 &&
                !String.IsNullOrEmpty(PlanetId) && PlanetId.Length == 32 &&
                !String.IsNullOrEmpty(PlanetIdentifier) && PlanetIdentifier.Length <= 8 &&
                !String.IsNullOrEmpty(PlanetName) && PlanetName.Length <= 32 &&
                PlanetFoundedEpochMs >= 0 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );

            Buildings.ForEach(b => b.Validate());
        }
    }

    public class SiteBuilding
    {
        [Key]
        [StringLength(65)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SiteBuildingId { get; set; } // SiteId-BuildingId

        [StringLength(32)]
        public string BuildingId { get; set; }

        public long BuildingCreated { get; set; }

        [StringLength(32)]
        public string BuildingName { get; set; }

        [StringLength(8)]
        public string BuildingTicker { get; set; }

        public long? BuildingLastRepair { get; set; }
        public double Condition { get; set; }

        public virtual List<SiteReclaimableMaterial> ReclaimableMaterials { get; set; } = new List<SiteReclaimableMaterial>();
        public virtual List<SiteRepairMaterial> RepairMaterials { get; set; } = new List<SiteRepairMaterial>();

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string SiteId { get; set; }

        [JsonIgnore]
        public virtual Site Site { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(SiteBuildingId) && SiteBuildingId.Length == 65 &&
                !String.IsNullOrEmpty(BuildingId) && BuildingId.Length == 32 &&
                BuildingCreated > 0 &&
                !String.IsNullOrEmpty(BuildingName) && BuildingName.Length <= 32 &&
                !String.IsNullOrEmpty(BuildingTicker) && BuildingTicker.Length <= 8 &&
                (BuildingLastRepair == null || BuildingLastRepair > 0) &&
                Condition > 0.0 &&
                !String.IsNullOrEmpty(SiteId) && SiteId.Length == 32
                );

            ReclaimableMaterials.ForEach(rm => rm.Validate());
            RepairMaterials.ForEach(rm => rm.Validate());
        }
    }

    public class SiteReclaimableMaterial
    {
        [Key]
        [JsonIgnore]
        [StringLength(75)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SiteReclaimableMaterialId { get; set; } // SiteBuildingId-MaterialTicker

        [StringLength(32)]
        public string MaterialId { get; set; }

        [StringLength(64)]
        public string MaterialName { get; set; }

        [StringLength(8)]
        public string MaterialTicker { get; set; }

        public int MaterialAmount { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(65)]
        public string SiteBuildingId { get; set; }

        [JsonIgnore]
        public virtual SiteBuilding SiteBuilding { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(SiteReclaimableMaterialId) && SiteReclaimableMaterialId.Length <= 75 &&
                !String.IsNullOrEmpty(MaterialId) && MaterialId.Length == 32 &&
                !String.IsNullOrEmpty(MaterialName) && MaterialName.Length <= 64 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                MaterialAmount > 0 &&
                !String.IsNullOrEmpty(SiteBuildingId) && SiteBuildingId.Length == 65
                );
        }
    }

    public class SiteRepairMaterial
    {
        [Key]
        [JsonIgnore]
        [StringLength(75)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SiteRepairMaterialId { get; set; } // SiteBuildingId-MaterialTicker

        [StringLength(32)]
        public string MaterialId { get; set; }

        [StringLength(64)]
        public string MaterialName { get; set; }

        [StringLength(8)]
        public string MaterialTicker { get; set; }

        public int MaterialAmount { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(65)]
        public string SiteBuildingId { get; set; }

        [JsonIgnore]
        public virtual SiteBuilding SiteBuilding { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(SiteRepairMaterialId) && SiteRepairMaterialId.Length <= 75 &&
                !String.IsNullOrEmpty(MaterialId) && MaterialId.Length == 32 &&
                !String.IsNullOrEmpty(MaterialName) && MaterialName.Length <= 64 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                MaterialAmount > 0 &&
                !String.IsNullOrEmpty(SiteBuildingId) && SiteBuildingId.Length == 65
                );
        }
    }
}
