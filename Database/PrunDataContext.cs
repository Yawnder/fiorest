﻿using System;
using Microsoft.EntityFrameworkCore;

using FIORest.Database.Models;

namespace FIORest.Database
{
    public class PRUNDataContext : DbContext
    {
        public DbSet<AuthenticationModel> AuthenticationModels { get; set; }
        public DbSet<PermissionAllowance> PermissionAllowances { get; set; }
        public DbSet<FailedLoginAttempt> FailedLoginAttempts { get; set; }
        public DbSet<Registration> Registrations { get; set; }
        public DbSet<APIKey> APIKeys { get; set; }

        public DbSet<GroupModel> GroupModels { get; set; }
        public DbSet<GroupUserEntryModel> GroupUserEntryModels { get; set; }
        public DbSet<GroupModelAdmin> GroupModelAdmins { get; set; }

        public DbSet<BuildingDegradation> BuildingDegradations { get; set; }
        public DbSet<BuildingDegradationReclaimableMaterial> BuildingDegradationReclaimableMaterials { get; set; }
        public DbSet<BuildingDegradationRepairMaterial> BuildingDegradationRepairMaterials { get; set; }
        
        public DbSet<Building> Buildings { get; set; }
        public DbSet<BuildingCost> BuildingCosts { get; set; }
        public DbSet<BuildingRecipeInput> BuildingRecipeInputs { get; set; }
        public DbSet<BuildingRecipeOutput> BuildingRecipeOutputs { get; set; }
        public DbSet<BuildingRecipe> BuildingRecipes { get; set; }
        
        public DbSet<Company> Companies { get; set; }
        public DbSet<CompanyCurrencyBalance> CompanyCurrencyBalances { get; set; }
        
        public DbSet<CXDataModel> CXDataModels { get; set; }
        public DbSet<CXBuyOrder> CXBuyOrders { get; set; }
        public DbSet<CXSellOrder> CXSellOrders { get; set; }
        public DbSet<CXPCData> CXPCData { get; set; }
        public DbSet<CXPCDataEntry> CXPCDataEntries { get; set; }
        public DbSet<Station> Stations { get; set; }
        
        public DbSet<Experts> Experts { get; set; }
        
        public DbSet<Infrastructure> Infrastructures { get; set; }
        public DbSet<InfrastructureProject> InfrastructureProjects { get; set; }
        public DbSet<InfrastructureProjectUpgradeCosts> InfrastructureProjectUpgradeCosts { get; set; }
        public DbSet<InfrastructureProjectUpkeeps> InfrastructureProjectUpkeeps { get; set; }
        public DbSet<InfrastructureProjectContributions> InfrastructureProjectContributions { get; set; }
        public DbSet<InfrastructureReport> InfrastructureReports { get; set; }
        public DbSet<InfrastructureProgram> InfrastructurePrograms { get; set; }
        
        public DbSet<LocalMarket> LocalMarkets { get; set; }
        public DbSet<ShippingAd> ShippingAds { get; set; }
        public DbSet<BuyingAd> BuyingAds { get; set; }
        public DbSet<SellingAd> SellingAds { get; set; }
        
        public DbSet<ProductionLine> ProductionLines { get; set; }
        public DbSet<ProductionLineOrder> ProductionLineOrders { get; set; }
        public DbSet<ProductionLineInput> ProductionLineInputs { get; set; }
        public DbSet<ProductionLineOutput> ProductionLineOutputs { get; set; }
        
        public DbSet<Ship> Ships { get; set; }
        public DbSet<ShipRepairMaterial> ShipRepairMaterials { get; set; }
        public DbSet<ShipLocationAddressLine> ShipLocationAddressLines { get; set; }
        
        public DbSet<Flight> Flights { get; set; }
        public DbSet<FlightSegment> FlightSegments { get; set; }
        public DbSet<OriginLine> OriginLines { get; set; }
        public DbSet<DestinationLine> DestinationLines { get; set; }

        public DbSet<PlanetDataModel> PlanetDataModels { get; set; }
        public DbSet<PlanetDataResource> PlanetDataResources { get; set; }
        public DbSet<PlanetBuildRequirement> PlanetBuildRequirements { get; set; }
        public DbSet<PlanetProductionFee> PlanetProductionFees { get; set; }
        public DbSet<PlanetCOGCProgram> PlanetCOGCPrograms { get; set; }
        public DbSet<PlanetCOGCVote> PlanetCOGCVotes { get; set; }
        public DbSet<PlanetCOGCUpkeep> PlanetCOGCUpkeep { get; set; }
        public DbSet<PlanetSite> PlanetSites { get; set; }

        public DbSet<Site> Sites { get; set; }
        public DbSet<SiteBuilding> SiteBuildings { get; set; }
        public DbSet<SiteReclaimableMaterial> SiteReclaimableMaterials { get; set; }
        public DbSet<SiteRepairMaterial> SiteRepairMaterials { get; set; }
        
        public DbSet<Warehouse> Warehouses { get; set; }
        
        public DbSet<Workforce> Workforces { get; set; }
        public DbSet<WorkforceDescription> WorkforceDescriptions { get; set; }
        public DbSet<WorkforceNeed> WorkforceNeeds { get; set; }
        
        public DbSet<Material> Materials { get; set; }
        
        public DbSet<Storage> Storages { get; set; }
        public DbSet<StorageItem> StorageItems { get; set; }
        
        public DbSet<Models.System> Systems { get; set; }
        public DbSet<SystemConnection> SystemConnections { get; set; }
        public DbSet<SystemStar> SystemStars { get; set; }
        
        public DbSet<Sector> Sectors { get; set; }
        public DbSet<SubSector> SubSectors { get; set; }
        public DbSet<SubSectorVertex> SubSectorVertices { get; set; }
        
        public DbSet<UserData> UserData { get; set; }
        public DbSet<UserDataPlanet> UserDataPlanets { get; set; }
        public DbSet<UserDataBalance> UserDataBalances { get; set; }
        public DbSet<UserDataOffice> UserDataOffices { get; set; }

        public DbSet<ChatModel> ChatModels { get; set; }
        public DbSet<ChatMessage> ChatMessages { get; set; }

        public DbSet<UserSettings> UserSettings { get; set; }
        public DbSet<UserSettingsBurnRate> UserSettingsBurnRates { get; set; }
        public DbSet<UserSettingsBurnRateExclusion> UserSettingsBurnRateExclusions { get; set; }
        
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<ContractDependency> ContractDependencies { get; set; }
        public DbSet<ContractCondition> ContractConditions { get; set; }
        
        public DbSet<CXOSTradeOrder> CXOSTradeOrders { get; set; }
        public DbSet<CXOSTrade> CXOSTrades { get; set; }
        
        public DbSet<JumpCache> JumpCache { get; set; }
        public DbSet<JumpCacheRouteJump> JumpCacheRoutes { get; set; }
        
        public DbSet<ComexExchange> ComexExchanges { get; set; }
        public DbSet<CountryRegistryCountry> CountryRegistryCountries { get; set; }
        public DbSet<SimulationData> SimulationData { get; set; }
        public DbSet<WorkforcePerOneHundred> WorkforcePerOneHundreds { get; set; }
        public DbSet<WorkforcePerOneHundreedNeed> WorkforcePerOneHundredNeeds { get; set; }
        
        public DbSet<FXDataPair> FXDataPairs { get; set; }

        public DbSet<PriceIndexModel> PriceIndexModels { get; set; }

        public static void LogDebug(DbContextOptionsBuilder options)
        {
            if (Globals.Opts.DebugDatabase)
            {
                options.LogTo(Console.WriteLine, Microsoft.Extensions.Logging.LogLevel.Information);
            }
        }

        public static PRUNDataContext GetNewContext()
        {
            switch (Globals.Opts.DatabaseType)
            {
                case Options.DBTypes.Postgres:
                    return new PostgresDataContext();
                case Options.DBTypes.Sqlite:
                default:
                    return new SqliteDataContext();
            }
        }
    }
}