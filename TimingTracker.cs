#define USING_TIMING_TRACKER

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;

using Nancy;

namespace FIORest
{
    public class RequestTimingData
    {
        public string Endpoint { get; set; }
        public long MinMS { get; set; } = long.MaxValue;
        public long MaxMS { get; set; } = long.MinValue;
        public long TotalMS { get; set; } = 0;
        public double AvgMS { get; set; } = 0.0;
        public long NumReqs { get; set; } = 0;
    }

    public static class TimingTracker
    {
        public static bool IsEnabled =
#if USING_TIMING_TRACKER
            true;
#else
            false;
#endif // USING_TIMING_TRACKER

        public static ConcurrentDictionary<string, RequestTimingData> TimingData = new ConcurrentDictionary<string, RequestTimingData>();
        public static ConcurrentDictionary<Request, Stopwatch> PendingTimings = new ConcurrentDictionary<Request, Stopwatch>();
        public static int PendingTimingIndex = 0;

        public static Func<NancyContext, Response> BeforeRequest()
        {
            return context =>
            {
                var sw = new Stopwatch();
                var PendingTimingIdx = PendingTimingIndex++;
                if (PendingTimings.TryAdd(context.Request, sw))
                {
                    sw.Start();
                }
                
                return null;
            };
        }

        public static Action<NancyContext> AfterRequest()
        {
            return context =>
            {
                if (PendingTimings.ContainsKey(context.Request))
                {
                    Stopwatch sw;
                    PendingTimings.TryRemove(context.Request, out sw);
                    if (sw != null)
                    {
                        sw.Stop();

                        var endpoint = $"{context.Request.Method}-{context.Request.Path}";
                        var rtd = new RequestTimingData
                        {
                            Endpoint = endpoint,
                            MinMS = sw.ElapsedMilliseconds,
                            MaxMS = sw.ElapsedMilliseconds,
                            TotalMS = sw.ElapsedMilliseconds,
                            AvgMS = sw.ElapsedMilliseconds,
                            NumReqs = 1,
                        };

                        TimingData.AddOrUpdate(endpoint, rtd, (key, existingVal) =>
                        {
                            existingVal.MinMS = Math.Min(existingVal.MinMS, rtd.MinMS);
                            existingVal.MaxMS = Math.Max(existingVal.MaxMS, rtd.MaxMS);
                            existingVal.TotalMS += rtd.TotalMS;
                            existingVal.NumReqs++;
                            existingVal.AvgMS = existingVal.TotalMS / existingVal.NumReqs;
                            return existingVal;
                        });
                    }
                }
            };
        }

        public static Func<NancyContext, Exception, dynamic> OnError()
        {
            return (ctx, ex) =>
            {
                Stopwatch dummySw;
                PendingTimings.TryRemove(ctx.Request, out dummySw);
                return null;
            };
        }

        public static List<RequestTimingData> GetTimingData()
        {
            return TimingData.Values.ToList();
        }
    }
}