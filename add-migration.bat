﻿REM AddMigration.bat MigrationName
REM Creates migration entries for defined databases

dotnet ef migrations add %1 --context=SqliteDataContext --output-dir=Migrations/SqliteMigrations
dotnet ef migrations add %1 --context=PostgresDataContext --output-dir=Migrations/PostgresMigrations
REM dotnet ef migrations add %1 --context=SqlServerDataContext --output-dir=Migrations/SqlServer
