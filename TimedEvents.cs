﻿using System;
using System.Collections.Generic;
using System.Timers;

using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest
{
    public static class TimedEvents
    {
        private static Timer timer = null;

        static TimedEvents()
        {
            
        }

        public static void Start()
        {
            RegisterEvent(PullIndices);

            timer = new Timer();
            timer.Interval = new TimeSpan(hours: 3, minutes: 0, seconds: 0).TotalMilliseconds;
            timer.Elapsed += OnElapsed;
            timer.AutoReset = true;
            timer.Enabled = true;

            // Trigger it at the start
            OnElapsed(null, null);
        }

        public static void Stop()
        {
            timer.Enabled = false;
        }

        private static void OnElapsed(object source, ElapsedEventArgs e)
        {
            foreach (var TimedEventAction in TimedEventActions)
            {
                try
                {
                    TimedEventAction.Invoke();
                }
                catch (Exception ex)
                {
                    Logger.LogBadRequest("TimedEvent", "Server", ex);
                }
            }
        }

        private static List<Action> TimedEventActions = new List<Action>();
        private static void RegisterEvent(Action action)
        {
            TimedEventActions.Add(action);
        }

        private static void PullIndices()
        {
#if WITH_MODULES
            var priceIndexBuildings = Modules.StatsModule.CalculatePriceIndexBuildings();
            var priceIndexConsumables = Modules.StatsModule.CalculatePriceIndexConsumables();

            using(var DB = PRUNDataContext.GetNewContext())
            {
                DB.Database.BeginTransaction();

                foreach(var pib in priceIndexBuildings)
                {
                    var pim = new PriceIndexModel();

                    pim.TimeStamp = DateTime.UtcNow;
                    pim.PriceIndexLabel = $"Building-{pib.Currency}";
                    pim.PriceIndexValue = pib.PriceIndex;

                    DB.PriceIndexModels.Add(pim);
                }

                foreach(var pic in priceIndexConsumables)
                {
                    var pim = new PriceIndexModel();

                    pim.TimeStamp = DateTime.UtcNow;
                    pim.PriceIndexLabel = $"Consumables-{pic.Currency}";
                    pim.PriceIndexValue = pic.PriceIndex;

                    DB.PriceIndexModels.Add(pim);
                }

                DB.SaveChanges();
                DB.Database.CommitTransaction();
            }
#endif // WITH_MODULES
        }
    }
}
