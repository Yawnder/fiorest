﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class AddUserDataOfficesTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserDataOffices",
                columns: table => new
                {
                    UserDataOfficeId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    PlanetNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    StartEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    EndEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    UserDataId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDataOffices", x => x.UserDataOfficeId);
                    table.ForeignKey(
                        name: "FK_UserDataOffices_UserData_UserDataId",
                        column: x => x.UserDataId,
                        principalTable: "UserData",
                        principalColumn: "UserDataId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserDataOffices_UserDataId",
                table: "UserDataOffices",
                column: "UserDataId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserDataOffices");
        }
    }
}
