﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class AddedFXDataModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FXDataModels",
                columns: table => new
                {
                    FXDataModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BrokerId = table.Column<string>(type: "TEXT", nullable: true),
                    BaseCurrencyCode = table.Column<string>(type: "TEXT", nullable: true),
                    BaseCurrencyName = table.Column<string>(type: "TEXT", nullable: true),
                    BaseCurrencyNumericCode = table.Column<int>(type: "INTEGER", nullable: false),
                    QuoteCurrencyCode = table.Column<string>(type: "TEXT", nullable: true),
                    QuoteCurrencyName = table.Column<string>(type: "TEXT", nullable: true),
                    QuoteCurrencyNumericCode = table.Column<int>(type: "INTEGER", nullable: false),
                    High = table.Column<decimal>(type: "TEXT", nullable: false),
                    Low = table.Column<decimal>(type: "TEXT", nullable: false),
                    Open = table.Column<decimal>(type: "TEXT", nullable: false),
                    Previous = table.Column<decimal>(type: "TEXT", nullable: false),
                    Traded = table.Column<decimal>(type: "TEXT", nullable: false),
                    Volume = table.Column<decimal>(type: "TEXT", nullable: false),
                    PriceUpdateEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FXDataModels", x => x.FXDataModelId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FXDataModels");
        }
    }
}
