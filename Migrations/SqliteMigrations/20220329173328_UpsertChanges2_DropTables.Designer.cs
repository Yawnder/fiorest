﻿// <auto-generated />
using System;
using FIORest.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace FIORest.Migrations.SqliteMigrations
{
    [DbContext(typeof(SqliteDataContext))]
    [Migration("20220329173328_UpsertChanges2_DropTables")]
    partial class UpsertChanges2_DropTables
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "5.0.7");

            modelBuilder.Entity("FIORest.Database.Models.APIKey", b =>
                {
                    b.Property<int>("APIKeyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<bool>("AllowWrites")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Application")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT");

                    b.Property<Guid>("AuthAPIKey")
                        .HasColumnType("TEXT");

                    b.Property<int>("AuthenticationModelId")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("LastAccessTime")
                        .HasColumnType("TEXT");

                    b.HasKey("APIKeyId");

                    b.HasIndex("AuthenticationModelId");

                    b.ToTable("APIKeys");
                });

            modelBuilder.Entity("FIORest.Database.Models.AuthenticationModel", b =>
                {
                    b.Property<int>("AuthenticationModelId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<bool>("AccountEnabled")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("AuthorizationExpiry")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("AuthorizationKey")
                        .HasColumnType("TEXT");

                    b.Property<string>("DisabledReason")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("DiscordId")
                        .HasMaxLength(40)
                        .HasColumnType("TEXT");

                    b.Property<bool>("IsAdministrator")
                        .HasColumnType("INTEGER");

                    b.Property<string>("PasswordHash")
                        .HasMaxLength(128)
                        .HasColumnType("TEXT");

                    b.Property<string>("UserName")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.HasKey("AuthenticationModelId");

                    b.ToTable("AuthenticationModels");
                });

            modelBuilder.Entity("FIORest.Database.Models.BuildingDegradation", b =>
                {
                    b.Property<int>("BuildingDegradationId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<long>("BuildingCreated")
                        .HasColumnType("INTEGER");

                    b.Property<string>("BuildingTicker")
                        .HasMaxLength(8)
                        .HasColumnType("TEXT");

                    b.Property<double>("Condition")
                        .HasColumnType("REAL");

                    b.Property<double>("DaysSinceLastRepair")
                        .HasColumnType("REAL");

                    b.Property<bool>("HasBeenRepaired")
                        .HasColumnType("INTEGER");

                    b.HasKey("BuildingDegradationId");

                    b.HasIndex("BuildingTicker", "DaysSinceLastRepair", "HasBeenRepaired")
                        .IsUnique();

                    b.ToTable("BuildingDegradations");
                });

            modelBuilder.Entity("FIORest.Database.Models.BuildingDegradationReclaimableMaterial", b =>
                {
                    b.Property<int>("BuildingDegradationReclaimableMaterialModelId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("BuildingDegradationId")
                        .HasColumnType("INTEGER");

                    b.Property<int>("MaterialCount")
                        .HasColumnType("INTEGER");

                    b.Property<string>("MaterialTicker")
                        .HasMaxLength(8)
                        .HasColumnType("TEXT");

                    b.HasKey("BuildingDegradationReclaimableMaterialModelId");

                    b.HasIndex("BuildingDegradationId", "MaterialTicker")
                        .IsUnique();

                    b.ToTable("BuildingDegradationReclaimableMaterials");
                });

            modelBuilder.Entity("FIORest.Database.Models.BuildingDegradationRepairMaterial", b =>
                {
                    b.Property<int>("BuildingDegradationRepairMaterialId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("BuildingDegradationId")
                        .HasColumnType("INTEGER");

                    b.Property<int>("MaterialCount")
                        .HasColumnType("INTEGER");

                    b.Property<string>("MaterialTicker")
                        .HasMaxLength(8)
                        .HasColumnType("TEXT");

                    b.HasKey("BuildingDegradationRepairMaterialId");

                    b.HasIndex("BuildingDegradationId", "MaterialTicker")
                        .IsUnique();

                    b.ToTable("BuildingDegradationRepairMaterials");
                });

            modelBuilder.Entity("FIORest.Database.Models.ChatMessage", b =>
                {
                    b.Property<string>("MessageId")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("ChatModelId")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<bool>("MessageDeleted")
                        .HasColumnType("INTEGER");

                    b.Property<string>("MessageText")
                        .HasColumnType("TEXT");

                    b.Property<long>("MessageTimestamp")
                        .HasColumnType("INTEGER");

                    b.Property<string>("MessageType")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("SenderId")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("Timestamp")
                        .HasColumnType("TEXT");

                    b.Property<string>("UserName")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("UserNameSubmitted")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.HasKey("MessageId");

                    b.HasIndex("ChatModelId");

                    b.HasIndex("MessageTimestamp");

                    b.HasIndex("Timestamp");

                    b.ToTable("ChatMessages");
                });

            modelBuilder.Entity("FIORest.Database.Models.ChatModel", b =>
                {
                    b.Property<string>("ChannelId")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<long>("CreationTime")
                        .HasColumnType("INTEGER");

                    b.Property<string>("DisplayName")
                        .HasMaxLength(64)
                        .HasColumnType("TEXT");

                    b.Property<long>("LastActivity")
                        .HasColumnType("INTEGER");

                    b.Property<string>("NaturalId")
                        .HasMaxLength(16)
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("Timestamp")
                        .HasColumnType("TEXT");

                    b.Property<string>("Type")
                        .HasMaxLength(8)
                        .HasColumnType("TEXT");

                    b.Property<int>("UserCount")
                        .HasColumnType("INTEGER");

                    b.Property<string>("UserNameSubmitted")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.HasKey("ChannelId");

                    b.ToTable("ChatModels");
                });

            modelBuilder.Entity("FIORest.Database.Models.FailedLoginAttempt", b =>
                {
                    b.Property<int>("FailedLoginAttemptId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Address")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<int>("AuthenticationModelId")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("FailedAttemptDateTime")
                        .HasColumnType("TEXT");

                    b.HasKey("FailedLoginAttemptId");

                    b.HasIndex("AuthenticationModelId");

                    b.ToTable("FailedLoginAttempts");
                });

            modelBuilder.Entity("FIORest.Database.Models.GroupModel", b =>
                {
                    b.Property<int>("GroupModelId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("GroupName")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("GroupOwner")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.HasKey("GroupModelId");

                    b.ToTable("GroupModels");
                });

            modelBuilder.Entity("FIORest.Database.Models.GroupUserEntryModel", b =>
                {
                    b.Property<int>("GroupUserEntryModelId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("GroupModelId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("GroupUserName")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.HasKey("GroupUserEntryModelId");

                    b.HasIndex("GroupModelId");

                    b.ToTable("GroupUserEntryModels");
                });

            modelBuilder.Entity("FIORest.Database.Models.PermissionAllowance", b =>
                {
                    b.Property<int>("PermissionAllowanceId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("AuthenticationModelId")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("BuildingData")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("ContractData")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("ExpertsData")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("FlightData")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("ProductionData")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("ShipmentTrackingData")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("StorageData")
                        .HasColumnType("INTEGER");

                    b.Property<string>("UserName")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<bool>("WorkforceData")
                        .HasColumnType("INTEGER");

                    b.HasKey("PermissionAllowanceId");

                    b.HasIndex("AuthenticationModelId");

                    b.ToTable("PermissionAllowances");
                });

            modelBuilder.Entity("FIORest.Database.Models.PlanetBuildRequirement", b =>
                {
                    b.Property<int>("PlanetBuildRequirementId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("MaterialAmount")
                        .HasColumnType("INTEGER");

                    b.Property<string>("MaterialCategory")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("MaterialId")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("MaterialName")
                        .HasMaxLength(64)
                        .HasColumnType("TEXT");

                    b.Property<string>("MaterialTicker")
                        .HasMaxLength(8)
                        .HasColumnType("TEXT");

                    b.Property<double>("MaterialVolume")
                        .HasColumnType("REAL");

                    b.Property<double>("MaterialWeight")
                        .HasColumnType("REAL");

                    b.Property<int>("PlanetDataModelId")
                        .HasColumnType("INTEGER");

                    b.HasKey("PlanetBuildRequirementId");

                    b.HasIndex("PlanetDataModelId");

                    b.ToTable("PlanetBuildRequirements");
                });

            modelBuilder.Entity("FIORest.Database.Models.PlanetCOGCProgram", b =>
                {
                    b.Property<int>("PlanetCOGCProgramId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<long?>("EndEpochMs")
                        .HasColumnType("INTEGER");

                    b.Property<int>("PlanetDataModelId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("ProgramType")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<long?>("StartEpochMs")
                        .HasColumnType("INTEGER");

                    b.HasKey("PlanetCOGCProgramId");

                    b.HasIndex("PlanetDataModelId");

                    b.ToTable("PlanetCOGCPrograms");
                });

            modelBuilder.Entity("FIORest.Database.Models.PlanetCOGCUpkeep", b =>
                {
                    b.Property<int>("PlanetCOGCUpkeepId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("CurrentAmount")
                        .HasColumnType("INTEGER");

                    b.Property<long>("DueDateEpochMs")
                        .HasColumnType("INTEGER");

                    b.Property<int>("MaterialAmount")
                        .HasColumnType("INTEGER");

                    b.Property<string>("MaterialName")
                        .HasMaxLength(64)
                        .HasColumnType("TEXT");

                    b.Property<string>("MaterialTicker")
                        .HasMaxLength(8)
                        .HasColumnType("TEXT");

                    b.Property<double>("MaterialVolume")
                        .HasColumnType("REAL");

                    b.Property<double>("MaterialWeight")
                        .HasColumnType("REAL");

                    b.Property<int>("PlanetDataModelId")
                        .HasColumnType("INTEGER");

                    b.HasKey("PlanetCOGCUpkeepId");

                    b.HasIndex("PlanetDataModelId");

                    b.ToTable("PlanetCOGCUpkeep");
                });

            modelBuilder.Entity("FIORest.Database.Models.PlanetCOGCVote", b =>
                {
                    b.Property<int>("PlanetCOGCVoteId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("CompanyCode")
                        .HasMaxLength(8)
                        .HasColumnType("TEXT");

                    b.Property<string>("CompanyName")
                        .HasMaxLength(64)
                        .HasColumnType("TEXT");

                    b.Property<float>("Influence")
                        .HasColumnType("REAL");

                    b.Property<int>("PlanetDataModelId")
                        .HasColumnType("INTEGER");

                    b.Property<long>("VoteTimeEpochMs")
                        .HasColumnType("INTEGER");

                    b.Property<string>("VoteType")
                        .HasColumnType("TEXT");

                    b.HasKey("PlanetCOGCVoteId");

                    b.HasIndex("PlanetDataModelId");

                    b.ToTable("PlanetCOGCVotes");
                });

            modelBuilder.Entity("FIORest.Database.Models.PlanetDataModel", b =>
                {
                    b.Property<int>("PlanetDataModelId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<double?>("BaseLocalMarketFee")
                        .HasColumnType("REAL");

                    b.Property<string>("COGCProgramStatus")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("CollectorCode")
                        .HasMaxLength(8)
                        .HasColumnType("TEXT");

                    b.Property<string>("CollectorId")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("CollectorName")
                        .HasMaxLength(64)
                        .HasColumnType("TEXT");

                    b.Property<string>("CurrencyCode")
                        .HasMaxLength(8)
                        .HasColumnType("TEXT");

                    b.Property<string>("CurrencyName")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("FactionCode")
                        .HasMaxLength(8)
                        .HasColumnType("TEXT");

                    b.Property<string>("FactionName")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<double>("Fertility")
                        .HasColumnType("REAL");

                    b.Property<string>("GovernorCorporationCode")
                        .HasMaxLength(8)
                        .HasColumnType("TEXT");

                    b.Property<string>("GovernorCorporationId")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("GovernorCorporationName")
                        .HasMaxLength(64)
                        .HasColumnType("TEXT");

                    b.Property<string>("GovernorId")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("GovernorUserName")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<double>("Gravity")
                        .HasColumnType("REAL");

                    b.Property<bool>("HasAdministrationCenter")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("HasChamberOfCommerce")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("HasLocalMarket")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("HasShipyard")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("HasWarehouse")
                        .HasColumnType("INTEGER");

                    b.Property<double?>("LocalMarketFeeFactor")
                        .HasColumnType("REAL");

                    b.Property<double>("MagneticField")
                        .HasColumnType("REAL");

                    b.Property<double>("Mass")
                        .HasColumnType("REAL");

                    b.Property<double>("MassEarth")
                        .HasColumnType("REAL");

                    b.Property<bool>("Nameable")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Namer")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<long>("NamingDataEpochMs")
                        .HasColumnType("INTEGER");

                    b.Property<double>("OrbitEccentricity")
                        .HasColumnType("REAL");

                    b.Property<double>("OrbitInclination")
                        .HasColumnType("REAL");

                    b.Property<int>("OrbitIndex")
                        .HasColumnType("INTEGER");

                    b.Property<double>("OrbitPeriapsis")
                        .HasColumnType("REAL");

                    b.Property<double>("OrbitRightAscension")
                        .HasColumnType("REAL");

                    b.Property<double>("OrbitSemiMajorAxis")
                        .HasColumnType("REAL");

                    b.Property<string>("PlanetId")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("PlanetName")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("PlanetNaturalId")
                        .HasMaxLength(8)
                        .HasColumnType("TEXT");

                    b.Property<int>("PlanetTier")
                        .HasColumnType("INTEGER");

                    b.Property<string>("PopulationId")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<double>("Pressure")
                        .HasColumnType("REAL");

                    b.Property<double>("Radiation")
                        .HasColumnType("REAL");

                    b.Property<double>("Radius")
                        .HasColumnType("REAL");

                    b.Property<double>("Sunlight")
                        .HasColumnType("REAL");

                    b.Property<bool>("Surface")
                        .HasColumnType("INTEGER");

                    b.Property<string>("SystemId")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<double>("Temperature")
                        .HasColumnType("REAL");

                    b.Property<DateTime>("Timestamp")
                        .HasColumnType("TEXT");

                    b.Property<string>("UserNameSubmitted")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<double?>("WarehouseFee")
                        .HasColumnType("REAL");

                    b.HasKey("PlanetDataModelId");

                    b.ToTable("PlanetDataModels");
                });

            modelBuilder.Entity("FIORest.Database.Models.PlanetDataResource", b =>
                {
                    b.Property<int>("PlanetDataResourceId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<double>("Factor")
                        .HasColumnType("REAL");

                    b.Property<string>("MaterialId")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<int>("PlanetDataModelId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("ResourceType")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.HasKey("PlanetDataResourceId");

                    b.HasIndex("PlanetDataModelId");

                    b.ToTable("PlanetDataResources");
                });

            modelBuilder.Entity("FIORest.Database.Models.PlanetProductionFee", b =>
                {
                    b.Property<int>("PlanetProductionFeeId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Category")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<double>("FeeAmount")
                        .HasColumnType("REAL");

                    b.Property<string>("FeeCurrency")
                        .HasColumnType("TEXT");

                    b.Property<int>("PlanetDataModelId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("WorkforceLevel")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.HasKey("PlanetProductionFeeId");

                    b.HasIndex("PlanetDataModelId");

                    b.ToTable("PlanetProductionFees");
                });

            modelBuilder.Entity("FIORest.Database.Models.PlanetSite", b =>
                {
                    b.Property<int>("PlanetSiteId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("OwnerCode")
                        .HasMaxLength(8)
                        .HasColumnType("TEXT");

                    b.Property<string>("OwnerId")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("OwnerName")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("PlanetId")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<string>("PlotId")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<int>("PlotNumber")
                        .HasColumnType("INTEGER");

                    b.Property<string>("SiteId")
                        .HasMaxLength(8)
                        .HasColumnType("TEXT");

                    b.HasKey("PlanetSiteId");

                    b.ToTable("PlanetSites");
                });

            modelBuilder.Entity("FIORest.Database.Models.PriceIndexModel", b =>
                {
                    b.Property<int>("PriceIndexModelId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("PriceIndexLabel")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.Property<double>("PriceIndexValue")
                        .HasColumnType("REAL");

                    b.Property<DateTime>("TimeStamp")
                        .HasColumnType("TEXT");

                    b.HasKey("PriceIndexModelId");

                    b.ToTable("PriceIndexModels");
                });

            modelBuilder.Entity("FIORest.Database.Models.Registration", b =>
                {
                    b.Property<int>("RegistrationId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("RegistrationGuid")
                        .HasMaxLength(64)
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("RegistrationTime")
                        .HasColumnType("TEXT");

                    b.Property<string>("UserName")
                        .HasMaxLength(32)
                        .HasColumnType("TEXT");

                    b.HasKey("RegistrationId");

                    b.ToTable("Registrations");
                });

            modelBuilder.Entity("FIORest.Database.Models.APIKey", b =>
                {
                    b.HasOne("FIORest.Database.Models.AuthenticationModel", "AuthenticationModel")
                        .WithMany("APIKeys")
                        .HasForeignKey("AuthenticationModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("AuthenticationModel");
                });

            modelBuilder.Entity("FIORest.Database.Models.BuildingDegradationReclaimableMaterial", b =>
                {
                    b.HasOne("FIORest.Database.Models.BuildingDegradation", "BuildingDegradation")
                        .WithMany("ReclaimableMaterials")
                        .HasForeignKey("BuildingDegradationId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("BuildingDegradation");
                });

            modelBuilder.Entity("FIORest.Database.Models.BuildingDegradationRepairMaterial", b =>
                {
                    b.HasOne("FIORest.Database.Models.BuildingDegradation", "BuildingDegradation")
                        .WithMany("RepairMaterials")
                        .HasForeignKey("BuildingDegradationId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("BuildingDegradation");
                });

            modelBuilder.Entity("FIORest.Database.Models.ChatMessage", b =>
                {
                    b.HasOne("FIORest.Database.Models.ChatModel", "ChatModel")
                        .WithMany("Messages")
                        .HasForeignKey("ChatModelId");

                    b.Navigation("ChatModel");
                });

            modelBuilder.Entity("FIORest.Database.Models.FailedLoginAttempt", b =>
                {
                    b.HasOne("FIORest.Database.Models.AuthenticationModel", "AuthenticationModel")
                        .WithMany("FailedAttempts")
                        .HasForeignKey("AuthenticationModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("AuthenticationModel");
                });

            modelBuilder.Entity("FIORest.Database.Models.GroupUserEntryModel", b =>
                {
                    b.HasOne("FIORest.Database.Models.GroupModel", "GroupModel")
                        .WithMany("GroupUsers")
                        .HasForeignKey("GroupModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("GroupModel");
                });

            modelBuilder.Entity("FIORest.Database.Models.PermissionAllowance", b =>
                {
                    b.HasOne("FIORest.Database.Models.AuthenticationModel", "AuthenticationModel")
                        .WithMany("Allowances")
                        .HasForeignKey("AuthenticationModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("AuthenticationModel");
                });

            modelBuilder.Entity("FIORest.Database.Models.PlanetBuildRequirement", b =>
                {
                    b.HasOne("FIORest.Database.Models.PlanetDataModel", "PlanetDataModel")
                        .WithMany("BuildRequirements")
                        .HasForeignKey("PlanetDataModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("PlanetDataModel");
                });

            modelBuilder.Entity("FIORest.Database.Models.PlanetCOGCProgram", b =>
                {
                    b.HasOne("FIORest.Database.Models.PlanetDataModel", "PlanetDataModel")
                        .WithMany("COGCPrograms")
                        .HasForeignKey("PlanetDataModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("PlanetDataModel");
                });

            modelBuilder.Entity("FIORest.Database.Models.PlanetCOGCUpkeep", b =>
                {
                    b.HasOne("FIORest.Database.Models.PlanetDataModel", "PlanetDataModel")
                        .WithMany("COGCUpkeep")
                        .HasForeignKey("PlanetDataModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("PlanetDataModel");
                });

            modelBuilder.Entity("FIORest.Database.Models.PlanetCOGCVote", b =>
                {
                    b.HasOne("FIORest.Database.Models.PlanetDataModel", "PlanetDataModel")
                        .WithMany("COGCVotes")
                        .HasForeignKey("PlanetDataModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("PlanetDataModel");
                });

            modelBuilder.Entity("FIORest.Database.Models.PlanetDataResource", b =>
                {
                    b.HasOne("FIORest.Database.Models.PlanetDataModel", "PlanetDataModel")
                        .WithMany("Resources")
                        .HasForeignKey("PlanetDataModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("PlanetDataModel");
                });

            modelBuilder.Entity("FIORest.Database.Models.PlanetProductionFee", b =>
                {
                    b.HasOne("FIORest.Database.Models.PlanetDataModel", "PlanetDataModel")
                        .WithMany("ProductionFees")
                        .HasForeignKey("PlanetDataModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("PlanetDataModel");
                });

            modelBuilder.Entity("FIORest.Database.Models.AuthenticationModel", b =>
                {
                    b.Navigation("Allowances");

                    b.Navigation("APIKeys");

                    b.Navigation("FailedAttempts");
                });

            modelBuilder.Entity("FIORest.Database.Models.BuildingDegradation", b =>
                {
                    b.Navigation("ReclaimableMaterials");

                    b.Navigation("RepairMaterials");
                });

            modelBuilder.Entity("FIORest.Database.Models.ChatModel", b =>
                {
                    b.Navigation("Messages");
                });

            modelBuilder.Entity("FIORest.Database.Models.GroupModel", b =>
                {
                    b.Navigation("GroupUsers");
                });

            modelBuilder.Entity("FIORest.Database.Models.PlanetDataModel", b =>
                {
                    b.Navigation("BuildRequirements");

                    b.Navigation("COGCPrograms");

                    b.Navigation("COGCUpkeep");

                    b.Navigation("COGCVotes");

                    b.Navigation("ProductionFees");

                    b.Navigation("Resources");
                });
#pragma warning restore 612, 618
        }
    }
}
