﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class FixupUserDataPlanetId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PlanetId",
                table: "UserDataPlanets",
                type: "TEXT",
                maxLength: 32,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PlanetId",
                table: "UserDataPlanets");
        }
    }
}
