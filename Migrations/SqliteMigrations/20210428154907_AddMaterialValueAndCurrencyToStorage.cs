﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class AddMaterialValueAndCurrencyToStorage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "MaterialValue",
                table: "StorageItems",
                type: "REAL",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<string>(
                name: "MaterialValueCurrency",
                table: "StorageItems",
                type: "TEXT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaterialValue",
                table: "StorageItems");

            migrationBuilder.DropColumn(
                name: "MaterialValueCurrency",
                table: "StorageItems");
        }
    }
}
