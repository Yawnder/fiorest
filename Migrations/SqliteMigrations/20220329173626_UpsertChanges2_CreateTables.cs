﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class UpsertChanges2_CreateTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Buildings",
                columns: table => new
                {
                    BuildingId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    Ticker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Expertise = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Pioneers = table.Column<int>(type: "INTEGER", nullable: false),
                    Settlers = table.Column<int>(type: "INTEGER", nullable: false),
                    Technicians = table.Column<int>(type: "INTEGER", nullable: false),
                    Engineers = table.Column<int>(type: "INTEGER", nullable: false),
                    Scientists = table.Column<int>(type: "INTEGER", nullable: false),
                    AreaCost = table.Column<int>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Buildings", x => x.BuildingId);
                });

            migrationBuilder.CreateTable(
                name: "ComexExchanges",
                columns: table => new
                {
                    ComexExchangeId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    ExchangeName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    ExchangeCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    ExchangeOperatorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    ExchangeOperatorCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    ExchangeOperatorName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CurrencyNumericCode = table.Column<int>(type: "INTEGER", nullable: false),
                    CurrencyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    CurrencyName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CurrencyDecimals = table.Column<int>(type: "INTEGER", nullable: false),
                    LocationId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    LocationName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    LocationNaturalId = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComexExchanges", x => x.ComexExchangeId);
                });

            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    CompanyId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    CompanyName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CompanyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    UserName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    HighestTier = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Pioneer = table.Column<bool>(type: "INTEGER", nullable: false),
                    Team = table.Column<bool>(type: "INTEGER", nullable: false),
                    CreatedEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    CountryId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CurrencyCode = table.Column<string>(type: "TEXT", maxLength: 4, nullable: true),
                    StartingProfile = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    StartingLocation = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    OverallRating = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    ActivityRating = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    ReliabilityRating = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    StabilityRating = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.CompanyId);
                });

            migrationBuilder.CreateTable(
                name: "Contracts",
                columns: table => new
                {
                    ContractId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    ContractLocalId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    DateEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    ExtensionDeadlineEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    DueDateEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    CanExtend = table.Column<bool>(type: "INTEGER", nullable: false),
                    Party = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Status = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    PartnerId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PartnerName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    PartnerCompanyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contracts", x => x.ContractId);
                });

            migrationBuilder.CreateTable(
                name: "CountryRegistryCountries",
                columns: table => new
                {
                    CountryRegistryCountryId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    CountryCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    CountryName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CurrencyNumericCode = table.Column<int>(type: "INTEGER", nullable: false),
                    CurrencyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    CurrencyName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CurrencyDecimals = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CountryRegistryCountries", x => x.CountryRegistryCountryId);
                });

            migrationBuilder.CreateTable(
                name: "CXDataModels",
                columns: table => new
                {
                    CXDataModelId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    ExchangeName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    ExchangeCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Currency = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Previous = table.Column<double>(type: "REAL", nullable: true),
                    Price = table.Column<double>(type: "REAL", nullable: true),
                    PriceTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    High = table.Column<double>(type: "REAL", nullable: true),
                    AllTimeHigh = table.Column<double>(type: "REAL", nullable: true),
                    Low = table.Column<double>(type: "REAL", nullable: true),
                    AllTimeLow = table.Column<double>(type: "REAL", nullable: true),
                    Ask = table.Column<double>(type: "REAL", nullable: true),
                    AskCount = table.Column<int>(type: "INTEGER", nullable: true),
                    Bid = table.Column<double>(type: "REAL", nullable: true),
                    BidCount = table.Column<int>(type: "INTEGER", nullable: true),
                    Supply = table.Column<int>(type: "INTEGER", nullable: true),
                    Demand = table.Column<int>(type: "INTEGER", nullable: true),
                    Traded = table.Column<int>(type: "INTEGER", nullable: true),
                    VolumeAmount = table.Column<double>(type: "REAL", nullable: true),
                    PriceAverage = table.Column<double>(type: "REAL", nullable: true),
                    NarrowPriceBandLow = table.Column<double>(type: "REAL", nullable: true),
                    NarrowPriceBandHigh = table.Column<double>(type: "REAL", nullable: true),
                    WidePriceBandLow = table.Column<double>(type: "REAL", nullable: true),
                    WidePriceBandHigh = table.Column<double>(type: "REAL", nullable: true),
                    MMBuy = table.Column<double>(type: "REAL", nullable: true),
                    MMSell = table.Column<double>(type: "REAL", nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXDataModels", x => x.CXDataModelId);
                });

            migrationBuilder.CreateTable(
                name: "CXOSTradeOrders",
                columns: table => new
                {
                    CXOSTradeOrderId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    ExchangeName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    ExchangeCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    BrokerId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    OrderType = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    InitialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    Limit = table.Column<double>(type: "REAL", nullable: false),
                    LimitCurrency = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Status = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    CreatedEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXOSTradeOrders", x => x.CXOSTradeOrderId);
                });

            migrationBuilder.CreateTable(
                name: "CXPCData",
                columns: table => new
                {
                    CXPCDataId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    ExchangeCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    StartDataEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    EndDataEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXPCData", x => x.CXPCDataId);
                });

            migrationBuilder.CreateTable(
                name: "Experts",
                columns: table => new
                {
                    ExpertsId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    PlanetId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    AgricultureActive = table.Column<int>(type: "INTEGER", nullable: false),
                    AgricultureAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    AgricultureEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ResourceExtractioneActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ResourceExtractionAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ResourceExtractionEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    FoodIndustriesActive = table.Column<int>(type: "INTEGER", nullable: false),
                    FoodIndustriesAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    FoodIndustriesEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ChemistryActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ChemistryAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ChemistryEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ConstructionActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ConstructionAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ConstructionEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ElectronicsActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ElectronicsAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ElectronicsEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    FuelRefiningActive = table.Column<int>(type: "INTEGER", nullable: false),
                    FuelRefiningAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    FuelRefiningEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ManufacturingActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ManufacturingAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ManufacturingEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    MetallurgyActive = table.Column<int>(type: "INTEGER", nullable: false),
                    MetallurgyAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    MetallurgyEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Experts", x => x.ExpertsId);
                });

            migrationBuilder.CreateTable(
                name: "Flights",
                columns: table => new
                {
                    FlightId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    ShipId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Origin = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    Destination = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    DepartureTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    ArrivalTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    CurrentSegmentIndex = table.Column<int>(type: "INTEGER", nullable: false),
                    StlDistance = table.Column<double>(type: "REAL", nullable: false),
                    FtlDistance = table.Column<double>(type: "REAL", nullable: false),
                    IsAborted = table.Column<bool>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Flights", x => x.FlightId);
                });

            migrationBuilder.CreateTable(
                name: "FXDataPairs",
                columns: table => new
                {
                    FXPairId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    BaseCurrencyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    BaseCurrencyName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    BaseCurrencyNumericCode = table.Column<int>(type: "INTEGER", nullable: false),
                    QuoteCurrencyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    QuoteCurrencyName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    QuoteCurrencyNumericCode = table.Column<int>(type: "INTEGER", nullable: false),
                    High = table.Column<decimal>(type: "TEXT", nullable: false),
                    Low = table.Column<decimal>(type: "TEXT", nullable: false),
                    Open = table.Column<decimal>(type: "TEXT", nullable: false),
                    Previous = table.Column<decimal>(type: "TEXT", nullable: false),
                    Traded = table.Column<decimal>(type: "TEXT", nullable: false),
                    Volume = table.Column<decimal>(type: "TEXT", nullable: false),
                    PriceUpdateEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FXDataPairs", x => x.FXPairId);
                });

            migrationBuilder.CreateTable(
                name: "Infrastructures",
                columns: table => new
                {
                    InfrastructureId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Infrastructures", x => x.InfrastructureId);
                });

            migrationBuilder.CreateTable(
                name: "JumpCache",
                columns: table => new
                {
                    JumpCacheId = table.Column<string>(type: "TEXT", maxLength: 65, nullable: false),
                    SourceSystemId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SourceSystemName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SourceSystemNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    DestinationSystemId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    DestinationSystemName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    DestinationNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    OverallDistance = table.Column<double>(type: "REAL", nullable: false),
                    JumpCount = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JumpCache", x => x.JumpCacheId);
                });

            migrationBuilder.CreateTable(
                name: "LocalMarkets",
                columns: table => new
                {
                    LocalMarketId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocalMarkets", x => x.LocalMarketId);
                });

            migrationBuilder.CreateTable(
                name: "Materials",
                columns: table => new
                {
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    CategoryName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CategoryId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    Ticker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Weight = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Materials", x => x.MaterialId);
                });

            migrationBuilder.CreateTable(
                name: "ProductionLines",
                columns: table => new
                {
                    ProductionLineId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    SiteId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PlanetId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Type = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Capacity = table.Column<int>(type: "INTEGER", nullable: false),
                    Efficiency = table.Column<double>(type: "REAL", nullable: false),
                    Condition = table.Column<double>(type: "REAL", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductionLines", x => x.ProductionLineId);
                });

            migrationBuilder.CreateTable(
                name: "Sectors",
                columns: table => new
                {
                    SectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    HexQ = table.Column<int>(type: "INTEGER", nullable: false),
                    HexR = table.Column<int>(type: "INTEGER", nullable: false),
                    HexS = table.Column<int>(type: "INTEGER", nullable: false),
                    Size = table.Column<int>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sectors", x => x.SectorId);
                });

            migrationBuilder.CreateTable(
                name: "Ships",
                columns: table => new
                {
                    ShipId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    StoreId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    StlFuelStoreId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    FtlFuelStoreId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Registration = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CommissioningTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    BlueprintNaturalId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    FlightId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Acceleration = table.Column<double>(type: "REAL", nullable: false),
                    Thrust = table.Column<double>(type: "REAL", nullable: false),
                    Mass = table.Column<double>(type: "REAL", nullable: false),
                    OperatingEmptyMass = table.Column<double>(type: "REAL", nullable: false),
                    ReactorPower = table.Column<double>(type: "REAL", nullable: false),
                    EmitterPower = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Condition = table.Column<double>(type: "REAL", nullable: false),
                    LastRepairEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    Location = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    StlFuelFlowRate = table.Column<double>(type: "REAL", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ships", x => x.ShipId);
                });

            migrationBuilder.CreateTable(
                name: "SimulationData",
                columns: table => new
                {
                    SimulationDataId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SimulationInterval = table.Column<int>(type: "INTEGER", nullable: false),
                    FlightSTLFactor = table.Column<int>(type: "INTEGER", nullable: false),
                    FlightFTLFactor = table.Column<int>(type: "INTEGER", nullable: false),
                    PlanetaryMotionFactor = table.Column<int>(type: "INTEGER", nullable: false),
                    ParsecLength = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SimulationData", x => x.SimulationDataId);
                });

            migrationBuilder.CreateTable(
                name: "Sites",
                columns: table => new
                {
                    SiteId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    PlanetId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PlanetIdentifier = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PlanetFoundedEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    InvestedPermits = table.Column<int>(type: "INTEGER", nullable: false),
                    MaximumPermits = table.Column<int>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sites", x => x.SiteId);
                });

            migrationBuilder.CreateTable(
                name: "Stations",
                columns: table => new
                {
                    StationId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    NaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    SystemId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SystemNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    SystemName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CommisionTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    ComexId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    ComexName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    ComexCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    WarehouseId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CountryId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CountryCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    CountryName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CurrencyNumericCode = table.Column<int>(type: "INTEGER", nullable: false),
                    CurrencyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    CurrencyName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CurrencyDecimals = table.Column<int>(type: "INTEGER", nullable: false),
                    GovernorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    GovernorUserName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    GovernorCorporationId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    GovernorCorporationName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    GovernorCorporationCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stations", x => x.StationId);
                });

            migrationBuilder.CreateTable(
                name: "Storages",
                columns: table => new
                {
                    StorageId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    AddressableId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    WeightLoad = table.Column<double>(type: "REAL", nullable: false),
                    WeightCapacity = table.Column<double>(type: "REAL", nullable: false),
                    VolumeLoad = table.Column<double>(type: "REAL", nullable: false),
                    VolumeCapacity = table.Column<double>(type: "REAL", nullable: false),
                    FixedStore = table.Column<bool>(type: "INTEGER", nullable: false),
                    Type = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Storages", x => x.StorageId);
                });

            migrationBuilder.CreateTable(
                name: "Systems",
                columns: table => new
                {
                    SystemId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    NaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Type = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    PositionX = table.Column<double>(type: "REAL", nullable: false),
                    PositionY = table.Column<double>(type: "REAL", nullable: false),
                    PositionZ = table.Column<double>(type: "REAL", nullable: false),
                    SectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SubSectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Systems", x => x.SystemId);
                });

            migrationBuilder.CreateTable(
                name: "SystemStars",
                columns: table => new
                {
                    SystemStarId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    SystemId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SystemNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    SystemName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Type = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    Luminosity = table.Column<double>(type: "REAL", nullable: false),
                    PositionX = table.Column<double>(type: "REAL", nullable: false),
                    PositionY = table.Column<double>(type: "REAL", nullable: false),
                    PositionZ = table.Column<double>(type: "REAL", nullable: false),
                    SectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SubSectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Mass = table.Column<double>(type: "REAL", nullable: false),
                    MassSol = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemStars", x => x.SystemStarId);
                });

            migrationBuilder.CreateTable(
                name: "UserData",
                columns: table => new
                {
                    UserDataId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    UserName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Tier = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    Team = table.Column<bool>(type: "INTEGER", nullable: false),
                    Pioneer = table.Column<bool>(type: "INTEGER", nullable: false),
                    SystemNamingRights = table.Column<int>(type: "INTEGER", nullable: false),
                    PlanetNamingRights = table.Column<int>(type: "INTEGER", nullable: false),
                    IsPayingUser = table.Column<bool>(type: "INTEGER", nullable: false),
                    IsModeratorChat = table.Column<bool>(type: "INTEGER", nullable: false),
                    CreatedEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserData", x => x.UserDataId);
                });

            migrationBuilder.CreateTable(
                name: "UserSettings",
                columns: table => new
                {
                    UserSettingsId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    UserName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSettings", x => x.UserSettingsId);
                });

            migrationBuilder.CreateTable(
                name: "Warehouses",
                columns: table => new
                {
                    WarehouseId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    StoreId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Units = table.Column<int>(type: "INTEGER", nullable: false),
                    WeightCapacity = table.Column<double>(type: "REAL", nullable: false),
                    VolumeCapacity = table.Column<double>(type: "REAL", nullable: false),
                    NextPaymentTimestampEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    FeeAmount = table.Column<double>(type: "REAL", nullable: true),
                    FeeCurrency = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    FeeCollectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    FeeCollectorName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    FeeCollectorCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    LocationName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    LocationNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Warehouses", x => x.WarehouseId);
                });

            migrationBuilder.CreateTable(
                name: "WorkforcePerOneHundreds",
                columns: table => new
                {
                    WorkforcePerOneHundredId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    WorkforceType = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkforcePerOneHundreds", x => x.WorkforcePerOneHundredId);
                });

            migrationBuilder.CreateTable(
                name: "Workforces",
                columns: table => new
                {
                    WorkforceId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    PlanetId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SiteId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    LastWorkforceUpdateTime = table.Column<DateTime>(type: "TEXT", nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workforces", x => x.WorkforceId);
                });

            migrationBuilder.CreateTable(
                name: "BuildingCosts",
                columns: table => new
                {
                    BuildingCostId = table.Column<string>(type: "TEXT", maxLength: 40, nullable: false),
                    CommodityName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CommodityTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Weight = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    BuildingId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingCosts", x => x.BuildingCostId);
                    table.ForeignKey(
                        name: "FK_BuildingCosts_Buildings_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Buildings",
                        principalColumn: "BuildingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingRecipes",
                columns: table => new
                {
                    BuildingRecipeId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: false),
                    DurationMs = table.Column<int>(type: "INTEGER", nullable: false),
                    RecipeName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    BuildingId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingRecipes", x => x.BuildingRecipeId);
                    table.ForeignKey(
                        name: "FK_BuildingRecipes_Buildings_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Buildings",
                        principalColumn: "BuildingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompanyCurrencyBalances",
                columns: table => new
                {
                    CompanyCurrencyBalanceId = table.Column<string>(type: "TEXT", maxLength: 40, nullable: false),
                    Currency = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Balance = table.Column<double>(type: "REAL", nullable: false),
                    CompanyId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyCurrencyBalances", x => x.CompanyCurrencyBalanceId);
                    table.ForeignKey(
                        name: "FK_CompanyCurrencyBalances_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContractConditions",
                columns: table => new
                {
                    ContractConditionId = table.Column<string>(type: "TEXT", maxLength: 40, nullable: false),
                    Address = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: true),
                    Weight = table.Column<double>(type: "REAL", nullable: true),
                    Volume = table.Column<double>(type: "REAL", nullable: true),
                    BlockId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Type = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    ConditionId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Party = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    ConditionIndex = table.Column<int>(type: "INTEGER", nullable: false),
                    Status = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    DeadlineEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    Amount = table.Column<double>(type: "REAL", nullable: true),
                    Currency = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Destination = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    ShipmentItemId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PickedUpMaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PickedUpMaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    PickedUpAmount = table.Column<int>(type: "INTEGER", nullable: true),
                    ContractId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractConditions", x => x.ContractConditionId);
                    table.ForeignKey(
                        name: "FK_ContractConditions_Contracts_ContractId",
                        column: x => x.ContractId,
                        principalTable: "Contracts",
                        principalColumn: "ContractId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CXBuyOrders",
                columns: table => new
                {
                    OrderId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    CompanyId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CompanyName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CompanyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    ItemCount = table.Column<int>(type: "INTEGER", nullable: true),
                    ItemCost = table.Column<double>(type: "REAL", nullable: false),
                    CXDataModelId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXBuyOrders", x => x.OrderId);
                    table.ForeignKey(
                        name: "FK_CXBuyOrders_CXDataModels_CXDataModelId",
                        column: x => x.CXDataModelId,
                        principalTable: "CXDataModels",
                        principalColumn: "CXDataModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CXSellOrders",
                columns: table => new
                {
                    OrderId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    CompanyId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CompanyName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CompanyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    ItemCount = table.Column<int>(type: "INTEGER", nullable: true),
                    ItemCost = table.Column<double>(type: "REAL", nullable: false),
                    CXDataModelId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXSellOrders", x => x.OrderId);
                    table.ForeignKey(
                        name: "FK_CXSellOrders_CXDataModels_CXDataModelId",
                        column: x => x.CXDataModelId,
                        principalTable: "CXDataModels",
                        principalColumn: "CXDataModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CXOSTrades",
                columns: table => new
                {
                    CXOSTradeId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    Price = table.Column<double>(type: "REAL", nullable: false),
                    PriceCurrency = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    TradeTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    PartnerId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PartnerName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    PartnerCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    CXOSTradeOrderId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXOSTrades", x => x.CXOSTradeId);
                    table.ForeignKey(
                        name: "FK_CXOSTrades_CXOSTradeOrders_CXOSTradeOrderId",
                        column: x => x.CXOSTradeOrderId,
                        principalTable: "CXOSTradeOrders",
                        principalColumn: "CXOSTradeOrderId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CXPCDataEntries",
                columns: table => new
                {
                    CXPCDataEntryId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: false),
                    TimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    Open = table.Column<double>(type: "REAL", nullable: false),
                    Close = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Traded = table.Column<int>(type: "INTEGER", nullable: false),
                    CXPCDataId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXPCDataEntries", x => x.CXPCDataEntryId);
                    table.ForeignKey(
                        name: "FK_CXPCDataEntries_CXPCData_CXPCDataId",
                        column: x => x.CXPCDataId,
                        principalTable: "CXPCData",
                        principalColumn: "CXPCDataId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FlightSegments",
                columns: table => new
                {
                    FlightSegmentId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: false),
                    Type = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    DepartureTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    ArrivalTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    StlDistance = table.Column<double>(type: "REAL", nullable: true),
                    StlFuelConsumption = table.Column<double>(type: "REAL", nullable: true),
                    FtlDistance = table.Column<double>(type: "REAL", nullable: true),
                    FtlFuelConsumption = table.Column<double>(type: "REAL", nullable: true),
                    Origin = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    Destination = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    FlightId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FlightSegments", x => x.FlightSegmentId);
                    table.ForeignKey(
                        name: "FK_FlightSegments_Flights_FlightId",
                        column: x => x.FlightId,
                        principalTable: "Flights",
                        principalColumn: "FlightId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureProjects",
                columns: table => new
                {
                    InfrastructureProjectId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: false),
                    InfraProjectId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SimulationPeriod = table.Column<int>(type: "INTEGER", nullable: false),
                    Type = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Ticker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    Level = table.Column<int>(type: "INTEGER", nullable: false),
                    ActiveLevel = table.Column<int>(type: "INTEGER", nullable: false),
                    CurrentLevel = table.Column<int>(type: "INTEGER", nullable: false),
                    UpkeepStatus = table.Column<double>(type: "REAL", nullable: false),
                    UpgradeStatus = table.Column<double>(type: "REAL", nullable: false),
                    InfrastructureId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureProjects", x => x.InfrastructureProjectId);
                    table.ForeignKey(
                        name: "FK_InfrastructureProjects_Infrastructures_InfrastructureId",
                        column: x => x.InfrastructureId,
                        principalTable: "Infrastructures",
                        principalColumn: "InfrastructureId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureReports",
                columns: table => new
                {
                    InfrastructureReportId = table.Column<string>(type: "TEXT", maxLength: 40, nullable: false),
                    ExplorersGraceEnabled = table.Column<bool>(type: "INTEGER", nullable: false),
                    SimulationPeriod = table.Column<int>(type: "INTEGER", nullable: false),
                    TimestampMs = table.Column<long>(type: "INTEGER", nullable: false),
                    NextPopulationPioneer = table.Column<int>(type: "INTEGER", nullable: false),
                    NextPopulationSettler = table.Column<int>(type: "INTEGER", nullable: false),
                    NextPopulationTechnician = table.Column<int>(type: "INTEGER", nullable: false),
                    NextPopulationEngineer = table.Column<int>(type: "INTEGER", nullable: false),
                    NextPopulationScientist = table.Column<int>(type: "INTEGER", nullable: false),
                    PopulationDifferencePioneer = table.Column<int>(type: "INTEGER", nullable: false),
                    PopulationDifferenceSettler = table.Column<int>(type: "INTEGER", nullable: false),
                    PopulationDifferenceTechnician = table.Column<int>(type: "INTEGER", nullable: false),
                    PopulationDifferenceEngineer = table.Column<int>(type: "INTEGER", nullable: false),
                    PopulationDifferenceScientist = table.Column<int>(type: "INTEGER", nullable: false),
                    AverageHappinessPioneer = table.Column<float>(type: "REAL", nullable: false),
                    AverageHappinessSettler = table.Column<float>(type: "REAL", nullable: false),
                    AverageHappinessTechnician = table.Column<float>(type: "REAL", nullable: false),
                    AverageHappinessEngineer = table.Column<float>(type: "REAL", nullable: false),
                    AverageHappinessScientist = table.Column<float>(type: "REAL", nullable: false),
                    UnemploymentRatePioneer = table.Column<float>(type: "REAL", nullable: false),
                    UnemploymentRateSettler = table.Column<float>(type: "REAL", nullable: false),
                    UnemploymentRateTechnician = table.Column<float>(type: "REAL", nullable: false),
                    UnemploymentRateEngineer = table.Column<float>(type: "REAL", nullable: false),
                    UnemploymentRateScientist = table.Column<float>(type: "REAL", nullable: false),
                    OpenJobsPioneer = table.Column<float>(type: "REAL", nullable: false),
                    OpenJobsSettler = table.Column<float>(type: "REAL", nullable: false),
                    OpenJobsTechnician = table.Column<float>(type: "REAL", nullable: false),
                    OpenJobsEngineer = table.Column<float>(type: "REAL", nullable: false),
                    OpenJobsScientist = table.Column<float>(type: "REAL", nullable: false),
                    NeedFulfillmentLifeSupport = table.Column<float>(type: "REAL", nullable: false),
                    NeedFulfillmentSafety = table.Column<float>(type: "REAL", nullable: false),
                    NeedFulfillmentHealth = table.Column<float>(type: "REAL", nullable: false),
                    NeedFulfillmentComfort = table.Column<float>(type: "REAL", nullable: false),
                    NeedFulfillmentCulture = table.Column<float>(type: "REAL", nullable: false),
                    NeedFulfillmentEducation = table.Column<float>(type: "REAL", nullable: false),
                    InfrastructureId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureReports", x => x.InfrastructureReportId);
                    table.ForeignKey(
                        name: "FK_InfrastructureReports_Infrastructures_InfrastructureId",
                        column: x => x.InfrastructureId,
                        principalTable: "Infrastructures",
                        principalColumn: "InfrastructureId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JumpCacheRoutes",
                columns: table => new
                {
                    JumpCacheRouteJumpId = table.Column<string>(type: "TEXT", maxLength: 131, nullable: false),
                    SourceSystemId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SourceSystemName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SourceSystemNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    DestinationSystemId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    DestinationSystemName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    DestinationNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Distance = table.Column<double>(type: "REAL", nullable: false),
                    JumpCacheId = table.Column<string>(type: "TEXT", maxLength: 65, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JumpCacheRoutes", x => x.JumpCacheRouteJumpId);
                    table.ForeignKey(
                        name: "FK_JumpCacheRoutes_JumpCache_JumpCacheId",
                        column: x => x.JumpCacheId,
                        principalTable: "JumpCache",
                        principalColumn: "JumpCacheId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuyingAds",
                columns: table => new
                {
                    BuyingAdId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: false),
                    ContractNaturalId = table.Column<int>(type: "INTEGER", nullable: false),
                    PlanetId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CreatorCompanyId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CreatorCompanyName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CreatorCompanyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialCategory = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialWeight = table.Column<double>(type: "REAL", nullable: false),
                    MaterialVolume = table.Column<double>(type: "REAL", nullable: false),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    Price = table.Column<double>(type: "REAL", nullable: false),
                    PriceCurrency = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    DeliveryTime = table.Column<int>(type: "INTEGER", nullable: false),
                    CreationTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    ExpiryTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    MinimumRating = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    LocalMarketId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuyingAds", x => x.BuyingAdId);
                    table.ForeignKey(
                        name: "FK_BuyingAds_LocalMarkets_LocalMarketId",
                        column: x => x.LocalMarketId,
                        principalTable: "LocalMarkets",
                        principalColumn: "LocalMarketId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SellingAds",
                columns: table => new
                {
                    SellingAdId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: false),
                    ContractNaturalId = table.Column<int>(type: "INTEGER", nullable: false),
                    PlanetId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CreatorCompanyId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CreatorCompanyName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CreatorCompanyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialCategory = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialWeight = table.Column<double>(type: "REAL", nullable: false),
                    MaterialVolume = table.Column<double>(type: "REAL", nullable: false),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    Price = table.Column<double>(type: "REAL", nullable: false),
                    PriceCurrency = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    DeliveryTime = table.Column<int>(type: "INTEGER", nullable: false),
                    CreationTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    ExpiryTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    MinimumRating = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    LocalMarketId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SellingAds", x => x.SellingAdId);
                    table.ForeignKey(
                        name: "FK_SellingAds_LocalMarkets_LocalMarketId",
                        column: x => x.LocalMarketId,
                        principalTable: "LocalMarkets",
                        principalColumn: "LocalMarketId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ShippingAds",
                columns: table => new
                {
                    ShippingAdId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: false),
                    ContractNaturalId = table.Column<int>(type: "INTEGER", nullable: false),
                    PlanetId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    OriginPlanetId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    OriginPlanetNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    OriginPlanetName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    DestinationPlanetId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    DestinationPlanetNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    DestinationPlanetName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CargoWeight = table.Column<double>(type: "REAL", nullable: false),
                    CargoVolume = table.Column<double>(type: "REAL", nullable: false),
                    CreatorCompanyId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CreatorCompanyName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CreatorCompanyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    PayoutPrice = table.Column<double>(type: "REAL", nullable: false),
                    PayoutCurrency = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    DeliveryTime = table.Column<int>(type: "INTEGER", nullable: false),
                    CreationTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    ExpiryTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    MinimumRating = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    LocalMarketId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShippingAds", x => x.ShippingAdId);
                    table.ForeignKey(
                        name: "FK_ShippingAds_LocalMarkets_LocalMarketId",
                        column: x => x.LocalMarketId,
                        principalTable: "LocalMarkets",
                        principalColumn: "LocalMarketId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductionLineOrders",
                columns: table => new
                {
                    ProductionLineOrderId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    CreatedEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    StartedEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    CompletionEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    DurationMs = table.Column<long>(type: "INTEGER", nullable: true),
                    LastUpdatedEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    CompletedPercentage = table.Column<double>(type: "REAL", nullable: true),
                    IsHalted = table.Column<bool>(type: "INTEGER", nullable: false),
                    Recurring = table.Column<bool>(type: "INTEGER", nullable: false),
                    ProductionFee = table.Column<double>(type: "REAL", nullable: false),
                    ProductionFeeCurrency = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    ProductionFeeCollectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    ProductionFeeCollectorName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    ProductionFeeCollectorCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    ProductionLineId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductionLineOrders", x => x.ProductionLineOrderId);
                    table.ForeignKey(
                        name: "FK_ProductionLineOrders_ProductionLines_ProductionLineId",
                        column: x => x.ProductionLineId,
                        principalTable: "ProductionLines",
                        principalColumn: "ProductionLineId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubSectors",
                columns: table => new
                {
                    SubSectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    SectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubSectors", x => x.SubSectorId);
                    table.ForeignKey(
                        name: "FK_SubSectors_Sectors_SectorId",
                        column: x => x.SectorId,
                        principalTable: "Sectors",
                        principalColumn: "SectorId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ShipRepairMaterials",
                columns: table => new
                {
                    ShipRepairMaterialId = table.Column<string>(type: "TEXT", maxLength: 41, nullable: false),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    ShipId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipRepairMaterials", x => x.ShipRepairMaterialId);
                    table.ForeignKey(
                        name: "FK_ShipRepairMaterials_Ships_ShipId",
                        column: x => x.ShipId,
                        principalTable: "Ships",
                        principalColumn: "ShipId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SiteBuildings",
                columns: table => new
                {
                    SiteBuildingId = table.Column<string>(type: "TEXT", maxLength: 65, nullable: false),
                    BuildingId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    BuildingCreated = table.Column<long>(type: "INTEGER", nullable: false),
                    BuildingName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    BuildingTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    BuildingLastRepair = table.Column<long>(type: "INTEGER", nullable: true),
                    Condition = table.Column<double>(type: "REAL", nullable: false),
                    SiteId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiteBuildings", x => x.SiteBuildingId);
                    table.ForeignKey(
                        name: "FK_SiteBuildings_Sites_SiteId",
                        column: x => x.SiteId,
                        principalTable: "Sites",
                        principalColumn: "SiteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StorageItems",
                columns: table => new
                {
                    StorageItemId = table.Column<string>(type: "TEXT", maxLength: 65, nullable: false),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialCategory = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialWeight = table.Column<double>(type: "REAL", nullable: false),
                    MaterialVolume = table.Column<double>(type: "REAL", nullable: false),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    MaterialValue = table.Column<float>(type: "REAL", nullable: false),
                    MaterialValueCurrency = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Type = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    TotalWeight = table.Column<double>(type: "REAL", nullable: false),
                    TotalVolume = table.Column<double>(type: "REAL", nullable: false),
                    StorageId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StorageItems", x => x.StorageItemId);
                    table.ForeignKey(
                        name: "FK_StorageItems_Storages_StorageId",
                        column: x => x.StorageId,
                        principalTable: "Storages",
                        principalColumn: "StorageId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SystemConnections",
                columns: table => new
                {
                    SystemConnectionId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    SystemId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemConnections", x => x.SystemConnectionId);
                    table.ForeignKey(
                        name: "FK_SystemConnections_Systems_SystemId",
                        column: x => x.SystemId,
                        principalTable: "Systems",
                        principalColumn: "SystemId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserSettingsBurnRates",
                columns: table => new
                {
                    UserSettingsBurnRateId = table.Column<string>(type: "TEXT", maxLength: 41, nullable: false),
                    PlanetNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    UserSettingsId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSettingsBurnRates", x => x.UserSettingsBurnRateId);
                    table.ForeignKey(
                        name: "FK_UserSettingsBurnRates_UserSettings_UserSettingsId",
                        column: x => x.UserSettingsId,
                        principalTable: "UserSettings",
                        principalColumn: "UserSettingsId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkforcePerOneHundredNeeds",
                columns: table => new
                {
                    WorkforcePerOneHundreedNeedId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialCategory = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Amount = table.Column<double>(type: "REAL", nullable: false),
                    WorkforcePerOneHundredId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkforcePerOneHundredNeeds", x => x.WorkforcePerOneHundreedNeedId);
                    table.ForeignKey(
                        name: "FK_WorkforcePerOneHundredNeeds_WorkforcePerOneHundreds_WorkforcePerOneHundredId",
                        column: x => x.WorkforcePerOneHundredId,
                        principalTable: "WorkforcePerOneHundreds",
                        principalColumn: "WorkforcePerOneHundredId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkforceDescriptions",
                columns: table => new
                {
                    WorkforceDescriptionId = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    WorkforceTypeName = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    Population = table.Column<int>(type: "INTEGER", nullable: false),
                    Reserve = table.Column<int>(type: "INTEGER", nullable: false),
                    Capacity = table.Column<int>(type: "INTEGER", nullable: false),
                    Required = table.Column<int>(type: "INTEGER", nullable: false),
                    Satisfaction = table.Column<double>(type: "REAL", nullable: false),
                    WorkforceId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkforceDescriptions", x => x.WorkforceDescriptionId);
                    table.ForeignKey(
                        name: "FK_WorkforceDescriptions_Workforces_WorkforceId",
                        column: x => x.WorkforceId,
                        principalTable: "Workforces",
                        principalColumn: "WorkforceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingRecipeInputs",
                columns: table => new
                {
                    BuildingRecipeInputId = table.Column<string>(type: "TEXT", maxLength: 60, nullable: false),
                    CommodityName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CommodityTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Weight = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    BuildingRecipeId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingRecipeInputs", x => x.BuildingRecipeInputId);
                    table.ForeignKey(
                        name: "FK_BuildingRecipeInputs_BuildingRecipes_BuildingRecipeId",
                        column: x => x.BuildingRecipeId,
                        principalTable: "BuildingRecipes",
                        principalColumn: "BuildingRecipeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingRecipeOutputs",
                columns: table => new
                {
                    BuildingRecipeOutputId = table.Column<string>(type: "TEXT", maxLength: 60, nullable: false),
                    CommodityName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CommodityTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Weight = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    BuildingRecipeId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingRecipeOutputs", x => x.BuildingRecipeOutputId);
                    table.ForeignKey(
                        name: "FK_BuildingRecipeOutputs_BuildingRecipes_BuildingRecipeId",
                        column: x => x.BuildingRecipeId,
                        principalTable: "BuildingRecipes",
                        principalColumn: "BuildingRecipeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContractDependencies",
                columns: table => new
                {
                    ContractDependencyId = table.Column<string>(type: "TEXT", maxLength: 80, nullable: false),
                    Dependency = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    ContractConditionId = table.Column<string>(type: "TEXT", maxLength: 40, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractDependencies", x => x.ContractDependencyId);
                    table.ForeignKey(
                        name: "FK_ContractDependencies_ContractConditions_ContractConditionId",
                        column: x => x.ContractConditionId,
                        principalTable: "ContractConditions",
                        principalColumn: "ContractConditionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DestinationLines",
                columns: table => new
                {
                    DestinationLineId = table.Column<string>(type: "TEXT", maxLength: 128, nullable: false),
                    Type = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    LineId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    LineNaturalId = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    LineName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    FlightSegmentId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DestinationLines", x => x.DestinationLineId);
                    table.ForeignKey(
                        name: "FK_DestinationLines_FlightSegments_FlightSegmentId",
                        column: x => x.FlightSegmentId,
                        principalTable: "FlightSegments",
                        principalColumn: "FlightSegmentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OriginLines",
                columns: table => new
                {
                    OriginLineId = table.Column<string>(type: "TEXT", maxLength: 128, nullable: false),
                    Type = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    LineId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    LineNaturalId = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    LineName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    FlightSegmentId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OriginLines", x => x.OriginLineId);
                    table.ForeignKey(
                        name: "FK_OriginLines_FlightSegments_FlightSegmentId",
                        column: x => x.FlightSegmentId,
                        principalTable: "FlightSegments",
                        principalColumn: "FlightSegmentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureProjectContributions",
                columns: table => new
                {
                    InfrastructureProjectContributionsId = table.Column<string>(type: "TEXT", maxLength: 128, nullable: false),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    TimestampEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    CompanyId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CompanyName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CompanyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    InfrastructureProjectId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureProjectContributions", x => x.InfrastructureProjectContributionsId);
                    table.ForeignKey(
                        name: "FK_InfrastructureProjectContributions_InfrastructureProjects_InfrastructureProjectId",
                        column: x => x.InfrastructureProjectId,
                        principalTable: "InfrastructureProjects",
                        principalColumn: "InfrastructureProjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureProjectUpgradeCosts",
                columns: table => new
                {
                    InfrastructureProjectUpgradeCostsId = table.Column<string>(type: "TEXT", maxLength: 128, nullable: false),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    CurrentAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    InfrastructureProjectId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureProjectUpgradeCosts", x => x.InfrastructureProjectUpgradeCostsId);
                    table.ForeignKey(
                        name: "FK_InfrastructureProjectUpgradeCosts_InfrastructureProjects_InfrastructureProjectId",
                        column: x => x.InfrastructureProjectId,
                        principalTable: "InfrastructureProjects",
                        principalColumn: "InfrastructureProjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureProjectUpkeeps",
                columns: table => new
                {
                    InfrastructureProjectUpkeepsId = table.Column<string>(type: "TEXT", maxLength: 128, nullable: false),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Stored = table.Column<int>(type: "INTEGER", nullable: false),
                    StoreCapacity = table.Column<int>(type: "INTEGER", nullable: false),
                    Duration = table.Column<int>(type: "INTEGER", nullable: false),
                    NextTickTimestampEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    CurrentAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    InfrastructureProjectId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureProjectUpkeeps", x => x.InfrastructureProjectUpkeepsId);
                    table.ForeignKey(
                        name: "FK_InfrastructureProjectUpkeeps_InfrastructureProjects_InfrastructureProjectId",
                        column: x => x.InfrastructureProjectId,
                        principalTable: "InfrastructureProjects",
                        principalColumn: "InfrastructureProjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductionLineInputs",
                columns: table => new
                {
                    ProductionLineInputId = table.Column<string>(type: "TEXT", maxLength: 41, nullable: false),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    ProductionLineOrderId = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductionLineInputs", x => x.ProductionLineInputId);
                    table.ForeignKey(
                        name: "FK_ProductionLineInputs_ProductionLineOrders_ProductionLineOrderId",
                        column: x => x.ProductionLineOrderId,
                        principalTable: "ProductionLineOrders",
                        principalColumn: "ProductionLineOrderId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductionLineOutputs",
                columns: table => new
                {
                    ProductionLineOutputId = table.Column<string>(type: "TEXT", maxLength: 41, nullable: false),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    ProductionLineOrderId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductionLineOutputs", x => x.ProductionLineOutputId);
                    table.ForeignKey(
                        name: "FK_ProductionLineOutputs_ProductionLineOrders_ProductionLineOrderId",
                        column: x => x.ProductionLineOrderId,
                        principalTable: "ProductionLineOrders",
                        principalColumn: "ProductionLineOrderId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubSectorVertices",
                columns: table => new
                {
                    SubSectorVertexId = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    X = table.Column<double>(type: "REAL", nullable: false),
                    Y = table.Column<double>(type: "REAL", nullable: false),
                    Z = table.Column<double>(type: "REAL", nullable: false),
                    SubSectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubSectorVertices", x => x.SubSectorVertexId);
                    table.ForeignKey(
                        name: "FK_SubSectorVertices_SubSectors_SubSectorId",
                        column: x => x.SubSectorId,
                        principalTable: "SubSectors",
                        principalColumn: "SubSectorId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SiteReclaimableMaterials",
                columns: table => new
                {
                    SiteReclaimableMaterialId = table.Column<string>(type: "TEXT", maxLength: 75, nullable: false),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    SiteBuildingId = table.Column<string>(type: "TEXT", maxLength: 65, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiteReclaimableMaterials", x => x.SiteReclaimableMaterialId);
                    table.ForeignKey(
                        name: "FK_SiteReclaimableMaterials_SiteBuildings_SiteBuildingId",
                        column: x => x.SiteBuildingId,
                        principalTable: "SiteBuildings",
                        principalColumn: "SiteBuildingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SiteRepairMaterials",
                columns: table => new
                {
                    SiteRepairMaterialId = table.Column<string>(type: "TEXT", maxLength: 75, nullable: false),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    SiteBuildingId = table.Column<string>(type: "TEXT", maxLength: 65, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiteRepairMaterials", x => x.SiteRepairMaterialId);
                    table.ForeignKey(
                        name: "FK_SiteRepairMaterials_SiteBuildings_SiteBuildingId",
                        column: x => x.SiteBuildingId,
                        principalTable: "SiteBuildings",
                        principalColumn: "SiteBuildingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserSettingsBurnRateExclusions",
                columns: table => new
                {
                    UserSettingsBurnRateExclusionId = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    UserSettingsBurnRateId = table.Column<string>(type: "TEXT", maxLength: 41, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSettingsBurnRateExclusions", x => x.UserSettingsBurnRateExclusionId);
                    table.ForeignKey(
                        name: "FK_UserSettingsBurnRateExclusions_UserSettingsBurnRates_UserSettingsBurnRateId",
                        column: x => x.UserSettingsBurnRateId,
                        principalTable: "UserSettingsBurnRates",
                        principalColumn: "UserSettingsBurnRateId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkforceNeeds",
                columns: table => new
                {
                    WorkforceNeedId = table.Column<string>(type: "TEXT", maxLength: 60, nullable: false),
                    Category = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Essential = table.Column<bool>(type: "INTEGER", nullable: false),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Satisfaction = table.Column<double>(type: "REAL", nullable: false),
                    UnitsPerInterval = table.Column<double>(type: "REAL", nullable: false),
                    UnitsPerOneHundred = table.Column<double>(type: "REAL", nullable: false),
                    WorkforceDescriptionId = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkforceNeeds", x => x.WorkforceNeedId);
                    table.ForeignKey(
                        name: "FK_WorkforceNeeds_WorkforceDescriptions_WorkforceDescriptionId",
                        column: x => x.WorkforceDescriptionId,
                        principalTable: "WorkforceDescriptions",
                        principalColumn: "WorkforceDescriptionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BuildingCosts_BuildingId",
                table: "BuildingCosts",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingRecipeInputs_BuildingRecipeId",
                table: "BuildingRecipeInputs",
                column: "BuildingRecipeId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingRecipeOutputs_BuildingRecipeId",
                table: "BuildingRecipeOutputs",
                column: "BuildingRecipeId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingRecipes_BuildingId",
                table: "BuildingRecipes",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Buildings_Ticker",
                table: "Buildings",
                column: "Ticker");

            migrationBuilder.CreateIndex(
                name: "IX_BuyingAds_LocalMarketId",
                table: "BuyingAds",
                column: "LocalMarketId");

            migrationBuilder.CreateIndex(
                name: "IX_BuyingAds_PlanetId",
                table: "BuyingAds",
                column: "PlanetId");

            migrationBuilder.CreateIndex(
                name: "IX_BuyingAds_PlanetName",
                table: "BuyingAds",
                column: "PlanetName");

            migrationBuilder.CreateIndex(
                name: "IX_BuyingAds_PlanetNaturalId",
                table: "BuyingAds",
                column: "PlanetNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_CompanyCode",
                table: "Companies",
                column: "CompanyCode");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_CompanyName",
                table: "Companies",
                column: "CompanyName");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_UserName",
                table: "Companies",
                column: "UserName");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyCurrencyBalances_CompanyId",
                table: "CompanyCurrencyBalances",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ContractConditions_ContractId",
                table: "ContractConditions",
                column: "ContractId");

            migrationBuilder.CreateIndex(
                name: "IX_ContractDependencies_ContractConditionId",
                table: "ContractDependencies",
                column: "ContractConditionId");

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_ContractLocalId",
                table: "Contracts",
                column: "ContractLocalId");

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_PartnerCompanyCode",
                table: "Contracts",
                column: "PartnerCompanyCode");

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_PartnerName",
                table: "Contracts",
                column: "PartnerName");

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_Party",
                table: "Contracts",
                column: "Party");

            migrationBuilder.CreateIndex(
                name: "IX_CXBuyOrders_CXDataModelId",
                table: "CXBuyOrders",
                column: "CXDataModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CXBuyOrders_OrderId",
                table: "CXBuyOrders",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_CXBuyOrders_OrderId_CXDataModelId",
                table: "CXBuyOrders",
                columns: new[] { "OrderId", "CXDataModelId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CXDataModels_ExchangeCode",
                table: "CXDataModels",
                column: "ExchangeCode");

            migrationBuilder.CreateIndex(
                name: "IX_CXDataModels_ExchangeCode_MaterialId",
                table: "CXDataModels",
                columns: new[] { "ExchangeCode", "MaterialId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CXDataModels_MaterialId",
                table: "CXDataModels",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_CXDataModels_MaterialName",
                table: "CXDataModels",
                column: "MaterialName");

            migrationBuilder.CreateIndex(
                name: "IX_CXDataModels_MaterialTicker",
                table: "CXDataModels",
                column: "MaterialTicker");

            migrationBuilder.CreateIndex(
                name: "IX_CXOSTrades_CXOSTradeOrderId",
                table: "CXOSTrades",
                column: "CXOSTradeOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_CXPCDataEntries_CXPCDataId",
                table: "CXPCDataEntries",
                column: "CXPCDataId");

            migrationBuilder.CreateIndex(
                name: "IX_CXSellOrders_CXDataModelId",
                table: "CXSellOrders",
                column: "CXDataModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CXSellOrders_OrderId",
                table: "CXSellOrders",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_CXSellOrders_OrderId_CXDataModelId",
                table: "CXSellOrders",
                columns: new[] { "OrderId", "CXDataModelId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DestinationLines_FlightSegmentId",
                table: "DestinationLines",
                column: "FlightSegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Flights_ShipId",
                table: "Flights",
                column: "ShipId");

            migrationBuilder.CreateIndex(
                name: "IX_FlightSegments_FlightId",
                table: "FlightSegments",
                column: "FlightId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureProjectContributions_InfrastructureProjectId",
                table: "InfrastructureProjectContributions",
                column: "InfrastructureProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureProjects_InfrastructureId",
                table: "InfrastructureProjects",
                column: "InfrastructureId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureProjectUpgradeCosts_InfrastructureProjectId",
                table: "InfrastructureProjectUpgradeCosts",
                column: "InfrastructureProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureProjectUpkeeps_InfrastructureProjectId",
                table: "InfrastructureProjectUpkeeps",
                column: "InfrastructureProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureReports_InfrastructureId",
                table: "InfrastructureReports",
                column: "InfrastructureId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCache_DestinationNaturalId",
                table: "JumpCache",
                column: "DestinationNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCache_DestinationSystemId",
                table: "JumpCache",
                column: "DestinationSystemId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCache_DestinationSystemName",
                table: "JumpCache",
                column: "DestinationSystemName");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCache_SourceSystemId",
                table: "JumpCache",
                column: "SourceSystemId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCache_SourceSystemName",
                table: "JumpCache",
                column: "SourceSystemName");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCache_SourceSystemNaturalId",
                table: "JumpCache",
                column: "SourceSystemNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCacheRoutes_DestinationSystemId",
                table: "JumpCacheRoutes",
                column: "DestinationSystemId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCacheRoutes_JumpCacheId",
                table: "JumpCacheRoutes",
                column: "JumpCacheId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCacheRoutes_SourceSystemId",
                table: "JumpCacheRoutes",
                column: "SourceSystemId");

            migrationBuilder.CreateIndex(
                name: "IX_Materials_Ticker",
                table: "Materials",
                column: "Ticker");

            migrationBuilder.CreateIndex(
                name: "IX_OriginLines_FlightSegmentId",
                table: "OriginLines",
                column: "FlightSegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLineInputs_ProductionLineOrderId",
                table: "ProductionLineInputs",
                column: "ProductionLineOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLineOrders_ProductionLineId",
                table: "ProductionLineOrders",
                column: "ProductionLineId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLineOutputs_ProductionLineOrderId",
                table: "ProductionLineOutputs",
                column: "ProductionLineOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLines_PlanetId",
                table: "ProductionLines",
                column: "PlanetId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLines_PlanetName",
                table: "ProductionLines",
                column: "PlanetName");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLines_PlanetNaturalId",
                table: "ProductionLines",
                column: "PlanetNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLines_SiteId",
                table: "ProductionLines",
                column: "SiteId");

            migrationBuilder.CreateIndex(
                name: "IX_Sectors_Name",
                table: "Sectors",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_SellingAds_LocalMarketId",
                table: "SellingAds",
                column: "LocalMarketId");

            migrationBuilder.CreateIndex(
                name: "IX_SellingAds_PlanetId",
                table: "SellingAds",
                column: "PlanetId");

            migrationBuilder.CreateIndex(
                name: "IX_SellingAds_PlanetName",
                table: "SellingAds",
                column: "PlanetName");

            migrationBuilder.CreateIndex(
                name: "IX_SellingAds_PlanetNaturalId",
                table: "SellingAds",
                column: "PlanetNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_ShippingAds_LocalMarketId",
                table: "ShippingAds",
                column: "LocalMarketId");

            migrationBuilder.CreateIndex(
                name: "IX_ShippingAds_PlanetId",
                table: "ShippingAds",
                column: "PlanetId");

            migrationBuilder.CreateIndex(
                name: "IX_ShippingAds_PlanetName",
                table: "ShippingAds",
                column: "PlanetName");

            migrationBuilder.CreateIndex(
                name: "IX_ShippingAds_PlanetNaturalId",
                table: "ShippingAds",
                column: "PlanetNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_ShipRepairMaterials_ShipId",
                table: "ShipRepairMaterials",
                column: "ShipId");

            migrationBuilder.CreateIndex(
                name: "IX_Ships_FtlFuelStoreId",
                table: "Ships",
                column: "FtlFuelStoreId");

            migrationBuilder.CreateIndex(
                name: "IX_Ships_Name",
                table: "Ships",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Ships_Registration",
                table: "Ships",
                column: "Registration");

            migrationBuilder.CreateIndex(
                name: "IX_Ships_StlFuelStoreId",
                table: "Ships",
                column: "StlFuelStoreId");

            migrationBuilder.CreateIndex(
                name: "IX_Ships_StoreId",
                table: "Ships",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_SiteBuildings_SiteId",
                table: "SiteBuildings",
                column: "SiteId");

            migrationBuilder.CreateIndex(
                name: "IX_SiteReclaimableMaterials_SiteBuildingId",
                table: "SiteReclaimableMaterials",
                column: "SiteBuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_SiteRepairMaterials_SiteBuildingId",
                table: "SiteRepairMaterials",
                column: "SiteBuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Sites_PlanetId",
                table: "Sites",
                column: "PlanetId");

            migrationBuilder.CreateIndex(
                name: "IX_Sites_PlanetIdentifier",
                table: "Sites",
                column: "PlanetIdentifier");

            migrationBuilder.CreateIndex(
                name: "IX_Sites_PlanetName",
                table: "Sites",
                column: "PlanetName");

            migrationBuilder.CreateIndex(
                name: "IX_StorageItems_StorageId",
                table: "StorageItems",
                column: "StorageId");

            migrationBuilder.CreateIndex(
                name: "IX_Storages_AddressableId",
                table: "Storages",
                column: "AddressableId");

            migrationBuilder.CreateIndex(
                name: "IX_SubSectors_SectorId",
                table: "SubSectors",
                column: "SectorId");

            migrationBuilder.CreateIndex(
                name: "IX_SubSectorVertices_SubSectorId",
                table: "SubSectorVertices",
                column: "SubSectorId");

            migrationBuilder.CreateIndex(
                name: "IX_SystemConnections_SystemId",
                table: "SystemConnections",
                column: "SystemId");

            migrationBuilder.CreateIndex(
                name: "IX_Systems_Name",
                table: "Systems",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Systems_NaturalId",
                table: "Systems",
                column: "NaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_SystemStars_SystemId",
                table: "SystemStars",
                column: "SystemId");

            migrationBuilder.CreateIndex(
                name: "IX_SystemStars_SystemName",
                table: "SystemStars",
                column: "SystemName");

            migrationBuilder.CreateIndex(
                name: "IX_SystemStars_SystemNaturalId",
                table: "SystemStars",
                column: "SystemNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_UserData_UserName",
                table: "UserData",
                column: "UserName");

            migrationBuilder.CreateIndex(
                name: "IX_UserSettings_UserName",
                table: "UserSettings",
                column: "UserName");

            migrationBuilder.CreateIndex(
                name: "IX_UserSettingsBurnRateExclusions_UserSettingsBurnRateId",
                table: "UserSettingsBurnRateExclusions",
                column: "UserSettingsBurnRateId");

            migrationBuilder.CreateIndex(
                name: "IX_UserSettingsBurnRates_UserSettingsId",
                table: "UserSettingsBurnRates",
                column: "UserSettingsId");

            migrationBuilder.CreateIndex(
                name: "IX_Warehouses_StoreId",
                table: "Warehouses",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkforceDescriptions_WorkforceId",
                table: "WorkforceDescriptions",
                column: "WorkforceId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkforceNeeds_WorkforceDescriptionId",
                table: "WorkforceNeeds",
                column: "WorkforceDescriptionId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkforcePerOneHundredNeeds_WorkforcePerOneHundredId",
                table: "WorkforcePerOneHundredNeeds",
                column: "WorkforcePerOneHundredId");

            migrationBuilder.CreateIndex(
                name: "IX_Workforces_PlanetId",
                table: "Workforces",
                column: "PlanetId");

            migrationBuilder.CreateIndex(
                name: "IX_Workforces_PlanetName",
                table: "Workforces",
                column: "PlanetName");

            migrationBuilder.CreateIndex(
                name: "IX_Workforces_PlanetNaturalId",
                table: "Workforces",
                column: "PlanetNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_Workforces_SiteId",
                table: "Workforces",
                column: "SiteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BuildingCosts");

            migrationBuilder.DropTable(
                name: "BuildingRecipeInputs");

            migrationBuilder.DropTable(
                name: "BuildingRecipeOutputs");

            migrationBuilder.DropTable(
                name: "BuyingAds");

            migrationBuilder.DropTable(
                name: "ComexExchanges");

            migrationBuilder.DropTable(
                name: "CompanyCurrencyBalances");

            migrationBuilder.DropTable(
                name: "ContractDependencies");

            migrationBuilder.DropTable(
                name: "CountryRegistryCountries");

            migrationBuilder.DropTable(
                name: "CXBuyOrders");

            migrationBuilder.DropTable(
                name: "CXOSTrades");

            migrationBuilder.DropTable(
                name: "CXPCDataEntries");

            migrationBuilder.DropTable(
                name: "CXSellOrders");

            migrationBuilder.DropTable(
                name: "DestinationLines");

            migrationBuilder.DropTable(
                name: "Experts");

            migrationBuilder.DropTable(
                name: "FXDataPairs");

            migrationBuilder.DropTable(
                name: "InfrastructureProjectContributions");

            migrationBuilder.DropTable(
                name: "InfrastructureProjectUpgradeCosts");

            migrationBuilder.DropTable(
                name: "InfrastructureProjectUpkeeps");

            migrationBuilder.DropTable(
                name: "InfrastructureReports");

            migrationBuilder.DropTable(
                name: "JumpCacheRoutes");

            migrationBuilder.DropTable(
                name: "Materials");

            migrationBuilder.DropTable(
                name: "OriginLines");

            migrationBuilder.DropTable(
                name: "ProductionLineInputs");

            migrationBuilder.DropTable(
                name: "ProductionLineOutputs");

            migrationBuilder.DropTable(
                name: "SellingAds");

            migrationBuilder.DropTable(
                name: "ShippingAds");

            migrationBuilder.DropTable(
                name: "ShipRepairMaterials");

            migrationBuilder.DropTable(
                name: "SimulationData");

            migrationBuilder.DropTable(
                name: "SiteReclaimableMaterials");

            migrationBuilder.DropTable(
                name: "SiteRepairMaterials");

            migrationBuilder.DropTable(
                name: "Stations");

            migrationBuilder.DropTable(
                name: "StorageItems");

            migrationBuilder.DropTable(
                name: "SubSectorVertices");

            migrationBuilder.DropTable(
                name: "SystemConnections");

            migrationBuilder.DropTable(
                name: "SystemStars");

            migrationBuilder.DropTable(
                name: "UserData");

            migrationBuilder.DropTable(
                name: "UserSettingsBurnRateExclusions");

            migrationBuilder.DropTable(
                name: "Warehouses");

            migrationBuilder.DropTable(
                name: "WorkforceNeeds");

            migrationBuilder.DropTable(
                name: "WorkforcePerOneHundredNeeds");

            migrationBuilder.DropTable(
                name: "BuildingRecipes");

            migrationBuilder.DropTable(
                name: "Companies");

            migrationBuilder.DropTable(
                name: "ContractConditions");

            migrationBuilder.DropTable(
                name: "CXOSTradeOrders");

            migrationBuilder.DropTable(
                name: "CXPCData");

            migrationBuilder.DropTable(
                name: "CXDataModels");

            migrationBuilder.DropTable(
                name: "InfrastructureProjects");

            migrationBuilder.DropTable(
                name: "JumpCache");

            migrationBuilder.DropTable(
                name: "FlightSegments");

            migrationBuilder.DropTable(
                name: "ProductionLineOrders");

            migrationBuilder.DropTable(
                name: "LocalMarkets");

            migrationBuilder.DropTable(
                name: "Ships");

            migrationBuilder.DropTable(
                name: "SiteBuildings");

            migrationBuilder.DropTable(
                name: "Storages");

            migrationBuilder.DropTable(
                name: "SubSectors");

            migrationBuilder.DropTable(
                name: "Systems");

            migrationBuilder.DropTable(
                name: "UserSettingsBurnRates");

            migrationBuilder.DropTable(
                name: "WorkforceDescriptions");

            migrationBuilder.DropTable(
                name: "WorkforcePerOneHundreds");

            migrationBuilder.DropTable(
                name: "Buildings");

            migrationBuilder.DropTable(
                name: "Contracts");

            migrationBuilder.DropTable(
                name: "Infrastructures");

            migrationBuilder.DropTable(
                name: "Flights");

            migrationBuilder.DropTable(
                name: "ProductionLines");

            migrationBuilder.DropTable(
                name: "Sites");

            migrationBuilder.DropTable(
                name: "Sectors");

            migrationBuilder.DropTable(
                name: "UserSettings");

            migrationBuilder.DropTable(
                name: "Workforces");
        }
    }
}
