﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class AddCXPCSupport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MaterialBrokerId",
                table: "CXDataModels",
                type: "TEXT",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CXPCData",
                columns: table => new
                {
                    CXPCDataId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialTicker = table.Column<string>(type: "TEXT", nullable: true),
                    ExchangeCode = table.Column<string>(type: "TEXT", nullable: true),
                    StartDataEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    EndDataEpochMs = table.Column<long>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXPCData", x => x.CXPCDataId);
                });

            migrationBuilder.CreateTable(
                name: "CXPCDataEntries",
                columns: table => new
                {
                    CXPCDataEntryId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    Open = table.Column<double>(type: "REAL", nullable: false),
                    Close = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Traded = table.Column<int>(type: "INTEGER", nullable: false),
                    CXPCDataId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXPCDataEntries", x => x.CXPCDataEntryId);
                    table.ForeignKey(
                        name: "FK_CXPCDataEntries_CXPCData_CXPCDataId",
                        column: x => x.CXPCDataId,
                        principalTable: "CXPCData",
                        principalColumn: "CXPCDataId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CXPCDataEntries_CXPCDataId",
                table: "CXPCDataEntries",
                column: "CXPCDataId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CXPCDataEntries");

            migrationBuilder.DropTable(
                name: "CXPCData");

            migrationBuilder.DropColumn(
                name: "MaterialBrokerId",
                table: "CXDataModels");
        }
    }
}
