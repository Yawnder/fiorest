﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class AddInfrastructurePrograms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ContractConditionId",
                table: "ContractDependencies",
                type: "character varying(65)",
                maxLength: 65,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(40)",
                oldMaxLength: 40,
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "InfrastructurePrograms",
                columns: table => new
                {
                    InfrastructureProgramId = table.Column<string>(type: "character varying(40)", maxLength: 40, nullable: false),
                    Number = table.Column<int>(type: "integer", nullable: false),
                    StartTimestampEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    EndTimestampEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    Category = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Program = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    InfrastructureId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructurePrograms", x => x.InfrastructureProgramId);
                    table.ForeignKey(
                        name: "FK_InfrastructurePrograms_Infrastructures_InfrastructureId",
                        column: x => x.InfrastructureId,
                        principalTable: "Infrastructures",
                        principalColumn: "InfrastructureId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructurePrograms_InfrastructureId",
                table: "InfrastructurePrograms",
                column: "InfrastructureId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InfrastructurePrograms");

            migrationBuilder.AlterColumn<string>(
                name: "ContractConditionId",
                table: "ContractDependencies",
                type: "character varying(40)",
                maxLength: 40,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(65)",
                oldMaxLength: 65,
                oldNullable: true);
        }
    }
}
