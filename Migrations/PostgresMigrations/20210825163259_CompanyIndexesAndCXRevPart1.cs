﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class CompanyIndexesAndCXRevPart1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"delete from \"CompanyDataModels\" as a using \"CompanyDataModels\" as b where a.\"Timestamp\" < b.\"Timestamp\" and a.\"CompanyId\" = b.\"CompanyId\"");
            migrationBuilder.Sql($"delete from \"CXDataModels\"");

            migrationBuilder.DropTable(
                name: "CXBuyOrders");

            migrationBuilder.DropTable(
                name: "CXSellOrders");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CXDataModels",
                table: "CXDataModels");

            migrationBuilder.DropColumn(
                name: "CXDataModelId",
                table: "CXDataModels");

            migrationBuilder.DropColumn(
                name: "MaterialBrokerId",
                table: "CXDataModels");

            migrationBuilder.AddColumn<string>(
                name: "ApexDataModelId",
                table: "CXDataModels",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CXDataModels",
                table: "CXDataModels",
                column: "ApexDataModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CXDataModels_ExchangeCode",
                table: "CXDataModels",
                column: "ExchangeCode");

            migrationBuilder.CreateIndex(
                name: "IX_CXDataModels_ExchangeCode_MaterialId",
                table: "CXDataModels",
                columns: new[] { "ExchangeCode", "MaterialId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CXDataModels_MaterialId",
                table: "CXDataModels",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_CXDataModels_MaterialName",
                table: "CXDataModels",
                column: "MaterialName");

            migrationBuilder.CreateIndex(
                name: "IX_CXDataModels_MaterialTicker",
                table: "CXDataModels",
                column: "MaterialTicker");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyDataModels_CompanyId",
                table: "CompanyDataModels",
                column: "CompanyId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CompanyDataModels_UserName",
                table: "CompanyDataModels",
                column: "UserName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_CXDataModels",
                table: "CXDataModels");

            migrationBuilder.DropIndex(
                name: "IX_CXDataModels_ExchangeCode",
                table: "CXDataModels");

            migrationBuilder.DropIndex(
                name: "IX_CXDataModels_ExchangeCode_MaterialId",
                table: "CXDataModels");

            migrationBuilder.DropIndex(
                name: "IX_CXDataModels_MaterialId",
                table: "CXDataModels");

            migrationBuilder.DropIndex(
                name: "IX_CXDataModels_MaterialName",
                table: "CXDataModels");

            migrationBuilder.DropIndex(
                name: "IX_CXDataModels_MaterialTicker",
                table: "CXDataModels");

            migrationBuilder.DropIndex(
                name: "IX_CompanyDataModels_CompanyId",
                table: "CompanyDataModels");

            migrationBuilder.DropIndex(
                name: "IX_CompanyDataModels_UserName",
                table: "CompanyDataModels");

            migrationBuilder.DropColumn(
                name: "ApexDataModelId",
                table: "CXDataModels");

            migrationBuilder.AddColumn<int>(
                name: "CXDataModelId",
                table: "CXDataModels",
                type: "integer",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddColumn<string>(
                name: "MaterialBrokerId",
                table: "CXDataModels",
                type: "text",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_CXDataModels",
                table: "CXDataModels",
                column: "CXDataModelId");

            migrationBuilder.CreateTable(
                name: "CXBuyOrders",
                columns: table => new
                {
                    CXBuyOrderId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CXDataModelId = table.Column<int>(type: "integer", nullable: false),
                    CompanyCode = table.Column<string>(type: "text", nullable: true),
                    CompanyId = table.Column<string>(type: "text", nullable: true),
                    CompanyName = table.Column<string>(type: "text", nullable: true),
                    ItemCost = table.Column<double>(type: "double precision", nullable: false),
                    ItemCount = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXBuyOrders", x => x.CXBuyOrderId);
                    table.ForeignKey(
                        name: "FK_CXBuyOrders_CXDataModels_CXDataModelId",
                        column: x => x.CXDataModelId,
                        principalTable: "CXDataModels",
                        principalColumn: "CXDataModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CXSellOrders",
                columns: table => new
                {
                    CXSellOrderId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CXDataModelId = table.Column<int>(type: "integer", nullable: false),
                    CompanyCode = table.Column<string>(type: "text", nullable: true),
                    CompanyId = table.Column<string>(type: "text", nullable: true),
                    CompanyName = table.Column<string>(type: "text", nullable: true),
                    ItemCost = table.Column<double>(type: "double precision", nullable: false),
                    ItemCount = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXSellOrders", x => x.CXSellOrderId);
                    table.ForeignKey(
                        name: "FK_CXSellOrders_CXDataModels_CXDataModelId",
                        column: x => x.CXDataModelId,
                        principalTable: "CXDataModels",
                        principalColumn: "CXDataModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CXBuyOrders_CXDataModelId",
                table: "CXBuyOrders",
                column: "CXDataModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CXSellOrders_CXDataModelId",
                table: "CXSellOrders",
                column: "CXDataModelId");
        }
    }
}
