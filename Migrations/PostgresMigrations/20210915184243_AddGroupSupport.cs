﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class AddGroupSupport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GroupModels",
                columns: table => new
                {
                    GroupModelId = table.Column<int>(type: "integer", nullable: false),
                    GroupOwner = table.Column<string>(type: "text", nullable: true),
                    GroupName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupModels", x => x.GroupModelId);
                });

            migrationBuilder.CreateTable(
                name: "GroupUserEntryModels",
                columns: table => new
                {
                    GroupUserEntryModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    GroupUserName = table.Column<string>(type: "text", nullable: true),
                    GroupModelId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupUserEntryModels", x => x.GroupUserEntryModelId);
                    table.ForeignKey(
                        name: "FK_GroupUserEntryModels_GroupModels_GroupModelId",
                        column: x => x.GroupModelId,
                        principalTable: "GroupModels",
                        principalColumn: "GroupModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GroupUserEntryModels_GroupModelId",
                table: "GroupUserEntryModels",
                column: "GroupModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GroupUserEntryModels");

            migrationBuilder.DropTable(
                name: "GroupModels");
        }
    }
}
