﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class FixALotOfCascadingDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("delete from \"ProductionLineOrders\" where \"ProductionLineId\" is null;");
            migrationBuilder.Sql("delete from \"UserSettingsBurnRateExclusions\" where \"UserSettingsBurnRateId\" is null;");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingCosts_Buildings_BuildingId",
                table: "BuildingCosts");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingRecipeInputs_BuildingRecipes_BuildingRecipeId",
                table: "BuildingRecipeInputs");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingRecipeOutputs_BuildingRecipes_BuildingRecipeId",
                table: "BuildingRecipeOutputs");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingRecipes_Buildings_BuildingId",
                table: "BuildingRecipes");

            migrationBuilder.DropForeignKey(
                name: "FK_BuyingAds_LocalMarkets_LocalMarketId",
                table: "BuyingAds");

            migrationBuilder.DropForeignKey(
                name: "FK_CompanyCurrencyBalances_Companies_CompanyId",
                table: "CompanyCurrencyBalances");

            migrationBuilder.DropForeignKey(
                name: "FK_ContractConditions_Contracts_ContractId",
                table: "ContractConditions");

            migrationBuilder.DropForeignKey(
                name: "FK_ContractDependencies_ContractConditions_ContractConditionId",
                table: "ContractDependencies");

            migrationBuilder.DropForeignKey(
                name: "FK_CXBuyOrders_CXDataModels_CXDataModelId",
                table: "CXBuyOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_CXOSTrades_CXOSTradeOrders_CXOSTradeOrderId",
                table: "CXOSTrades");

            migrationBuilder.DropForeignKey(
                name: "FK_CXPCDataEntries_CXPCData_CXPCDataId",
                table: "CXPCDataEntries");

            migrationBuilder.DropForeignKey(
                name: "FK_CXSellOrders_CXDataModels_CXDataModelId",
                table: "CXSellOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_DestinationLines_FlightSegments_FlightSegmentId",
                table: "DestinationLines");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructurePrograms_Infrastructures_InfrastructureId",
                table: "InfrastructurePrograms");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureProjectContributions_InfrastructureProjects_I~",
                table: "InfrastructureProjectContributions");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureProjects_Infrastructures_InfrastructureId",
                table: "InfrastructureProjects");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureProjectUpgradeCosts_InfrastructureProjects_In~",
                table: "InfrastructureProjectUpgradeCosts");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureProjectUpkeeps_InfrastructureProjects_Infrast~",
                table: "InfrastructureProjectUpkeeps");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureReports_Infrastructures_InfrastructureId",
                table: "InfrastructureReports");

            migrationBuilder.DropForeignKey(
                name: "FK_JumpCacheRoutes_JumpCache_JumpCacheId",
                table: "JumpCacheRoutes");

            migrationBuilder.DropForeignKey(
                name: "FK_OriginLines_FlightSegments_FlightSegmentId",
                table: "OriginLines");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductionLineInputs_ProductionLineOrders_ProductionLineOrd~",
                table: "ProductionLineInputs");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductionLineOrders_ProductionLines_ProductionLineId",
                table: "ProductionLineOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductionLineOutputs_ProductionLineOrders_ProductionLineOr~",
                table: "ProductionLineOutputs");

            migrationBuilder.DropForeignKey(
                name: "FK_SellingAds_LocalMarkets_LocalMarketId",
                table: "SellingAds");

            migrationBuilder.DropForeignKey(
                name: "FK_ShipLocationAddressLines_Ships_ShipId",
                table: "ShipLocationAddressLines");

            migrationBuilder.DropForeignKey(
                name: "FK_ShippingAds_LocalMarkets_LocalMarketId",
                table: "ShippingAds");

            migrationBuilder.DropForeignKey(
                name: "FK_ShipRepairMaterials_Ships_ShipId",
                table: "ShipRepairMaterials");

            migrationBuilder.DropForeignKey(
                name: "FK_SiteBuildings_Sites_SiteId",
                table: "SiteBuildings");

            migrationBuilder.DropForeignKey(
                name: "FK_SiteReclaimableMaterials_SiteBuildings_SiteBuildingId",
                table: "SiteReclaimableMaterials");

            migrationBuilder.DropForeignKey(
                name: "FK_SiteRepairMaterials_SiteBuildings_SiteBuildingId",
                table: "SiteRepairMaterials");

            migrationBuilder.DropForeignKey(
                name: "FK_StorageItems_Storages_StorageId",
                table: "StorageItems");

            migrationBuilder.DropForeignKey(
                name: "FK_SubSectors_Sectors_SectorId",
                table: "SubSectors");

            migrationBuilder.DropForeignKey(
                name: "FK_SubSectorVertices_SubSectors_SubSectorId",
                table: "SubSectorVertices");

            migrationBuilder.DropForeignKey(
                name: "FK_SystemConnections_Systems_SystemId",
                table: "SystemConnections");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDataBalances_UserData_UserDataId",
                table: "UserDataBalances");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDataOffices_UserData_UserDataId",
                table: "UserDataOffices");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDataPlanets_UserData_UserDataId",
                table: "UserDataPlanets");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSettingsBurnRateExclusions_UserSettingsBurnRates_UserSe~",
                table: "UserSettingsBurnRateExclusions");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSettingsBurnRates_UserSettings_UserSettingsId",
                table: "UserSettingsBurnRates");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkforceDescriptions_Workforces_WorkforceId",
                table: "WorkforceDescriptions");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkforceNeeds_WorkforceDescriptions_WorkforceDescriptionId",
                table: "WorkforceNeeds");

            migrationBuilder.AlterColumn<string>(
                name: "WorkforceDescriptionId",
                table: "WorkforceNeeds",
                type: "character varying(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "WorkforceId",
                table: "WorkforceDescriptions",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserSettingsId",
                table: "UserSettingsBurnRates",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserSettingsBurnRateId",
                table: "UserSettingsBurnRateExclusions",
                type: "character varying(41)",
                maxLength: 41,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(41)",
                oldMaxLength: 41,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserDataId",
                table: "UserDataPlanets",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserDataId",
                table: "UserDataOffices",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserDataId",
                table: "UserDataBalances",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SystemId",
                table: "SystemConnections",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SubSectorId",
                table: "SubSectorVertices",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SectorId",
                table: "SubSectors",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "StorageId",
                table: "StorageItems",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SiteBuildingId",
                table: "SiteRepairMaterials",
                type: "character varying(65)",
                maxLength: 65,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(65)",
                oldMaxLength: 65,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SiteBuildingId",
                table: "SiteReclaimableMaterials",
                type: "character varying(65)",
                maxLength: 65,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(65)",
                oldMaxLength: 65,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SiteId",
                table: "SiteBuildings",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ShipId",
                table: "ShipRepairMaterials",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "LocalMarketId",
                table: "ShippingAds",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ShipId",
                table: "ShipLocationAddressLines",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "LocalMarketId",
                table: "SellingAds",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ProductionLineOrderId",
                table: "ProductionLineOutputs",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ProductionLineId",
                table: "ProductionLineOrders",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ProductionLineOrderId",
                table: "ProductionLineInputs",
                type: "character varying(32)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FlightSegmentId",
                table: "OriginLines",
                type: "character varying(64)",
                maxLength: 64,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(64)",
                oldMaxLength: 64,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "JumpCacheId",
                table: "JumpCacheRoutes",
                type: "character varying(65)",
                maxLength: 65,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(65)",
                oldMaxLength: 65,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureId",
                table: "InfrastructureReports",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureProjectId",
                table: "InfrastructureProjectUpkeeps",
                type: "character varying(64)",
                maxLength: 64,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(64)",
                oldMaxLength: 64,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureProjectId",
                table: "InfrastructureProjectUpgradeCosts",
                type: "character varying(64)",
                maxLength: 64,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(64)",
                oldMaxLength: 64,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureId",
                table: "InfrastructureProjects",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureProjectId",
                table: "InfrastructureProjectContributions",
                type: "character varying(64)",
                maxLength: 64,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(64)",
                oldMaxLength: 64,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureId",
                table: "InfrastructurePrograms",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FlightSegmentId",
                table: "DestinationLines",
                type: "character varying(64)",
                maxLength: 64,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(64)",
                oldMaxLength: 64,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CXDataModelId",
                table: "CXSellOrders",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CXPCDataId",
                table: "CXPCDataEntries",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CXOSTradeOrderId",
                table: "CXOSTrades",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CXDataModelId",
                table: "CXBuyOrders",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ContractConditionId",
                table: "ContractDependencies",
                type: "character varying(65)",
                maxLength: 65,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(65)",
                oldMaxLength: 65,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ContractId",
                table: "ContractConditions",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyId",
                table: "CompanyCurrencyBalances",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "LocalMarketId",
                table: "BuyingAds",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BuildingId",
                table: "BuildingRecipes",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BuildingRecipeId",
                table: "BuildingRecipeOutputs",
                type: "character varying(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BuildingRecipeId",
                table: "BuildingRecipeInputs",
                type: "character varying(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BuildingId",
                table: "BuildingCosts",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingCosts_Buildings_BuildingId",
                table: "BuildingCosts",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "BuildingId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingRecipeInputs_BuildingRecipes_BuildingRecipeId",
                table: "BuildingRecipeInputs",
                column: "BuildingRecipeId",
                principalTable: "BuildingRecipes",
                principalColumn: "BuildingRecipeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingRecipeOutputs_BuildingRecipes_BuildingRecipeId",
                table: "BuildingRecipeOutputs",
                column: "BuildingRecipeId",
                principalTable: "BuildingRecipes",
                principalColumn: "BuildingRecipeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingRecipes_Buildings_BuildingId",
                table: "BuildingRecipes",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "BuildingId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuyingAds_LocalMarkets_LocalMarketId",
                table: "BuyingAds",
                column: "LocalMarketId",
                principalTable: "LocalMarkets",
                principalColumn: "LocalMarketId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyCurrencyBalances_Companies_CompanyId",
                table: "CompanyCurrencyBalances",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractConditions_Contracts_ContractId",
                table: "ContractConditions",
                column: "ContractId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractDependencies_ContractConditions_ContractConditionId",
                table: "ContractDependencies",
                column: "ContractConditionId",
                principalTable: "ContractConditions",
                principalColumn: "ContractConditionId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CXBuyOrders_CXDataModels_CXDataModelId",
                table: "CXBuyOrders",
                column: "CXDataModelId",
                principalTable: "CXDataModels",
                principalColumn: "CXDataModelId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CXOSTrades_CXOSTradeOrders_CXOSTradeOrderId",
                table: "CXOSTrades",
                column: "CXOSTradeOrderId",
                principalTable: "CXOSTradeOrders",
                principalColumn: "CXOSTradeOrderId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CXPCDataEntries_CXPCData_CXPCDataId",
                table: "CXPCDataEntries",
                column: "CXPCDataId",
                principalTable: "CXPCData",
                principalColumn: "CXPCDataId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CXSellOrders_CXDataModels_CXDataModelId",
                table: "CXSellOrders",
                column: "CXDataModelId",
                principalTable: "CXDataModels",
                principalColumn: "CXDataModelId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DestinationLines_FlightSegments_FlightSegmentId",
                table: "DestinationLines",
                column: "FlightSegmentId",
                principalTable: "FlightSegments",
                principalColumn: "FlightSegmentId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructurePrograms_Infrastructures_InfrastructureId",
                table: "InfrastructurePrograms",
                column: "InfrastructureId",
                principalTable: "Infrastructures",
                principalColumn: "InfrastructureId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureProjectContributions_InfrastructureProjects_I~",
                table: "InfrastructureProjectContributions",
                column: "InfrastructureProjectId",
                principalTable: "InfrastructureProjects",
                principalColumn: "InfrastructureProjectId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureProjects_Infrastructures_InfrastructureId",
                table: "InfrastructureProjects",
                column: "InfrastructureId",
                principalTable: "Infrastructures",
                principalColumn: "InfrastructureId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureProjectUpgradeCosts_InfrastructureProjects_In~",
                table: "InfrastructureProjectUpgradeCosts",
                column: "InfrastructureProjectId",
                principalTable: "InfrastructureProjects",
                principalColumn: "InfrastructureProjectId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureProjectUpkeeps_InfrastructureProjects_Infrast~",
                table: "InfrastructureProjectUpkeeps",
                column: "InfrastructureProjectId",
                principalTable: "InfrastructureProjects",
                principalColumn: "InfrastructureProjectId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureReports_Infrastructures_InfrastructureId",
                table: "InfrastructureReports",
                column: "InfrastructureId",
                principalTable: "Infrastructures",
                principalColumn: "InfrastructureId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_JumpCacheRoutes_JumpCache_JumpCacheId",
                table: "JumpCacheRoutes",
                column: "JumpCacheId",
                principalTable: "JumpCache",
                principalColumn: "JumpCacheId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OriginLines_FlightSegments_FlightSegmentId",
                table: "OriginLines",
                column: "FlightSegmentId",
                principalTable: "FlightSegments",
                principalColumn: "FlightSegmentId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductionLineInputs_ProductionLineOrders_ProductionLineOrd~",
                table: "ProductionLineInputs",
                column: "ProductionLineOrderId",
                principalTable: "ProductionLineOrders",
                principalColumn: "ProductionLineOrderId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductionLineOrders_ProductionLines_ProductionLineId",
                table: "ProductionLineOrders",
                column: "ProductionLineId",
                principalTable: "ProductionLines",
                principalColumn: "ProductionLineId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductionLineOutputs_ProductionLineOrders_ProductionLineOr~",
                table: "ProductionLineOutputs",
                column: "ProductionLineOrderId",
                principalTable: "ProductionLineOrders",
                principalColumn: "ProductionLineOrderId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SellingAds_LocalMarkets_LocalMarketId",
                table: "SellingAds",
                column: "LocalMarketId",
                principalTable: "LocalMarkets",
                principalColumn: "LocalMarketId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ShipLocationAddressLines_Ships_ShipId",
                table: "ShipLocationAddressLines",
                column: "ShipId",
                principalTable: "Ships",
                principalColumn: "ShipId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ShippingAds_LocalMarkets_LocalMarketId",
                table: "ShippingAds",
                column: "LocalMarketId",
                principalTable: "LocalMarkets",
                principalColumn: "LocalMarketId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ShipRepairMaterials_Ships_ShipId",
                table: "ShipRepairMaterials",
                column: "ShipId",
                principalTable: "Ships",
                principalColumn: "ShipId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SiteBuildings_Sites_SiteId",
                table: "SiteBuildings",
                column: "SiteId",
                principalTable: "Sites",
                principalColumn: "SiteId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SiteReclaimableMaterials_SiteBuildings_SiteBuildingId",
                table: "SiteReclaimableMaterials",
                column: "SiteBuildingId",
                principalTable: "SiteBuildings",
                principalColumn: "SiteBuildingId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SiteRepairMaterials_SiteBuildings_SiteBuildingId",
                table: "SiteRepairMaterials",
                column: "SiteBuildingId",
                principalTable: "SiteBuildings",
                principalColumn: "SiteBuildingId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StorageItems_Storages_StorageId",
                table: "StorageItems",
                column: "StorageId",
                principalTable: "Storages",
                principalColumn: "StorageId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubSectors_Sectors_SectorId",
                table: "SubSectors",
                column: "SectorId",
                principalTable: "Sectors",
                principalColumn: "SectorId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubSectorVertices_SubSectors_SubSectorId",
                table: "SubSectorVertices",
                column: "SubSectorId",
                principalTable: "SubSectors",
                principalColumn: "SubSectorId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SystemConnections_Systems_SystemId",
                table: "SystemConnections",
                column: "SystemId",
                principalTable: "Systems",
                principalColumn: "SystemId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserDataBalances_UserData_UserDataId",
                table: "UserDataBalances",
                column: "UserDataId",
                principalTable: "UserData",
                principalColumn: "UserDataId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserDataOffices_UserData_UserDataId",
                table: "UserDataOffices",
                column: "UserDataId",
                principalTable: "UserData",
                principalColumn: "UserDataId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserDataPlanets_UserData_UserDataId",
                table: "UserDataPlanets",
                column: "UserDataId",
                principalTable: "UserData",
                principalColumn: "UserDataId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSettingsBurnRateExclusions_UserSettingsBurnRates_UserSe~",
                table: "UserSettingsBurnRateExclusions",
                column: "UserSettingsBurnRateId",
                principalTable: "UserSettingsBurnRates",
                principalColumn: "UserSettingsBurnRateId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSettingsBurnRates_UserSettings_UserSettingsId",
                table: "UserSettingsBurnRates",
                column: "UserSettingsId",
                principalTable: "UserSettings",
                principalColumn: "UserSettingsId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkforceDescriptions_Workforces_WorkforceId",
                table: "WorkforceDescriptions",
                column: "WorkforceId",
                principalTable: "Workforces",
                principalColumn: "WorkforceId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkforceNeeds_WorkforceDescriptions_WorkforceDescriptionId",
                table: "WorkforceNeeds",
                column: "WorkforceDescriptionId",
                principalTable: "WorkforceDescriptions",
                principalColumn: "WorkforceDescriptionId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BuildingCosts_Buildings_BuildingId",
                table: "BuildingCosts");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingRecipeInputs_BuildingRecipes_BuildingRecipeId",
                table: "BuildingRecipeInputs");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingRecipeOutputs_BuildingRecipes_BuildingRecipeId",
                table: "BuildingRecipeOutputs");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingRecipes_Buildings_BuildingId",
                table: "BuildingRecipes");

            migrationBuilder.DropForeignKey(
                name: "FK_BuyingAds_LocalMarkets_LocalMarketId",
                table: "BuyingAds");

            migrationBuilder.DropForeignKey(
                name: "FK_CompanyCurrencyBalances_Companies_CompanyId",
                table: "CompanyCurrencyBalances");

            migrationBuilder.DropForeignKey(
                name: "FK_ContractConditions_Contracts_ContractId",
                table: "ContractConditions");

            migrationBuilder.DropForeignKey(
                name: "FK_ContractDependencies_ContractConditions_ContractConditionId",
                table: "ContractDependencies");

            migrationBuilder.DropForeignKey(
                name: "FK_CXBuyOrders_CXDataModels_CXDataModelId",
                table: "CXBuyOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_CXOSTrades_CXOSTradeOrders_CXOSTradeOrderId",
                table: "CXOSTrades");

            migrationBuilder.DropForeignKey(
                name: "FK_CXPCDataEntries_CXPCData_CXPCDataId",
                table: "CXPCDataEntries");

            migrationBuilder.DropForeignKey(
                name: "FK_CXSellOrders_CXDataModels_CXDataModelId",
                table: "CXSellOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_DestinationLines_FlightSegments_FlightSegmentId",
                table: "DestinationLines");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructurePrograms_Infrastructures_InfrastructureId",
                table: "InfrastructurePrograms");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureProjectContributions_InfrastructureProjects_I~",
                table: "InfrastructureProjectContributions");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureProjects_Infrastructures_InfrastructureId",
                table: "InfrastructureProjects");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureProjectUpgradeCosts_InfrastructureProjects_In~",
                table: "InfrastructureProjectUpgradeCosts");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureProjectUpkeeps_InfrastructureProjects_Infrast~",
                table: "InfrastructureProjectUpkeeps");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureReports_Infrastructures_InfrastructureId",
                table: "InfrastructureReports");

            migrationBuilder.DropForeignKey(
                name: "FK_JumpCacheRoutes_JumpCache_JumpCacheId",
                table: "JumpCacheRoutes");

            migrationBuilder.DropForeignKey(
                name: "FK_OriginLines_FlightSegments_FlightSegmentId",
                table: "OriginLines");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductionLineInputs_ProductionLineOrders_ProductionLineOrd~",
                table: "ProductionLineInputs");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductionLineOrders_ProductionLines_ProductionLineId",
                table: "ProductionLineOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductionLineOutputs_ProductionLineOrders_ProductionLineOr~",
                table: "ProductionLineOutputs");

            migrationBuilder.DropForeignKey(
                name: "FK_SellingAds_LocalMarkets_LocalMarketId",
                table: "SellingAds");

            migrationBuilder.DropForeignKey(
                name: "FK_ShipLocationAddressLines_Ships_ShipId",
                table: "ShipLocationAddressLines");

            migrationBuilder.DropForeignKey(
                name: "FK_ShippingAds_LocalMarkets_LocalMarketId",
                table: "ShippingAds");

            migrationBuilder.DropForeignKey(
                name: "FK_ShipRepairMaterials_Ships_ShipId",
                table: "ShipRepairMaterials");

            migrationBuilder.DropForeignKey(
                name: "FK_SiteBuildings_Sites_SiteId",
                table: "SiteBuildings");

            migrationBuilder.DropForeignKey(
                name: "FK_SiteReclaimableMaterials_SiteBuildings_SiteBuildingId",
                table: "SiteReclaimableMaterials");

            migrationBuilder.DropForeignKey(
                name: "FK_SiteRepairMaterials_SiteBuildings_SiteBuildingId",
                table: "SiteRepairMaterials");

            migrationBuilder.DropForeignKey(
                name: "FK_StorageItems_Storages_StorageId",
                table: "StorageItems");

            migrationBuilder.DropForeignKey(
                name: "FK_SubSectors_Sectors_SectorId",
                table: "SubSectors");

            migrationBuilder.DropForeignKey(
                name: "FK_SubSectorVertices_SubSectors_SubSectorId",
                table: "SubSectorVertices");

            migrationBuilder.DropForeignKey(
                name: "FK_SystemConnections_Systems_SystemId",
                table: "SystemConnections");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDataBalances_UserData_UserDataId",
                table: "UserDataBalances");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDataOffices_UserData_UserDataId",
                table: "UserDataOffices");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDataPlanets_UserData_UserDataId",
                table: "UserDataPlanets");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSettingsBurnRateExclusions_UserSettingsBurnRates_UserSe~",
                table: "UserSettingsBurnRateExclusions");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSettingsBurnRates_UserSettings_UserSettingsId",
                table: "UserSettingsBurnRates");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkforceDescriptions_Workforces_WorkforceId",
                table: "WorkforceDescriptions");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkforceNeeds_WorkforceDescriptions_WorkforceDescriptionId",
                table: "WorkforceNeeds");

            migrationBuilder.AlterColumn<string>(
                name: "WorkforceDescriptionId",
                table: "WorkforceNeeds",
                type: "character varying(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(50)",
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "WorkforceId",
                table: "WorkforceDescriptions",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "UserSettingsId",
                table: "UserSettingsBurnRates",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "UserSettingsBurnRateId",
                table: "UserSettingsBurnRateExclusions",
                type: "character varying(41)",
                maxLength: 41,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(41)",
                oldMaxLength: 41);

            migrationBuilder.AlterColumn<string>(
                name: "UserDataId",
                table: "UserDataPlanets",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "UserDataId",
                table: "UserDataOffices",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "UserDataId",
                table: "UserDataBalances",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "SystemId",
                table: "SystemConnections",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "SubSectorId",
                table: "SubSectorVertices",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "SectorId",
                table: "SubSectors",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "StorageId",
                table: "StorageItems",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "SiteBuildingId",
                table: "SiteRepairMaterials",
                type: "character varying(65)",
                maxLength: 65,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(65)",
                oldMaxLength: 65);

            migrationBuilder.AlterColumn<string>(
                name: "SiteBuildingId",
                table: "SiteReclaimableMaterials",
                type: "character varying(65)",
                maxLength: 65,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(65)",
                oldMaxLength: 65);

            migrationBuilder.AlterColumn<string>(
                name: "SiteId",
                table: "SiteBuildings",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "ShipId",
                table: "ShipRepairMaterials",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "LocalMarketId",
                table: "ShippingAds",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "ShipId",
                table: "ShipLocationAddressLines",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "LocalMarketId",
                table: "SellingAds",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "ProductionLineOrderId",
                table: "ProductionLineOutputs",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "ProductionLineId",
                table: "ProductionLineOrders",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "ProductionLineOrderId",
                table: "ProductionLineInputs",
                type: "character varying(32)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)");

            migrationBuilder.AlterColumn<string>(
                name: "FlightSegmentId",
                table: "OriginLines",
                type: "character varying(64)",
                maxLength: 64,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(64)",
                oldMaxLength: 64);

            migrationBuilder.AlterColumn<string>(
                name: "JumpCacheId",
                table: "JumpCacheRoutes",
                type: "character varying(65)",
                maxLength: 65,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(65)",
                oldMaxLength: 65);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureId",
                table: "InfrastructureReports",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureProjectId",
                table: "InfrastructureProjectUpkeeps",
                type: "character varying(64)",
                maxLength: 64,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(64)",
                oldMaxLength: 64);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureProjectId",
                table: "InfrastructureProjectUpgradeCosts",
                type: "character varying(64)",
                maxLength: 64,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(64)",
                oldMaxLength: 64);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureId",
                table: "InfrastructureProjects",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureProjectId",
                table: "InfrastructureProjectContributions",
                type: "character varying(64)",
                maxLength: 64,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(64)",
                oldMaxLength: 64);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureId",
                table: "InfrastructurePrograms",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "FlightSegmentId",
                table: "DestinationLines",
                type: "character varying(64)",
                maxLength: 64,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(64)",
                oldMaxLength: 64);

            migrationBuilder.AlterColumn<string>(
                name: "CXDataModelId",
                table: "CXSellOrders",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "CXPCDataId",
                table: "CXPCDataEntries",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "CXOSTradeOrderId",
                table: "CXOSTrades",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "CXDataModelId",
                table: "CXBuyOrders",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "ContractConditionId",
                table: "ContractDependencies",
                type: "character varying(65)",
                maxLength: 65,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(65)",
                oldMaxLength: 65);

            migrationBuilder.AlterColumn<string>(
                name: "ContractId",
                table: "ContractConditions",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyId",
                table: "CompanyCurrencyBalances",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "LocalMarketId",
                table: "BuyingAds",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "BuildingId",
                table: "BuildingRecipes",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "BuildingRecipeId",
                table: "BuildingRecipeOutputs",
                type: "character varying(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(100)",
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<string>(
                name: "BuildingRecipeId",
                table: "BuildingRecipeInputs",
                type: "character varying(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(100)",
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<string>(
                name: "BuildingId",
                table: "BuildingCosts",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingCosts_Buildings_BuildingId",
                table: "BuildingCosts",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "BuildingId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingRecipeInputs_BuildingRecipes_BuildingRecipeId",
                table: "BuildingRecipeInputs",
                column: "BuildingRecipeId",
                principalTable: "BuildingRecipes",
                principalColumn: "BuildingRecipeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingRecipeOutputs_BuildingRecipes_BuildingRecipeId",
                table: "BuildingRecipeOutputs",
                column: "BuildingRecipeId",
                principalTable: "BuildingRecipes",
                principalColumn: "BuildingRecipeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingRecipes_Buildings_BuildingId",
                table: "BuildingRecipes",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "BuildingId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BuyingAds_LocalMarkets_LocalMarketId",
                table: "BuyingAds",
                column: "LocalMarketId",
                principalTable: "LocalMarkets",
                principalColumn: "LocalMarketId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyCurrencyBalances_Companies_CompanyId",
                table: "CompanyCurrencyBalances",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractConditions_Contracts_ContractId",
                table: "ContractConditions",
                column: "ContractId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractDependencies_ContractConditions_ContractConditionId",
                table: "ContractDependencies",
                column: "ContractConditionId",
                principalTable: "ContractConditions",
                principalColumn: "ContractConditionId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CXBuyOrders_CXDataModels_CXDataModelId",
                table: "CXBuyOrders",
                column: "CXDataModelId",
                principalTable: "CXDataModels",
                principalColumn: "CXDataModelId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CXOSTrades_CXOSTradeOrders_CXOSTradeOrderId",
                table: "CXOSTrades",
                column: "CXOSTradeOrderId",
                principalTable: "CXOSTradeOrders",
                principalColumn: "CXOSTradeOrderId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CXPCDataEntries_CXPCData_CXPCDataId",
                table: "CXPCDataEntries",
                column: "CXPCDataId",
                principalTable: "CXPCData",
                principalColumn: "CXPCDataId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CXSellOrders_CXDataModels_CXDataModelId",
                table: "CXSellOrders",
                column: "CXDataModelId",
                principalTable: "CXDataModels",
                principalColumn: "CXDataModelId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DestinationLines_FlightSegments_FlightSegmentId",
                table: "DestinationLines",
                column: "FlightSegmentId",
                principalTable: "FlightSegments",
                principalColumn: "FlightSegmentId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructurePrograms_Infrastructures_InfrastructureId",
                table: "InfrastructurePrograms",
                column: "InfrastructureId",
                principalTable: "Infrastructures",
                principalColumn: "InfrastructureId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureProjectContributions_InfrastructureProjects_I~",
                table: "InfrastructureProjectContributions",
                column: "InfrastructureProjectId",
                principalTable: "InfrastructureProjects",
                principalColumn: "InfrastructureProjectId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureProjects_Infrastructures_InfrastructureId",
                table: "InfrastructureProjects",
                column: "InfrastructureId",
                principalTable: "Infrastructures",
                principalColumn: "InfrastructureId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureProjectUpgradeCosts_InfrastructureProjects_In~",
                table: "InfrastructureProjectUpgradeCosts",
                column: "InfrastructureProjectId",
                principalTable: "InfrastructureProjects",
                principalColumn: "InfrastructureProjectId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureProjectUpkeeps_InfrastructureProjects_Infrast~",
                table: "InfrastructureProjectUpkeeps",
                column: "InfrastructureProjectId",
                principalTable: "InfrastructureProjects",
                principalColumn: "InfrastructureProjectId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureReports_Infrastructures_InfrastructureId",
                table: "InfrastructureReports",
                column: "InfrastructureId",
                principalTable: "Infrastructures",
                principalColumn: "InfrastructureId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JumpCacheRoutes_JumpCache_JumpCacheId",
                table: "JumpCacheRoutes",
                column: "JumpCacheId",
                principalTable: "JumpCache",
                principalColumn: "JumpCacheId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OriginLines_FlightSegments_FlightSegmentId",
                table: "OriginLines",
                column: "FlightSegmentId",
                principalTable: "FlightSegments",
                principalColumn: "FlightSegmentId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductionLineInputs_ProductionLineOrders_ProductionLineOrd~",
                table: "ProductionLineInputs",
                column: "ProductionLineOrderId",
                principalTable: "ProductionLineOrders",
                principalColumn: "ProductionLineOrderId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductionLineOrders_ProductionLines_ProductionLineId",
                table: "ProductionLineOrders",
                column: "ProductionLineId",
                principalTable: "ProductionLines",
                principalColumn: "ProductionLineId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductionLineOutputs_ProductionLineOrders_ProductionLineOr~",
                table: "ProductionLineOutputs",
                column: "ProductionLineOrderId",
                principalTable: "ProductionLineOrders",
                principalColumn: "ProductionLineOrderId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SellingAds_LocalMarkets_LocalMarketId",
                table: "SellingAds",
                column: "LocalMarketId",
                principalTable: "LocalMarkets",
                principalColumn: "LocalMarketId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ShipLocationAddressLines_Ships_ShipId",
                table: "ShipLocationAddressLines",
                column: "ShipId",
                principalTable: "Ships",
                principalColumn: "ShipId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ShippingAds_LocalMarkets_LocalMarketId",
                table: "ShippingAds",
                column: "LocalMarketId",
                principalTable: "LocalMarkets",
                principalColumn: "LocalMarketId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ShipRepairMaterials_Ships_ShipId",
                table: "ShipRepairMaterials",
                column: "ShipId",
                principalTable: "Ships",
                principalColumn: "ShipId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SiteBuildings_Sites_SiteId",
                table: "SiteBuildings",
                column: "SiteId",
                principalTable: "Sites",
                principalColumn: "SiteId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SiteReclaimableMaterials_SiteBuildings_SiteBuildingId",
                table: "SiteReclaimableMaterials",
                column: "SiteBuildingId",
                principalTable: "SiteBuildings",
                principalColumn: "SiteBuildingId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SiteRepairMaterials_SiteBuildings_SiteBuildingId",
                table: "SiteRepairMaterials",
                column: "SiteBuildingId",
                principalTable: "SiteBuildings",
                principalColumn: "SiteBuildingId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StorageItems_Storages_StorageId",
                table: "StorageItems",
                column: "StorageId",
                principalTable: "Storages",
                principalColumn: "StorageId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubSectors_Sectors_SectorId",
                table: "SubSectors",
                column: "SectorId",
                principalTable: "Sectors",
                principalColumn: "SectorId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubSectorVertices_SubSectors_SubSectorId",
                table: "SubSectorVertices",
                column: "SubSectorId",
                principalTable: "SubSectors",
                principalColumn: "SubSectorId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SystemConnections_Systems_SystemId",
                table: "SystemConnections",
                column: "SystemId",
                principalTable: "Systems",
                principalColumn: "SystemId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserDataBalances_UserData_UserDataId",
                table: "UserDataBalances",
                column: "UserDataId",
                principalTable: "UserData",
                principalColumn: "UserDataId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserDataOffices_UserData_UserDataId",
                table: "UserDataOffices",
                column: "UserDataId",
                principalTable: "UserData",
                principalColumn: "UserDataId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserDataPlanets_UserData_UserDataId",
                table: "UserDataPlanets",
                column: "UserDataId",
                principalTable: "UserData",
                principalColumn: "UserDataId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSettingsBurnRateExclusions_UserSettingsBurnRates_UserSe~",
                table: "UserSettingsBurnRateExclusions",
                column: "UserSettingsBurnRateId",
                principalTable: "UserSettingsBurnRates",
                principalColumn: "UserSettingsBurnRateId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSettingsBurnRates_UserSettings_UserSettingsId",
                table: "UserSettingsBurnRates",
                column: "UserSettingsId",
                principalTable: "UserSettings",
                principalColumn: "UserSettingsId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkforceDescriptions_Workforces_WorkforceId",
                table: "WorkforceDescriptions",
                column: "WorkforceId",
                principalTable: "Workforces",
                principalColumn: "WorkforceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkforceNeeds_WorkforceDescriptions_WorkforceDescriptionId",
                table: "WorkforceNeeds",
                column: "WorkforceDescriptionId",
                principalTable: "WorkforceDescriptions",
                principalColumn: "WorkforceDescriptionId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
