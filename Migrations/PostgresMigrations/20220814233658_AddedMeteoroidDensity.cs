﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class AddedMeteoroidDensity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "MeteoroidDensity",
                table: "SystemStars",
                type: "double precision",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MeteoroidDensity",
                table: "SystemStars");
        }
    }
}
