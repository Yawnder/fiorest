﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class AddAbilityToAddGroupsAsPermissions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "GroupNameAndId",
                table: "PermissionAllowances",
                type: "character varying(42)",
                maxLength: 42,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "GroupModelAdmin",
                columns: table => new
                {
                    GroupModelAdminId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    GroupAdminUserName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    GroupModelId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupModelAdmin", x => x.GroupModelAdminId);
                    table.ForeignKey(
                        name: "FK_GroupModelAdmin_GroupModels_GroupModelId",
                        column: x => x.GroupModelId,
                        principalTable: "GroupModels",
                        principalColumn: "GroupModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GroupModelAdmin_GroupModelId",
                table: "GroupModelAdmin",
                column: "GroupModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GroupModelAdmin");

            migrationBuilder.DropColumn(
                name: "GroupNameAndId",
                table: "PermissionAllowances");
        }
    }
}
