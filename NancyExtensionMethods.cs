using System;
using System.Linq;

using FIORest.Authentication;

using Nancy;
using Nancy.Extensions;
using Nancy.IO;

using Newtonsoft.Json;

namespace FIORest
{
    public static class NancyExtensionMethods
    {
        public static void Cacheable(this NancyModule module, UInt32 MaxAge = 86400)
        {
            // HEAD & GET are both used by Get("/...") definitions.
            module.After += ctx =>
            {
                if (ctx.Request.Method == "GET" || ctx.Request.Method == "HEAD")
                {
                    ctx.Response.Headers["Cache-Control"] = $"public, max-age={MaxAge}";
                }
            };
        }

        public static void DefaultUncached(this NancyContext context)
        {
            // Default set the Cache-Content to not be cached.
            if (!context.Response.Headers.ContainsKey("Cache-Control"))
            {
                context.Response.Headers["Cache-Control"] = "private, no-cache, max-age=0";
            }
        }

        public static Response ReturnBadResponse(Request request, Exception ex)
        {
            string UserName = request.GetUserName();

            using (var requestStream = RequestStream.FromStream(request.Body))
            {
                string requestBody = requestStream.AsString();
                Logger.LogBadRequest(requestBody, UserName, ex);

#if DEBUG
                // Only return the payload if we're debug
                string errorJson = JsonConvert.SerializeObject(ex);
                Response resp = errorJson;
                resp.StatusCode = HttpStatusCode.BadRequest;
                resp.ContentType = "application/json";
                return resp;
#else
				return HttpStatusCode.BadRequest;
#endif
            }
        }

        public static bool IsRequestingCSVResponse(this Request request)
        {
            return request.Headers.Accept.FirstOrDefault()?.Item1.ToLower() == "text/csv";
        }
    }
}
