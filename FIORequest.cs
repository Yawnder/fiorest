﻿using System;
using FIORest.Authentication;
using FIORest.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

using Nancy;
using Nancy.Extensions;
using Nancy.IO;

using Newtonsoft.Json;

namespace FIORest
{
    public class FIORequest<JsonRepr> : IDisposable
    {
        private Exception _exception = null;
        private IDbContextTransaction _transaction = null;

        public Request Request
        {
            get;
            private set;
        } = null;

        public string UserName
        {
            get;
            private set;
        } = null;

        public DateTime Now
        {
            get;
            private set;
        } = DateTime.UtcNow;

        public RequestStream RequestStream
        {
            get;
            private set;
        } = null;

        public JsonRepr JsonPayload
        {
            get; private set;
        } = default;

        public PRUNDataContext DB
        {
            get; private set;
        } = null;

        public bool BadRequest
        {
            get
            {
                return (DB == null || HasHydrationTimeout);
            }
        }

        public bool HasHydrationTimeout
        {
            get; private set;
        } = false;

        public FIORequest(Request request)
        {
            Request = request;
            UserName = request.GetUserName();

            RequestStream = RequestStream.FromStream(request.Body);
            string requestBody = RequestStream.AsString();

            const string HydrationTimeoutStr = "Hydration Timeout [";
            if (requestBody.Contains(HydrationTimeoutStr))
            {
                HasHydrationTimeout = true;
            }

            try
            {
                JsonPayload = JsonConvert.DeserializeObject<JsonRepr>(requestBody);
                DB = PRUNDataContext.GetNewContext();
                CreateNewTransaction();
            }
            catch (Exception ex)
            {
                JsonPayload = default(JsonRepr);
                _exception = ex;
            }
        }

        public void CreateNewTransaction()
        {
            System.Diagnostics.Trace.Assert(_transaction == null);
            _transaction = DB.Database.BeginTransaction();
        }

        public void Commit()
        {
            _transaction?.Commit();
            _transaction?.Dispose();
            _transaction = null;
        }

        public void PostRemoveRange()
        {
            DB.SaveChanges();
            Commit();
            CreateNewTransaction();
        }

        public void PostRemoveRangeIgnoreConcurrencyFailures()
        {
            SaveChangesIgnoreConcurrencyFailures();
            Commit();
            CreateNewTransaction();
        }

        public void SaveChangesIgnoreConcurrencyFailures()
        {
            try
            {
                DB.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Ignore Concurrency Failures
            }
        }

        public Response ReturnBadRequest()
        {
            if (HasHydrationTimeout)
            {
                return HttpStatusCode.OK;
            }

            return NancyExtensionMethods.ReturnBadResponse(Request, _exception);
        }

        public Response GetBadResponseWithMessage(string message)
        {
            Response resp = message;
            resp.ContentType = "text/plain";
            resp.StatusCode = HttpStatusCode.BadRequest;
            return resp;
        }

        public void Dispose()
        {
            Commit();

            DB?.Dispose();
            DB = null;

            RequestStream?.Dispose();
            RequestStream = null;
        }
    }
}
