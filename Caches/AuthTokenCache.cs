﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FIORest.Database;
using FIORest.Database.Models;

using Microsoft.EntityFrameworkCore;

namespace FIORest.Caches
{
    public static class AuthTokenCache
    {
        private static MRUCache<Guid, AuthenticationModel> MRUCache = new MRUCache<Guid, AuthenticationModel>(256);

        public static AuthenticationModel Get(Guid Key)
        {
            AuthenticationModel model = MRUCache.Get(Key);
            if (model != null && model.AuthorizationExpiry < DateTime.UtcNow)
            {
                MRUCache.Remove(Key);
                model = null;
            }

            return model;
        }

        public static void Set(Guid Key, AuthenticationModel Value)
        {
            if (Value.AuthorizationExpiry > DateTime.UtcNow)
            {
                MRUCache.Set(Key, Value);
            }
        }

        public static bool Remove(Guid Key)
        {
            return MRUCache.Remove(Key);
        }

        public static AuthenticationModel GetOrCache(Guid AuthorizationGuid, PRUNDataContext context = null)
        {
            var res = Get(AuthorizationGuid);
            if (res == null)
            {
                res = Cache(AuthorizationGuid, context);
            }

            return res;
        }

        public static AuthenticationModel Cache(Guid AuthorizationGuid, PRUNDataContext context = null)
        {
            PRUNDataContext DB = context ?? PRUNDataContext.GetNewContext();

            var authModel = DB.AuthenticationModels
                .AsNoTracking()
                .Include(am => am.Allowances)
                .Include(am => am.FailedAttempts)
                .Include(am => am.APIKeys)
                .Where(am => am.AuthorizationKey == AuthorizationGuid && DateTime.UtcNow < am.AuthorizationExpiry)
                .FirstOrDefault();
            if (authModel != null)
            {
                Set(AuthorizationGuid, authModel);
            }

            if (context == null)
            {
                DB.Dispose();
            }

            return authModel;
        }
    }
}
