#if WITH_MODULES
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules
{
    public class BuildingModule : NancyModule
    {
        public BuildingModule() : base("/building")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostBuilding();
            });

            Get("/allbuildings", _ =>
            {
                this.Cacheable();
                return GetAllBuildings();
            });

            Get("/corebasebuildings", _ =>
            {
                this.Cacheable();
                return GetCoreBaseBuildings();
            });

            Get("/habitationbuildings", _ =>
            {
                this.Cacheable();
                return GetHabitationBuildings();
            });

            Get("/planetaryprojectbuildings", _ =>
            {
                this.Cacheable();
                return GetPlanetaryProjectBuildings();
            });

            Get("/corporationprojectbuildings", _ =>
            {
                this.Cacheable();
                return GetCorporationProjectBuildings();
            });

            Get("/{building_ticker}", parameters =>
            {
                this.Cacheable();
                return GetBuilding(parameters.building_ticker);
            });

            Get("/degradation/{building}", parameters =>
            {
                return GetBuildingDegradation(parameters.building, false);
            });

            Get("/degradation/repaired/{building}", parameters =>
            {
                return GetBuildingDegradation(parameters.building, true);
            });

            Get("/degradation/all", parameters =>
            {
                return GetBuildingDegradation(null, false);
            });

            Get("/degradation/repaired/all", parameters =>
            {
                return GetBuildingDegradation(null, true);
            });
        }

        private Response PostBuilding()
        {
            using (var req = new FIORequest<JSONRepresentations.WorldReactorData.Rootobject>(Request))
            {
                if (req.BadRequest || req.JsonPayload == null)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;
                var model = new Building();
                model.BuildingId = data.id;
                model.Name = data.name;
                model.Ticker = data.ticker;
                model.Expertise = data.expertise;

                foreach (var workforceCapacity in data.workforceCapacities)
                {
                    switch (workforceCapacity.level)
                    {
                        case "PIONEER":
                            model.Pioneers = workforceCapacity.capacity;
                            break;
                        case "SETTLER":
                            model.Settlers = workforceCapacity.capacity;
                            break;
                        case "TECHNICIAN":
                            model.Technicians = workforceCapacity.capacity;
                            break;
                        case "ENGINEER":
                            model.Engineers = workforceCapacity.capacity;
                            break;
                        case "SCIENTIST":
                            model.Scientists = workforceCapacity.capacity;
                            break;
                    }
                }

                model.AreaCost = data.areaCost;

                foreach (var buildingCost in data.buildingCosts)
                {
                    var buiCost = new BuildingCost();
                    buiCost.BuildingCostId = $"{model.BuildingId}-{buildingCost.material.ticker}";
                    buiCost.BuildingId = model.BuildingId;
                    buiCost.Building = model;
                    buiCost.CommodityName = buildingCost.material.name;
                    buiCost.CommodityTicker = buildingCost.material.ticker;
                    buiCost.Weight = buildingCost.material.weight;
                    buiCost.Volume = buildingCost.material.volume;
                    buiCost.Amount = buildingCost.amount;

                    model.BuildingCosts.Add(buiCost);
                }

                foreach (var recipe in data.recipes)
                {
                    BuildingRecipe buiRecipe = new BuildingRecipe();

                    var RecipeIDSB = new StringBuilder();
                    var RecipeNameSB = new StringBuilder();
                    foreach (var input in recipe.inputs)
                    {
                        BuildingRecipeInput buiInput = new BuildingRecipeInput();
                        buiInput.CommodityName = input.material.name;
                        buiInput.CommodityTicker = input.material.ticker;
                        buiInput.Weight = input.material.weight;
                        buiInput.Volume = input.material.volume;
                        buiInput.Amount = input.amount;
                        buiInput.BuildingRecipe = buiRecipe;

                        buiRecipe.Inputs.Add(buiInput);
                        RecipeIDSB.Append($" {buiInput.Amount}x{buiInput.CommodityTicker}");
                        RecipeNameSB.Append($" {buiInput.Amount}x{buiInput.CommodityTicker}");
                    }

                    RecipeIDSB.Append($"@{model.Ticker}=>");
                    RecipeNameSB.Append($"=>");

                    foreach (var output in recipe.outputs)
                    {
                        BuildingRecipeOutput buiOutput = new BuildingRecipeOutput();
                        buiOutput.CommodityName = output.material.name;
                        buiOutput.CommodityTicker = output.material.ticker;
                        buiOutput.Weight = output.material.weight;
                        buiOutput.Volume = output.material.volume;
                        buiOutput.Amount = output.amount;
                        buiOutput.BuildingRecipe = buiRecipe;

                        buiRecipe.Outputs.Add(buiOutput);
                        RecipeIDSB.Append($"{buiOutput.Amount}x{buiOutput.CommodityTicker} ");
                        RecipeNameSB.Append($"{buiOutput.Amount}x{buiOutput.CommodityTicker} ");
                    }
                    buiRecipe.BuildingRecipeId = RecipeIDSB.ToString().Trim();
                    buiRecipe.Inputs.ForEach(i =>
                    {
                        i.BuildingRecipeId = buiRecipe.BuildingRecipeId;
                        i.BuildingRecipeInputId = $"{i.BuildingRecipeId}-{i.CommodityTicker}";
                    });
                    buiRecipe.Outputs.ForEach(o =>
                    {
                        o.BuildingRecipeId = buiRecipe.BuildingRecipeId;
                        o.BuildingRecipeOutputId = $"{o.BuildingRecipeId}-{o.CommodityTicker}";
                    });
                    buiRecipe.DurationMs = recipe.duration.millis;
                    buiRecipe.RecipeName = RecipeNameSB.ToString().Trim();
                    buiRecipe.BuildingId = model.BuildingId;

                    model.Recipes.Add(buiRecipe);
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;
                model.Validate();

                req.DB.Buildings.Upsert(model)
                    .On(bm => new { bm.BuildingId })
                    .Run();

                req.DB.BuildingCosts.UpsertRange(model.BuildingCosts)
                    .On(bbc => new { bbc.BuildingCostId })
                    .Run();

                req.DB.BuildingRecipes.UpsertRange(model.Recipes)
                    .On(r => new { r.BuildingRecipeId })
                    .Run();

                var inputs = model.Recipes.SelectMany(r => r.Inputs);
                req.DB.BuildingRecipeInputs.UpsertRange(inputs)
                    .On(bri => new { bri.BuildingRecipeInputId })
                    .Run();

                var outputs = model.Recipes.SelectMany(r => r.Outputs);
                req.DB.BuildingRecipeOutputs.UpsertRange(outputs)
                    .On(bro => new { bro.BuildingRecipeOutputId })
                    .Run();

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetAllBuildings()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.Buildings
                    .AsNoTracking()
                    .Include(model => model.BuildingCosts)
                    .Include(model => model.Recipes)
                        .ThenInclude(rec => rec.Inputs)
                    .Include(model => model.Recipes)
                        .ThenInclude(rec => rec.Outputs)
                    .AsSplitQuery();
                return JsonConvert.SerializeObject(res.ToList());
            }
        }

        private const string Habitation = "HABITATION";
        private const string PlanetaryProject = "PLANETARYPROJECT";
        private const string CorporationProject = "CORPORATIONPROJECT";

        private Response GetCoreBaseBuildings()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.Buildings
                    .AsNoTracking()
                    .Where(b => !b.Name.ToUpper().Contains(PlanetaryProject) && !b.Name.ToUpper().Contains(CorporationProject) && b.Pioneers == 0 && b.Settlers == 0 && b.Technicians == 0 && b.Engineers == 0 && b.Scientists == 0)
                    .Include(model => model.BuildingCosts)
                    .Include(model => model.Recipes)
                        .ThenInclude(rec => rec.Inputs)
                    .Include(model => model.Recipes)
                        .ThenInclude(rec => rec.Outputs)
                    .AsSplitQuery();
                return JsonConvert.SerializeObject(res.ToList());
            }
        }

        private Response GetHabitationBuildings()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.Buildings
                    .AsNoTracking()
                    .Where(b => b.Name.ToUpper().Contains(Habitation))
                    .Include(model => model.BuildingCosts)
                    .Include(model => model.Recipes)
                        .ThenInclude(rec => rec.Inputs)
                    .Include(model => model.Recipes)
                        .ThenInclude(rec => rec.Outputs)
                    .AsSplitQuery();
                return JsonConvert.SerializeObject(res.ToList());
            }
        }

        private Response GetPlanetaryProjectBuildings()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.Buildings
                    .AsNoTracking()
                    .Where(b => b.Name.ToUpper().Contains(PlanetaryProject))
                    .Include(model => model.BuildingCosts)
                    .Include(model => model.Recipes)
                        .ThenInclude(rec => rec.Inputs)
                    .Include(model => model.Recipes)
                        .ThenInclude(rec => rec.Outputs)
                    .AsSplitQuery();
                return JsonConvert.SerializeObject(res.ToList());
            }
        }

        private Response GetCorporationProjectBuildings()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.Buildings
                    .AsNoTracking()
                    .Where(b => b.Name.ToUpper().Contains(CorporationProject))
                    .Include(model => model.BuildingCosts)
                    .Include(model => model.Recipes)
                        .ThenInclude(rec => rec.Inputs)
                    .Include(model => model.Recipes)
                        .ThenInclude(rec => rec.Outputs)
                    .AsSplitQuery();
                return JsonConvert.SerializeObject(res.ToList());
            }
        }

        private Response GetBuilding(string buildingTicker)
        {
            buildingTicker = buildingTicker.ToUpper();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.Buildings
                    .AsNoTracking()
                    .Include(model => model.BuildingCosts)
                    .Include(model => model.Recipes)
                        .ThenInclude(rec => rec.Inputs)
                    .Include(model => model.Recipes)
                        .ThenInclude(rec => rec.Outputs)
                    .Where(b => b.Ticker == buildingTicker)
                    .FirstOrDefault();
                if (res != null)
                {
                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response GetBuildingDegradation(string buildingTicker, bool repaired)
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                List<BuildingDegradation> res = null;
                var query = DB.BuildingDegradations
                    .AsNoTracking()
                    .Where(bdm => bdm.HasBeenRepaired == repaired);
                if (buildingTicker != null)
                {
                    buildingTicker = buildingTicker.ToUpper();

                    query = query.Where(bdm => bdm.BuildingTicker.ToUpper() == buildingTicker);
                }
                res = query
                    .Include(bdm => bdm.ReclaimableMaterials)
                    .Include(bdm => bdm.RepairMaterials)
                    .ToList();

                if (res != null)
                {
                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }
    }
}
#endif // WITH_MODULES