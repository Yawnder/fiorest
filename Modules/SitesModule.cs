﻿#if WITH_MODULES
using System;
using System.Collections.Generic;
using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules
{
    public class SitesModule : NancyModule
    {
        public SitesModule() : base("/sites")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostSites();
            });

            Post("/built", _ =>
            {
                this.EnforceWriteAuth();
                return PostSiteBuilt();
            });

            Post("/demolish", _ =>
            {
                this.EnforceWriteAuth();
                return PostSiteDemolish();
            });

            Post("/warehouses", _ =>
            {
                this.EnforceWriteAuth();
                return PostWarehouses();
            });

            Get("/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetSites(parameters.username);
            });

            Get("/{username}/{planet}", parameters =>
            {
                this.EnforceReadAuth();
                return GetSite(parameters.username, parameters.planet);
            });

            Get("/planets/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetPlanetSites(parameters.username);
            });

            Get("/warehouses/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetWarehouses(parameters.username);
            });
        }

        class DegradationTracker
        {
            private readonly PRUNDataContext DB;
            private List<BuildingDegradation> TrackedModels;

            const double Threshold = 0.0001;

            public DegradationTracker(PRUNDataContext DB)
            {
                this.DB = DB;
                TrackedModels = new List<BuildingDegradation>();
            }

            public void Track(SiteBuilding building)
            {
                bool bRepaired = (building.BuildingLastRepair != null);
                long buildingAgeMs = Utils.GetCurrentEpochMs() - (bRepaired ? (long)building.BuildingLastRepair : building.BuildingCreated);
                TimeSpan buildingAgeTimeSpan = TimeSpan.FromMilliseconds(buildingAgeMs);
                double buildingAgeDays = buildingAgeTimeSpan.TotalDays.RoundToDecimalPlaces(2);

                // Skip buildings more than 200 days old
                if (buildingAgeDays > 200.0)
                    return;

                // First check TrackedModels to see if we've already added this entry
                if (!TrackedModels.Any(d =>
                       d.HasBeenRepaired == bRepaired &&
                       d.BuildingTicker.ToUpper() == building.BuildingTicker.ToUpper() &&
                       (d.DaysSinceLastRepair - buildingAgeDays < Threshold && -(d.DaysSinceLastRepair - buildingAgeDays) < Threshold)))
                {
                    var bdm = new BuildingDegradation(building);
                    TrackedModels.Add(bdm);
                }
            }

            public void Apply()
            {
                foreach (var TrackedModel in TrackedModels)
                {
                    DB.BuildingDegradations.Upsert(TrackedModel)
                        .On(bd => new { bd.BuildingTicker, bd.DaysSinceLastRepair, bd.HasBeenRepaired })
                        .Run();

                    var dbm_id = DB.BuildingDegradations
                        .Where(bd =>
                            bd.BuildingTicker == TrackedModel.BuildingTicker &&
                            bd.DaysSinceLastRepair == TrackedModel.DaysSinceLastRepair &&
                            bd.HasBeenRepaired == TrackedModel.HasBeenRepaired
                        )
                        .Select(bd => bd.BuildingDegradationId).First();

                    List<BuildingDegradationReclaimableMaterial> reclaimList = new List<BuildingDegradationReclaimableMaterial>();
                    foreach (var reclaimableMaterial in TrackedModel.ReclaimableMaterials)
                    {
                        var bdrmm = new BuildingDegradationReclaimableMaterial();
                        bdrmm.MaterialTicker = reclaimableMaterial.MaterialTicker;
                        bdrmm.MaterialCount = reclaimableMaterial.MaterialCount;
                        bdrmm.BuildingDegradationId = dbm_id;
                        reclaimList.Add(bdrmm);
                    }
                    DB.BuildingDegradationReclaimableMaterials.UpsertRange(reclaimList)
                        .On(rmm => new { rmm.BuildingDegradationId, rmm.MaterialTicker })
                        .Run();

                    List<BuildingDegradationRepairMaterial> repairList = new List<BuildingDegradationRepairMaterial>();
                    foreach (var repairMaterial in TrackedModel.RepairMaterials)
                    {
                        var bdrmm = new BuildingDegradationRepairMaterial();
                        bdrmm.MaterialTicker = repairMaterial.MaterialTicker;
                        bdrmm.MaterialCount = repairMaterial.MaterialCount;
                        bdrmm.BuildingDegradationId = dbm_id;
                        repairList.Add(bdrmm);
                    }
                    DB.BuildingDegradationRepairMaterials.UpsertRange(repairList)
                        .On(rmm => new { rmm.BuildingDegradationId, rmm.MaterialTicker })
                        .Run();
                }
            }
        }

        private Response PostSites()
        {
            using (var req = new FIORequest<JSONRepresentations.SiteSites.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;

                var sites = new List<Site>();
                var degradationTracker = new DegradationTracker(req.DB);

                foreach (var site in data.sites)
                {
                    var s = new Site();
                    s.SiteId = site.siteId;
                    foreach (var line in site.address.lines)
                    {
                        if (line.type == "PLANET")
                        {
                            s.PlanetId = line.entity.id;
                            s.PlanetIdentifier = line.entity.naturalId;
                            s.PlanetName = line.entity.name;
                            break;
                        }
                    }
                    s.PlanetFoundedEpochMs = site.founded.timestamp;
                    s.InvestedPermits = site.investedPermits;
                    s.MaximumPermits = site.maximumPermits;

                    foreach (var platform in site.platforms)
                    {
                        var siteBuilding = new SiteBuilding();
                        siteBuilding.SiteBuildingId = $"{s.SiteId}-{platform.module.id}";
                        siteBuilding.BuildingId = platform.module.id;
                        siteBuilding.BuildingCreated = platform.creationTime.timestamp;
                        siteBuilding.BuildingName = platform.module.reactorName;
                        siteBuilding.BuildingTicker = platform.module.reactorTicker;
                        siteBuilding.BuildingLastRepair = platform.lastRepair?.timestamp;
                        siteBuilding.Condition = platform.condition;

                        foreach (var reclaimable in platform.reclaimableMaterials)
                        {
                            var siteReclaimable = new SiteReclaimableMaterial();
                            siteReclaimable.SiteReclaimableMaterialId = $"{siteBuilding.SiteBuildingId}-{reclaimable.material.ticker}";
                            siteReclaimable.MaterialId = reclaimable.material.id;
                            siteReclaimable.MaterialTicker = reclaimable.material.ticker;
                            siteReclaimable.MaterialAmount = reclaimable.amount;
                            siteReclaimable.MaterialName = reclaimable.material.name;
                            siteReclaimable.SiteBuildingId = siteBuilding.SiteBuildingId;

                            siteBuilding.ReclaimableMaterials.Add(siteReclaimable);
                        }

                        foreach (var repairable in platform.repairMaterials)
                        {
                            var siteRepairable = new SiteRepairMaterial();
                            siteRepairable.SiteRepairMaterialId = $"{siteBuilding.SiteBuildingId}-{repairable.material.ticker}";
                            siteRepairable.MaterialId = repairable.material.id;
                            siteRepairable.MaterialTicker = repairable.material.ticker;
                            siteRepairable.MaterialAmount = repairable.amount;
                            siteRepairable.MaterialName = repairable.material.name;
                            siteRepairable.SiteBuildingId = siteBuilding.SiteBuildingId;

                            siteBuilding.RepairMaterials.Add(siteRepairable);
                        }

                        siteBuilding.SiteId = s.SiteId;

                        s.Buildings.Add(siteBuilding);
                        degradationTracker.Track(siteBuilding);
                    }

                    s.UserNameSubmitted = req.UserName;
                    s.Timestamp = req.Now;

                    s.Validate();
                    sites.Add(s);
                }

                req.DB.Sites.RemoveRange(req.DB.Sites.Where(s => s.UserNameSubmitted.ToUpper() == req.UserName));
                req.PostRemoveRangeIgnoreConcurrencyFailures();

                req.DB.Sites.UpsertRange(sites)
                    .On(s => new { s.SiteId })
                    .Run();

                var buildings = sites.SelectMany(s => s.Buildings);
                req.DB.SiteBuildings.UpsertRange(buildings)
                    .On(sb => new { sb.SiteBuildingId })
                    .Run();

                var reclaimables = buildings.SelectMany(b => b.ReclaimableMaterials);
                req.DB.SiteReclaimableMaterials.UpsertRange(reclaimables)
                    .On(srm => new { srm.SiteReclaimableMaterialId })
                    .Run();

                var repairables = buildings.SelectMany(b => b.RepairMaterials);
                req.DB.SiteRepairMaterials.UpsertRange(repairables)
                    .On(srm => new { srm.SiteRepairMaterialId })
                    .Run();

                req.DB.SaveChanges();
                req.Commit();

                // Apply degradation
                req.CreateNewTransaction();
                degradationTracker.Apply();
                req.DB.SaveChanges();
                req.Commit();

                return HttpStatusCode.OK;
            }
        }

        private Response PostSiteBuilt()
        {
            using (var req = new FIORequest<JSONRepresentations.SitePlatformBuilt.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload;
                var site = req.DB.Sites.Where(s => s.SiteId == payload.siteId).FirstOrDefault();
                if (site != null)
                {
                    var newBuilding = new SiteBuilding();
                    newBuilding.SiteBuildingId = $"{site.SiteId}-{payload.module.reactorId}";
                    newBuilding.BuildingId = payload.module.reactorId;
                    newBuilding.BuildingCreated = payload.creationTime.timestamp;
                    newBuilding.BuildingName = payload.module.reactorName;
                    newBuilding.BuildingTicker = payload.module.reactorTicker;
                    newBuilding.Condition = payload.condition;
                    newBuilding.SiteId = site.SiteId;

                    foreach (var payloadReclaimable in payload.reclaimableMaterials)
                    {
                        var newReclaimable = new SiteReclaimableMaterial();
                        newReclaimable.SiteReclaimableMaterialId = $"{newBuilding.SiteBuildingId}-{payloadReclaimable.material.ticker}";
                        newReclaimable.MaterialId = payloadReclaimable.material.id;
                        newReclaimable.MaterialName = payloadReclaimable.material.name;
                        newReclaimable.MaterialTicker = payloadReclaimable.material.ticker;
                        newReclaimable.MaterialAmount = payloadReclaimable.amount;
                        newReclaimable.SiteBuildingId = newBuilding.SiteBuildingId;

                        newBuilding.ReclaimableMaterials.Add(newReclaimable);
                    }

                    site.Timestamp = DateTime.Now;
                    site.Validate();

                    req.DB.Sites.Upsert(site)
                        .On(s => new { s.SiteId })
                        .Run();

                    req.DB.SiteBuildings.Upsert(newBuilding)
                        .On(sb => new { sb.SiteBuildingId })
                        .Run();

                    req.DB.SiteReclaimableMaterials.UpsertRange(newBuilding.ReclaimableMaterials)
                        .On(srm => new { srm.SiteReclaimableMaterialId })
                        .Run();

                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostSiteDemolish()
        {
            using (var req = new FIORequest<JSONRepresentations.SiteDemolish.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload;
                var site = req.DB.Sites.Where(s => s.SiteId == payload.siteId).FirstOrDefault();
                if (site != null)
                {
                    var building = site.Buildings.Where(b => b.SiteBuildingId == payload.platformId).FirstOrDefault();
                    if (building != null)
                    {
                        site.Buildings.Remove(building);
                        req.DB.SaveChanges();
                    }
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostWarehouses()
        {
            using (var req = new FIORequest<JSONRepresentations.WarehouseStorages.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var storages = req.JsonPayload.payload.message.payload.storages;
                req.DB.Warehouses.RemoveRange(req.DB.Warehouses.Where(wm => wm.UserNameSubmitted.ToUpper() == req.UserName));
                req.PostRemoveRangeIgnoreConcurrencyFailures();

                var allWarehouses = new List<Warehouse>();
                foreach (var warehouse in storages)
                {
                    var model = new Warehouse();
                    model.WarehouseId = warehouse.warehouseId;
                    model.StoreId = warehouse.storeId;
                    model.Units = warehouse.units;
                    model.WeightCapacity = warehouse.weightCapacity;
                    model.VolumeCapacity = warehouse.volumeCapacity;
                    if (warehouse.nextPayment != null)
                    {
                        model.NextPaymentTimestampEpochMs = warehouse.nextPayment.timestamp;
                    }

                    if (warehouse.fee != null)
                    {
                        model.FeeAmount = warehouse.fee.amount;
                        model.FeeCurrency = warehouse.fee.currency;
                    }

                    model.LocationName = warehouse.address.lines[warehouse.address.lines.Length - 1].entity.name;
                    model.LocationNaturalId = warehouse.address.lines[warehouse.address.lines.Length - 1].entity.naturalId;

                    model.UserNameSubmitted = req.UserName;
                    model.Timestamp = req.Now;

                    model.Validate();
                    allWarehouses.Add(model);
                }

                req.DB.Warehouses.UpsertRange(allWarehouses)
                    .On(w => new { w.WarehouseId })
                    .Run();

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetSites(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Building))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var sites = DB.Sites
                        .AsNoTracking()
                        .Include(s => s.Buildings)
                            .ThenInclude(b => b.ReclaimableMaterials)
                        .Include(s => s.Buildings)
                            .ThenInclude(b => b.RepairMaterials)
                        .Where(s => s.UserNameSubmitted.ToUpper() == UserName)
                        .AsSplitQuery()
                        .ToList();
                    return JsonConvert.SerializeObject(sites);
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetSite(string UserName, string Planet)
        {
            UserName = UserName.ToUpper();
            Planet = Planet.ToUpper();

            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Building))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var site = DB.Sites
                        .AsNoTracking()
                        .Include(s => s.Buildings)
                            .ThenInclude(b => b.ReclaimableMaterials)
                        .Include(s => s.Buildings)
                            .ThenInclude(b => b.RepairMaterials)
                        .Where(s => s.UserNameSubmitted.ToUpper() == UserName)
                        .Where(s => s.PlanetId.ToUpper() == Planet || s.PlanetIdentifier.ToUpper() == Planet || s.PlanetName.ToUpper() == Planet)
                        .FirstOrDefault();

                    return JsonConvert.SerializeObject(site);
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetPlanetSites(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Building))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var sites = DB.Sites
                        .AsNoTracking()
                        .Where(s => s.UserNameSubmitted.ToUpper() == UserName)
                        .Select(s => s.PlanetId)
                        .ToList();
                    return JsonConvert.SerializeObject(sites);
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetWarehouses(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Building))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var warehouses = DB.Warehouses
                        .AsNoTracking()
                        .Where(w => w.UserNameSubmitted.ToUpper() == UserName)
                        .ToList();
                    return JsonConvert.SerializeObject(warehouses);
                }
            }

            return HttpStatusCode.Unauthorized;
        }
    }
}
#endif // WITH_MODULES