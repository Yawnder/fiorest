﻿#if WITH_MODULES
using System.Collections.Generic;
using System.Linq;

using Nancy;
using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;
using static FIORest.Database.Models.UserData;

namespace FIORest.Modules
{
    public class UserModule : NancyModule
    {
        public UserModule() : base("/user")
        {
            Post("/users", _ =>
            {
                this.EnforceWriteAuth();
                return PostUserUsers();
            });

            Post("/companies", _ =>
            {
                this.EnforceWriteAuth();
                return PostUserCompanies();
            });

            Post("/offices", _ =>
            {
                this.EnforceWriteAuth();
                return PostOffices();
            });

            Post("/company_data", _ =>
            {
                this.EnforceWriteAuth();
                return PostUserCompanyData();
            });

            Get("/allusers", _ =>
            {
                this.EnforceReadAuth();
                return GetAllUsers();
            });

            Get("/{username}", parameters =>
            {
                return GetUserByUserName(parameters.username);
            });

            Get("/id/{usernameid}", parameters =>
            {
                return GetUserById(parameters.usernameid);
            });

            Get("/corporation/{corporation_code}", parameters =>
            {
                return GetUsersByCorporationCode(parameters.corporation_code);
            });

            Post("/resetalldata", _ =>
            {
                this.EnforceWriteAuth();
                return PostResetAllData();
            });
        }

        private Response PostUserUsers()
        {
            using (var req = new FIORequest<JSONRepresentations.UserUsers.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload.body;

                if (data.company?.id == null)
                {
                    // User was deleted
                    return HttpStatusCode.OK;
                }
                
                var userdata = new UserData();
                userdata.UserDataId = data.company?.id;
                userdata.UserId = data.id;
                userdata.UserName = data.username;
                userdata.SubscriptionLevel = data.subscriptionLevel;
                userdata.Tier = data.highestTier;
                userdata.Team = data.team;
                userdata.Pioneer = data.pioneer;
                userdata.Moderator = data.moderator;
                userdata.CreatedEpochMs = data.created.timestamp;
                userdata.CompanyId = data.company?.id;
                userdata.CompanyName = data.company?.name;
                userdata.CompanyCode = data.company?.code;
                userdata.UserNameSubmitted = req.UserName;
                userdata.Timestamp = req.Now;

                userdata.Validate(UserDataValidationType.CheckUserIdAndName | UserDataValidationType.CheckCompanyNameAndCode | UserDataValidationType.CheckSubAndTier);

                req.DB.UserData.Upsert(userdata)
                    .On(ud => new { ud.UserDataId })
                    .WhenMatched((existingData, newData) => new UserData
                    {
                        UserDataId = newData.UserDataId,
                        UserId = newData.UserId,
                        UserName = newData.UserName,
                        SubscriptionLevel = newData.SubscriptionLevel,
                        Tier = newData.Tier,
                        Team = newData.Team,
                        Pioneer = newData.Pioneer,
                        Moderator = newData.Moderator,
                        CreatedEpochMs = newData.CreatedEpochMs,
                        CompanyId = newData.CompanyId,
                        CompanyName = newData.CompanyName,
                        CompanyCode = newData.CompanyCode,
                        UserNameSubmitted = newData.UserNameSubmitted,
                        Timestamp = newData.Timestamp,
                    })
                    .Run();

                req.DB.SaveChanges();

                return HttpStatusCode.OK;
            }
        }

        private Response PostUserCompanies()
        {
            using (var req = new FIORequest<JSONRepresentations.UserCompanies.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload.body;

                if (data.id == null)
                {
                    // User was deleted
                    return HttpStatusCode.OK;
                }

                var userdata = new UserData();
                userdata.UserDataId = data.id;
                userdata.UserId = data.user.id;
                userdata.UserName = data.user.username;
                userdata.CompanyId = data.id;
                userdata.CompanyName = data.name;
                userdata.CompanyCode = data.code;
                userdata.CountryId = data.country.id;
                userdata.CountryName = data.country.name;
                userdata.CountryCode = data.country.code;
                userdata.CorporationId = data.corporation?.id;
                userdata.CorporationName = data.corporation?.name;
                userdata.CorporationCode = data.corporation?.code;
                userdata.OverallRating = data.ratingReport.overallRating;
                userdata.ActivityRating = data.ratingReport.subRatings.First(sr => sr.score == "ACTIVITY").rating;
                userdata.ReliabilityRating = data.ratingReport.subRatings.First(sr => sr.score == "RELIABILITY").rating;
                userdata.StabilityRating = data.ratingReport.subRatings.First(sr => sr.score == "STABILITY").rating;

                var sites = data.siteAddresses.SelectMany(sa => sa.lines).Select(l => l.entity).Where(l => l._type == "planet").ToList();
                foreach (var site in sites)
                {
                    var planet = new UserDataPlanet();
                    planet.UserDataPlanetId = $"{userdata.UserDataId}-{site.id}";
                    planet.PlanetId = site.id;
                    planet.PlanetNaturalId = site.naturalId;
                    planet.PlanetName = site.name;
                    planet.UserDataId = userdata.UserDataId;

                    userdata.Planets.Add(planet);
                }

                userdata.UserNameSubmitted = req.UserName;
                userdata.Timestamp = req.Now;

                userdata.Validate(UserDataValidationType.CheckUserIdAndName | UserDataValidationType.CheckCompanyNameAndCode | UserDataValidationType.CheckCountrySettings | UserDataValidationType.CheckCorporationSettings | UserDataValidationType.CheckRatings | UserDataValidationType.CheckPlanets);

                req.DB.UserData.Upsert(userdata)
                    .On(ud => new { ud.UserDataId })
                    .WhenMatched((existingData, newData) => new UserData
                    {
                        UserDataId = newData.UserDataId,
                        UserId = newData.UserId,
                        UserName = newData.UserName,
                        CompanyId = newData.CompanyId,
                        CompanyName = newData.CompanyName,
                        CompanyCode = newData.CompanyCode,
                        CountryId = newData.CountryId,
                        CountryCode = newData.CountryCode,
                        CountryName = newData.CountryName,
                        CorporationId = newData.CorporationId,
                        CorporationName = newData.CorporationName,
                        CorporationCode = newData.CorporationCode,
                        OverallRating = newData.OverallRating,
                        ActivityRating = newData.ActivityRating,
                        ReliabilityRating = newData.ReliabilityRating,
                        StabilityRating = newData.StabilityRating,
                        UserNameSubmitted = newData.UserNameSubmitted,
                        Timestamp = newData.Timestamp,
                    })
                    .Run();

                req.DB.UserDataPlanets.UpsertRange(userdata.Planets)
                    .On(udp => new { udp.UserDataPlanetId })
                    .Run();

                req.DB.SaveChanges();

                return HttpStatusCode.OK;
            }
        }

        private Response PostOffices()
        {
            using (var req = new FIORequest<JSONRepresentations.UserDataOffices.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var companyid = req.JsonPayload.payload.message.payload.path[1];
                var offices = req.JsonPayload.payload.message.payload.body;

                var UserDataId = req.DB.UserData
                    .AsNoTracking()
                    .Where(ud => ud.CompanyId.ToUpper() == companyid.ToUpper())
                    .Select(ud => ud.UserDataId)
                    .FirstOrDefault();

                if (UserDataId != null)
                {
                    var UserDataOffices = new List<UserDataOffice>();
                    foreach (var office in offices)
                    {
                        var NewUserDataOffice = new UserDataOffice();
                        NewUserDataOffice.UserDataOfficeId = office.id;
                        NewUserDataOffice.PlanetNaturalId = office.address.lines[1].entity.naturalId;
                        NewUserDataOffice.PlanetName = office.address.lines[1].entity.name;
                        NewUserDataOffice.StartEpochMs = office.start.timestamp;
                        NewUserDataOffice.EndEpochMs = office.end.timestamp;
                        NewUserDataOffice.UserDataId = UserDataId;

                        UserDataOffices.Add(NewUserDataOffice);
                    }

                    req.DB.UserDataOffices.UpsertRange(UserDataOffices)
                        .On(udo => new { udo.UserDataOfficeId })
                        .Run();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostUserCompanyData()
        {
            // This has private data
            using (var req = new FIORequest<JSONRepresentations.CompanyData.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;
                if (data.id == null)
                {
                    // User was deleted
                    return HttpStatusCode.OK;
                }

                var userdata = new UserData();
                userdata.UserDataId = data.id;
                userdata.CompanyId = data.id;
                userdata.CompanyName = data.name;
                userdata.CompanyCode = data.code;
                userdata.CountryId = data.countryId;
                userdata.OverallRating = data.ratingReport.overallRating;
                userdata.ActivityRating = data.ratingReport.subRatings.First(sr => sr.score == "ACTIVITY").rating;
                userdata.ReliabilityRating = data.ratingReport.subRatings.First(sr => sr.score == "RELIABILITY").rating;
                userdata.StabilityRating = data.ratingReport.subRatings.First(sr => sr.score == "STABILITY").rating;

                // Start private
                if (data.headquarters?.address?.lines != null)
                {
                    userdata.HeadquartersNaturalId = userdata.HeadquartersNaturalId = data.headquarters.address.lines.Select(l => l.entity).FirstOrDefault(e => e._type == "PLANET")?.naturalId;
                }
                
                userdata.HeadquartersLevel = data.headquarters != null ? data.headquarters.level : 0;
                userdata.HeadquartersBasePermits = data.headquarters != null ? data.headquarters.basePermits : 0;
                userdata.HeadquartersUsedBasePermits = data.headquarters != null ? data.headquarters.usedBasePermits : 0;
                userdata.AdditionalBasePermits = data.headquarters != null ? data.headquarters.additionalBasePermits : 0;
                userdata.AdditionalProductionQueueSlots = data.headquarters != null ? data.headquarters.additionalProductionQueueSlots : 0;
                userdata.RelocationLocked = data.headquarters != null ? data.headquarters.relocationLocked : false;
                userdata.NextRelocationTimeEpochMs = data.headquarters != null && data.headquarters.nextRelocationTime != null ? data.headquarters.nextRelocationTime.timestamp : 0;

                var currencyBalances = data.currencyAccounts.Select(ca => ca.currencyBalance).ToList();
                foreach (var currencyBalance in currencyBalances)
                {
                    var balance = new UserDataBalance();
                    balance.UserDataBalanceId = $"{userdata.UserDataId}-{currencyBalance.currency}";
                    balance.Currency = currencyBalance.currency;
                    balance.Amount = currencyBalance.amount;
                    balance.UserDataId = userdata.UserDataId;

                    userdata.Balances.Add(balance);
                }
                // End private

                userdata.UserNameSubmitted = req.UserName;
                userdata.Timestamp = req.Now;

                userdata.Validate(UserDataValidationType.CheckCompanyNameAndCode | UserDataValidationType.CheckCountrySettings | UserDataValidationType.CheckCorporationSettings | UserDataValidationType.CheckRatings | UserDataValidationType.CheckPlanets);

                req.DB.UserData.Upsert(userdata)
                    .On(ud => new { ud.UserDataId })
                    .WhenMatched((existingData, newData) => new UserData
                    {
                        UserDataId = newData.UserDataId,
                        CompanyId = newData.CompanyId,
                        CompanyName = newData.CompanyName,
                        CompanyCode = newData.CompanyCode,
                        CountryId = newData.CountryId,
                        OverallRating = newData.OverallRating,
                        ActivityRating = newData.ActivityRating,
                        ReliabilityRating = newData.ReliabilityRating,
                        StabilityRating = newData.StabilityRating,
                        HeadquartersNaturalId = newData.HeadquartersNaturalId,
                        HeadquartersLevel = newData.HeadquartersLevel,
                        HeadquartersBasePermits = newData.HeadquartersBasePermits,
                        HeadquartersUsedBasePermits = newData.HeadquartersUsedBasePermits,
                        AdditionalBasePermits = newData.AdditionalBasePermits,
                        AdditionalProductionQueueSlots = newData.AdditionalProductionQueueSlots,
                        RelocationLocked = newData.RelocationLocked,
                        NextRelocationTimeEpochMs = newData.NextRelocationTimeEpochMs,
                        UserNameSubmitted = newData.UserNameSubmitted,
                        Timestamp = newData.Timestamp,
                    })
                    .Run();

                req.DB.UserDataBalances.UpsertRange(userdata.Balances)
                    .On(udp => new { udp.UserDataBalanceId })
                    .Run();

                req.DB.SaveChanges();

                return HttpStatusCode.OK;
            }
        }

        private Response GetAllUsers()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var allUsers = DB.AuthenticationModels
                    .AsNoTracking()
                    .Select(u => u.UserName)
                    .ToList();
                return JsonConvert.SerializeObject(allUsers);
            }
        }

        public static Response GetUserByUserName(Request Request, string UserName)
        {
            UserName = UserName.ToUpper();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.UserData
                    .AsNoTracking()
                    .Include(ud => ud.Planets)
                    .Include(ud => ud.Balances)
                    .Include(ud => ud.Offices)
                    .Where(u => u.UserName.ToUpper() == UserName)
                    .AsSplitQuery()
                    .FirstOrDefault();
                if (res != null)
                {
                    if (!Auth.CanSeeData(Request.GetUserName(), res.UserName, Auth.PrivacyType.Contracts))
                    {
                        res.StripPrivateData();
                    }

                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        public Response GetUserByUserName(string UserName)
        {
            return GetUserByUserName(Request, UserName);
        }

        public Response GetUserById(string id)
        {
            id = id.ToUpper();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.UserData
                    .AsNoTracking()
                    .Include(ud => ud.Planets)
                    .Include(ud => ud.Balances)
                    .Include(ud => ud.Offices)
                    .Where(u => u.UserId.ToUpper() == id)
                    .AsSplitQuery()
                    .FirstOrDefault();
                if (res != null)
                {
                    if (!Auth.CanSeeData(Request.GetUserName(), res.UserName, Auth.PrivacyType.Contracts))
                    {
                        res.StripPrivateData();
                    }

                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        public Response GetUsersByCorporationCode(string corpCode)
        {
            corpCode = corpCode.ToUpper();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.UserData
                    .AsNoTracking()
                    .Where(u => u.CorporationCode == corpCode)
                    .Select(u => new
                    {
                        UserName = u.UserName,
                        CompanyCode = u.CompanyCode,
                    })
                    .ToList();
                return JsonConvert.SerializeObject(res);
            }
        }

        public static Response GetUserByCompanyCode(Request Request, string CompanyCode)
        {
            CompanyCode = CompanyCode.ToUpper();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.UserData
                    .AsNoTracking()
                    .Include(ud => ud.Planets)
                    .Include(ud => ud.Balances)
                    .Include(ud => ud.Offices)
                    .Where(u => u.CompanyCode.ToUpper() == CompanyCode)
                    .AsSplitQuery()
                    .FirstOrDefault();
                if (res != null)
                {
                    if (!Auth.CanSeeData(Request.GetUserName(), res.UserName, Auth.PrivacyType.Contracts))
                    {
                        res.StripPrivateData();
                    }

                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        public Response GetUserByCompanyCode(string CompanyCode)
        {
            return GetUserByCompanyCode(Request, CompanyCode);
        }

        public static Response GetUserByCompanyName(Request Request, string CompanyName)
        {
            CompanyName = CompanyName.ToUpper();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.UserData
                    .AsNoTracking()
                    .Include(ud => ud.Planets)
                    .Include(ud => ud.Balances)
                    .Include(ud => ud.Offices)
                    .Where(u => u.CompanyName.ToUpper() == CompanyName)
                    .AsSplitQuery()
                    .FirstOrDefault();
                if (res != null)
                {
                    if (!Auth.CanSeeData(Request.GetUserName(), res.UserName, Auth.PrivacyType.Contracts))
                    {
                        res.StripPrivateData();
                    }

                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        public static Response GetUserByCompanyId(Request Request, string CompanyId)
        {
            CompanyId = CompanyId.ToUpper();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.UserData
                    .AsNoTracking()
                    .Include(ud => ud.Planets)
                    .Include(ud => ud.Balances)
                    .Include(ud => ud.Offices)
                    .Where(u => u.CompanyId.ToUpper() == CompanyId)
                    .AsSplitQuery()
                    .FirstOrDefault();
                if (res != null)
                {
                    if (!Auth.CanSeeData(Request.GetUserName(), res.UserName, Auth.PrivacyType.Contracts))
                    {
                        res.StripPrivateData();
                    }

                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        public Response GetUserByCompanyName(string CompanyName)
        {
            return GetUserByCompanyName(Request, CompanyName);   
        }

        private Response PostResetAllData()
        {
            string UserName = Request.GetUserName().ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            using (var transaction = DB.Database.BeginTransaction())
            {
                DB.Companies.RemoveRange(DB.Companies.Where(c => c.UserNameSubmitted.ToUpper() == UserName));
                DB.ProductionLines.RemoveRange(DB.ProductionLines.Where(p => p.UserNameSubmitted.ToUpper() == UserName));
                DB.Ships.RemoveRange(DB.Ships.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.Sites.RemoveRange(DB.Sites.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.Warehouses.RemoveRange(DB.Warehouses.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.Storages.RemoveRange(DB.Storages.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.Workforces.RemoveRange(DB.Workforces.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.UserData.RemoveRange(DB.UserData.Where(s => s.UserName.ToUpper() == UserName));
                DB.Contracts.RemoveRange(DB.Contracts.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.UserSettings.RemoveRange(DB.UserSettings.Where(s => s.UserName.ToUpper() == UserName));

                DB.SaveChanges();
                transaction.Commit();
                return HttpStatusCode.OK;
            }
        }
    }
}
#endif // WITH_MODULES