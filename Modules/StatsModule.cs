#if WITH_MODULES
using System;
using System.Collections.Generic;
using System.Linq;

using FIORest.Database;

using Nancy;

using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules
{
    public class StatsModule : NancyModule
    {
        public StatsModule() : base("/stats")
        {
            Get("/priceindexconsumables", _ =>
            {
                return GetPriceIndexConsumables();
            });

            Get("/priceindexbuildings", _ =>
            {
                return GetPriceIndexBuildings();
            });

            Get("/historical/priceindices/{start:long}/{end:long}", parameters =>
            {
                return GetHistoricalPriceIndices(parameters.start, parameters.end);
            });
        }

        public class RainPriceIndex
        {
            public string Currency { get; set; }
            public double PriceIndex { get; set; }
        }

        public static List<RainPriceIndex> CalculatePriceIndexConsumables()
        {
            var result = new List<RainPriceIndex>();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var exchanges = DB.ComexExchanges
                    .AsNoTracking()
                    .Select(exch => new { exch.CurrencyCode, exch.ExchangeCode })
                    .ToList();
                foreach (var exchange in exchanges)
                {
                    var currencyCode = exchange.CurrencyCode;
                    var exchangeCode = exchange.ExchangeCode;
                    if (!exchangeCode.EndsWith("1"))
                    {
                        // Skip non-starter CXs
                        continue;
                    }

                    var rat = DB.CXDataModels
                        .AsNoTracking()
                        .Where(cx => cx.MaterialTicker == "RAT" && cx.ExchangeCode == exchangeCode)
                        .Select(cx => cx.Ask)
                        .FirstOrDefault();
                    var dw = DB.CXDataModels
                        .AsNoTracking()
                        .Where(cx => cx.MaterialTicker == "DW" && cx.ExchangeCode == exchangeCode)
                        .Select(cx => cx.Ask)
                        .FirstOrDefault();
                    var ove = DB.CXDataModels
                        .AsNoTracking()
                        .Where(cx => cx.MaterialTicker == "OVE" && cx.ExchangeCode == exchangeCode)
                        .Select(cx => cx.Ask)
                        .FirstOrDefault();
                    var pwo = DB.CXDataModels
                        .AsNoTracking()
                        .Where(cx => cx.MaterialTicker == "PWO" && cx.ExchangeCode == exchangeCode)
                        .Select(cx => cx.Ask)
                        .FirstOrDefault();
                    var cof = DB.CXDataModels
                        .AsNoTracking()
                        .Where(cx => cx.MaterialTicker == "COF" && cx.ExchangeCode == exchangeCode)
                        .Select(cx => cx.Ask)
                        .FirstOrDefault();

                    double ratAsk = (rat != null) ? (double)rat : 999;
                    double dwAsk = (dw != null) ? (double)dw : 999;
                    double oveAsk = (ove != null) ? (double)ove : 999;
                    double pwoAsk = (pwo != null) ? (double)pwo : 999;
                    double cofAsk = (cof != null) ? (double)cof : 999;

                    double pio100 = (1 / 0.7845) * (4.0 * dwAsk + 4.0 * ratAsk + 0.5 * oveAsk) / 10.0;
                    double pio100pwo = (1 / 0.87) * (4.0 * dwAsk + 4.0 * ratAsk + 0.5 * oveAsk + 0.2 * pwoAsk) / 10.0;
                    double pio100cof = (1 / 0.87) * (4.0 * dwAsk + 4.0 * ratAsk + 0.5 * oveAsk + 0.5 * cofAsk) / 10.0;
                    double pio100pwocof = 1.0 * (4.0 * dwAsk + 4.0 * ratAsk + 0.5 * oveAsk + 0.2 * pwoAsk + 0.5 * cofAsk) / 10.0;
                    pio100 = Math.Min(pio100, Math.Min(pio100pwo, Math.Min(pio100cof, pio100pwocof)));
                    result.Add(new RainPriceIndex
                    {
                        Currency = currencyCode,
                        PriceIndex = pio100
                    });
                }
            }

            return result;
        }

        private Response GetPriceIndexConsumables()
        {
            return JsonConvert.SerializeObject(CalculatePriceIndexConsumables());
        }

        public static List<RainPriceIndex> CalculatePriceIndexBuildings()
        {
            var result = new List<RainPriceIndex>();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var exchanges = DB.ComexExchanges
                    .AsNoTracking()
                    .Select(exch => new { exch.CurrencyCode, exch.ExchangeCode })
                    .ToList();
                foreach (var exchange in exchanges)
                {
                    var currencyCode = exchange.CurrencyCode;
                    var exchangeCode = exchange.ExchangeCode;
                    if (!exchangeCode.EndsWith("1"))
                    {
                        // Skip non-starter CXs
                        continue;
                    }

                    var bbh = DB.CXDataModels
                        .AsNoTracking()
                        .Where(cx => cx.MaterialTicker == "BBH" && cx.ExchangeCode == exchangeCode)
                        .Select(cx => cx.Ask)
                        .FirstOrDefault();
                    var bde = DB.CXDataModels
                        .AsNoTracking()
                        .Where(cx => cx.MaterialTicker == "BDE" && cx.ExchangeCode == exchangeCode)
                        .Select(cx => cx.Ask)
                        .FirstOrDefault();
                    var bse = DB.CXDataModels
                        .AsNoTracking()
                        .Where(cx => cx.MaterialTicker == "BSE" && cx.ExchangeCode == exchangeCode)
                        .Select(cx => cx.Ask)
                        .FirstOrDefault();
                    var bta = DB.CXDataModels
                        .AsNoTracking()
                        .Where(cx => cx.MaterialTicker == "BTA" && cx.ExchangeCode == exchangeCode)
                        .Select(cx => cx.Ask)
                        .FirstOrDefault();
                    var mcg = DB.CXDataModels
                        .AsNoTracking()
                        .Where(cx => cx.MaterialTicker == "MCG" && cx.ExchangeCode == exchangeCode)
                        .Select(cx => cx.Ask)
                        .FirstOrDefault();

                    double bbhAsk = (bbh != null) ? (double)bbh : 9999;
                    double bdeAsk = (bde != null) ? (double)bde : 9999;
                    double bseAsk = (bse != null) ? (double)bse : 9999;
                    double btaAsk = (bta != null) ? (double)bta : 9999;
                    double mcgAsk = (mcg != null) ? (double)mcg : 9999;

                    double buildprice = (4.6 * bbhAsk + 2.3 * bdeAsk + 10.3 * bseAsk + 0.65 * btaAsk + 70 * mcgAsk) / 359.7;
                    result.Add(new RainPriceIndex
                    {
                        Currency = currencyCode,
                        PriceIndex = buildprice
                    });
                }
            }

            return result;
        }

        private Response GetPriceIndexBuildings()
        {
            return JsonConvert.SerializeObject(CalculatePriceIndexBuildings());
        }

        private Response GetHistoricalPriceIndices(long start, long end)
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                DateTime startTime = Utils.FromUnixTime(start);
                DateTime endTime = Utils.FromUnixTime(end);

                var indices = DB.PriceIndexModels
                    .AsNoTracking()
                    .Where(pim => startTime <= pim.TimeStamp && endTime >= pim.TimeStamp)
                    .ToList();
                return JsonConvert.SerializeObject(indices);
            }
        }
    }
}
#endif // WITH_MODULES