﻿#if WITH_MODULES
using System;
using System.Collections.Generic;
using System.Linq;

using Dijkstra.NET.Graph;
using Dijkstra.NET.ShortestPath;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules
{
    public class SystemStarsModule : NancyModule
    {
        public SystemStarsModule() : base("/systemstars")
        {
            this.Cacheable();

            Post("/", _ =>
            {
                this.EnforceAuthAdmin();
                return PostSystemStars();
            });

            Post("/worldsectors", _ =>
            {
                this.EnforceAuthAdmin();
                return PostWorldSectors();
            });

            Post("/star", _ =>
            {
                this.EnforceWriteAuth();
                return PostStar();
            });

            Get("/", _ =>
            {
                this.Cacheable();
                return GetSystemStars();
            });

            Get("/hash", _ =>
            {
                this.EnforceReadAuth();
                return null;
            });

            Get("/star/{star}", parameters =>
            {
                return GetStar(parameters.star);
            });

            Get("/worldsectors", _ =>
            {
                this.Cacheable();
                return GetWorldSectors();
            });

            Get("/worldsectors/hash", _ =>
            {
                this.EnforceReadAuth();
                return null;
            });

            Get("/jumpcount/{source}/{destination}", parameters =>
            {
                return GetJumpCount(parameters.source, parameters.destination);
            });

            Get("/jumproute/{source}/{destination}", parameters =>
            {
                return GetJumpRoute(parameters.source, parameters.destination);
            });
        }

        private Response PostSystemStars()
        {
            using (var req = new FIORequest<JSONRepresentations.SystemStarsdata.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;

                var allSystems = new List<Database.Models.System>();

                var stars = rootObj.payload.message.payload.stars;
                foreach (var star in stars)
                {
                    var model = new Database.Models.System();

                    model.SystemId = star.systemId;
                    model.Name = star.address.lines[0].entity.name;
                    model.NaturalId = star.address.lines[0].entity.naturalId;
                    model.Type = star.type;

                    model.PositionX = star.position.x;
                    model.PositionY = star.position.y;
                    model.PositionZ = star.position.z;

                    model.SectorId = star.sectorId;
                    model.SubSectorId = star.subSectorId;

                    foreach (var connection in star.connections)
                    {
                        SystemConnection sysConnection = new SystemConnection();

                        sysConnection.SystemConnectionId = $"{model.SystemId}-{connection}";
                        sysConnection.ConnectingId = connection;
                        sysConnection.SystemId = model.SystemId;

                        model.Connections.Add(sysConnection);
                    }

                    model.UserNameSubmitted = req.UserName;
                    model.Timestamp = req.Now;

                    model.Validate();
                    allSystems.Add(model);
                }

                req.DB.Systems.UpsertRange(allSystems)
                    .On(s => new { s.SystemId })
                    .Run();

                var allConnections = allSystems.SelectMany(s => s.Connections);
                req.DB.SystemConnections.UpsertRange(allConnections)
                    .On(s => new { s.SystemConnectionId })
                    .Run();

                req.DB.SaveChanges();
                JumpCalculator.Initialize(allSystems);
                return HttpStatusCode.OK;
            }
        }

        private Response PostWorldSectors()
        {
            using (var req = new FIORequest<JSONRepresentations.WorldSectors.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;

                var allSectors = new List<Sector>();

                var sectors = rootObj.payload.message.payload.sectors;
                foreach (var sector in sectors)
                {
                    Sector sec = new Sector();

                    sec.SectorId = sector.id;
                    sec.Name = sector.name;
                    sec.HexQ = sector.hex.q;
                    sec.HexR = sector.hex.r;
                    sec.HexS = sector.hex.s;
                    sec.Size = sector.size;

                    foreach (var subsector in sector.subsectors)
                    {
                        SubSector ss = new SubSector();

                        ss.SubSectorId = subsector.id;

                        // Order the vertices first
                        subsector.vertices = subsector.vertices.OrderBy(v => v.x).ThenBy(v => v.y).ThenBy(v => v.z).ToArray();

                        for (int vertexIdx = 0; vertexIdx < subsector.vertices.Length; ++vertexIdx)
                        {
                            var vertex = subsector.vertices[vertexIdx];

                            SubSectorVertex ssv = new SubSectorVertex();
                            ssv.SubSectorVertexId = $"{ss.SubSectorId}-{vertexIdx}";
                            ssv.X = vertex.x;
                            ssv.Y = vertex.y;
                            ssv.Z = vertex.z;
                            ssv.SubSectorId = ss.SubSectorId;

                            ss.Vertices.Add(ssv);
                        }

                        ss.SectorId = sec.SectorId;

                        sec.SubSectors.Add(ss);
                    }

                    sec.UserNameSubmitted = req.UserName;
                    sec.Timestamp = req.Now;

                    sec.Validate();
                    allSectors.Add(sec);
                }

                req.DB.Sectors.UpsertRange(allSectors)
                    .On(s => new { s.SectorId })
                    .Run();

                var allSubSectors = allSectors.SelectMany(s => s.SubSectors);
                req.DB.SubSectors.UpsertRange(allSubSectors)
                    .On(ss => new { ss.SubSectorId })
                    .Run();

                var allVerts = allSubSectors.SelectMany(ss => ss.Vertices);
                req.DB.SubSectorVertices.UpsertRange(allVerts)
                    .On(v => new { v.SubSectorVertexId })
                    .Run();

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response PostStar()
        {
            using (var req = new FIORequest<JSONRepresentations.SystemStar.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var jsonStarPayload = req.JsonPayload.payload.message.payload.body;

                var star = new SystemStar();
                star.SystemStarId = jsonStarPayload.address.lines[0].entity.id;
                star.SystemId = jsonStarPayload.address.lines[0].entity.id;
                star.SystemNaturalId = jsonStarPayload.address.lines[0].entity.naturalId;
                star.SystemName = jsonStarPayload.address.lines[0].entity.name;
                star.Type = jsonStarPayload.star.type;
                star.Luminosity = jsonStarPayload.star.luminosity;
                star.PositionX = jsonStarPayload.star.position.x;
                star.PositionY = jsonStarPayload.star.position.y;
                star.PositionZ = jsonStarPayload.star.position.z;
                star.SectorId = jsonStarPayload.star.sectorId;
                star.SubSectorId = jsonStarPayload.star.subSectorId;
                star.Mass = jsonStarPayload.star.mass;
                star.MassSol = jsonStarPayload.star.massSol;
                star.MeteoroidDensity = jsonStarPayload.meteoroidDensity;

                star.Validate();

                req.DB.SystemStars.Upsert(star)
                    .On(s => new { s.SystemStarId })
                    .Run();

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetSystemStars()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var systems = DB.Systems
                    .AsNoTracking()
                    .Include(s => s.Connections)
                    .AsSplitQuery()
                    .ToList();
                return JsonConvert.SerializeObject(systems);
            }
        }

        private Response GetStar(string Star)
        {
            Star = Star.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                var star = DB.SystemStars
                    .AsNoTracking()
                    .Where(ss => ss.SystemId.ToUpper() == Star || ss.SystemName.ToUpper() == Star || ss.SystemNaturalId.ToUpper() == Star)
                    .FirstOrDefault();
                if (star != null)
                {
                    return JsonConvert.SerializeObject(star);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response GetWorldSectors()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var worldSectors = DB.Sectors
                    .AsNoTracking()
                    .Include(s => s.SubSectors)
                        .ThenInclude(ss => ss.Vertices)
                    .AsSplitQuery()
                    .ToList();
                return JsonConvert.SerializeObject(worldSectors);
            }
        }

        enum JumpResponseType
        {
            Count = 0,
            Route = 1,
        }

        private Response GetJump(string source, string destination, JumpResponseType type)
        {
            if (String.IsNullOrWhiteSpace(source) || String.IsNullOrWhiteSpace(destination))
            {
                return NancyExtensionMethods.ReturnBadResponse(Request, new ArgumentException("Source and destination must both be specified"));
            }

            string VerifiedSource = JumpCalculator.GetSystemId(source);
            string VerifiedDestination = JumpCalculator.GetSystemId(destination);
            if (VerifiedSource != null && VerifiedDestination != null)
            {
                if (type == JumpResponseType.Count)
                {
                    return JumpCalculator.GetJumpCount(VerifiedSource, VerifiedDestination).ToString();
                }
                else
                {
                    System.Diagnostics.Debug.Assert(type == JumpResponseType.Route);
                    return JsonConvert.SerializeObject(JumpCalculator.GetRoute(VerifiedSource, VerifiedDestination));
                }
            }
            else
            {
                return NancyExtensionMethods.ReturnBadResponse(Request, new ArgumentException("Failed to resolve source or destination"));
            }
        }

        private Response GetJumpCount(string source, string destination)
        {
            return GetJump(source, destination, JumpResponseType.Count);
        }

        private Response GetJumpRoute(string source, string destination)
        {
            return GetJump(source, destination, JumpResponseType.Route);
        }
    }
}
#endif // WITH_MODULES