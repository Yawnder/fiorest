﻿#if WITH_MODULES
using System;
using System.Linq;

using FlexLabs.EntityFrameworkCore.Upsert;

using Nancy;
using Nancy.Extensions;
using Nancy.IO;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules
{
    public class CompanyModule : NancyModule
    {
        public CompanyModule() : base("/company")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return HttpStatusCode.OK;
                //return PostCompany();
            });

            Post("/data", _ =>
            {
                this.EnforceWriteAuth();
                return HttpStatusCode.OK;
                //return PostCompanyData();
            });

            Get("/code/{company_code}", parameters =>
            {
                return GetCompanyByCode(parameters.company_code);
            });

            Get("/name/{company_name}", parameters =>
            {
               // this.EnforceReadAuth();
                return GetCompanyByName(parameters.company_name);
            });

            Get("/id/{company_id}", parameters =>
            {
                return GetCompanyById(parameters.company_id);
            });
        }

        private Response GetCompanyByUser(string UserName)
        {
            return UserModule.GetUserByUserName(Request, UserName);
        }

        private Response GetCompanyByCode(string CompanyCode)
        {
            return UserModule.GetUserByCompanyCode(Request, CompanyCode);
        }

        private Response GetCompanyByName(string CompanyName)
        {
            return UserModule.GetUserByCompanyName(Request, CompanyName);
        }

        private Response GetCompanyById(string CompanyId)
        {
            return UserModule.GetUserByCompanyId(Request, CompanyId);
        }
    }
}
#endif // WITH_MODULES