﻿#if WITH_MODULES
using System.Collections.Generic;
using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules
{
    public class ShipModule : NancyModule
    {
        public ShipModule() : base("/ship")
        {

            Post("/ships", _ =>
            {
                this.EnforceWriteAuth();
                return PostShips();
            });

            Get("/ships/", _ =>
            {
                this.EnforceReadAuth();
                return GetShips();
            });

            Get("/ships/{user_name}", parameters =>
            {
                this.EnforceReadAuth();
                return GetShips(parameters.user_name);
            });

            Get("/ships/fuel/{user_name}", parameters =>
            {
                this.EnforceReadAuth();
                return GetShipsFuel(parameters.user_name);
            });

            Post("/flights", _ =>
            {
                this.EnforceWriteAuth();
                return PostFlights();
            });

            Get("/flights", _ =>
            {
                this.EnforceReadAuth();
                return GetFlights();
            });

            Get("/flights/{user_name}", parameters =>
            {
                this.EnforceReadAuth();
                return GetFlights(parameters.user_name);
            });
        }

        private Response PostShips()
        {
            using (var req = new FIORequest<JSONRepresentations.ShipShips.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;

                var ships = new List<Ship>();

                foreach (var shipObj in rootObj.payload.message.payload.ships)
                {
                    var ship = new Ship();
                    ship.ShipId = shipObj.id;
                    ship.StoreId = shipObj.idShipStore;
                    ship.StlFuelStoreId = shipObj.idStlFuelStore;
                    ship.FtlFuelStoreId = shipObj.idFtlFuelStore;
                    ship.Registration = shipObj.registration;
                    ship.Name = shipObj.name;
                    ship.CommissioningTimeEpochMs = shipObj.commissioningTime.timestamp;
                    ship.BlueprintNaturalId = shipObj.blueprintNaturalId;
                    ship.FlightId = shipObj.flightId;
                    ship.Acceleration = shipObj.acceleration;
                    ship.Thrust = shipObj.thrust;
                    ship.Mass = shipObj.mass;
                    ship.OperatingEmptyMass = shipObj.operatingEmptyMass;
                    ship.ReactorPower = shipObj.reactorPower;
                    ship.EmitterPower = shipObj.emitterPower;
                    ship.Volume = shipObj.volume;

                    ship.Condition = shipObj.condition;
                    ship.LastRepairEpochMs = shipObj.lastRepair?.millis;

                    foreach (var repairMat in shipObj.repairMaterials)
                    {
                        var rm = new ShipRepairMaterial();
                        rm.ShipRepairMaterialId = $"{ship.ShipId}-{repairMat.material.ticker}";
                        rm.MaterialName = repairMat.material.name;
                        rm.MaterialId = repairMat.material.id;
                        rm.MaterialTicker = repairMat.material.ticker;
                        rm.Amount = repairMat.amount;
                        rm.ShipId = ship.ShipId;

                        ship.RepairMaterials.Add(rm);
                    }

                    List<string> addressLines = new List<string>();
                    if (shipObj.address != null)
                    {
                        foreach (var line in shipObj.address.lines)
                        {
                            if (line.type == "SYSTEM" || line.type == "PLANET")
                            {
                                // Look up planet name
                                if (line.entity.naturalId != line.entity.name)
                                {
                                    addressLines.Add($"{line.entity.name} ({line.entity.naturalId})");
                                }
                                else
                                {
                                    addressLines.Add(line.entity.naturalId);
                                }
                            }
                            else
                            {
                                addressLines.Add(line.type);
                            }

                            var al = new ShipLocationAddressLine();
                            al.ShipLocationAddressLineId = $"{ship.ShipId}-{line.entity.id}";
                            al.LineId = line.entity.id;
                            al.LineType = line.type;
                            al.NaturalId = line.entity.naturalId;
                            al.Name = line.entity.name;
                            al.ShipId = ship.ShipId;
                            ship.AddressLines.Add(al);
                        }
                    }
                    ship.Location = string.Join(" - ", addressLines);

                    ship.StlFuelFlowRate = shipObj.stlFuelFlowRate;
                    ship.UserNameSubmitted = req.UserName;
                    ship.Timestamp = req.Now;

                    ship.Validate();
                    ships.Add(ship);
                }

                req.DB.Ships.RemoveRange(req.DB.Ships.Where(s => s.UserNameSubmitted.ToUpper() == req.UserName));
                req.PostRemoveRangeIgnoreConcurrencyFailures();

                req.DB.Ships.UpsertRange(ships)
                    .On(s => new { s.ShipId })
                    .Run();
                
                var shipRepairMaterials = ships.SelectMany(s => s.RepairMaterials);
                req.DB.ShipRepairMaterials.UpsertRange(shipRepairMaterials)
                    .On(srm => new { srm.ShipRepairMaterialId })
                    .Run();

                var shipAddressLines = ships.SelectMany(s => s.AddressLines);
                req.DB.ShipLocationAddressLines.UpsertRange(shipAddressLines)
                    .On(sal => new { sal.ShipLocationAddressLineId })
                    .Run();

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetShips(string UserName = null)
        {
            if (string.IsNullOrEmpty(UserName))
            {
                UserName = Request.GetUserName();
            }

            UserName = UserName.ToUpper();

            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Flight))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var ships = DB.Ships
                        .AsNoTracking()
                        .Include(s => s.RepairMaterials)
                        .Include(s => s.AddressLines)
                        .Where(s => s.UserNameSubmitted.ToUpper() == UserName)
                        .AsSplitQuery()
                        .ToList();
                    return JsonConvert.SerializeObject(ships);
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetShipsFuel(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequestUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequestUserName, UserName, Auth.PrivacyType.Flight))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var res = DB.Storages
                        .AsNoTracking()
                        .Include(s => s.StorageItems)
                        .Where(s => s.UserNameSubmitted.ToUpper() == UserName && (s.Type == "STL_FUEL_STORE" || s.Type == "FTL_FUEL_STORE"))
                        .AsSplitQuery()
                        .ToList();
                    return JsonConvert.SerializeObject(res);
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response PostFlights()
        {
            using (var req = new FIORequest<JSONRepresentations.ShipFlightFlights.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;
                var existingFlights = req.DB.Flights
                    .Where(m => m.UserNameSubmitted.ToUpper() == req.UserName)
                    .ToList();

                var data = rootObj.payload.message.payload;

                var flights = new List<Flight>();
                foreach (var flightObj in data.flights)
                {
                    var flight = new Flight();
                    flight.FlightId = flightObj.id;
                    flight.ShipId = flightObj.shipId;

                    var originLines = new List<string>();
                    foreach (var lines in flightObj.origin.lines)
                    {
                        if (lines.type == "SYSTEM" || lines.type == "PLANET")
                        {
                            if (lines.entity.name != lines.entity.naturalId)
                            {
                                originLines.Add($"{lines.entity.name} ({lines.entity.naturalId})");
                            }
                            else
                            {
                                originLines.Add(lines.entity.naturalId);
                            }
                        }
                        else
                        {
                            originLines.Add(lines.type);
                        }
                    }
                    flight.Origin = string.Join(" - ", originLines);


                    var destLines = new List<string>();
                    foreach (var lines in flightObj.destination.lines)
                    {
                        if (lines.type == "SYSTEM" || lines.type == "PLANET")
                        {
                            if (lines.entity.name != lines.entity.naturalId)
                            {
                                destLines.Add($"{lines.entity.name} ({lines.entity.naturalId})");
                            }
                            else
                            {
                                destLines.Add(lines.entity.naturalId);
                            }
                        }
                        else
                        {
                            destLines.Add(lines.type);
                        }
                    }
                    flight.Destination = string.Join(" - ", destLines);

                    flight.DepartureTimeEpochMs = flightObj.departure.timestamp;
                    flight.ArrivalTimeEpochMs = flightObj.arrival.timestamp;

                    for (int segmentIdx = 0; segmentIdx < flightObj.segments.Length; ++segmentIdx)
                    {
                        var segment = flightObj.segments[segmentIdx];
                        FlightSegment flightSegment = new FlightSegment();
                        flightSegment.FlightSegmentId = $"{flight.FlightId}-{segmentIdx}";

                        flightSegment.Type = segment.type;
                        flightSegment.DepartureTimeEpochMs = segment.departure.timestamp;
                        flightSegment.ArrivalTimeEpochMs = segment.arrival.timestamp;

                        flightSegment.StlDistance = segment.stlDistance;
                        flightSegment.StlFuelConsumption = segment.stlFuelConsumption;
                        flightSegment.FtlDistance = segment.ftlDistance;
                        flightSegment.FtlFuelConsumption = segment.ftlFuelConsumption;

                        if (segment.origin != null)
                        {
                            flightSegment.Origin = "";
                            foreach (var segLine in segment.origin.lines)
                            {
                                if (segLine.orbit != null)
                                {
                                    flightSegment.Origin += " Orbit -";
                                }
                                else
                                {
                                    string entityName = (segLine.entity.name == segLine.entity.naturalId) ? segLine.entity.naturalId : $"{segLine.entity.name} ({segLine.entity.naturalId})";
                                    flightSegment.Origin += $" {entityName} -";
                                }

                                if (segLine.entity != null)
                                {
                                    OriginLine line = new OriginLine();
                                    line.OriginLineId = $"{flightSegment.FlightSegmentId}-{segLine.entity.id}";
                                    line.Type = segLine.entity._type;
                                    line.LineId = segLine.entity.id;
                                    line.LineName = segLine.entity.name;
                                    line.LineNaturalId = segLine.entity.naturalId;
                                    line.FlightSegmentId = flightSegment.FlightSegmentId;
                                    flightSegment.OriginLines.Add(line);
                                }
                            }
                        }
                        flightSegment.Origin = flightSegment.Origin.Substring(0, flightSegment.Origin.Length - 2).Trim();

                        if (segment.destination != null)
                        {
                            flightSegment.Destination = "";
                            foreach (var segLine in segment.destination.lines)
                            {
                                if (segLine.orbit != null)
                                {
                                    flightSegment.Destination += " Orbit -";
                                }
                                else
                                {
                                    string entityName = (segLine.entity.name == segLine.entity.naturalId) ? segLine.entity.naturalId : $"{segLine.entity.name} ({segLine.entity.naturalId})";
                                    flightSegment.Destination += $" {entityName} -";
                                }

                                if (segLine.entity != null)
                                {
                                    DestinationLine line = new DestinationLine();
                                    line.DestinationLineId = $"{flightSegment.FlightSegmentId}-{segLine.entity.id}";
                                    line.Type = segLine.entity._type;
                                    line.LineId = segLine.entity.id;
                                    line.LineName = segLine.entity.name;
                                    line.LineNaturalId = segLine.entity.naturalId;
                                    line.FlightSegmentId = flightSegment.FlightSegmentId;
                                    flightSegment.DestinationLines.Add(line);
                                }
                            }
                        }
                        flightSegment.Destination = flightSegment.Destination.Substring(0, flightSegment.Destination.Length - 2).Trim();
                        flightSegment.FlightId = flight.FlightId;

                        flight.Segments.Add(flightSegment);
                    }

                    flight.CurrentSegmentIndex = flightObj.currentSegmentIndex;

                    flight.StlDistance = flightObj.stlDistance;
                    flight.FtlDistance = flightObj.ftlDistance;
                    flight.IsAborted = flightObj.aborted;

                    flight.UserNameSubmitted = req.UserName;
                    flight.Timestamp = req.Now;

                    flight.Validate();
                    flights.Add(flight);
                }
                
                req.DB.Flights.RemoveRange(existingFlights);
                req.PostRemoveRangeIgnoreConcurrencyFailures();

                req.DB.Flights.UpsertRange(flights)
                    .On(f => new { f.FlightId })
                    .Run();

                var flightSegments = flights.SelectMany(f => f.Segments).ToList();
                req.DB.FlightSegments.UpsertRange(flightSegments)
                    .On(fs => new { fs.FlightSegmentId })
                    .Run();

                var flightOriginLines = flightSegments.SelectMany(fs => fs.OriginLines);
                req.DB.OriginLines.UpsertRange(flightOriginLines)
                    .On(ol => new { ol.OriginLineId })
                    .Run();

                var flightDestinationLines = flightSegments.SelectMany(fs => fs.DestinationLines);
                req.DB.DestinationLines.UpsertRange(flightDestinationLines)
                    .On(dl => new { dl.DestinationLineId })
                    .Run();

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetFlights(string UserName = null)
        {
            if (UserName == null)
            {
                UserName = Request.GetUserName();
            }
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Flight))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var res = DB.Flights
                        .AsNoTracking()
                        .Include(f => f.Segments)
                            .ThenInclude(s => s.OriginLines)
                        .Include(f => f.Segments)
                            .ThenInclude(s => s.DestinationLines)
                        .Where(s => s.UserNameSubmitted.ToUpper() == UserName)
                        .AsSplitQuery()
                        .ToList();
                    return JsonConvert.SerializeObject(res);
                }
            }

            return HttpStatusCode.Unauthorized;
        }
    }
}
#endif // WITH_MODULES