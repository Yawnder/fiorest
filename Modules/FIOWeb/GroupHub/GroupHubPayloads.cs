﻿using System;
using System.Collections.Generic;

namespace FIORest.Modules.FIOWeb.GroupHub
{
    // The main model of the group hub
    public class GroupHubModel
    {
        public string GroupName { get; set; }

        public List<GroupCXWarehouse> CXWarehouses { get; set; } = new List<GroupCXWarehouse>();

        public List<PlayerShip> PlayerShipsInFlight { get; set; } = new List<PlayerShip>();
        public List<PlayerShip> PlayerStationaryShips { get; set; } = new List<PlayerShip>();

        public List<PlayerModel> PlayerModels { get; set; } = new List<PlayerModel>();

        public List<string> Failures { get; set; } = new List<string>();
    }

    // The model of the group views of the CX warehouses
    public class GroupCXWarehouse
    {
        public string WarehouseLocationName { get; set; }
        public string WarehouseLocationNaturalId { get; set; }

        public List<PlayerStorage> PlayerCXWarehouses { get; set; } = new List<PlayerStorage>();
    }

    // A model of an individual storage location for a player
    public class PlayerStorage
    {
        public string PlayerName { get; set; }

        public string StorageType { get; set; }

        public List<GroupHubInventoryItem> Items { get; set; } = new List<GroupHubInventoryItem>();

        public DateTime LastUpdated { get; set; }
    }

    // The model of an individual player
    public class PlayerModel
    {
        public string UserName { get; set; }

        public List<PlayerCurrency> Currencies { get; set; } = new List<PlayerCurrency>();

        public List<PlayerLocation> Locations { get; set; } = new List<PlayerLocation>();
    }

    // The model of one of a player's currencies (CIS, AIC, etc...)
    public class PlayerCurrency
    {
        public string CurrencyName { get; set; }

        public double Amount { get; set; }

        public DateTime LastUpdated { get; set; }
    }

    // The model of an individual location a player has some presense in
    public class PlayerLocation
    {
        public string LocationIdentifier { get; set; }
        public string LocationName { get; set; }

        public List<PlayerBuilding> Buildings { get; set; } = new List<PlayerBuilding>();

        public List<PlayerProductionLine> ProductionLines { get; set; } = new List<PlayerProductionLine>();

        public PlayerStorage BaseStorage { get; set; }

        public PlayerStorage WarehouseStorage { get; set; }

        public List<PlayerShip> StationaryPlayerShips { get; set; } = new List<PlayerShip>();
    }

    public class PlayerBuilding
    {
        public string BuildingName { get; set; }
        public string BuildingTicker { get; set; }

        public double Condition { get; set; }
        public List<GroupHubInventoryItem> RepairMaterials { get; set; }
    }

    public class PlayerProductionLine
    {
        public string BuildingName { get; set; }
        public string BuildingTicker { get; set; }

        public int Capacity { get; set; }
        public double Efficiency { get; set; }
        public double Condition { get; set; }

        public List<PlayerProductionOrder> ProductionOrders { get; set; } = new List<PlayerProductionOrder>();
    }

    // The model of an individual production order in progress at a PlayerLocation
    public class PlayerProductionOrder
    {
        public bool Started { get; set; }
        public bool Halted { get; set; }
        public bool Recurring { get; set; }

        public TimeSpan OrderDuration { get; set; }
        public DateTime? TimeCompletion { get; set; }

        public List<GroupHubInventoryItem> Inputs { get; set; } = new List<GroupHubInventoryItem>();
        public List<GroupHubInventoryItem> Outputs { get; set; } = new List<GroupHubInventoryItem>();

        public DateTime LastUpdated { get; set; }
    }

    // A model of an individual inventory item of a player.
    public class GroupHubInventoryItem
    {
        public string MaterialTicker { get; set; }
        public string MaterialName { get; set; }
        public string MaterialCategoryName { get; set; }

        public int Units { get; set; }
    }

    // A model of one of a player's ships
    public class PlayerShip
    {
        public string PlayerName { get; set; }

        public string ShipName { get; set; }
        public string ShipRegistration { get; set; }

        public string Location { get; set; }
        public string Destination { get; set; }

        public DateTime? LocationETA { get; set; }

        public DateTime? LocationETALocalTime
        {
            get
            {
                if (LocationETA != null)
                {
                    return ((DateTime)LocationETA).ToLocalTime();
                }

                return null;
            }
        }

        public double Condition { get; set; }

        public List<ShipRepairMaterial> RepairMaterials { get; set; } = new List<ShipRepairMaterial>();
        public List<ShipAddressLine> AddressLines { get; set; } = new List<ShipAddressLine>();
        public ShipFlight Flight { get; set; } = null;
        public Fuel Fuel { get; set; } = new Fuel();
        public PlayerStorage Cargo { get; set; } = new PlayerStorage();

        public DateTime LastUpdated { get; set; }
    }

    public class ShipFlight
    {
        public List<ShipFlightSegment> Segments { get; set; } = new List<ShipFlightSegment>();
        public string FlightId { get; set; }
        public string ShipId { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public long DepartureTimeEpochMs { get; set; }
        public long ArrivalTimeEpochMs { get; set; }
        public int CurrentSegmentIndex { get; set; }
        public double StlDistance { get; set; }
        public double FtlDistance { get; set; }
        public bool IsAborted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class ShipFlightSegment
    {
        public List<ShipFlightLine> OriginLines { get; set; } = new List<ShipFlightLine>();
        public List<ShipFlightLine> DestinationLines { get; set; } = new List<ShipFlightLine>();
        public string Type { get; set; }
        public long DepartureTimeEpochMs { get; set; }
        public long ArrivalTimeEpochMs { get; set; }
        public double? StlDistance { get; set; }
        public double? StlFuelConsumption { get; set; }
        public double? FtlDistance { get; set; }
        public double? FtlFuelConsumption { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
    }

    public class ShipFlightLine
    {
        public string Type { get; set; }
        public string LineId { get; set; }
        public string LineNaturalId { get; set; }
        public string LineName { get; set; }
    }

    public class ShipRepairMaterial
    {
        public string MaterialTicker { get; set; }
        public int Amount { get; set; }
    }

    public class ShipAddressLine
    {
        public string LineId { get; set; }
        public string LineType { get; set; }
        public string NaturalId { get; set; }
        public string Name { get; set; }
    }

    // A representation of a ship's fuel.
    public class Fuel
    {
        public int CurrentSF { get; set; }
        public int MaxSF { get; set; }

        public int CurrentFF { get; set; }
        public int MaxFF { get; set; }
    }
}