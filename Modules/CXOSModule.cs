﻿#if WITH_MODULES
using System.Collections.Generic;
using System.Linq;

using Microsoft.EntityFrameworkCore;

using Nancy;
using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class CXOSModule : NancyModule
    {
        public CXOSModule() : base("/cxos")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostCXOS();
            });

            Post("/added", _ =>
            {
                this.EnforceWriteAuth();
                return PostCXOSAdded();
            });

            Post("/removed", _ =>
            {
                this.EnforceWriteAuth();
                return PostCXOSRemoved();
            });

            Post("/updated", _ =>
            {
                this.EnforceWriteAuth();
                return PostCXOSUpdated();
            });

            Get("/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetCXOS(parameters.username);
            });
        }

        private Response PostCXOS()
        {
            using (var req = new FIORequest<JSONRepresentations.JsonComexTraderOrders.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var allOrders = new List<CXOSTradeOrder>();

                var orders = req.JsonPayload.payload.message.payload.orders;
                foreach (var order in orders)
                {
                    var newOrder = new CXOSTradeOrder();
                    newOrder.CXOSTradeOrderId = order.id;

                    newOrder.ExchangeName = order.exchange.name;
                    newOrder.ExchangeCode = order.exchange.code;

                    newOrder.BrokerId = order.brokerId;
                    newOrder.OrderType = order.type;
                    newOrder.MaterialName = order.material.name;
                    newOrder.MaterialTicker = order.material.ticker;
                    newOrder.MaterialId = order.material.id;

                    newOrder.Amount = order.amount;
                    newOrder.InitialAmount = order.initialAmount;

                    newOrder.Limit = order.limit.amount;
                    newOrder.LimitCurrency = order.limit.currency;

                    newOrder.Status = order.status;
                    newOrder.CreatedEpochMs = order.created.timestamp;

                    foreach (var trade in order.trades)
                    {
                        var newTrade = new CXOSTrade();

                        newTrade.CXOSTradeId = trade.id;
                        newTrade.Amount = trade.amount;
                        newTrade.Price = trade.price.amount;
                        newTrade.PriceCurrency = trade.price.currency;

                        newTrade.TradeTimeEpochMs = trade.time.timestamp;

                        newTrade.PartnerId = trade.partner.id;
                        newTrade.PartnerName = trade.partner.name;
                        newTrade.PartnerCode = trade.partner.code;
                        newTrade.CXOSTradeOrderId = newOrder.CXOSTradeOrderId;

                        newOrder.Trades.Add(newTrade);
                    }

                    newOrder.UserNameSubmitted = req.UserName;
                    newOrder.Timestamp = req.Now;

                    newOrder.Validate();

                    allOrders.Add(newOrder);
                }

                req.DB.CXOSTradeOrders.RemoveRange(req.DB.CXOSTradeOrders.Where(to => to.UserNameSubmitted == req.UserName));
                req.PostRemoveRangeIgnoreConcurrencyFailures();

                req.DB.CXOSTradeOrders.UpsertRange(allOrders)
                    .On(cto => new { cto.CXOSTradeOrderId })
                    .Run();

                var allTrades = allOrders.SelectMany(o => o.Trades).ToList();
                req.DB.CXOSTrades.UpsertRange(allTrades)
                    .On(ct => new { ct.CXOSTradeId })
                    .Run();

                req.SaveChangesIgnoreConcurrencyFailures();
                return HttpStatusCode.OK;
            }
        }

        private Response PostCXOSAdded()
        {
            using (var req = new FIORequest<JSONRepresentations.JsonComexTraderOrderAdded.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload;

                var model = new CXOSTradeOrder();
                model.CXOSTradeOrderId = payload.id;
                model.ExchangeName = payload.exchange.name;
                model.ExchangeCode = payload.exchange.code;

                model.BrokerId = payload.brokerId;
                model.OrderType = payload.type;
                model.MaterialName = payload.material.name;
                model.MaterialTicker = payload.material.ticker;
                model.MaterialId = payload.material.id;

                model.Amount = payload.amount;
                model.InitialAmount = payload.initialAmount;

                model.Limit = payload.limit.amount;
                model.LimitCurrency = payload.limit.currency;

                model.Status = payload.status;
                model.CreatedEpochMs = payload.created.timestamp;

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                model.Validate();

                req.DB.Upsert(model)
                    .On(c => new { c.CXOSTradeOrderId })
                    .Run();

                req.DB.SaveChanges();

                return HttpStatusCode.OK;
            }
        }

        private Response PostCXOSRemoved()
        {
            using (var req = new FIORequest<JSONRepresentations.JsonComexTraderOrderRemoved.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var orderId = req.JsonPayload.payload.message.payload.orderId;
                var model = req.DB.CXOSTradeOrders.Where(c => c.CXOSTradeOrderId.ToUpper() == orderId.ToUpper()).FirstOrDefault();
                if (model != null)
                {
                    req.DB.CXOSTradeOrders.Remove(model);
                    req.SaveChangesIgnoreConcurrencyFailures();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostCXOSUpdated()
        {
            using (var req = new FIORequest<JSONRepresentations.JsonComexTraderOrderUpdated.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload;

                var model = new CXOSTradeOrder();
                model.CXOSTradeOrderId = payload.id;
                model.ExchangeName = payload.exchange.name;
                model.ExchangeCode = payload.exchange.code;

                model.BrokerId = payload.brokerId;
                model.OrderType = payload.type;
                model.MaterialName = payload.material.name;
                model.MaterialTicker = payload.material.ticker;
                model.MaterialId = payload.material.id;

                model.Amount = payload.amount;
                model.InitialAmount = payload.initialAmount;

                model.Limit = payload.limit.amount;
                model.LimitCurrency = payload.limit.currency;

                model.Status = payload.status;
                model.CreatedEpochMs = payload.created.timestamp;

                foreach (var trade in payload.trades)
                {
                    var newTrade = new CXOSTrade();

                    newTrade.CXOSTradeId = trade.id;
                    newTrade.Amount = trade.amount;
                    newTrade.Price = trade.price.amount;
                    newTrade.PriceCurrency = trade.price.currency;

                    newTrade.TradeTimeEpochMs = trade.time.timestamp;

                    newTrade.PartnerId = trade.partner.id;
                    newTrade.PartnerName = trade.partner.name;
                    newTrade.PartnerCode = trade.partner.code;

                    newTrade.CXOSTradeOrderId = model.CXOSTradeOrderId;

                    model.Trades.Add(newTrade);
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                model.Validate();

                req.DB.CXOSTradeOrders.Upsert(model)
                    .On(c => new { c.CXOSTradeOrderId })
                    .Run();

                req.DB.CXOSTrades.UpsertRange(model.Trades)
                    .On(ct => new { ct.CXOSTradeId })
                    .Run();

                req.SaveChangesIgnoreConcurrencyFailures();

                return HttpStatusCode.OK;
            }
        }

        private Response GetCXOS(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Contracts))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var res = DB.CXOSTradeOrders
                        .AsNoTracking()
                        .Include(cxto => cxto.Trades)
                        .Where(c => c.UserNameSubmitted.ToUpper() == UserName)
                        .ToList();
                    if (res != null)
                    {
                        return JsonConvert.SerializeObject(res);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }
    }
}
#endif // WITH_MODULES