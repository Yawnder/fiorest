﻿#if WITH_MODULES
using System;
using System.Collections.Generic;
using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules
{
    public class LocalMarketModule : NancyModule
    {
        public LocalMarketModule() : base("/localmarket")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostLocalMarket();
            });

            Get("/{local_market_id}", parameters =>
            {
                return GetLocalMarket(parameters.local_market_id);
            });

            Get("/ad/{planet_natural_id}/{contract_natural_id:int}", parameters =>
            {
                return GetAd(parameters.planet_natural_id, parameters.contract_natural_id);
            });

            Get("/planet/{planet}", parameters =>
            {
                return GetAds(parameters.planet);
            });

            Get("/planet/{planet}/{type}", parameters =>
            {
                return GetAds(parameters.planet, parameters.type);
            });

            Get("/shipping/source/{source}", parameters =>
            {
                return GetAdsWithSource(parameters.source);
            });

            Get("/shipping/destination/{destination}", parameters =>
            {
                return GetAdsWithDestination(parameters.destination);
            });

            Get("/shipping/all/{planet}", parameters =>
            {
                return GetAllShippingAds(parameters.planet);
            });

            Get("/company/{company_name}", parameters =>
            {
                return GetAdsForCompany(parameters.company_name);
            });

            Post("/search", _ =>
            {
                return PostSearch();
            });
        }

        private Response PostLocalMarket()
        {
            using (var req = new FIORequest<JSONRepresentations.LocalMarket.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;

                string MarketId = rootObj.payload.message.payload.path[1];

                var data = rootObj.payload.message.payload.body;
                var model = new LocalMarket();
                model.LocalMarketId = MarketId;

                foreach (var ad in data)
                {
                    switch (ad.type)
                    {
                        case "COMMODITY_SHIPPING":
                            {
                                ShippingAd shipAd = new ShippingAd();
                                shipAd.ShippingAdId = $"{model.LocalMarketId}-{ad.naturalId}";
                                shipAd.ContractNaturalId = ad.naturalId;

                                shipAd.PlanetId = ad.address.lines[1].entity.id;
                                shipAd.PlanetNaturalId = ad.address.lines[1].entity.naturalId;
                                shipAd.PlanetName = ad.address.lines[1].entity.name;

                                shipAd.OriginPlanetId = ad.origin.lines[1].entity.id;
                                shipAd.OriginPlanetNaturalId = ad.origin.lines[1].entity.naturalId;
                                shipAd.OriginPlanetName = ad.origin.lines[1].entity.name;

                                shipAd.DestinationPlanetId = ad.destination.lines[1].entity.id;
                                shipAd.DestinationPlanetNaturalId = ad.destination.lines[1].entity.naturalId;
                                shipAd.DestinationPlanetName = ad.destination.lines[1].entity.name;

                                shipAd.CargoWeight = ad.cargoWeight;
                                shipAd.CargoVolume = ad.cargoVolume;

                                shipAd.CreatorCompanyId = ad.creator.id;
                                shipAd.CreatorCompanyName = ad.creator.name;
                                shipAd.CreatorCompanyCode = ad.creator.code;

                                shipAd.PayoutPrice = ad.price.amount;
                                shipAd.PayoutCurrency = ad.price.currency;

                                shipAd.DeliveryTime = ad.advice;

                                shipAd.CreationTimeEpochMs = ad.creationTime.timestamp;
                                shipAd.ExpiryTimeEpochMs = ad.expiry.timestamp;

                                shipAd.MinimumRating = ad.minimumRating;
                                shipAd.LocalMarketId = MarketId;

                                model.ShippingAds.Add(shipAd);
                            }
                            break;
                        case "COMMODITY_BUYING":
                            {
                                BuyingAd buyAd = new BuyingAd();
                                buyAd.BuyingAdId = $"{model.LocalMarketId}-{ad.naturalId}";
                                buyAd.ContractNaturalId = ad.naturalId;

                                buyAd.PlanetId = ad.address.lines[1].entity.id;
                                buyAd.PlanetNaturalId = ad.address.lines[1].entity.naturalId;
                                buyAd.PlanetName = ad.address.lines[1].entity.name;

                                buyAd.CreatorCompanyId = ad.creator.id;
                                buyAd.CreatorCompanyName = ad.creator.name;
                                buyAd.CreatorCompanyCode = ad.creator.code;

                                buyAd.MaterialId = ad.quantity.material.id;
                                buyAd.MaterialName = ad.quantity.material.name;
                                buyAd.MaterialTicker = ad.quantity.material.ticker;
                                buyAd.MaterialCategory = ad.quantity.material.category;
                                buyAd.MaterialWeight = ad.quantity.material.weight;
                                buyAd.MaterialVolume = ad.quantity.material.volume;

                                buyAd.MaterialAmount = ad.quantity.amount;

                                buyAd.Price = ad.price.amount;
                                buyAd.PriceCurrency = ad.price.currency;

                                buyAd.DeliveryTime = ad.advice;

                                buyAd.CreationTimeEpochMs = ad.creationTime.timestamp;
                                buyAd.ExpiryTimeEpochMs = ad.expiry.timestamp;

                                buyAd.MinimumRating = ad.minimumRating;
                                buyAd.LocalMarketId = MarketId;

                                model.BuyingAds.Add(buyAd);
                            }
                            break;
                        case "COMMODITY_SELLING":
                            {
                                SellingAd sellAd = new SellingAd();
                                sellAd.SellingAdId = $"{model.LocalMarketId}-{ad.naturalId}";
                                sellAd.ContractNaturalId = ad.naturalId;

                                sellAd.PlanetId = ad.address.lines[1].entity.id;
                                sellAd.PlanetNaturalId = ad.address.lines[1].entity.naturalId;
                                sellAd.PlanetName = ad.address.lines[1].entity.name;

                                sellAd.CreatorCompanyId = ad.creator.id;
                                sellAd.CreatorCompanyName = ad.creator.name;
                                sellAd.CreatorCompanyCode = ad.creator.code;

                                sellAd.MaterialId = ad.quantity.material.id;
                                sellAd.MaterialName = ad.quantity.material.name;
                                sellAd.MaterialTicker = ad.quantity.material.ticker;
                                sellAd.MaterialCategory = ad.quantity.material.category;
                                sellAd.MaterialWeight = ad.quantity.material.weight;
                                sellAd.MaterialVolume = ad.quantity.material.volume;

                                sellAd.MaterialAmount = ad.quantity.amount;

                                sellAd.Price = ad.price.amount;
                                sellAd.PriceCurrency = ad.price.currency;

                                sellAd.DeliveryTime = ad.advice;

                                sellAd.CreationTimeEpochMs = ad.creationTime.timestamp;
                                sellAd.ExpiryTimeEpochMs = ad.expiry.timestamp;

                                sellAd.MinimumRating = ad.minimumRating;
                                sellAd.LocalMarketId = MarketId;

                                model.SellingAds.Add(sellAd);
                            }
                            break;
                    }
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                model.Validate();

                req.DB.LocalMarkets.RemoveRange(req.DB.LocalMarkets.Where(lm => lm.LocalMarketId == MarketId));
                req.PostRemoveRangeIgnoreConcurrencyFailures();

                req.DB.LocalMarkets.Upsert(model)
                    .On(lm => new { lm.LocalMarketId })
                    .Run();
                
                req.DB.ShippingAds.UpsertRange(model.ShippingAds)
                    .On(sa => new { sa.ShippingAdId })
                    .Run();
                
                req.DB.BuyingAds.UpsertRange(model.BuyingAds)
                    .On(ba => new { ba.BuyingAdId })
                    .Run();
                
                req.DB.SellingAds.UpsertRange(model.SellingAds)
                    .On(sa => new { sa.SellingAdId })
                    .Run();

                req.SaveChangesIgnoreConcurrencyFailures();
                return HttpStatusCode.OK;
            }
        }

        private Response GetLocalMarket(string MarketId)
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var model = DB.LocalMarkets
                    .AsNoTracking()
                    .Include(lm => lm.ShippingAds)
                    .Include(lm => lm.BuyingAds)
                    .Include(lm => lm.SellingAds)
                    .Where(lm => lm.LocalMarketId == MarketId)
                    .FirstOrDefault();
                if (model != null)
                {
                    return JsonConvert.SerializeObject(model);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response GetAd(string PlanetNaturalId, int ContractNaturalId)
		{
            PlanetNaturalId = PlanetNaturalId.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                var shippingAd = DB.ShippingAds
                    .AsNoTracking()
                    .FirstOrDefault(sa => sa.PlanetNaturalId.ToUpper() == PlanetNaturalId && sa.ContractNaturalId == ContractNaturalId);
                if (shippingAd != null)
                {
                    return JsonConvert.SerializeObject(shippingAd);
                }

                var buyingAd = DB.BuyingAds
                    .AsNoTracking()
                    .FirstOrDefault(ba => ba.PlanetNaturalId.ToUpper() == PlanetNaturalId && ba.ContractNaturalId == ContractNaturalId);
                if (buyingAd != null)
                {
                    return JsonConvert.SerializeObject(buyingAd);
                }

                var sellingAd = DB.SellingAds
                    .AsNoTracking()
                    .FirstOrDefault(sa => sa.PlanetNaturalId.ToUpper() == PlanetNaturalId && sa.ContractNaturalId == ContractNaturalId);
                if (sellingAd != null)
                {
                    return JsonConvert.SerializeObject(sellingAd);
                }
            }

            return HttpStatusCode.NoContent;
		}

        protected class AdsResult
        {
            public List<ShippingAd> ShippingAds;
            public List<BuyingAd> BuyingAds;
            public List<SellingAd> SellingAds;
        }


        private Response GetAds(string Planet)
        {
            Planet = Planet.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                AdsResult res = new AdsResult();
                res.ShippingAds = DB.ShippingAds
                    .AsNoTracking()
                    .Where(a => a.PlanetId.ToUpper() == Planet || a.PlanetNaturalId.ToUpper() == Planet || a.PlanetName.ToUpper() == Planet)
                    .ToList();
                res.BuyingAds = DB.BuyingAds
                    .AsNoTracking()
                    .Where(a => a.PlanetId.ToUpper() == Planet || a.PlanetNaturalId.ToUpper() == Planet || a.PlanetName.ToUpper() == Planet)
                    .ToList();
                res.SellingAds = DB.SellingAds
                    .AsNoTracking()
                    .Where(a => a.PlanetId.ToUpper() == Planet || a.PlanetNaturalId.ToUpper() == Planet || a.PlanetName.ToUpper() == Planet)
                    .ToList();
                if (res.ShippingAds.Count > 0 || res.BuyingAds.Count > 0 || res.SellingAds.Count > 0)
                {
                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response GetAds(string Planet, string Type)
        {
            Planet = Planet.ToUpper();
            Type = Type.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                switch (Type)
                {
                    case "BUY":
                    case "BUYS":
                    case "BUYING":
                        {
                            var res = DB.BuyingAds
                                .AsNoTracking()
                                .Where(a => a.PlanetId.ToUpper() == Planet || a.PlanetNaturalId.ToUpper() == Planet || a.PlanetName.ToUpper() == Planet)
                                .ToList();
                            if (res.Count > 0)
                            {
                                return JsonConvert.SerializeObject(res);
                            }
                        }
                        break;
                    case "SELL":
                    case "SELLS":
                    case "SELLING":
                        {
                            var res = DB.SellingAds
                                .AsNoTracking()
                                .Where(a => a.PlanetId.ToUpper() == Planet || a.PlanetNaturalId.ToUpper() == Planet || a.PlanetName.ToUpper() == Planet)
                                .ToList();
                            if (res.Count > 0)
                            {
                                return JsonConvert.SerializeObject(res);
                            }
                        }
                        break;
                    case "SHIP":
                    case "SHIPPING":
                        {
                            var res = DB.ShippingAds
                                .AsNoTracking()
                                .Where(a => a.PlanetId.ToUpper() == Planet || a.PlanetNaturalId.ToUpper() == Planet || a.PlanetName.ToUpper() == Planet)
                                .ToList();
                            if (res.Count > 0)
                            {
                                return JsonConvert.SerializeObject(res);
                            }
                        }
                        break;
                }

                return HttpStatusCode.NoContent;
            }
        }

        public class ShippingAdResults : ShippingAd
        {
            public bool SourceIsGaseous { get; set; }
            public bool DestinationIsGaseous { get; set; }

            public int JumpDistance { get; set; }
        }

        public Response GetAdsWithSource(string SourcePlanet)
        {
            SourcePlanet = SourcePlanet.ToUpper();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var shippingAds = DB.ShippingAds
                    .AsNoTracking()
                    .Where(a => a.OriginPlanetId.ToUpper() == SourcePlanet || a.OriginPlanetName.ToUpper() == SourcePlanet || a.OriginPlanetNaturalId.ToUpper() == SourcePlanet)
                    .ToList();
                if (shippingAds.Count > 0)
                {
                    // Serialize/de-serialize into ShippingAdResults
                    List<ShippingAdResults> results = JsonConvert.DeserializeObject<List<ShippingAdResults>>(JsonConvert.SerializeObject(shippingAds));

                    string VerifiedSource = JumpCalculator.GetSystemId(SourcePlanet, DB);
                    for (int i = 0; i < results.Count; ++i)
                    {
                        string VerifiedDestination = JumpCalculator.GetSystemId(results[i].DestinationPlanetNaturalId, DB);
                        if (VerifiedSource != null && VerifiedDestination != null)
                        {
                            results[i].JumpDistance = JumpCalculator.GetJumpCount(VerifiedSource, VerifiedDestination, DB);
                        }

                        results[i].SourceIsGaseous = DB.PlanetDataModels
                            .AsNoTracking()
                            .Where(pdm => pdm.PlanetId == results[i].OriginPlanetId).Select(pdm => pdm.Surface == false)
                            .FirstOrDefault();
                        results[i].DestinationIsGaseous = DB.PlanetDataModels
                            .AsNoTracking()
                            .Where(pdm => pdm.PlanetId == results[i].DestinationPlanetId)
                            .Select(pdm => pdm.Surface == false)
                            .FirstOrDefault();
                    }

                    return JsonConvert.SerializeObject(results);
                }

                return HttpStatusCode.NoContent;
            }
        }

        public Response GetAdsWithDestination(string DestinationPlanet)
        {
            DestinationPlanet = DestinationPlanet.ToUpper();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var shippingAds = DB.ShippingAds
                    .AsNoTracking()
                    .Where(a => a.DestinationPlanetId.ToUpper() == DestinationPlanet || a.DestinationPlanetName.ToUpper() == DestinationPlanet || a.DestinationPlanetNaturalId.ToUpper() == DestinationPlanet)
                    .ToList();
                if (shippingAds.Count > 0)
                {
                    // Serialize/de-serialize into ShippingAdResults
                    List<ShippingAdResults> results = JsonConvert.DeserializeObject<List<ShippingAdResults>>(JsonConvert.SerializeObject(shippingAds));

                    string VerifiedDestination = JumpCalculator.GetSystemId(DestinationPlanet, DB);
                    for (int i = 0; i < results.Count; ++i)
                    {
                        string VerifiedSource = JumpCalculator.GetSystemId(results[i].OriginPlanetNaturalId, DB);
                        if (VerifiedSource != null && VerifiedDestination != null)
                        {
                            results[i].JumpDistance = JumpCalculator.GetJumpCount(VerifiedSource, VerifiedDestination, DB);
                        }

                        results[i].SourceIsGaseous = DB.PlanetDataModels
                            .AsNoTracking()
                            .Where(pdm => pdm.PlanetId == results[i].OriginPlanetId)
                            .Select(pdm => pdm.Surface == false)
                            .FirstOrDefault();
                        results[i].DestinationIsGaseous = DB.PlanetDataModels
                            .AsNoTracking()
                            .Where(pdm => pdm.PlanetId == results[i].DestinationPlanetId)
                            .Select(pdm => pdm.Surface == false)
                            .FirstOrDefault();
                    }

                    return JsonConvert.SerializeObject(results);
                }

                return HttpStatusCode.NoContent;
            }
        }

        private Response GetAllShippingAds(string Planet)
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var allShippingAds = DB.ShippingAds
                    .AsNoTracking()
                    .ToList();
                List<ShippingAdResults> results = JsonConvert.DeserializeObject<List<ShippingAdResults>>(JsonConvert.SerializeObject(allShippingAds));
                string VerifiedSource = JumpCalculator.GetSystemId(Planet, DB);
                for (int i = 0; i < results.Count; ++i)
                {
                    string VerifiedDestination = JumpCalculator.GetSystemId(results[i].PlanetNaturalId, DB);
                    if (VerifiedSource != null && VerifiedDestination != null)
                    {
                        results[i].JumpDistance = JumpCalculator.GetJumpCount(VerifiedSource, VerifiedDestination, DB);
                    }

                    results[i].SourceIsGaseous = DB.PlanetDataModels
                        .AsNoTracking()
                        .Where(pdm => pdm.PlanetId == results[i].OriginPlanetId)
                        .Select(pdm => pdm.Surface == false)
                        .FirstOrDefault();
                    results[i].DestinationIsGaseous = DB.PlanetDataModels
                        .AsNoTracking()
                        .Where(pdm => pdm.PlanetId == results[i].DestinationPlanetId)
                        .Select(pdm => pdm.Surface == false)
                        .FirstOrDefault();
                }

                return JsonConvert.SerializeObject(results);
            }
        }

        class AllCompanyAds
        {
            public List<BuyingAd> BuyingAds { get; set; } = new List<BuyingAd>();
            public List<SellingAd> SellingAds { get; set; } = new List<SellingAd>();
            public List<ShippingAd> ShippingAds { get; set; } = new List<ShippingAd>();
        }

        public Response GetAdsForCompany(string CompanyName)
        {
            CompanyName = CompanyName.ToUpper();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var buyAds = DB.BuyingAds
                    .AsNoTracking()
                    .Where(a => a.CreatorCompanyCode.ToUpper() == CompanyName || a.CreatorCompanyId.ToUpper() == CompanyName || a.CreatorCompanyName.ToUpper() == CompanyName)
                    .ToList();
                var sellAds = DB.SellingAds
                    .AsNoTracking()
                    .Where(a => a.CreatorCompanyCode.ToUpper() == CompanyName || a.CreatorCompanyId.ToUpper() == CompanyName || a.CreatorCompanyName.ToUpper() == CompanyName)
                    .ToList();
                var shippingAds = DB.ShippingAds
                    .AsNoTracking()
                    .Where(a => a.CreatorCompanyCode.ToUpper() == CompanyName || a.CreatorCompanyId.ToUpper() == CompanyName || a.CreatorCompanyName.ToUpper() == CompanyName)
                    .ToList();
                if (buyAds.Count > 0 || sellAds.Count > 0 || shippingAds.Count > 0)
                {
                    AllCompanyAds all = new AllCompanyAds();
                    all.BuyingAds = buyAds;
                    all.SellingAds = sellAds;
                    all.ShippingAds = shippingAds;
                    return JsonConvert.SerializeObject(all);
                }

                return HttpStatusCode.NoContent;
            }
        }

        class LocalMarketSearchParameters
        {
            public bool SearchBuys { get; set; } = true;
            public bool SearchSells { get; set; } = true;
            public string Ticker { get; set; } = null;
            public double CostThreshold { get; set; } = 1.0;
            public string SourceLocation { get; set; } = null;
        }

        class LocalMarketAd
        {
            public LocalMarketAd(BuyingAd ad)
            {
                ContractNaturalId = ad.ContractNaturalId;
                PlanetName = ad.PlanetName;
                PlanetNaturalId = ad.PlanetNaturalId;
                CreatorCompanyCode = ad.CreatorCompanyCode;
                CreatorCompanyName = ad.CreatorCompanyName;
                MaterialTicker = ad.MaterialTicker;
                MaterialName = ad.MaterialName;
                MaterialAmount = ad.MaterialAmount;
                Weight = ad.MaterialWeight;
                Volume = ad.MaterialVolume;
                Price = ad.Price;
                Currency = ad.PriceCurrency;
                DeliveryTime = ad.DeliveryTime;
                MinimumRating = ad.MinimumRating;
            }

            public LocalMarketAd(SellingAd ad)
            {
                ContractNaturalId = ad.ContractNaturalId;
                PlanetName = ad.PlanetName;
                PlanetNaturalId = ad.PlanetNaturalId;
                CreatorCompanyCode = ad.CreatorCompanyCode;
                CreatorCompanyName = ad.CreatorCompanyName;
                MaterialTicker = ad.MaterialTicker;
                MaterialName = ad.MaterialName;
                MaterialAmount = ad.MaterialAmount;
                Weight = ad.MaterialWeight;
                Volume = ad.MaterialVolume;
                Price = ad.Price;
                Currency = ad.PriceCurrency;
                DeliveryTime = ad.DeliveryTime;
                MinimumRating = ad.MinimumRating;
            }

            public int ContractNaturalId { get; set; }

            public string PlanetName { get; set; }
            public string PlanetNaturalId { get; set; }

            public string CreatorCompanyCode { get; set; }
            public string CreatorCompanyName { get; set; }

            public string MaterialTicker { get; set; }
            public string MaterialName { get; set; }
            public int MaterialAmount { get; set; }

            public double Weight { get; set; }
            public double Volume { get; set; }

            public double Price { get; set; }
            public string Currency { get; set; }

            public int DeliveryTime { get; set; }
            public string MinimumRating { get; set; }

            public int JumpCount { get; set; } = -1;
        }

        class LocalMarketSearchResult
        {
            public List<LocalMarketAd> BuyingAds { get; set; } = new List<LocalMarketAd>();
            public List<LocalMarketAd> SellingAds { get; set; } = new List<LocalMarketAd>();
        }

        public Response PostSearch()
        {
            using (var req = new FIORequest<LocalMarketSearchParameters>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var searchParams = req.JsonPayload;
                string SourceSystemId = !string.IsNullOrWhiteSpace(searchParams.SourceLocation) ? JumpCalculator.GetSystemId(searchParams.SourceLocation, req.DB) : null;

                var res = new LocalMarketSearchResult();
                if (!string.IsNullOrWhiteSpace(searchParams.Ticker))
                {
                    var ticker = searchParams.Ticker.ToUpper();
                    if (searchParams.SearchBuys)
                    {
                        res.BuyingAds = req.DB.BuyingAds
                            .AsNoTracking()
                            .Where(ba => ba.MaterialTicker.ToUpper() == ticker && ba.Price > searchParams.CostThreshold)
                            .Select(ba => new LocalMarketAd(ba))
                            .ToList();

                        if (SourceSystemId != null)
                        {
                            foreach (var BuyingAd in res.BuyingAds)
                            {
                                var DestinationSystemId = JumpCalculator.GetSystemId(BuyingAd.PlanetNaturalId, req.DB);
                                BuyingAd.JumpCount = JumpCalculator.GetJumpCount(SourceSystemId, DestinationSystemId, req.DB);
                            }

                            res.BuyingAds = res.BuyingAds.OrderBy(ba => ba.JumpCount).ToList();
                        }
                    }

                    if (searchParams.SearchSells)
                    {
                        res.SellingAds = req.DB.SellingAds
                            .AsNoTracking()
                            .Where(sa => sa.MaterialTicker.ToUpper() == ticker && sa.Price > searchParams.CostThreshold)
                            .Select(sa => new LocalMarketAd(sa))
                            .ToList();

                        if (SourceSystemId != null)
                        {
                            foreach (var SellingAd in res.SellingAds)
                            {
                                var DestinationSystemId = JumpCalculator.GetSystemId(SellingAd.PlanetNaturalId, req.DB);
                                SellingAd.JumpCount = JumpCalculator.GetJumpCount(SourceSystemId, DestinationSystemId, req.DB);
                            }

                            res.SellingAds = res.SellingAds.OrderBy(ba => ba.JumpCount).ToList();
                        }
                    }
                }

                return JsonConvert.SerializeObject(res);
            }
        }
    }
}
#endif // WITH_MODULES