﻿#if WITH_MODULES
using System;
using System.Collections.Generic;
using System.Linq;

using Nancy;
using Nancy.Extensions;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;


namespace FIORest.Modules
{
    public class ProductionModule : NancyModule
    {
        public ProductionModule() : base("/production")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostProduction();
            });

            Post("/lineupdated", _ =>
            {
                this.EnforceWriteAuth();
                return PostProductionLineUpdated();
            });

            Post("/lineremoved", _ =>
            {
                this.EnforceWriteAuth();
                return PostProductionLineRemoved();
            });

            Post("/orderadded", _ =>
            {
                this.EnforceWriteAuth();
                return PostProductionOrderAdded();
            });

            Post("/orderupdated", _ =>
            {
                this.EnforceWriteAuth();
                return PostProductionOrderUpdated();
            });

            Post("/orderremoved", _ =>
            {
                this.EnforceWriteAuth();
                return PostProductionOrderRemoved();
            });

            Get("/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetProduction(parameters.username);
            });

            Get("/{username}/{planet_or_site_id}", parameters =>
            {
                this.EnforceReadAuth();
                return GetProduction(parameters.username, parameters.planet_or_site_id);
            });

            Get("/planets/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetProductionPlanets(parameters.username);
            });
        }

        private Response PostProduction()
        {
            using (var req = new FIORequest<JSONRepresentations.ProductionSiteProductionLines.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;

                var productionLines = new List<ProductionLine>();
                foreach (var productionLine in data.productionLines)
                {
                    ProductionLine prodLine = new ProductionLine();
                    prodLine.ProductionLineId = productionLine.id;
                    prodLine.SiteId = productionLine.siteId;

                    prodLine.PlanetId = productionLine.address.lines[1].entity.id;
                    prodLine.PlanetNaturalId = productionLine.address.lines[1].entity.naturalId;
                    prodLine.PlanetName = productionLine.address.lines[1].entity.name;

                    prodLine.Type = productionLine.type;
                    prodLine.Capacity = productionLine.capacity;

                    prodLine.Efficiency = productionLine.efficiency;
                    prodLine.Condition = productionLine.condition;

                    foreach (var order in productionLine.orders)
                    {
                        ProductionLineOrder prodOrder = new ProductionLineOrder();

                        prodOrder.ProductionLineOrderId = order.id;

                        foreach (var input in order.inputs)
                        {
                            ProductionLineInput prodInput = new ProductionLineInput();
                            prodInput.ProductionLineInputId = $"{prodOrder.ProductionLineOrderId}-{input.material.ticker}";
                            prodInput.MaterialName = input.material.name;
                            prodInput.MaterialTicker = input.material.ticker;
                            prodInput.MaterialId = input.material.id;
                            prodInput.MaterialAmount = input.amount;
                            prodInput.ProductionLineOrderId = prodOrder.ProductionLineOrderId;

                            prodOrder.Inputs.Add(prodInput);
                        }

                        foreach (var output in order.outputs)
                        {
                            ProductionLineOutput prodOutput = new ProductionLineOutput();
                            prodOutput.ProductionLineOutputId = $"{prodOrder.ProductionLineOrderId}-{output.material.ticker}";
                            prodOutput.MaterialName = output.material.name;
                            prodOutput.MaterialTicker = output.material.ticker;
                            prodOutput.MaterialId = output.material.id;
                            prodOutput.MaterialAmount = output.amount;
                            prodOutput.ProductionLineOrderId = prodOrder.ProductionLineOrderId;

                            prodOrder.Outputs.Add(prodOutput);
                        }

                        prodOrder.CreatedEpochMs = order.created?.timestamp;
                        prodOrder.StartedEpochMs = order.started?.timestamp;
                        prodOrder.CompletionEpochMs = order.completion?.timestamp;
                        prodOrder.DurationMs = order.duration?.millis;
                        prodOrder.LastUpdatedEpochMs = order.lastUpdated?.timestamp;

                        if (prodOrder.StartedEpochMs != null && prodOrder.DurationMs != null)
                        {
                            long UnixMsNow = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
                            long ProgressMs = UnixMsNow - (long)prodOrder.StartedEpochMs;
                            prodOrder.CompletedPercentage = (double)ProgressMs / prodOrder.DurationMs;
                        }
                        else
                        {
                            prodOrder.CompletedPercentage = null;
                        }

                        prodOrder.IsHalted = order.halted;
                        prodOrder.Recurring = order.recurring;

                        prodOrder.ProductionFee = (order.productionFee != null) ? order.productionFee.amount : 0.0;
                        prodOrder.ProductionFeeCurrency = order.productionFee?.currency;

                        prodOrder.ProductionFeeCollectorId = order.productionFeeCollector?.id;
                        prodOrder.ProductionFeeCollectorName = order.productionFeeCollector?.name;
                        prodOrder.ProductionFeeCollectorCode = order.productionFeeCollector?.code;

                        prodOrder.ProductionLineId = prodLine.ProductionLineId;

                        prodLine.Orders.Add(prodOrder);
                    }

                    prodLine.UserNameSubmitted = req.UserName;
                    prodLine.Timestamp = req.Now;

                    prodLine.Validate();
                    productionLines.Add(prodLine);
                }

                var productionLineIdsToRemove = productionLines
                    .Select(pl => pl.ProductionLineId);

                var userProdLinesToRemove = req.DB.ProductionLines
                    .Where(pl => pl.UserNameSubmitted.ToUpper() == req.UserName && productionLineIdsToRemove.Contains(pl.ProductionLineId));

                if (userProdLinesToRemove.Any())
                {
                    req.DB.ProductionLines.RemoveRange(userProdLinesToRemove);
                    req.PostRemoveRangeIgnoreConcurrencyFailures();
                }

                req.DB.ProductionLines.UpsertRange(productionLines)
                    .On(p => new { p.ProductionLineId })
                    .Run();

                var prodLineOrders = productionLines.SelectMany(pl => pl.Orders);
                req.DB.ProductionLineOrders.UpsertRange(prodLineOrders)
                    .On(plo => new { plo.ProductionLineOrderId })
                    .Run();

                var prodLineInputs = prodLineOrders.SelectMany(pli => pli.Inputs);
                req.DB.ProductionLineInputs.UpsertRange(prodLineInputs)
                    .On(pli => new { pli.ProductionLineInputId })
                    .Run();

                var prodLineOutputs = prodLineOrders.SelectMany(plo => plo.Outputs);
                req.DB.ProductionLineOutputs.UpsertRange(prodLineOutputs)
                    .On(plo => new { plo.ProductionLineOutputId })
                    .Run();

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response PostProductionLineUpdated()
        {
            using (var req = new FIORequest<JSONRepresentations.ProductionLineUpdated.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var productionLine = req.JsonPayload.payload;

                var prodLine = new ProductionLine();
                prodLine.ProductionLineId = productionLine.id;
                prodLine.SiteId = productionLine.siteId;

                prodLine.PlanetId = productionLine.address.lines[1].entity.id;
                prodLine.PlanetNaturalId = productionLine.address.lines[1].entity.naturalId;
                prodLine.PlanetName = productionLine.address.lines[1].entity.name;

                prodLine.Type = productionLine.type;
                prodLine.Capacity = productionLine.capacity;

                prodLine.Efficiency = productionLine.efficiency;
                prodLine.Condition = productionLine.condition;

                foreach (var order in productionLine.orders)
                {
                    ProductionLineOrder prodOrder = new ProductionLineOrder();
                    prodOrder.ProductionLineOrderId = order.id;

                    foreach (var input in order.inputs)
                    {
                        ProductionLineInput prodInput = new ProductionLineInput();
                        prodInput.ProductionLineInputId = $"{prodOrder.ProductionLineOrderId}-{input.material.ticker}";
                        prodInput.MaterialName = input.material.name;
                        prodInput.MaterialTicker = input.material.ticker;
                        prodInput.MaterialId = input.material.id;
                        prodInput.MaterialAmount = input.amount;
                        prodInput.ProductionLineOrderId = prodOrder.ProductionLineOrderId;

                        prodOrder.Inputs.Add(prodInput);
                    }

                    foreach (var output in order.outputs)
                    {
                        ProductionLineOutput prodOutput = new ProductionLineOutput();
                        prodOutput.ProductionLineOutputId = $"{prodOrder.ProductionLineOrderId}-{output.material.ticker}";
                        prodOutput.MaterialName = output.material.name;
                        prodOutput.MaterialTicker = output.material.ticker;
                        prodOutput.MaterialId = output.material.id;
                        prodOutput.MaterialAmount = output.amount;
                        prodOutput.ProductionLineOrderId = prodOrder.ProductionLineOrderId;

                        prodOrder.Outputs.Add(prodOutput);
                    }

                    prodOrder.CreatedEpochMs = order.created?.timestamp;
                    prodOrder.StartedEpochMs = order.started?.timestamp;
                    prodOrder.CompletionEpochMs = order.completion?.timestamp;
                    prodOrder.DurationMs = order.duration?.millis;
                    prodOrder.LastUpdatedEpochMs = order.lastUpdated?.timestamp;

                    if (prodOrder.StartedEpochMs != null && prodOrder.DurationMs != null)
                    {
                        long UnixMsNow = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
                        long ProgressMs = UnixMsNow - (long)prodOrder.StartedEpochMs;
                        prodOrder.CompletedPercentage = (double)ProgressMs / prodOrder.DurationMs;
                    }
                    else
                    {
                        prodOrder.CompletedPercentage = null;
                    }

                    prodOrder.IsHalted = order.halted;
                    prodOrder.Recurring = order.recurring;

                    prodOrder.ProductionFee = (order.productionFee != null) ? order.productionFee.amount : 0.0;
                    prodOrder.ProductionFeeCurrency = order.productionFee?.currency;

                    prodOrder.ProductionFeeCollectorId = order.productionFeeCollector?.id;
                    prodOrder.ProductionFeeCollectorName = order.productionFeeCollector?.name;
                    prodOrder.ProductionFeeCollectorCode = order.productionFeeCollector?.code;
                    prodOrder.ProductionLineId = prodLine.ProductionLineId;

                    prodLine.Orders.Add(prodOrder);
                }

                prodLine.UserNameSubmitted = req.UserName;
                prodLine.Timestamp = req.Now;

                prodLine.Validate();

                req.DB.ProductionLines.Upsert(prodLine)
                    .On(p => new { p.ProductionLineId })
                    .Run();

                req.DB.ProductionLineOrders.UpsertRange(prodLine.Orders)
                    .On(plo => new { plo.ProductionLineOrderId })
                    .Run();

                var prodLineInputs = prodLine.Orders.SelectMany(plo => plo.Inputs);
                var prodLineInputsList = prodLineInputs.ToList();
                req.DB.ProductionLineInputs.UpsertRange(prodLineInputs)
                    .On(pli => new { pli.ProductionLineInputId })
                    .Run();

                var prodLineOutputs = prodLine.Orders.SelectMany(plo => plo.Outputs);
                req.DB.ProductionLineOutputs.UpsertRange(prodLineOutputs)
                    .On(plo => new { plo.ProductionLineOutputId })
                    .Run();

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response PostProductionLineRemoved()
        {
            using (var req = new FIORequest<JSONRepresentations.ProductionLineRemoved.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload;
                var prodLine = req.DB.ProductionLines.Where(pl => pl.SiteId == payload.siteId && pl.ProductionLineId == payload.productionLineId).FirstOrDefault();
                if (prodLine != null)
                {
                    req.DB.ProductionLines.Remove(prodLine);
                    req.SaveChangesIgnoreConcurrencyFailures();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostProductionOrderAdded()
        {
            using (var req = new FIORequest<JSONRepresentations.ProductionOrderAdded.Message>(Request))
            {
                JSONRepresentations.ProductionOrderAdded.Payload order = null;
                if (req.JsonPayload.payload.id != null)
                {
                    order = req.JsonPayload.payload;
                }
                else
                {
                    // This is a complete and utter hack--need to handle this extension side
                    var requestStream = Nancy.IO.RequestStream.FromStream(req.Request.Body);
                    var requestBody = requestStream.AsString();
                    var rootObj = JsonConvert.DeserializeObject<JSONRepresentations.ProductionOrderAdded.Rootobject>(requestBody);
                    order = rootObj.payload.message.payload;
                }

                if (order == null || order.id == null)
                {
                    return req.ReturnBadRequest();
                }
                
                var prodLine = req.DB.ProductionLines
                    .Include(pl => pl.Orders)
                    .Where(pl => pl.ProductionLineId.ToUpper() == order.productionLineId.ToUpper())
                    .AsSplitQuery()
                    .FirstOrDefault();

                if (prodLine != null)
                {
                    var prodOrder = new ProductionLineOrder();

                    prodOrder.ProductionLineOrderId = order.id;

                    foreach (var input in order.inputs)
                    {
                        ProductionLineInput prodInput = new ProductionLineInput();

                        prodInput.ProductionLineInputId = $"{prodOrder.ProductionLineOrderId}-{input.material.ticker}";
                        prodInput.MaterialName = input.material.name;
                        prodInput.MaterialTicker = input.material.ticker;
                        prodInput.MaterialId = input.material.id;
                        prodInput.MaterialAmount = input.amount;
                        prodInput.ProductionLineOrderId = prodOrder.ProductionLineOrderId;

                        prodOrder.Inputs.Add(prodInput);
                    }

                    foreach (var output in order.outputs)
                    {
                        ProductionLineOutput prodOutput = new ProductionLineOutput();

                        prodOutput.ProductionLineOutputId = $"{prodOrder.ProductionLineOrderId}-{output.material.ticker}";
                        prodOutput.MaterialName = output.material.name;
                        prodOutput.MaterialTicker = output.material.ticker;
                        prodOutput.MaterialId = output.material.id;
                        prodOutput.MaterialAmount = output.amount;
                        prodOutput.ProductionLineOrderId = prodOrder.ProductionLineOrderId;

                        prodOrder.Outputs.Add(prodOutput);
                    }

                    prodOrder.CreatedEpochMs = order.created?.timestamp;
                    prodOrder.StartedEpochMs = order.started?.timestamp;
                    prodOrder.CompletionEpochMs = order.completion?.timestamp;
                    prodOrder.DurationMs = order.duration?.millis;
                    prodOrder.LastUpdatedEpochMs = order.lastUpdated?.timestamp;
                    if (prodOrder.StartedEpochMs != null && prodOrder.DurationMs != null)
                    {
                        long UnixMsNow = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
                        long ProgressMs = UnixMsNow - (long)prodOrder.StartedEpochMs;
                        prodOrder.CompletedPercentage = (double)ProgressMs / prodOrder.DurationMs;
                    }
                    else
                    {
                        prodOrder.CompletedPercentage = null;
                    }

                    prodOrder.IsHalted = order.halted;
                    prodOrder.Recurring = order.recurring;

                    prodOrder.ProductionFee = (order.productionFee != null) ? order.productionFee.amount : 0.0;
                    prodOrder.ProductionFeeCurrency = order.productionFee?.currency;

                    prodOrder.ProductionFeeCollectorId = order.productionFeeCollector?.id;
                    prodOrder.ProductionFeeCollectorName = order.productionFeeCollector?.name;
                    prodOrder.ProductionFeeCollectorCode = order.productionFeeCollector?.code;

                    prodOrder.ProductionLineId = prodLine.ProductionLineId;

                    prodOrder.Validate();
                    
                    req.DB.ProductionLineOrders.Upsert(prodOrder)
                        .On(po => new { po.ProductionLineOrderId })
                        .Run();
                        
                    req.DB.ProductionLineInputs.UpsertRange(prodOrder.Inputs)
                        .On(pli => new { pli.ProductionLineInputId })
                        .Run();

                    req.DB.ProductionLineOutputs.UpsertRange(prodOrder.Outputs)
                        .On(plo => new { plo.ProductionLineOutputId })
                        .Run();

                    req.SaveChangesIgnoreConcurrencyFailures();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostProductionOrderUpdated()
        {
            using (var req = new FIORequest<JSONRepresentations.ProductionOrderUpdated.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var order = req.JsonPayload.payload;
                var prodOrderModel = req.DB.ProductionLineOrders.Where(plo => plo.ProductionLineOrderId == order.id).FirstOrDefault();
                if (prodOrderModel != null)
                {
                    var prodOrder = new ProductionLineOrder();

                    prodOrder.ProductionLineOrderId = order.id;

                    foreach (var input in order.inputs)
                    {
                        ProductionLineInput prodInput = new ProductionLineInput();
                        prodInput.ProductionLineInputId = $"{prodOrder.ProductionLineOrderId}-{input.material.ticker}";
                        prodInput.MaterialName = input.material.name;
                        prodInput.MaterialTicker = input.material.ticker;
                        prodInput.MaterialId = input.material.id;
                        prodInput.MaterialAmount = input.amount;
                        prodInput.ProductionLineOrderId = prodOrder.ProductionLineOrderId;

                        prodOrder.Inputs.Add(prodInput);
                    }

                    foreach (var output in order.outputs)
                    {
                        ProductionLineOutput prodOutput = new ProductionLineOutput();
                        prodOutput.ProductionLineOutputId = $"{prodOrder.ProductionLineOrderId}-{output.material.ticker}";
                        prodOutput.MaterialName = output.material.name;
                        prodOutput.MaterialTicker = output.material.ticker;
                        prodOutput.MaterialId = output.material.id;
                        prodOutput.MaterialAmount = output.amount;
                        prodOutput.ProductionLineOrderId = prodOrder.ProductionLineOrderId;

                        prodOrder.Outputs.Add(prodOutput);
                    }

                    prodOrder.CreatedEpochMs = order.created?.timestamp;
                    prodOrder.StartedEpochMs = order.started?.timestamp;
                    prodOrder.CompletionEpochMs = order.completion?.timestamp;
                    prodOrder.DurationMs = order.duration?.millis;
                    prodOrder.LastUpdatedEpochMs = order.lastUpdated?.timestamp;
                    if (prodOrder.StartedEpochMs != null && prodOrder.DurationMs != null)
                    {
                        long UnixMsNow = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
                        long ProgressMs = UnixMsNow - (long)prodOrder.StartedEpochMs;
                        prodOrder.CompletedPercentage = (double)ProgressMs / prodOrder.DurationMs;
                    }
                    else
                    {
                        prodOrder.CompletedPercentage = null;
                    }

                    prodOrder.IsHalted = order.halted;
                    prodOrder.Recurring = order.recurring;

                    prodOrder.ProductionFee = (order.productionFee != null) ? order.productionFee.amount : 0.0;
                    prodOrder.ProductionFeeCurrency = order.productionFee?.currency;

                    prodOrder.ProductionFeeCollectorId = order.productionFeeCollector?.id;
                    prodOrder.ProductionFeeCollectorName = order.productionFeeCollector?.name;
                    prodOrder.ProductionFeeCollectorCode = order.productionFeeCollector?.code;

                    prodOrder.ProductionLineId = prodOrderModel.ProductionLineId;

                    req.DB.ProductionLineOrders.Upsert(prodOrder)
                        .On(o => new { o.ProductionLineOrderId })
                        .Run();

                    req.DB.ProductionLineInputs.UpsertRange(prodOrder.Inputs)
                        .On(pli => new { pli.ProductionLineInputId })
                        .Run();

                    req.DB.ProductionLineOutputs.UpsertRange(prodOrder.Outputs)
                        .On(plo => new { plo.ProductionLineOutputId })
                        .Run();

                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostProductionOrderRemoved()
        {
            using (var req = new FIORequest<JSONRepresentations.ProductionOrderRemoved.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                JSONRepresentations.ProductionOrderRemoved.Payload1 payload = null;
                if (req.JsonPayload.payload.message != null)
                {
                    payload = req.JsonPayload.payload.message.payload;
                }
                else
                {
                    // This is a complete and utter hack--need to handle this extension side
                    var requestStream = Nancy.IO.RequestStream.FromStream(req.Request.Body);
                    var requestBody = requestStream.AsString();
                    var rootObj = JsonConvert.DeserializeObject<JSONRepresentations.ProductionOrderRemoved.Message>(requestBody);
                    payload = rootObj.payload;
                }

                var pair = (from pl in req.DB.ProductionLines
                            from plo in pl.Orders
                            where plo.ProductionLineOrderId == payload.orderId
                            select new
                            {
                                ProductionLine = pl,
                                ProductionLineOrder = plo
                            })
                            .FirstOrDefault();

                if (pair != null)
                {
                    pair.ProductionLine.Orders.Remove(pair.ProductionLineOrder);
                    req.SaveChangesIgnoreConcurrencyFailures();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response GetProduction(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Production))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var prodLines = DB.ProductionLines
                        .AsNoTracking()
                        .Where(p => p.UserNameSubmitted.ToUpper() == UserName)
                        .Include(p => p.Orders)
                            .ThenInclude(o => o.Inputs)
                        .Include(p => p.Orders)
                            .ThenInclude(o => o.Outputs)
                        .AsSplitQuery()
                        .ToList();

                    return JsonConvert.SerializeObject(prodLines);
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetProduction(string UserName, string PlanetOrSiteId)
        {
            UserName = UserName.ToUpper();
            PlanetOrSiteId = PlanetOrSiteId.ToUpper();

            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Production))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var prodLines = DB.ProductionLines
                        .AsNoTracking()
                        .Where(p => p.UserNameSubmitted.ToUpper() == UserName)
                        .Where(p => p.PlanetId.ToUpper() == PlanetOrSiteId || p.PlanetName.ToUpper() == PlanetOrSiteId || p.PlanetNaturalId.ToUpper() == PlanetOrSiteId || p.SiteId.ToUpper() == PlanetOrSiteId)
                        .Include(p => p.Orders)
                            .ThenInclude(o => o.Inputs)
                        .Include(p => p.Orders)
                            .ThenInclude(o => o.Outputs)
                        .AsSplitQuery()
                        .ToList();
                    return JsonConvert.SerializeObject(prodLines);
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetProductionPlanets(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Production))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var productionPlanets = DB.ProductionLines
                        .AsNoTracking()
                        .Where(p => p.UserNameSubmitted.ToUpper() == UserName)
                        .Select(p => p.PlanetNaturalId)
                        .Distinct()
                        .ToList();

                    return JsonConvert.SerializeObject(productionPlanets);
                }
            }

            return HttpStatusCode.Unauthorized;
        }
    }
}
#endif // WITH_MODULES