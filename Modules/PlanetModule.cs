﻿#if WITH_MODULES
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using FIORest.JSONRepresentations;

namespace FIORest.Modules
{
    public class PlanetModule : NancyModule
    {
        public PlanetModule() : base("/planet")
        {
            Get("/", _ =>
            {
                this.EnforceReadAuth();
                return null;
            });

            Get("/hash", _ =>
            {
                this.EnforceReadAuth();
                return null;
            });

            Get("/allplanets", _ =>
            {
                this.Cacheable();
                return GetAllPlanets();
            });

            Get("/allplanets/full", _ =>
            {
                this.Cacheable();
                return GetAllPlanetsFull();
            });

            Get("/allplanets/settled", _ =>
            {
                this.Cacheable(60 * 30);
                return GetSettledPlanets();
            });

            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostPlanet();
            });

            Post("/sites", _ =>
            {
                this.EnforceWriteAuth();
                return PostPlanetSites();
            });

            Post("/cogc", _ =>
            {
                this.EnforceWriteAuth();
                return PostPlanetCOGC();
            });

            Post("/search", _ =>
            {
                return SearchPlanet();
            });

            Get("/system/{systemname}", parameters =>
            {
                return GetPlanetsFromSystem(parameters.systemname);
            });

            Get("/{planet}", parameters =>
            {
                return GetPlanet(parameters.planet);
            });

            Get("/sites/{planet}", parameters =>
            {
                return GetPlanetSites(parameters.planet);
            });

            Get("/sites/all", parameters =>
            {
                this.Cacheable(3600); // One hour
                return GetAllSites();
            });

            Get("/sitescount/{planet}", parameters =>
            {
                return GetPlanetSitesCount(parameters.planet);
            });

            Get("/companysearch/{company}", parameters =>
            {
                return GetPlanetCompanySearch(parameters.company);
            });

            Get("/basecount", _ =>
            {
                return GetBaseCount();
            });

            Get("/basecount/{threshold:int}", parameters =>
            {
                return GetBaseCount(parameters.threshold);
            });

            Get("/missingscans", _ =>
            {
                return GetMissingScans();
            });
        }

        private class AllPlanetResult
        {
            public string PlanetNaturalId;
            public string PlanetName;
        }

        private Response GetAllPlanets()
        {
            List<AllPlanetResult> planets = null;
            using (var DB = PRUNDataContext.GetNewContext())
            {
                planets = DB.PlanetDataModels
                    .AsNoTracking()
                    .Select(p => new AllPlanetResult 
                    { 
                        PlanetNaturalId = p.PlanetNaturalId, PlanetName = p.PlanetName 
                    })
                    .ToList();
            }

            return JsonConvert.SerializeObject(planets);
        }

        private Response GetAllPlanetsFull()
        {
            List<PlanetDataModel> planets = null;
            using (var DB = PRUNDataContext.GetNewContext())
            {
                 planets = DB.PlanetDataModels
                    .AsNoTracking()
                    .Include(pdm => pdm.Resources)
                    .Include(pdm => pdm.BuildRequirements)
                    .Include(pdm => pdm.ProductionFees)
                    .Include(pdm => pdm.COGCPrograms)
                    .Include(pdm => pdm.COGCVotes)
                    .Include(pdm => pdm.COGCUpkeep)
                    .AsSplitQuery()
                    .ToList();
            }

            return JsonConvert.SerializeObject(planets);
        }

        public class SettledPlanetData
        {
            public string Name { get; set; }
            public string NaturalId { get; set; }
            public int BaseCount { get; set; }
            public bool HasADM { get; set; }
            public bool HasLM { get; set; }
            public bool HasWarehouse { get; set; }
            public bool HasCOGC { get; set; }
            public string GovernorUsername { get; set; }
            public string GovernorCorporationName { get; set; }
            public string GovernorCorporationCode { get; set; }
            public string FactionCode { get; set; }
        }

        private Response GetSettledPlanets()
        {
            var SettledPlanetDatas = new List<SettledPlanetData>();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var planetIds = DB.PlanetDataModels.Select(planet => planet.PlanetId);
                var plotData = DB.PlanetSites
                    .AsNoTracking()
                    .Where(ps => planetIds.Contains(ps.PlanetId))
                    .GroupBy(ps => ps.PlanetId)
                    .Select(gr => new { PlanetId = gr.Key, Count = gr.Count() })
                    .ToList();
                
                foreach (var pdm in DB.PlanetDataModels)
                {
                    var plotInfo = plotData.Find(pd => pd.PlanetId == pdm.PlanetId);
                    if (plotInfo != null && plotInfo.Count > 1) // 1 - Since POPI takes a slot
                    {
                        // @TODO: This does not take into account corporation projects
                        int realBaseCount = plotInfo.Count - 1;
                        if (pdm.HasAdministrationCenter)
                            realBaseCount--;
                        if (pdm.HasChamberOfCommerce)
                            realBaseCount--;
                        if (pdm.HasLocalMarket)
                            realBaseCount--;
                        if (pdm.HasShipyard)
                            realBaseCount--;
                        if (pdm.HasWarehouse)
                            realBaseCount--;

                        if (realBaseCount > 0)
                        {
                            var spd = new SettledPlanetData();
                            spd.Name = pdm.PlanetName;
                            spd.NaturalId = pdm.PlanetNaturalId;
                            spd.BaseCount = realBaseCount;
                            spd.HasADM = pdm.HasAdministrationCenter;
                            spd.HasLM = pdm.HasLocalMarket;
                            spd.HasWarehouse = pdm.HasWarehouse;
                            spd.HasCOGC = pdm.HasChamberOfCommerce;
                            spd.GovernorUsername = pdm.GovernorUserName;
                            spd.GovernorCorporationName = pdm.GovernorCorporationName;
                            spd.GovernorCorporationCode = pdm.GovernorCorporationCode;
                            spd.FactionCode = pdm.FactionCode;

                            SettledPlanetDatas.Add(spd);
                        }
                    }
                }
            }

            return JsonConvert.SerializeObject(SettledPlanetDatas);
        }

        private Response PostPlanet()
        {
            using (var req = new FIORequest<JSONRepresentations.PlanetData.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                // @TODO: We're getting plots data coming through here.  For now, just let it pass-through
                var pathData = req.JsonPayload.payload.message.payload.path;
                if (pathData.Length == 4 && pathData[0] == "planets" && pathData[2] == "sites")
                {
                    return HttpStatusCode.OK;
                }

                var data = req.JsonPayload.payload.message.payload.body;
                PlanetDataModel model = new PlanetDataModel();
                PlanetDataModel oldModel = req.DB.PlanetDataModels.Where(p => p.PlanetNaturalId == data.naturalId).FirstOrDefault();
                bool bIsAdd = (oldModel == null);

                model.PlanetId = data.id;
                model.PlanetNaturalId = data.naturalId;
                model.PlanetName = data.name;
                model.Namer = (data.namer != null) ? data.namer.username.ToString() : null;
                model.NamingDataEpochMs = (data.namingDate != null) ? data.namingDate.timestamp : 0;
                model.Nameable = data.nameable;

                var systemIdFromPlanetNaturalId = model.PlanetNaturalId.Substring(0, (model.PlanetNaturalId.Length - 1));
                var system = req.DB.Systems.Where(ssm => ssm.NaturalId == systemIdFromPlanetNaturalId).FirstOrDefault();
                if (system == null)
                {
                    return req.ReturnBadRequest();
                }
                model.SystemId = system.SystemId;

                model.Gravity = data.data.gravity;
                model.MagneticField = data.data.magneticField;
                model.Mass = data.data.mass;
                model.MassEarth = data.data.massEarth;

                model.OrbitSemiMajorAxis = data.data.orbit.semiMajorAxis;
                model.OrbitEccentricity = data.data.orbit.eccentricity;
                model.OrbitInclination = data.data.orbit.inclination;
                model.OrbitRightAscension = data.data.orbit.rightAscension;
                model.OrbitPeriapsis = data.data.orbit.periapsis;
                model.OrbitIndex = data.data.orbitIndex;

                model.Pressure = data.data.pressure;
                model.Radiation = data.data.radiation;
                model.Radius = data.data.radius;

                foreach (var resource in data.data.resources)
                {
                    PlanetDataResource planetResource = new PlanetDataResource();

                    planetResource.MaterialId = resource.materialId;
                    planetResource.ResourceType = resource.type;
                    planetResource.Factor = resource.factor;

                    model.Resources.Add(planetResource);
                }

                model.Sunlight = data.data.sunlight;
                model.Surface = data.data.surface;
                model.Temperature = data.data.temperature;
                model.Fertility = data.data.fertility;

                foreach (var option in data.buildOptions.options)
                {
                    if (option.siteType == "PLAYER_SITE")
                    {
                        foreach (var quantity in option.billOfMaterial.quantities)
                        {
                            PlanetBuildRequirement requirement = new PlanetBuildRequirement();

                            requirement.MaterialName = quantity.material.name;
                            requirement.MaterialId = quantity.material.id;
                            requirement.MaterialTicker = quantity.material.ticker;
                            requirement.MaterialCategory = quantity.material.category;

                            requirement.MaterialAmount = quantity.amount;

                            requirement.MaterialWeight = quantity.material.weight;
                            requirement.MaterialVolume = quantity.material.volume;

                            model.BuildRequirements.Add(requirement);
                        }
                    }
                }

                model.HasLocalMarket = false;
                model.HasChamberOfCommerce = false;
                model.HasWarehouse = false;
                model.HasAdministrationCenter = false;
                model.HasShipyard = false;
                foreach (var project in data.projects)
                {
                    switch (project.type)
                    {
                        case "LOCM":
                            model.HasLocalMarket = true;
                            break;

                        case "COGC":
                            model.HasChamberOfCommerce = true;
                            break;

                        case "WAR":
                            model.HasWarehouse = true;
                            break;

                        case "ADM":
                            model.HasAdministrationCenter = true;
                            break;

                        case "SHY":
                            model.HasShipyard = true;
                            break;
                    }
                }

                model.FactionCode = data.country?.code;
                model.FactionName = data.country?.name;

                model.GovernorId = data.governor?.id;
                model.GovernorUserName = data.governor?.username;

                model.GovernorCorporationId = data.governingEntity?.id;
                model.GovernorCorporationName = data.governingEntity?.name;
                model.GovernorCorporationCode = data.governingEntity?.code;

                model.CurrencyName = data.currency?.name;
                model.CurrencyCode = data.currency?.code;

                model.CollectorId = data.localRules?.collector?.id;
                model.CollectorName = data.localRules?.collector?.name;
                model.CollectorCode = data.localRules?.collector?.code;

                if (data.localRules?.productionFees?.fees != null)
                {
                    foreach (var fee in data.localRules.productionFees.fees)
                    {
                        PlanetProductionFee productionFee = new PlanetProductionFee();

                        productionFee.Category = fee.category;
                        productionFee.WorkforceLevel = fee.workforceLevel;
                        productionFee.FeeAmount = (fee.fee != null) ? fee.fee.amount : 0.0;
                        productionFee.FeeCurrency = fee.fee?.currency;

                        model.ProductionFees.Add(productionFee);
                    }
                }

                model.BaseLocalMarketFee = data.localRules?.localMarketFee?._base;
                model.LocalMarketFeeFactor = data.localRules?.localMarketFee?.timeFactor;

                model.WarehouseFee = data.localRules?.warehouseFee?.fee;

                model.PopulationId = data.populationId;

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.PlanetDataModels.Add(model);
                }
                else if (!model.Equals(oldModel))
                {
                    req.DB.PlanetDataModels.Remove(oldModel);
                    req.DB.PlanetDataModels.Add(model);
                }

                req.SaveChangesIgnoreConcurrencyFailures();
                return HttpStatusCode.OK;
            }
        }

        private Response PostPlanetSites()
        {
            using (var req = new FIORequest<FIORest.JSONRepresentations.JsonPlanetSites.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var sites = req.JsonPayload.payload.message.payload.sites;
                var sitesToClear = req.DB.PlanetSites.Where(ps => ps.PlanetId == sites.First().planetId);
                req.DB.PlanetSites.RemoveRange(sitesToClear);
                req.PostRemoveRangeIgnoreConcurrencyFailures();

                foreach (var site in sites)
                {
                    var planetSite = new PlanetSite();

                    planetSite.PlanetId = site.planetId;
                    planetSite.OwnerId = site.ownerId;
                    if (site.entity != null)
                    {
                        planetSite.OwnerName = site.entity.name;
                        planetSite.OwnerCode = site.entity.code;
                    }

                    planetSite.PlotNumber = site.plot;
                    planetSite.PlotId = site.id;
                    if (planetSite.PlotId.Length >= 8)
                    {
                        planetSite.SiteId = planetSite.PlotId.Substring(0, 8);
                    }

                    req.DB.PlanetSites.Add(planetSite);
                }

                req.SaveChangesIgnoreConcurrencyFailures();
                return HttpStatusCode.OK;
            }
        }

        private Response PostPlanetCOGC()
        {
            using (var req = new FIORequest<FIORest.JSONRepresentations.COGCData.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                // Not sure if we can actually hit this condition, but let's look...
                if (ValidateCOGCPayloadPath(req.JsonPayload.payload.message.payload.path) == false)
                {
                    var pathstr = string.Join(", ", req.JsonPayload.payload.message.payload.path);
                    return req.ReturnBadRequest();
                }                                                                                               

                var data = req.JsonPayload.payload.message.payload.body;
                PlanetDataModel model = req.DB.PlanetDataModels.Where(p => p.PlanetNaturalId.ToUpper() == data.planet.naturalId.ToUpper()).FirstOrDefault();
                if (model != null)
                {
                    model.COGCProgramStatus = data.status;
                    model.COGCPrograms.Clear();
                    foreach (var program in data.programs)
                    {
                        PlanetCOGCProgram prog = new PlanetCOGCProgram();

                        prog.ProgramType = program.type;
                        prog.StartEpochMs = program.start?.timestamp;
                        prog.EndEpochMs = program.end?.timestamp;

                        model.COGCPrograms.Add(prog);
                    }

                    model.COGCVotes.Clear();
                    foreach (var vote in data.votes)
                    {
                        PlanetCOGCVote v = new PlanetCOGCVote();

                        v.CompanyName = vote.voter.name;
                        v.CompanyCode = vote.voter.code;
                        v.Influence = vote.influence != null ? (float)vote.influence : 0;
                        v.VoteType = vote.type;
                        v.VoteTimeEpochMs = vote.time.timestamp;

                        model.COGCVotes.Add(v);
                    }

                    //// @TODO: Handle upkeep
                    //model.COGCUpkeep.Clear();
                    //foreach( var bill in data.upkeep.billOfMaterial)
                    //{
                    //    PlanetCOGCUpkeep upkeep = new PlanetCOGCUpkeep();
                    //
                    //
                    //
                    //    model.COGCUpkeep.Add(upkeep);
                    //}

                    req.SaveChangesIgnoreConcurrencyFailures();
                }

                return HttpStatusCode.OK;
            }
        }

        private bool ValidateCOGCPayloadPath(string[] path)
        {
            if (path.Length == 4 && 
                path[0] == "planets" && 
                path[2] == "cogc" && 
                path[3] == "00000000000000000000000000000000")
            {
                return true;
            }
            return false;
        }

        public class PlanetDataModelEx : PlanetDataModel
        {
            public bool HasJobData { get; set; }

            public int? TotalPioneers { get; set; }
            public int? OpenPioneers { get; set; }

            public int? TotalSettlers { get; set; }
            public int? OpenSettlers { get; set; }

            public int? TotalTechnicians { get; set; }
            public int? OpenTechnicians { get; set; }

            public int? TotalEngineers { get; set; }
            public int? OpenEngineers { get; set; }

            public int? TotalScientists { get; set; }
            public int? OpenScientists { get; set; }

            public int? TotalPlots { get; set; }

            public List<int> DistanceResults { get; set; } = new List<int>();
        }

        private Response SearchPlanet()
        {
            List<PlanetDataModelEx> results = null;
            JsonPlanetSearch queryData = null;

            bool UserIsAuthenticated = Auth.IsReadAuthenticated(Request);

            using (var req = new FIORequest<JsonPlanetSearch>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                queryData = req.JsonPayload;

                // One or the other needs to be present.  Equality is the same as XNOR
                if (queryData.UseNormalPlanetSearch == queryData.UsePlanetTierSearch)
                {
                    return req.ReturnBadRequest();
                }

                // Search the materials first
                // if there are no materials queried, we'll start with ALL planets.
                IQueryable<PlanetDataModel> res = req.DB.PlanetDataModels.AsNoTracking();
                if (queryData.Materials.Count > 0)
                {
                    var matquery = req.DB.Materials
                        .AsNoTracking()
                        .Where(m => queryData.Materials.Contains(m.MaterialId) || queryData.Materials.Contains(m.Name) || queryData.Materials.Contains(m.Ticker));
                    List<string> materialIds = matquery
                        .Select(m => m.MaterialId)
                        .ToList();

                    // Determine potential planets based on material opportunity
                    // Leave the concentration check for later.
                    var planetIdList = req.DB.PlanetDataResources
                        .AsNoTracking()
                        .Where(pdr => materialIds.Contains(pdr.MaterialId))
                        .GroupBy(pdr => pdr.PlanetDataModelId)
                        .Select(g => new { PlanetDataModelId = g.Key, count = g.Count() })
                        .Where(q => q.count >= materialIds.Count);
                    ;
                    List<int> potentialPlanetIds = planetIdList.Select(pid => pid.PlanetDataModelId).ToList();

                    res = res.Where(pdm => potentialPlanetIds.Contains(pdm.PlanetDataModelId));
                    // update the list of planets with matching concentrations
                    if (queryData.ConcentrationThreshold > 0.0)
                    {
                        var planetWithResources = res.Include(pdm => pdm.Resources).ToList();
                        for (int idx = planetWithResources.Count() - 1; idx >= 0; idx--)
                        {
                            var planet = planetWithResources[idx];
                            var removePlanet = false;
                            foreach (var materialId in materialIds)
                            {
                                PlanetDataResource planetMaterial = planetWithResources[idx].Resources.Find(res => res.MaterialId == materialId);
                                if (planetMaterial.Factor < queryData.ConcentrationThreshold)
                                {
                                    removePlanet = true;
                                    break;
                                }
                            }
                            if (removePlanet)
                            {
                                planetWithResources.RemoveAt(idx);
                            }
                        }
                        // Update our res query with the updated list.
                        var validPlanetIdList = planetWithResources.Select(pwr => pwr.PlanetDataModelId);
                        res = req.DB.PlanetDataModels
                            .AsNoTracking()
                            .Where(pdm => validPlanetIdList.Contains(pdm.PlanetDataModelId));
                    }
                }


                if (queryData.UseNormalPlanetSearch)
                {
                    using (var pf = new PerfTracer("PlanetSearch: Includes"))
                    {
                        if (!queryData.IncludeRocky)
                        {
                            res = res.Where(pdm => pdm.Surface == false);
                        }
                        if (!queryData.IncludeGaseous)
                        {
                            res = res.Where(pdm => pdm.Surface == true);
                        }
                        if (!queryData.IncludeLowGravity)
                        {
                            res = res.Where(pdm => pdm.Gravity >= 0.25);
                        }
                        if (!queryData.IncludeHighGravity)
                        {
                            res = res.Where(pdm => pdm.Gravity <= 2.5);
                        }
                        if (!queryData.IncludeLowPressure)
                        {
                            res = res.Where(pdm => pdm.Pressure >= 0.25);
                        }
                        if (!queryData.IncludeHighPressure)
                        {
                            res = res.Where(pdm => pdm.Pressure <= 2.0);
                        }
                        if (!queryData.IncludeLowTemperature)
                        {
                            res = res.Where(pdm => pdm.Temperature >= -25.0);
                        }
                        if (!queryData.IncludeHighTemperature)
                        {
                            res = res.Where(pdm => pdm.Temperature <= 75.0);
                        }
                    }
                }
                else
                {
                    res = res.Where(pdm => pdm.PlanetTier <= queryData.PlanetTier);
                }

                using (var pf = new PerfTracer("PlanetSearch: Must-haves"))
                {
                    if (queryData.MustBeFertile)
                    {
                        res = res.Where(pdm => pdm.Fertility > -1.0);
                    }
                    if (queryData.MustHaveLocalMarket)
                    {
                        res = res.Where(pdm => pdm.HasLocalMarket);
                    }
                    if (queryData.MustHaveChamberOfCommerce)
                    {
                        res = res.Where(pdm => pdm.HasChamberOfCommerce);
                    }
                    if (queryData.MustHaveWarehouse)
                    {
                        res = res.Where(pdm => pdm.HasWarehouse);
                    }
                    if (queryData.MustHaveAdministrationCenter)
                    {
                        res = res.Where(pdm => pdm.HasAdministrationCenter);
                    }
                    if (queryData.MustHaveShipyard)
                    {
                        res = res.Where(pdm => pdm.HasShipyard);
                    }
                }
                // Eager-load planet's informations.
                res = res
                    .Include(pl => pl.Resources)
                    .Include(pl => pl.BuildRequirements)
                    .Include(pl => pl.ProductionFees)
                    .Include(pl => pl.COGCPrograms)
                    .Include(pl => pl.COGCVotes)
                    .Include(pl => pl.COGCUpkeep)
                    .AsSplitQuery();


                // Up-Cast
                using (var pf = new PerfTracer("PlanetSearch: Up-cast"))
                {
                    // Note, that this will be the "expensive" bit since it's actually resolving/doing the DB query
                    results = JsonConvert.DeserializeObject<List<PlanetDataModelEx>>(JsonConvert.SerializeObject(res.ToList()));
                }

                if (queryData.JumpDistanceThreshold >= 0 && !String.IsNullOrWhiteSpace(queryData.JumpDistanceThresholdSystem))
                {
                    string originThresholdSystem = JumpCalculator.GetSystemId(queryData.JumpDistanceThresholdSystem, req.DB);
                    if (originThresholdSystem != null)
                    {
                        using (var pf = new PerfTracer("PlanetSearch: JumpDistanceThreshold"))
                        {
                            for (int ResultIdx = results.Count - 1; ResultIdx >= 0; --ResultIdx)
                            {
                                var rSystemId = results[ResultIdx].SystemId;
                                var jumpDistance = JumpCalculator.GetJumpCount(originThresholdSystem, rSystemId, req.DB);
                                if (jumpDistance > queryData.JumpDistanceThreshold)
                                {
                                    results.RemoveAt(ResultIdx);
                                }
                            }
                        }
                    }
                }

                using (var pf = new PerfTracer("PlanetSearch: Infrastructure-New"))
                {
                    // Get the Infrastructure models we need
                    var popIds = results.Select(pdm => pdm.PopulationId);
                    var infs = req.DB.Infrastructures
                        .AsNoTracking()
                        .Where(im => popIds.Contains(im.InfrastructureId))
                        .AsSplitQuery()
                        .ToList();
                    // Get the InfrastructureReports of interest
                    // - Query for Id/Date combination
                    // - Fetch a list which matches at least one of those two combinations
                    // - Filter out the ones which don't match both.
                    var infIds = infs.Select(inf => inf.InfrastructureId);
                    var reportset = req.DB.InfrastructureReports
                        .AsNoTracking()
                        .Where(ir => infIds.Contains(ir.InfrastructureId))
                        .GroupBy(ir => ir.InfrastructureId)
                        .Select(gr => new { ModelId = gr.Key, SimDate = gr.Max(ir => ir.SimulationPeriod) })
                        .AsSplitQuery()
                        .ToList();
                    var rawReportQuery = from infReports in req.DB.InfrastructureReports.AsNoTracking()
                                         where reportset.Select(r => r.ModelId).Contains(infReports.InfrastructureId)
                                            && reportset.Select(r => r.SimDate).Contains(infReports.SimulationPeriod)
                                         select infReports;
                    var reports = rawReportQuery.ToList();
                    for (int idx = reports.Count() - 1; idx >= 0; idx--)
                    {
                        var report = reports[idx];
                        if (!reportset.Contains(new { ModelId = report.InfrastructureId, SimDate = report.SimulationPeriod }))
                        {
                            reports.RemoveAt(idx);
                        }
                    }
                    // Get planet plot data
                    var planetIds = results.Select(planet => planet.PlanetId);
                    var plotData = req.DB.PlanetSites
                        .AsNoTracking()
                        .Where(ps => planetIds.Contains(ps.PlanetId))
                        .GroupBy(ps => ps.PlanetId)
                        .Select(gr => new { PlanetId = gr.Key, Count = gr.Count() })
                        .AsSplitQuery()
                        .ToList();
                    // Build out our data set.
                    for (int idx = 0; idx < results.Count; idx++)
                    {
                        PlanetDataModelEx pdmex = results[idx];
                        var inf = infs.Find(inf => inf.InfrastructureId == pdmex.PopulationId);
                        if (inf != null)
                        {
                            var report = reports.Find(rep => rep.InfrastructureId == inf.InfrastructureId);
                            if (report != null)
                            {
                                DateTime thresh = Utils.FromUnixTime(report.TimestampMs) + new TimeSpan(7, 0, 0, 0);
                                pdmex.HasJobData = (DateTime.UtcNow < thresh);
                                if (pdmex.HasJobData)
                                {
                                    // Valid report
                                    pdmex.TotalPioneers = report.NextPopulationPioneer;
                                    pdmex.OpenPioneers = (report.OpenJobsPioneer > 0) ? -(int)report.OpenJobsPioneer : ((int)(report.NextPopulationPioneer * report.UnemploymentRatePioneer));

                                    pdmex.TotalSettlers = report.NextPopulationSettler;
                                    pdmex.OpenSettlers = (report.OpenJobsSettler > 0) ? -(int)report.OpenJobsSettler : ((int)(report.NextPopulationSettler * report.UnemploymentRateSettler));

                                    pdmex.TotalTechnicians = report.NextPopulationTechnician;
                                    pdmex.OpenTechnicians = (report.OpenJobsTechnician > 0) ? -(int)report.OpenJobsTechnician : ((int)(report.NextPopulationTechnician * report.UnemploymentRateTechnician));

                                    pdmex.TotalEngineers = report.NextPopulationEngineer;
                                    pdmex.OpenEngineers = (report.OpenJobsEngineer > 0) ? -(int)report.OpenJobsEngineer : ((int)(report.NextPopulationEngineer * report.UnemploymentRateEngineer));

                                    pdmex.TotalScientists = report.NextPopulationScientist;
                                    pdmex.OpenScientists = (report.OpenJobsScientist > 0) ? -(int)report.OpenJobsScientist : ((int)(report.NextPopulationScientist * report.UnemploymentRateScientist));
                                }
                            }
                        }
                        var plotInfo = plotData.Find(pd => pd.PlanetId == pdmex.PlanetId);
                        if (plotInfo != null && plotInfo.Count > 0)
                        {
                            pdmex.TotalPlots = plotInfo.Count;
                        }
                    }
                }

                var JumpCountPT = new PerfTracer("PlanetSearch: GetJumpCountAccumulation", false);

                using (var pf = new PerfTracer("PlanetSearch: DistanceChecks"))
                {
                    // Limit to 10 distance checks
                    queryData.DistanceChecks = queryData.DistanceChecks.Take(10).ToList();
                    for (int i = queryData.DistanceChecks.Count - 1; i >= 0; --i)
                    {
                        queryData.DistanceChecks[i] = JumpCalculator.GetSystemId(queryData.DistanceChecks[i], req.DB);
                    }

                    for (int resIdx = 0; resIdx < results.Count; ++resIdx)
                    {
                        PlanetDataModelEx result = results[resIdx];
                        string jumpDest = result.SystemId;
                        foreach (var jumpSource in queryData.DistanceChecks)
                        {
                            if (jumpSource == null)
                            {
                                result.DistanceResults.Add(-1);
                            }
                            else
                            {
                                JumpCountPT.Start();
                                int JumpCount = JumpCalculator.GetJumpCount(jumpSource, result.SystemId, req.DB);
                                JumpCountPT.Stop();

                                result.DistanceResults.Add(JumpCount);
                            }
                        }
                    }
                }

                JumpCountPT.Dispose();
            }

            using (var pf = new PerfTracer("PlanetSearch: CachePendingRoutes"))
            {
                JumpCalculator.CachePendingRoutes();
            }

            using (var pf = new PerfTracer("PlanetSearch: Serialization"))
            {
                return JsonConvert.SerializeObject(results);
            }
        }

        private Response GetPlanetsFromSystem(string SystemName)
        {
            List<PlanetDataModel> list = null;
            SystemName = SystemName.ToUpper();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var system = DB.Systems.Where(ssm => ssm.SystemId.ToUpper() == SystemName || ssm.Name.ToUpper() == SystemName.ToUpper() || ssm.NaturalId.ToUpper() == SystemName.ToUpper()).FirstOrDefault();
                if (system != null)
                {
                    list = DB.PlanetDataModels
                        .AsNoTracking()
                        .Where(p => p.SystemId == system.SystemId)
                        .OrderBy(p => p.PlanetNaturalId)
                        .Include(pl => pl.Resources)
                        .Include(pl => pl.BuildRequirements)
                        .Include(pl => pl.ProductionFees)
                        .Include(pl => pl.COGCPrograms)
                        .Include(pl => pl.COGCVotes)
                        .Include(pl => pl.COGCUpkeep)
                        .AsSplitQuery()
                        .ToList();
                }
            }

            if (list != null)
            {
                return JsonConvert.SerializeObject(list);
            }
            else
            {
                return HttpStatusCode.NoContent;
            }
        }

        private Response GetPlanet(string Planet)
        {
            PlanetDataModel res = null;
            Planet = Planet.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                res = DB.PlanetDataModels
                    .AsNoTracking()
                    .Where(p => p.PlanetId.ToUpper() == Planet || p.PlanetNaturalId.ToUpper() == Planet || p.PlanetName.ToUpper() == Planet)
                    .Include(pl => pl.Resources)
                    .Include(pl => pl.BuildRequirements)
                    .Include(pl => pl.ProductionFees)
                    .Include(pl => pl.COGCPrograms)
                    .Include(pl => pl.COGCVotes)
                    .Include(pl => pl.COGCUpkeep)
                    .AsSplitQuery()
                    .FirstOrDefault();
            }

            if (res != null)
            {
                return JsonConvert.SerializeObject(res);
            }
            else
            {
                return HttpStatusCode.NoContent;
            }
        }

        private Response GetPlanetSites(string Planet)
        {
            List<PlanetSite> sites = null;
            Planet = Planet.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.PlanetDataModels
                    .AsNoTracking()
                    .Where(p => p.PlanetId.ToUpper() == Planet || p.PlanetNaturalId.ToUpper() == Planet || p.PlanetName.ToUpper() == Planet)
                    .FirstOrDefault();
                if (res != null)
                {
                    sites = DB.PlanetSites
                        .AsNoTracking()
                        .Where(ps => ps.PlanetId == res.PlanetId)
                        .ToList();
                }
            }

            if (sites != null)
            {
                return JsonConvert.SerializeObject(sites);
            }
            else
            {
                return HttpStatusCode.NoContent;
            }
        }

        public class AllSitesPayload
        {
            public string PlanetNaturalId { get; set; }
            public int SitesCount { get; set; }
        }

        public Response GetAllSites()
        {
            List<AllSitesPayload> res = null;
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var query = from planet in DB.PlanetDataModels.AsNoTracking()
                            join site in DB.PlanetSites.AsNoTracking()
                            on planet.PlanetId equals site.PlanetId
                            select new { planet.PlanetNaturalId, site };

                res = (from q in query
                          group q by q.PlanetNaturalId into g
                          select new AllSitesPayload
                          {
                              PlanetNaturalId = g.Key,
                              SitesCount = g.Count()
                          })
                          .ToList();
            }

            return JsonConvert.SerializeObject(res);
        }

        public class CountObj
        {
            public int Count { get; set; }
        }

        private Response GetPlanetSitesCount(string Planet)
        {
            Planet = Planet.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.PlanetDataModels
                    .AsNoTracking()
                    .Where(p => p.PlanetId.ToUpper() == Planet || p.PlanetNaturalId.ToUpper() == Planet || p.PlanetName.ToUpper() == Planet)
                    .FirstOrDefault();
                if (res != null)
                {
                    return DB.PlanetSites
                        .AsNoTracking()
                        .Where(ps => ps.PlanetId == res.PlanetId)
                        .Count()
                        .ToString();
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response GetPlanetCompanySearch(string Company)
        {
            var PlanetNames = new List<string>();
            Company = Company.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                var planetSitesResults = DB.PlanetSites
                    .AsNoTracking()
                    .Where(ps => ps.OwnerId.ToUpper() == Company || ps.OwnerName.ToUpper() == Company || ps.OwnerCode.ToUpper() == Company)
                    .Select(ps => ps.PlanetId)
                    .ToList();

                
                foreach (var planetSite in planetSitesResults)
                {
                    var res = DB.PlanetDataModels.Where(p => p.PlanetId == planetSite).FirstOrDefault();
                    if (res != null)
                    {
                        PlanetNames.Add(res.PlanetName);
                    }
                }
            }

            return JsonConvert.SerializeObject(PlanetNames);
        }

        private Response GetBaseCount(int Threshold = 2)
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.PlanetSites
                    .AsNoTracking()
                    .Where(ps => ps.OwnerCode != null)
                    .GroupBy(ps => ps.OwnerCode)
                    .Where(grp => grp.Count() >= Threshold)
                    .Select(grp => new 
                    { 
                        CompanyCode = grp.Key, 
                        BaseCount = grp.Count() 
                    })
                    .ToList();
                return JsonConvert.SerializeObject(res);
            }
        }

        private Response GetMissingScans()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var sectors = DB.Systems
                    .AsNoTracking()
                    .OrderBy(ss => ss.NaturalId)
                    .Select(s => s.NaturalId)
                    .ToList();
                var planets = DB.PlanetDataModels
                    .AsNoTracking()
                    .OrderBy(pdm => pdm.PlanetNaturalId)
                    .Select(pdm => pdm.PlanetNaturalId.Remove(pdm.PlanetNaturalId.Length - 1, 1))
                    .Distinct()
                    .ToList();
                return JsonConvert.SerializeObject(sectors.Except(planets).ToList());
            }
        }
    }
}
#endif // WITH_MODULES