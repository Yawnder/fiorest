﻿#if WITH_MODULES
using System;
using System.Collections.Generic;
using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules
{
    public class InfrastructureModule : NancyModule
    {
        public InfrastructureModule() : base("/infrastructure")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostInfrastructure();
            });

            Post("/project", _ =>
            {
                this.EnforceWriteAuth();
                return PostInfrastructureProject();
            });

            Post("/programs", _ =>
            {
                this.EnforceWriteAuth();
                return PostInfrastructurePrograms();
            });

            Get("/{planet_or_infrastructure_id}", parameters =>
            {
                this.Cacheable();
                return GetInfrastructure(parameters.planet_or_infrastructure_id);
            });

            Get("/all", _ =>
            {
                this.Cacheable(3600); // 1 hour
                return GetAllInfrastructure();
            });
        }

        private Response PostInfrastructure()
        {
            using (var req = new FIORequest<JSONRepresentations.Infrastructure.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload.body;
                var model = new Infrastructure();

                model.InfrastructureId = data.id;

                var LastReport = data.reports.OrderByDescending(r => r.simulationPeriod).FirstOrDefault();
                int SimulationPeriod = (LastReport != null) ? data.reports.OrderByDescending(r => r.simulationPeriod).First().simulationPeriod : 0;

                foreach (var infrastructureProject in data.infrastructure)
                {
                    InfrastructureProject infProj = new InfrastructureProject();
                    infProj.InfrastructureProjectId = $"{infrastructureProject.projectId}-{SimulationPeriod}";
                    infProj.InfraProjectId = infrastructureProject.projectId;
                    infProj.SimulationPeriod = SimulationPeriod;
                    infProj.Type = infrastructureProject.type;
                    infProj.Ticker = infrastructureProject.ticker;
                    infProj.Name = infrastructureProject.projectName;
                    infProj.Level = infrastructureProject.level;
                    infProj.ActiveLevel = infrastructureProject.activeLevel;
                    infProj.CurrentLevel = infrastructureProject.currentLevel;
                    infProj.UpkeepStatus = infrastructureProject.upkeepStatus;
                    infProj.UpgradeStatus = infrastructureProject.upgradeStatus;
                    infProj.InfrastructureId = data.id;

                    model.InfrastructureProjects.Add(infProj);
                }

                foreach (var report in data.reports)
                {
                    InfrastructureReport infReport = new InfrastructureReport();
                    infReport.InfrastructureReportId = $"{data.id}-{report.simulationPeriod}";
                    infReport.ExplorersGraceEnabled = report.explorersGraceEnabled;
                    infReport.SimulationPeriod = report.simulationPeriod;
                    infReport.TimestampMs = report.time.timestamp;

                    infReport.NextPopulationPioneer = report.nextPopulation.PIONEER;
                    infReport.NextPopulationSettler = report.nextPopulation.SETTLER;
                    infReport.NextPopulationTechnician = report.nextPopulation.TECHNICIAN;
                    infReport.NextPopulationEngineer = report.nextPopulation.ENGINEER;
                    infReport.NextPopulationScientist = report.nextPopulation.SCIENTIST;

                    infReport.PopulationDifferencePioneer = report.populationDifference.PIONEER;
                    infReport.PopulationDifferenceSettler = report.populationDifference.SETTLER;
                    infReport.PopulationDifferenceTechnician = report.populationDifference.TECHNICIAN;
                    infReport.PopulationDifferenceEngineer = report.populationDifference.ENGINEER;
                    infReport.PopulationDifferenceScientist = report.populationDifference.SCIENTIST;

                    infReport.AverageHappinessPioneer = report.averageHappiness.PIONEER;
                    infReport.AverageHappinessSettler = report.averageHappiness.SETTLER;
                    infReport.AverageHappinessTechnician = report.averageHappiness.TECHNICIAN;
                    infReport.AverageHappinessEngineer = report.averageHappiness.ENGINEER;
                    infReport.AverageHappinessScientist = report.averageHappiness.SCIENTIST;

                    infReport.UnemploymentRatePioneer = report.unemploymentRate.PIONEER;
                    infReport.UnemploymentRateSettler = report.unemploymentRate.SETTLER;
                    infReport.UnemploymentRateTechnician = report.unemploymentRate.TECHNICIAN;
                    infReport.UnemploymentRateEngineer = report.unemploymentRate.ENGINEER;
                    infReport.UnemploymentRateScientist = report.unemploymentRate.SCIENTIST;

                    infReport.OpenJobsPioneer = report.openJobs.PIONEER;
                    infReport.OpenJobsSettler = report.openJobs.SETTLER;
                    infReport.OpenJobsTechnician = report.openJobs.TECHNICIAN;
                    infReport.OpenJobsEngineer = report.openJobs.ENGINEER;
                    infReport.OpenJobsScientist = report.openJobs.SCIENTIST;

                    infReport.NeedFulfillmentLifeSupport = report.needFulfillment.LIFE_SUPPORT;
                    infReport.NeedFulfillmentSafety = report.needFulfillment.SAFETY;
                    infReport.NeedFulfillmentHealth = report.needFulfillment.HEALTH;
                    infReport.NeedFulfillmentComfort = report.needFulfillment.COMFORT;
                    infReport.NeedFulfillmentCulture = report.needFulfillment.CULTURE;
                    infReport.NeedFulfillmentEducation = report.needFulfillment.EDUCATION;
                    infReport.InfrastructureId = data.id;

                    model.InfrastructureReports.Add(infReport);
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                model.Validate();

                req.DB.Infrastructures.Upsert(model)
                    .On(i => new { i.InfrastructureId })
                    .Run();

                req.DB.InfrastructureProjects.UpsertRange(model.InfrastructureProjects)
                    .On(p => new { p.InfrastructureProjectId })
                    .Run();

                req.DB.InfrastructureReports.UpsertRange(model.InfrastructureReports)
                    .On(r => new { r.InfrastructureReportId })
                    .Run();

                req.SaveChangesIgnoreConcurrencyFailures();
                return HttpStatusCode.OK;
            }
        }

        private int FindSimulationPeriod(InfrastructureReport report, DateTime now)
        {
            int SimulationPeriod = report.SimulationPeriod;

            var PrevReportTime = Utils.FromUnixTime(report.TimestampMs);
            var NextReportTime = PrevReportTime.AddDays(7.0);

            // Keep working towards the SimulationPeriod while we're outside the range
            while (now < PrevReportTime || now > NextReportTime)
            {
                if (now < PrevReportTime)
                {
                    SimulationPeriod--;
                    PrevReportTime = PrevReportTime.AddDays(-7.0);
                    NextReportTime = NextReportTime.AddDays(-7.0);
                }
                else
                {
                    SimulationPeriod++;
                    PrevReportTime = PrevReportTime.AddDays(7.0);
                    NextReportTime = NextReportTime.AddDays(7.0);
                }
            }

            return SimulationPeriod;
        }

        private Response PostInfrastructureProject()
        {
            using (var req = new FIORequest<JSONRepresentations.InfrastructureProject.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var lastReport = req.DB.InfrastructureReports.OrderByDescending(ir => ir.SimulationPeriod).FirstOrDefault();
                int SimulationPeriod = FindSimulationPeriod(lastReport, req.Now);

                var data = req.JsonPayload.payload.message.payload.body;

                var existingProject = req.DB.InfrastructureProjects.FirstOrDefault(ip => ip.InfrastructureProjectId == $"{data.id}-{SimulationPeriod}");
                if (existingProject != null)
                {
                    var model = new InfrastructureProject();
                    model.InfrastructureProjectId = $"{data.id}-{SimulationPeriod}";
                    model.InfraProjectId = data.id;
                    model.SimulationPeriod = SimulationPeriod;
                    model.Type = data.type;
                    model.Ticker = existingProject.Ticker;
                    model.Name = data.projectIdentifier;
                    model.Level = data.level;
                    model.ActiveLevel = data.activeLevel;
                    model.CurrentLevel = data.currentLevel;
                    model.UpgradeStatus = data.upgradeStatus;

                    if (data.upgradeCosts != null)
                    {
                        foreach (var upgradeCost in data.upgradeCosts)
                        {
                            var uc = new InfrastructureProjectUpgradeCosts();
                            uc.InfrastructureProjectUpgradeCostsId = $"{model.InfrastructureProjectId}-{upgradeCost.material.ticker}";

                            uc.MaterialId = upgradeCost.material.id;
                            uc.MaterialName = upgradeCost.material.name;
                            uc.MaterialTicker = upgradeCost.material.ticker;

                            uc.Amount = upgradeCost.amount;
                            uc.CurrentAmount = upgradeCost.currentAmount;
                            uc.InfrastructureProjectId = model.InfrastructureProjectId;

                            model.UpgradeCosts.Add(uc);
                        }
                    }

                    if (data.upkeeps != null)
                    {
                        foreach (var upkeep in data.upkeeps)
                        {
                            var uk = new InfrastructureProjectUpkeeps();
                            uk.InfrastructureProjectUpkeepsId = $"{model.InfrastructureProjectId}-{upkeep.material.ticker}";

                            uk.MaterialId = upkeep.material.id;
                            uk.MaterialName = upkeep.material.name;
                            uk.MaterialTicker = upkeep.material.ticker;

                            uk.Stored = upkeep.stored;
                            uk.StoreCapacity = upkeep.storeCapacity;
                            uk.Duration = upkeep.duration;
                            uk.NextTickTimestampEpochMs = upkeep.nextTick.timestamp;

                            uk.Amount = upkeep.amount;
                            uk.CurrentAmount = upkeep.currentAmount;
                            uk.InfrastructureProjectId = model.InfrastructureProjectId;

                            model.Upkeeps.Add(uk);
                        }
                    }

                    if (data.contributions != null)
                    {
                        foreach (var contribution in data.contributions)
                        {
                            foreach (var matContribution in contribution.materials)
                            {
                                var c = new InfrastructureProjectContributions();
                                c.InfrastructureProjectContributionsId = $"{model.InfrastructureProjectId}-{matContribution.material.ticker}-{contribution.time.timestamp}";

                                c.MaterialId = matContribution.material.id;
                                c.MaterialName = matContribution.material.name;
                                c.MaterialTicker = matContribution.material.ticker;

                                c.Amount = matContribution.amount;

                                c.TimestampEpochMs = contribution.time.timestamp;
                                c.CompanyId = contribution.contributor.id;
                                c.CompanyName = contribution.contributor.name;
                                c.CompanyCode = contribution.contributor.code;
                                c.InfrastructureProjectId = model.InfrastructureProjectId;

                                model.Contributions.Add(c);
                            }
                        }
                    }

                    model.InfrastructureId = existingProject.InfrastructureId;

                    model.Validate();

                    req.DB.InfrastructureProjects.Upsert(model)
                        .On(i => new { i.InfrastructureProjectId })
                        .Run();

                    req.DB.InfrastructureProjectUpgradeCosts.UpsertRange(model.UpgradeCosts)
                        .On(uc => new { uc.InfrastructureProjectUpgradeCostsId })
                        .Run();

                    req.DB.InfrastructureProjectUpkeeps.UpsertRange(model.Upkeeps)
                        .On(pu => new { pu.InfrastructureProjectUpkeepsId })
                        .Run();

                    req.DB.InfrastructureProjectContributions.UpsertRange(model.Contributions)
                        .On(c => new { c.InfrastructureProjectContributionsId })
                        .Run();

                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostInfrastructurePrograms()
        {
            using (var req = new FIORequest<JSONRepresentations.InfrastructureProgram.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload.message.payload.body;
                var programs = new List<InfrastructureProgram>();

                var pastPrograms = payload.programs.pastPrograms;
                if (pastPrograms != null)
                {
                    foreach (var pastProgram in pastPrograms)
                    {
                        programs.Add(new InfrastructureProgram
                        {
                            InfrastructureProgramId = $"{payload.populationId}-{pastProgram.number}",
                            Number = pastProgram.number,
                            StartTimestampEpochMs = pastProgram.start.timestamp,
                            EndTimestampEpochMs = pastProgram.end.timestamp,
                            Category = pastProgram.category,
                            Program = pastProgram.program,
                            InfrastructureId = payload.populationId
                        });
                    }
                }

                var currentProgram = payload.programs.currentProgram;
                if (currentProgram != null)
                {
                    programs.Add(new InfrastructureProgram
                    {
                        InfrastructureProgramId = $"{payload.populationId}-{currentProgram.number}",
                        Number = currentProgram.number,
                        StartTimestampEpochMs = currentProgram.start.timestamp,
                        EndTimestampEpochMs = currentProgram.end.timestamp,
                        Category = currentProgram.category,
                        Program = currentProgram.program,
                        InfrastructureId = payload.populationId
                    });
                }

                var nextProgram = payload.programs.nextProgram;
                if (nextProgram != null)
                {
                    programs.Add(new InfrastructureProgram
                    {
                        InfrastructureProgramId = $"{payload.populationId}-{nextProgram.number}",
                        Number = nextProgram.number,
                        StartTimestampEpochMs = nextProgram.start.timestamp,
                        EndTimestampEpochMs = nextProgram.end.timestamp,
                        Category = nextProgram.category,
                        Program = nextProgram.program,
                        InfrastructureId = payload.populationId
                    });
                }

                programs.ForEach(p => p.Validate());

                req.DB.InfrastructurePrograms.UpsertRange(programs)
                        .On(i => new { i.InfrastructureProgramId })
                        .Run();
                req.DB.SaveChanges();

                return HttpStatusCode.OK;
            }
        }

        private Response GetInfrastructure(string PlanetOrInfrastructureId)
        {
            Infrastructure model = null;
            PlanetOrInfrastructureId = PlanetOrInfrastructureId.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                model = DB.Infrastructures
                    .AsNoTracking()
                    .Include(i => i.InfrastructureProjects)
                        .ThenInclude(ip => ip.UpgradeCosts)
                    .Include(i => i.InfrastructureProjects)
                        .ThenInclude(ip => ip.Upkeeps)
                    .Include(i => i.InfrastructureProjects)
                        .ThenInclude(ip => ip.Contributions)
                    .Include(i => i.InfrastructureReports)
                    .Include(i => i.InfrastructurePrograms)
                    .Where(i => i.InfrastructureId.ToUpper() == PlanetOrInfrastructureId)
                    .FirstOrDefault();
                if (model == null)
                {
                    var planet = DB.PlanetDataModels
                        .AsNoTracking()
                        .Where(p => p.PlanetId.ToUpper() == PlanetOrInfrastructureId || p.PlanetNaturalId.ToUpper() == PlanetOrInfrastructureId || p.PlanetName.ToUpper() == PlanetOrInfrastructureId)
                        .FirstOrDefault();
                    if (planet != null)
                    {
                        model = DB.Infrastructures
                            .AsNoTracking()
                            .Include(i => i.InfrastructureProjects)
                                .ThenInclude(ip => ip.UpgradeCosts)
                            .Include(i => i.InfrastructureProjects)
                                .ThenInclude(ip => ip.Upkeeps)
                            .Include(i => i.InfrastructureProjects)
                                .ThenInclude(ip => ip.Contributions)
                            .Include(i => i.InfrastructureReports)
                            .Include(i => i.InfrastructurePrograms)
                            .Where(i => i.InfrastructureId == planet.PopulationId)
                            .AsSplitQuery()
                            .FirstOrDefault();
                    }
                }
            }

            if (model != null)
            {
                return JsonConvert.SerializeObject(model);
            }
            else
            {
                return HttpStatusCode.NoContent;
            }
        }

        public class AllInfrastructureReponse
		{
            public string PlanetNaturalId { get; set; }
            public string PlanetName { get; set; }

            public List<InfrastructureProject> Projects { get; set; }
            public List<InfrastructureReport> Reports { get; set; }
            public List<InfrastructureProgram> Programs { get; set; }
		}

        private Response GetAllInfrastructure()
		{
            using (var DB = PRUNDataContext.GetNewContext())
			{
                var allInfrastructure = new List<AllInfrastructureReponse>();

                var infs = DB.Infrastructures
                    .AsNoTracking()
                    .Include(im => im.InfrastructureProjects)
                        .ThenInclude(ii => ii.UpgradeCosts)
                    .Include(im => im.InfrastructureProjects)
                        .ThenInclude(ii => ii.Upkeeps)
                    .Include(im => im.InfrastructureProjects)
                        .ThenInclude(ii => ii.Contributions)
                    .Include(im => im.InfrastructureReports)
                    .Include(im => im.InfrastructurePrograms)
                    .AsSplitQuery()
                    .ToList();

                var pdms = DB.PlanetDataModels
                    .AsNoTracking()
                    .Select(pdm => new
                    {
                        PopulationId = pdm.PopulationId,
                        PlanetNaturalId = pdm.PlanetNaturalId,
                        PlanetName = pdm.PlanetName
                    })
                    .ToList();

                var infex = from pdm in pdms
                            join inf in infs
                                on pdm.PopulationId equals inf.InfrastructureId
                            select new AllInfrastructureReponse
                            {
                                PlanetNaturalId = pdm.PlanetNaturalId,
                                PlanetName = pdm.PlanetName,
                                Projects = inf.InfrastructureProjects
                                    .Where(i => i.Level > 0 || i.UpgradeStatus > 0.0)
                                    .OrderBy(i => i.Ticker)
                                    .ToList(),
                                Reports = inf.InfrastructureReports
                                    .OrderBy(r => r.SimulationPeriod)
                                    .ToList(),
                                Programs = inf.InfrastructurePrograms
                                    .OrderBy(r => r.Number)
                                    .ToList()
                            };

                return JsonConvert.SerializeObject(infex);
			}
		}
    }
}
#endif // WITH_MODULES