﻿//#define DISABLE_AUTH

using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Extensions.Caching.Memory;
using Microsoft.EntityFrameworkCore;

using Nancy;
using Nancy.ErrorHandling;
using Nancy.Extensions;

using FIORest;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Authentication
{
    public static class Auth
    {
        public static bool IsWriteAuthenticated(this Request request)
        {
            return request.EnforceAuthInner(RequireAdmin: false, RequireWrite: true) == null;
        }

        public static bool IsReadAuthenticated(this Request request)
        {
            return request.EnforceAuthInner(RequireAdmin: false, RequireWrite: false) == null;
        }

        public static string GetUserName(this Request request)
        {
            string AuthorizationStr = GetAuthToken(request);
            if (AuthorizationStr == null)
            {
                return null;
            }

            Guid AuthorizationGuid;
            if (Guid.TryParse(AuthorizationStr, out AuthorizationGuid))
            {
                AuthenticationModel result = null;

                // Check the caches first
                APIKey apires = Caches.APIKeyCache.Get(AuthorizationGuid);
                if (apires != null)
                {
                    result = apires.AuthenticationModel;
                }
                else
                {
                    // Second cache to check.
                    result = Caches.AuthTokenCache.Get(AuthorizationGuid);
                    if (result == null)
                    {
                        using (var DB = PRUNDataContext.GetNewContext())
                        {
                            apires = Caches.APIKeyCache.Cache(AuthorizationGuid, DB);
                            if (apires != null)
                            {
                                result = apires.AuthenticationModel;
                            }
                            else
                            {
                                result = Caches.AuthTokenCache.Cache(AuthorizationGuid, DB);
                            }
                        }
                    }
                }

                if (result != null)
                {
                    return result.UserName.ToUpper();
                }
            }

            return null;
        }

        public static bool HasAuthToken(this Request request)
        {
            return request.Headers.Keys.ToList().Contains("Authorization");
        }

        public static string GetAuthToken(this Request request)
        {
            if (!HasAuthToken(request))
            {
                return null;
            }

            return request.Headers["Authorization"].ToList()[0];
        }


        private static Response EnforceAuthInner(this Request request, bool RequireAdmin, bool RequireWrite)
        {
#if DISABLE_AUTH
            return null;
#else
            bool bTriedAdminAndFailed = false;
            bool bContainsAutorization = HasAuthToken(request);
            if (bContainsAutorization)
            {
                DateTime now = DateTime.Now.ToUniversalTime();

                Guid AuthorizationGuid;
                if (Guid.TryParse(GetAuthToken(request), out AuthorizationGuid))
                {
                    AuthenticationModel result = null;
                    using (var DB = PRUNDataContext.GetNewContext())
                    {
                        // See if it's present in the APIKeyCache
                        APIKey apiKeyRes = Caches.APIKeyCache.Get(AuthorizationGuid);
                        if (apiKeyRes != null)
                        {
                            result = apiKeyRes.AuthenticationModel;
                        }
                        
                        if (result == null)
                        {
                            // See if it's present in the AuthTokenCache
                            result = Caches.AuthTokenCache.Get(AuthorizationGuid);
                        }

                        if (result == null)
                        {
                            // Try to hard-cache the APIKeyCache from the DB
                            apiKeyRes = Caches.APIKeyCache.Cache(AuthorizationGuid, DB);

                            if (apiKeyRes != null)
                            {
                                result = apiKeyRes.AuthenticationModel;
                            }
                        }

                        if (result == null)
                        {
                            // Try to hard-cache the AuthTokenCache from the DB
                            result = Caches.AuthTokenCache.Cache(AuthorizationGuid, DB);
                        }

                        if (apiKeyRes != null)
                        {
                            if (RequireWrite && !apiKeyRes.AllowWrites)
                            {
                                Response apiKeyNoWriteResp = "APIKey provided which does not allow writes.";
                                apiKeyNoWriteResp.ContentType = "text/plain";
                                apiKeyNoWriteResp.StatusCode = HttpStatusCode.Unauthorized;
                                return apiKeyNoWriteResp;
                            }
                        }

                        bTriedAdminAndFailed = (RequireAdmin && result != null && !result.IsAdministrator);
                    }

                    if (!bTriedAdminAndFailed && result != null)
                    {
                        // Success
                        return null;
                    }
                }
            }

            string message = "You are not logged in or your ticket expired.";
            if (bTriedAdminAndFailed)
            {
                message = "You are not an administrator.";
            }

            Response resp = message;
            resp.ContentType = "text/plain";
            resp.StatusCode = HttpStatusCode.Unauthorized;
            return resp;
#endif
        }

        public enum PrivacyType
        {
            Flight,
            Building,
            Storage,
            Production,
            Workforce,
            Experts,
            Contracts,
            ShipmentTracking
        }

        private static bool CheckPrivacyType(PermissionAllowance permissionAllowance, PrivacyType privacyType)
        {
            if (permissionAllowance != null)
            {
                switch (privacyType)
                {
                    case PrivacyType.Flight:
                        return permissionAllowance.FlightData;

                    case PrivacyType.Building:
                        return permissionAllowance.BuildingData;

                    case PrivacyType.Storage:
                        return permissionAllowance.StorageData;

                    case PrivacyType.Production:
                        return permissionAllowance.ProductionData;

                    case PrivacyType.Workforce:
                        return permissionAllowance.WorkforceData;

                    case PrivacyType.Experts:
                        return permissionAllowance.ExpertsData;

                    case PrivacyType.Contracts:
                        return permissionAllowance.ContractData;

                    case PrivacyType.ShipmentTracking:
                        return permissionAllowance.ShipmentTrackingData;
                }
            }

            return false;
        }

        public static string GetUserNameFromAPIKey(string APIKey)
        {
            if (Guid.TryParse(APIKey, out Guid apiKeyGuid))
            {
                var result = Caches.APIKeyCache.GetOrCache(apiKeyGuid);

                if (result != null)
                {
                    return result.AuthenticationModel.UserName;
                }
            }

            return null;
        }

        public static bool CanSeeData(string RequesterUserName, string AccessUserName, PrivacyType privacyType)
        {
            if (RequesterUserName == null)
            {
                return false;
            }

            RequesterUserName = RequesterUserName.ToUpper();
            AccessUserName = AccessUserName.ToUpper();

#if DISABLE_AUTH
            return true;
#else
            if (RequesterUserName == AccessUserName)
            {
                // Everyone has permission to see their own data
                return true;
            }

            var Allowances = Caches.PermissionAllowancesCache.GetOrCache(RequesterUserName);
            if (Allowances != null)
            {
                if (Allowances.TryGetValue(AccessUserName, out var permissionAllowance))
                {
                    return CheckPrivacyType(permissionAllowance, privacyType);
                }
            }
            
            return false;
#endif
        }

        public static void EnforceAuthAdmin(this NancyModule module)
        {
            if (module.RouteExecuting())
            {
                var result = module.Context.Request.EnforceAuthInner(RequireAdmin: true, RequireWrite: true);
                if (result != null)
                {
                    throw new RouteExecutionEarlyExitException(result);
                }
            }
            else
            {
                module.Before += ctx =>
                {
                    return ctx.Request.EnforceAuthInner(RequireAdmin: true, RequireWrite: true);
                };
            }
        }

        private static void EnforceAuthInternal(this NancyModule module, bool RequireWrite)
        {
            if (module.RouteExecuting())
            {
                var result = module.Context.Request.EnforceAuthInner(RequireAdmin: false, RequireWrite: RequireWrite);
                if (result != null)
                {
                    throw new RouteExecutionEarlyExitException(result);
                }
            }
            else
            {
                module.Before += ctx =>
                {
                    return ctx.Request.EnforceAuthInner(RequireAdmin: false, RequireWrite: RequireWrite);
                };
            }
        }

        public static void EnforceWriteAuth(this NancyModule module)
        {
            EnforceAuthInternal(module, RequireWrite: true);
        }

        public static void EnforceReadAuth(this NancyModule module)
        {
            EnforceAuthInternal(module, RequireWrite: false);
        }
    }
}
